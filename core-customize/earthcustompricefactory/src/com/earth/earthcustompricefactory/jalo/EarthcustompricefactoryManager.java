package com.earth.earthcustompricefactory.jalo;

import com.earth.earthcustompricefactory.constants.EarthcustompricefactoryConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustompricefactoryManager extends GeneratedEarthcustompricefactoryManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustompricefactoryManager.class.getName() );
	
	public static final EarthcustompricefactoryManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustompricefactoryManager) em.getExtension(EarthcustompricefactoryConstants.EXTENSIONNAME);
	}
	
}
