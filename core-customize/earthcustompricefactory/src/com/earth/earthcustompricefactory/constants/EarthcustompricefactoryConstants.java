/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustompricefactory.constants;

/**
 * Global class for all Earthcustompricefactory constants. You can add global constants for your extension into this class.
 */
public final class EarthcustompricefactoryConstants extends GeneratedEarthcustompricefactoryConstants
{
	public static final String EXTENSIONNAME = "earthcustompricefactory";

	private EarthcustompricefactoryConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
