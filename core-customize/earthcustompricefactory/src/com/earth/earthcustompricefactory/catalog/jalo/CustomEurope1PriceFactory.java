/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustompricefactory.catalog.jalo;

import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.localization.Localization;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthcustompricefactory.jalo.EarthcustompricefactoryManager;


/**
 *
 */
public class CustomEurope1PriceFactory extends EarthcustompricefactoryManager
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomEurope1PriceFactory.class);

	@Resource(name = "retrieveChannelStrategy")
	private RetrieveChannelStrategy retrieveChannelStrategy;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		LOG.info("getBasePrice For {}", entry.getProduct().getCode());
		final SessionContext ctx = this.getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		final Product product = entry.getProduct();
		final boolean giveAwayMode = entry.isGiveAway(ctx);
		final boolean entryIsRejected = entry.isRejected(ctx);
		final Currency currency = order.getCurrency(ctx);
		final EnumerationValue productGroup = this.getPPG(ctx, product);
		final User user = order.getUser();
		final EnumerationValue userGroup = this.getUPG(ctx, user);
		final Unit unit = entry.getUnit(ctx);
		final long quantity = entry.getQuantity(ctx);
		final boolean net = order.isNet();
		final Date date = new Date();
		PriceRow row;
		if (giveAwayMode && entryIsRejected)
		{
			row = null;
		}
		else
		{
			row = this.matchPriceRowForPrice(ctx, product, productGroup, user, userGroup, quantity, unit, currency, date, net,
					giveAwayMode);
		}

		if (row != null)
		{
			final Currency rowCurr = row.getCurrency();
			double price;
			if (currency.equals(rowCurr))
			{
				price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
			}
			else
			{
				price = rowCurr.convert(currency, row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive());
			}

			final Unit priceUnit = row.getUnit();
			final Unit entryUnit = entry.getUnit();
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);
			return new PriceValue(currency.getIsoCode(), convertedPrice, row.isNetAsPrimitive());
		}
		else if (giveAwayMode)
		{
			return new PriceValue(order.getCurrency(ctx).getIsoCode(), 0.0D, order.isNet());
		}
		else
		{
			final String msg = Localization
					.getLocalizedString("exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1", new Object[]
					{ product, productGroup, user, userGroup, Long.toString(quantity), unit, currency, date, Boolean.toString(net) });
			throw new JaloPriceFactoryException(msg, 0);
		}
	}

}
