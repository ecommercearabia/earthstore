/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartheventtrackingwscustomaddon.controllers;

/**
 */
public interface EartheventtrackingwscustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
