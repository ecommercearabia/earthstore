/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomocc.controllers;

/**
 */
public interface EarthconsignmenttrackingcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
