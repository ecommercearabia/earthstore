/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponsamplescustomaddon.jalo;

import com.earth.earthcustomercouponsamplescustomaddon.constants.EarthcustomercouponsamplescustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustomercouponsamplescustomaddonManager extends GeneratedEarthcustomercouponsamplescustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustomercouponsamplescustomaddonManager.class.getName() );
	
	public static final EarthcustomercouponsamplescustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustomercouponsamplescustomaddonManager) em.getExtension(EarthcustomercouponsamplescustomaddonConstants.EXTENSIONNAME);
	}
	
}
