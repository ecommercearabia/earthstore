/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponsamplescustomaddon.constants;

/**
 * Earthcustomercouponsamplescustomaddon constants
 */
public final class EarthcustomercouponsamplescustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthcustomercouponsamplescustomaddon";

	private EarthcustomercouponsamplescustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
