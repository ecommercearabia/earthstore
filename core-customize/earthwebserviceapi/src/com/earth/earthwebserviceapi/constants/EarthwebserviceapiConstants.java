/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthwebserviceapi.constants;

/**
 * Global class for all Earthwebserviceapi constants. You can add global constants for your extension into this class.
 */
public final class EarthwebserviceapiConstants extends GeneratedEarthwebserviceapiConstants
{
	public static final String EXTENSIONNAME = "earthwebserviceapi";

	private EarthwebserviceapiConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
