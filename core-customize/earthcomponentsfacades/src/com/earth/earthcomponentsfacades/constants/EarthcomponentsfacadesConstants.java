/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcomponentsfacades.constants;

/**
 * Global class for all Earthcomponentsfacades constants. You can add global constants for your extension into this class.
 */
public final class EarthcomponentsfacadesConstants extends GeneratedEarthcomponentsfacadesConstants
{
	public static final String EXTENSIONNAME = "earthcomponentsfacades";

	private EarthcomponentsfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthcomponentsfacadesPlatformLogo";
}
