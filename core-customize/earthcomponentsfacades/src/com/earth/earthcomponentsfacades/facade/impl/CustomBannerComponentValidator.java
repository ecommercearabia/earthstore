/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcomponentsfacades.facade.impl;

import de.hybris.platform.cms2lib.model.components.BannerComponentModel;
import de.hybris.platform.cmsfacades.cmsitems.validator.DefaultBannerComponentValidator;

/**
 *
 */
public class CustomBannerComponentValidator extends DefaultBannerComponentValidator
{
	@Override
	public void validate(final BannerComponentModel validatee)
	{
		validateField((languageData) -> validatee.getMedia(getCommonI18NService().getLocaleForIsoCode(languageData.getIsocode())),
				BannerComponentModel.MEDIA);
	}
}
