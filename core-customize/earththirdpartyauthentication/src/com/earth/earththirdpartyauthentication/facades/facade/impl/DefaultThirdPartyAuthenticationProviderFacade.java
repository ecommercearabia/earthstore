/**
 *
 */
package com.earth.earththirdpartyauthentication.facades.facade.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earththirdpartyauthentication.context.ThirdPartyAuthenticationContext;
import com.earth.earththirdpartyauthentication.context.ThirdPartyAuthenticationProviderContext;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyUserData;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.facades.facade.ThirdPartyAuthenticationProviderFacade;
import com.earth.earththirdpartyauthentication.model.AppleAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.FacebookAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.GoogleAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.service.ThirdPartyUserService;
import com.earth.thirdpartyauthentication.provider.ThirdPartyAuthenticationProviderListData;
import com.earth.thirdpartyauthentication.provider.apple.AppleAuthenticationProviderData;
import com.earth.thirdpartyauthentication.provider.facebook.FacebookAuthenticationProviderData;
import com.earth.thirdpartyauthentication.provider.google.GoogleAuthenticationProviderData;

/**
 * @author monzer
 *
 */
public class DefaultThirdPartyAuthenticationProviderFacade implements ThirdPartyAuthenticationProviderFacade
{

	@Resource(name = "thirdPartyAuthenticationProviderContext")
	private ThirdPartyAuthenticationProviderContext thirdPartyAuthenticationProviderContext;

	@Resource(name = "thirdPartyAuthenticationContext")
	private ThirdPartyAuthenticationContext thirdPartyAuthenticationContext;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "thirdPartyUserService")
	private ThirdPartyUserService thirdPartyUserService;

	@Override
	public Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderByCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getFacebookAuthenticationProviderBySite(currentSite);
	}

	@Override
	public Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySiteUid(final String siteUid)
	{
		final CMSSiteModel siteForUID = (CMSSiteModel) baseSiteService.getBaseSiteForUID(siteUid);
		return getFacebookAuthenticationProviderBySite(siteForUID);
	}

	@Override
	public Optional<FacebookAuthenticationProviderData> getFacebookAuthenticationProviderBySite(final CMSSiteModel site)
	{
		if (site == null)
		{
			return Optional.empty();
		}
		final Optional<ThirdPartyAuthenticationProviderModel> authenticationProvider = thirdPartyAuthenticationProviderContext
				.getThirdPartyAuthenticationProvider(site,
				FacebookAuthenticationProviderModel.class);
		if (authenticationProvider.isEmpty())
		{
			return Optional.empty();
		}
		if (!(authenticationProvider.get() instanceof FacebookAuthenticationProviderModel))
		{
			return Optional.empty();
		}

		final FacebookAuthenticationProviderData data = populateFacebookProviderData(
				(FacebookAuthenticationProviderModel) authenticationProvider.get());
		return Optional.ofNullable(data);
	}

	private FacebookAuthenticationProviderData populateFacebookProviderData(final FacebookAuthenticationProviderModel provider)
	{
		final FacebookAuthenticationProviderData providerData = new FacebookAuthenticationProviderData();
		providerData.setAppId(provider.getAppId());
		providerData.setActive(provider.isActive());
		providerData.setAppSecret(provider.getAppSecret());
		providerData.setId(provider.getAppId());
		providerData.setType(ThirdPartyAuthenticationType.FACEBOOK.getCode());
		providerData.setEnabledForMobile(provider.isEnabledForMobile());
		return providerData;
	}

	@Override
	public Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderByCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getGoogleAuthenticationProviderBySite(currentSite);
	}

	@Override
	public Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySiteUid(final String siteUid)
	{
		final CMSSiteModel siteForUID = (CMSSiteModel) baseSiteService.getBaseSiteForUID(siteUid);
		return getGoogleAuthenticationProviderBySite(siteForUID);
	}

	@Override
	public Optional<GoogleAuthenticationProviderData> getGoogleAuthenticationProviderBySite(final CMSSiteModel site)
	{
		if (site == null)
		{
			return Optional.empty();
		}
		final Optional<ThirdPartyAuthenticationProviderModel> authenticationProvider = thirdPartyAuthenticationProviderContext
				.getThirdPartyAuthenticationProvider(site, GoogleAuthenticationProviderModel.class);
		if (authenticationProvider.isEmpty())
		{
			return Optional.empty();
		}
		if (!(authenticationProvider.get() instanceof GoogleAuthenticationProviderModel))
		{
			return Optional.empty();
		}

		final GoogleAuthenticationProviderData data = populateGoogleProviderData(
				(GoogleAuthenticationProviderModel) authenticationProvider.get());
		return Optional.ofNullable(data);
	}

	private GoogleAuthenticationProviderData populateGoogleProviderData(final GoogleAuthenticationProviderModel provider)
	{
		final GoogleAuthenticationProviderData providerData = new GoogleAuthenticationProviderData();
		providerData.setActive(provider.isActive());
		providerData.setClientId(provider.getClientId());
		providerData.setClientSecret(provider.getClientSecret());
		providerData.setType(ThirdPartyAuthenticationType.GOOGLE.getCode());
		providerData.setEnabledForMobile(provider.isEnabledForMobile());
		return providerData;
	}

	@Override
	public Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderByCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getAppleAuthenticationProviderBySite(currentSite);
	}

	@Override
	public Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySiteUid(final String siteUid)
	{
		final CMSSiteModel siteForUID = (CMSSiteModel) baseSiteService.getBaseSiteForUID(siteUid);
		return getAppleAuthenticationProviderBySite(siteForUID);
	}

	@Override
	public Optional<AppleAuthenticationProviderData> getAppleAuthenticationProviderBySite(final CMSSiteModel site)
	{
		if (site == null)
		{
			return Optional.empty();
		}
		final Optional<ThirdPartyAuthenticationProviderModel> authenticationProvider = thirdPartyAuthenticationProviderContext
				.getThirdPartyAuthenticationProvider(site, AppleAuthenticationProviderModel.class);
		if (authenticationProvider.isEmpty())
		{
			return Optional.empty();
		}
		if (!(authenticationProvider.get() instanceof AppleAuthenticationProviderModel))
		{
			return Optional.empty();
		}

		final AppleAuthenticationProviderData data = populateAppleProviderData(
				(AppleAuthenticationProviderModel) authenticationProvider.get());
		return Optional.ofNullable(data);
	}

	private AppleAuthenticationProviderData populateAppleProviderData(final AppleAuthenticationProviderModel provider)
	{
		final AppleAuthenticationProviderData providerData = new AppleAuthenticationProviderData();
		providerData.setActive(provider.isActive());
		providerData.setAppId(provider.getAppId());
		providerData.setKeyId(provider.getKeyId());
		providerData.setServiceId(provider.getServiceId());
		providerData.setTeamId(provider.getTeamId());
		providerData.setType(ThirdPartyAuthenticationType.APPLE.getCode());
		providerData.setEnabledForMobile(provider.isEnabledForMobile());
		return providerData;
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersByCurrentSite()
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getAllThirdPartyProvidersBySite(currentSite);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySiteUid(final String siteUid)
	{
		final CMSSiteModel siteForUID = (CMSSiteModel) baseSiteService.getBaseSiteForUID(siteUid);
		return getAllThirdPartyProvidersBySite(siteForUID);
	}

	@Override
	public Optional<ThirdPartyAuthenticationProviderListData> getAllThirdPartyProvidersBySite(final CMSSiteModel site)
	{
		try
		{
			final List<ThirdPartyAuthenticationProviderData> list = thirdPartyAuthenticationContext
					.getSupportedThirdPartyAuthenticationProviderData(site);
			final ThirdPartyAuthenticationProviderListData providers = new ThirdPartyAuthenticationProviderListData();
			providers.setProviders(list);
			return Optional.ofNullable(providers);
		}
		catch (final ThirdPartyAuthenticationException e)
		{
			return Optional.empty();
		}
	}


	@Override
	public Optional<ThirdPartyUserData> getThirdPartyUserDataByCurrentSite(final Object authData,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getThirdPartyUserData(authData, type, currentSite);
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyUserData(final Object authData, final ThirdPartyAuthenticationType type,
			final CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException
	{
		final Optional<ThirdPartyUserData> thirdPartyUserData = thirdPartyAuthenticationContext.getThirdPartyProfileBySite(authData,
				type, cmsSite);
		return thirdPartyUserData;
	}

	@Override
	public boolean verifyThirdPartyToken(final Object token, final ThirdPartyAuthenticationType type, final CMSSiteModel site)
			throws ThirdPartyAuthenticationException
	{
		return thirdPartyAuthenticationContext.verifyAccessTokenWithThirdParty(site, token, type, site, null);
	}

	@Override
	public boolean verifyThirdPartyTokenByCurrentSite(final Object token, final ThirdPartyAuthenticationType type)
			throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return verifyThirdPartyToken(token, type, currentSite);
	}

	@Override
	public void saveUser(final CustomerData customer) {
		thirdPartyUserService.saveUser(customer);
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyUserRegistrationData(final Object authData,
			final ThirdPartyAuthenticationType type, final CMSSiteModel cmsSite) throws ThirdPartyAuthenticationException
	{
		final Optional<ThirdPartyUserData> thirdPartyUserData = thirdPartyAuthenticationContext
				.getThirdPartyRegistrationProfileBySite(authData, type, cmsSite);
		return thirdPartyUserData;
	}

	@Override
	public Optional<ThirdPartyUserData> getThirdPartyUserRegistrationDataByCurrentSite(final Object authData,
			final ThirdPartyAuthenticationType type) throws ThirdPartyAuthenticationException
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		return getThirdPartyUserRegistrationData(authData, type, currentSite);
	}

}
