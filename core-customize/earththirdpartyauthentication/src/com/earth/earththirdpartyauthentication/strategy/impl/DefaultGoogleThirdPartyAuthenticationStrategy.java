/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.strategy.impl;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earththirdpartyauthentication.data.GoogleThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.entry.TwitterFormData;
import com.earth.earththirdpartyauthentication.enums.ThirdPartyAuthenticationType;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.exception.enums.ThirdPartyAuthenticationExceptionType;
import com.earth.earththirdpartyauthentication.model.GoogleAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;
import com.earth.earththirdpartyauthentication.service.GoogleBasicService;
import com.earth.earththirdpartyauthentication.service.GoogleOauth2Service;
import com.earth.earththirdpartyauthentication.strategy.ThirdPartyAuthenticationStrategy;


/**
 *
 */
public class DefaultGoogleThirdPartyAuthenticationStrategy implements ThirdPartyAuthenticationStrategy
{
	@Resource(name = "googleBasicService")
	private GoogleBasicService googleBasicService;
	@Resource(name = "googleOauth2Service")
	private GoogleOauth2Service googleOauth2Service;

	@Override
	public Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof GoogleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final String appId = ((GoogleAuthenticationProviderModel) provider).getClientId();
		final String appSecret = ((GoogleAuthenticationProviderModel) provider).getClientSecret();
		final GoogleThirdPartyAuthenticationProviderData thirdPartyAuthenticationProviderData = new GoogleThirdPartyAuthenticationProviderData();
		thirdPartyAuthenticationProviderData.setId(appId);
		thirdPartyAuthenticationProviderData.setType(ThirdPartyAuthenticationType.GOOGLE.getCode());
		thirdPartyAuthenticationProviderData.setGoogleType(((GoogleAuthenticationProviderModel) provider).getType().getCode());
		thirdPartyAuthenticationProviderData.setActive(provider.isActive());
		thirdPartyAuthenticationProviderData.setEnabledForMobile(provider.isActive() && provider.isEnabledForMobile());
		thirdPartyAuthenticationProviderData.setEnabledForStoreFront(provider.isActive() && provider.isEnabledForStoreFront());
		return Optional.ofNullable(thirdPartyAuthenticationProviderData);

	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof GoogleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final GoogleAuthenticationProviderModel googleProvider = (GoogleAuthenticationProviderModel) provider;

		switch (googleProvider.getType())
		{
			case BASIC:
				final Optional<ThirdPartyAuthenticationUserData> basicUser = googleBasicService.getData((String) data,
						googleProvider.getClientId());
				if (!basicUser.isPresent())
				{
					throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
							ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
				}

				return basicUser;

			case OAUTH2:
				final Optional<ThirdPartyAuthenticationUserData> outh2User = googleOauth2Service.getData((String) data,
						googleProvider.getClientId(), googleProvider.getClientSecret());

				if (!outh2User.isPresent())
				{
					throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED,
							ThirdPartyAuthenticationExceptionType.NON_AUTHORIZED.getMsg());
				}


				return outh2User;

			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER_TYPE,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER_TYPE.getMsg());
		}
	}

	@Override
	public Optional<TwitterFormData> getFormData(final Object data, final ThirdPartyAuthenticationProviderModel provider,
			final String callbackUrl)
			throws ThirdPartyAuthenticationException
	{
		throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED,
				ThirdPartyAuthenticationExceptionType.METHOD_NOT_SUPPORTED.getMsg());
	}

	@Override
	public boolean verifyAccessTokenWithThirdParty(final Object data, final Object token,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		if (data == null || token == null || provider == null)
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.BAD_REQUEST,
					ThirdPartyAuthenticationExceptionType.BAD_REQUEST.getMsg());
		}

		if (!(provider instanceof GoogleAuthenticationProviderModel))
		{
			throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER,
					ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER.getMsg());
		}

		final GoogleAuthenticationProviderModel googleProvider = (GoogleAuthenticationProviderModel) provider;

		switch (googleProvider.getType())
		{
			case BASIC:
				return googleBasicService.verifyThirdPartyAccessToken(String.valueOf(data), String.valueOf(token),
						googleProvider.getClientId());

			case OAUTH2:
				return googleOauth2Service.verifyThirdPartyAccessToken(String.valueOf(data), googleProvider.getClientId(),
						googleProvider.getClientSecret());

			default:
				throw new ThirdPartyAuthenticationException(ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER_TYPE,
						ThirdPartyAuthenticationExceptionType.INVALID_PROVIDER_TYPE.getMsg());
		}
	}

	@Override
	public Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(final Object data,
			final ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException
	{
		return this.getThirdPartyUserData(data, provider);
	}

}
