/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.strategy;

import java.util.Optional;

import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationProviderData;
import com.earth.earththirdpartyauthentication.data.ThirdPartyAuthenticationUserData;
import com.earth.earththirdpartyauthentication.entry.TwitterFormData;
import com.earth.earththirdpartyauthentication.exception.ThirdPartyAuthenticationException;
import com.earth.earththirdpartyauthentication.model.ThirdPartyAuthenticationProviderModel;


/**
 *
 */
public interface ThirdPartyAuthenticationStrategy
{
	Optional<ThirdPartyAuthenticationProviderData> getThirdPartyAuthenticationProviderData(
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserData(Object data, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;

	Optional<ThirdPartyAuthenticationUserData> getThirdPartyUserRegistrationData(Object data,
			ThirdPartyAuthenticationProviderModel provider) throws ThirdPartyAuthenticationException;

	Optional<TwitterFormData> getFormData(Object data, ThirdPartyAuthenticationProviderModel provider, String callbackUrl)
			throws ThirdPartyAuthenticationException;

	boolean verifyAccessTokenWithThirdParty(Object data, Object token, ThirdPartyAuthenticationProviderModel provider)
			throws ThirdPartyAuthenticationException;
}
