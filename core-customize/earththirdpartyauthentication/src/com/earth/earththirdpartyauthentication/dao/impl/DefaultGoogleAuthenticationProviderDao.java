/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.dao.impl;

import com.earth.earththirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.earth.earththirdpartyauthentication.model.GoogleAuthenticationProviderModel;

/**
 *
 */
public class DefaultGoogleAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultGoogleAuthenticationProviderDao()
	{
		super(GoogleAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return GoogleAuthenticationProviderModel._TYPECODE;
	}


}
