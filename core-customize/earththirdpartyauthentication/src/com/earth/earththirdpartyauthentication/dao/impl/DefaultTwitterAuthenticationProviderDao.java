/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.dao.impl;

import com.earth.earththirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.earth.earththirdpartyauthentication.model.TwitterAuthenticationProviderModel;


/**
 *
 */
public class DefaultTwitterAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{


	/**
	 *
	 */
	public DefaultTwitterAuthenticationProviderDao()
	{
		super(TwitterAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return TwitterAuthenticationProviderModel._TYPECODE;
	}


}
