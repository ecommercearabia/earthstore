/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earththirdpartyauthentication.dao.impl;

import com.earth.earththirdpartyauthentication.dao.ThirdPartyAuthenticationProviderDao;
import com.earth.earththirdpartyauthentication.model.FacebookAuthenticationProviderModel;

/**
 *
 */
public class DefaultFacebookAuthenticationProviderDao extends DefaultThirdPartyAuthenticationProviderDao
		implements ThirdPartyAuthenticationProviderDao
{

	/**
	 *
	 */
	public DefaultFacebookAuthenticationProviderDao()
	{
		super(FacebookAuthenticationProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return FacebookAuthenticationProviderModel._TYPECODE;
	}

}
