/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.filter.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;

import com.earth.eartherpintegration.filter.ConsignmentsERPLocationFilter;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultConsignmentsERPLocationFilter implements ConsignmentsERPLocationFilter
{
	private static final Logger LOG = Logger.getLogger(DefaultConsignmentsERPLocationFilter.class);


	@Override
	public Map<String, List<ConsignmentModel>> filter(final List<ConsignmentModel> consignments, final BaseStoreModel store)
	{
		final Map<String, List<ConsignmentModel>> map = new HashMap<>();
		Preconditions.checkArgument(store != null, "consignments error msg");
		Preconditions.checkArgument(!CollectionUtils.isEmpty(consignments), "consignments error msg");

		for (final ConsignmentModel consignment : consignments)
		{

			if (!isConsginmentValid(consignment, store))
			{
				continue;
			}

			final String erpLocationCode = consignment.getWarehouse().getErpLocationCode();
			final List<ConsignmentModel> list = map.getOrDefault(erpLocationCode, new ArrayList<>());
			list.add(consignment);
			map.put(erpLocationCode, list);

		}

		return map;
	}

	/**
	 *
	 */
	private boolean isConsginmentValid(final ConsignmentModel consignment, final BaseStoreModel store)
	{
		if (!isSameStore(consignment, store))
		{
			LOG.warn("Consignmet with code " + consignment.getCode() + " is not from store " + store.getName()
					+ ", so it will be ignored");
			return false;
		}
		if (consignment.getWarehouse() == null)
		{
			LOG.warn("Consignmet with code " + consignment.getCode() + " doesn't have warehouse");
			return false;
		}
		if (Strings.isBlank(consignment.getWarehouse().getErpLocationCode()))
		{
			LOG.warn("Consignmet with code " + consignment.getCode() + " doesn't have an ERP Location Code");
			return false;
		}
		return true;
	}

	/**
	 *
	 */
	private boolean isSameStore(final ConsignmentModel consignmentModel, final BaseStoreModel store)
	{
		return !(consignmentModel == null || consignmentModel.getOrder() == null || consignmentModel.getOrder().getStore() == null
				|| Strings.isBlank(consignmentModel.getOrder().getStore().getUid())
				|| !consignmentModel.getOrder().getStore().getUid().equals(store.getUid()));

	}

}
