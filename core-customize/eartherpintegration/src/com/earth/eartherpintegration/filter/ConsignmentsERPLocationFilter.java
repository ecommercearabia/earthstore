/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.filter;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Map;


/**
 *
 */
public interface ConsignmentsERPLocationFilter
{

	Map<String, List<ConsignmentModel>> filter(final List<ConsignmentModel> consignments, final BaseStoreModel store);
}