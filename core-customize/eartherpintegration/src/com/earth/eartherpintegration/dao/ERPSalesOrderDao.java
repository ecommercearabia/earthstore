/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.dao;

import java.util.List;

import com.earth.eartherpintegration.enums.ERPSalesOrderSendStatus;
import com.earth.eartherpintegration.model.ERPSalesOrderModel;


/**
 *
 */
public interface ERPSalesOrderDao
{
	List<ERPSalesOrderModel> getByStatus(ERPSalesOrderSendStatus status);

	List<ERPSalesOrderModel> getAll();
}
