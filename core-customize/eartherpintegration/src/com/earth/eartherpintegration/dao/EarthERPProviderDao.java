/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.eartherpintegration.model.EarthERPProviderModel;


/**
 * The Interface EarthERPProviderModel.
 */
public interface EarthERPProviderDao
{

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the optional
	 */
	public Optional<EarthERPProviderModel> get(String code);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @return the active provider
	 */
	public Optional<EarthERPProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the active provider
	 */
	public Optional<EarthERPProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @return the active provider by current base store
	 */
	public Optional<EarthERPProviderModel> getActiveProviderByCurrentBaseStore();

}
