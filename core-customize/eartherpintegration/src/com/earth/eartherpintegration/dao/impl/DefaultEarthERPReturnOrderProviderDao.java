/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.dao.impl;

import com.earth.eartherpintegration.dao.EarthERPProviderDao;
import com.earth.eartherpintegration.model.EarthERPReturnOrderProviderModel;


/**
 * The Class DefaultGiiftLoyaltyProgramProviderDao.
 */
public class DefaultEarthERPReturnOrderProviderDao extends DefaultEarthERPProviderDao implements EarthERPProviderDao
{



	/**
	 * Instantiates a new default giift loyalty program provider dao.
	 */
	public DefaultEarthERPReturnOrderProviderDao()
	{
		super(EarthERPReturnOrderProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EarthERPReturnOrderProviderModel._TYPECODE;
	}

}
