/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.earth.eartherpintegration.dao.ERPSalesOrderDao;
import com.earth.eartherpintegration.enums.ERPSalesOrderSendStatus;
import com.earth.eartherpintegration.model.ERPSalesOrderModel;


/**
 *
 */
public class DefaultERPSalesOrderDao implements ERPSalesOrderDao
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<ERPSalesOrderModel> getAll()
	{
		final StringBuilder query = new StringBuilder("SELECT {pk} FROM {ERPSalesOrder} ");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		searchQuery.setResultClassList(Collections.singletonList(ERPSalesOrderModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ERPSalesOrderModel> getByStatus(final ERPSalesOrderSendStatus status)
	{
		final StringBuilder query = new StringBuilder("SELECT {pk} FROM {ERPSalesOrder} WHERE {status}=?status");
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", status);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ERPSalesOrderModel.class));
		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}


	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}





}
