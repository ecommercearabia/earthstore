/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.dao.impl;

import com.earth.eartherpintegration.dao.EarthERPProviderDao;
import com.earth.eartherpintegration.model.EarthERPProductProviderModel;


/**
 * The Class DefaultGiiftLoyaltyProgramProviderDao.
 */
public class DefaultEarthERPProductProviderDao extends DefaultEarthERPProviderDao implements EarthERPProviderDao
{



	/**
	 * Instantiates a new default giift loyalty program provider dao.
	 */
	public DefaultEarthERPProductProviderDao()
	{
		super(EarthERPProductProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return EarthERPProductProviderModel._TYPECODE;
	}

}
