package com.earth.eartherpintegration.util;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.logging.log4j.util.Strings;

import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.exception.type.EarthERPExceptionType;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import kong.unirest.UnirestParsingException;


public class EartErpConnectionUtil
{

	private EartErpConnectionUtil()
	{

	}

	static
	{
		Unirest.config().reset();
		Unirest.config().verifySsl(false).connectTimeout(0);
	}

	public static <T> T httpGet(final String url, final Map<String, Object> params, final Class<T> clazz) throws EarthERPException
	{
		HttpResponse<T> httpResponse = null;

		try
		{
			httpResponse = Unirest.get(url).queryString(params).header("Accept", "application/json").asObject(clazz);
		}
		catch (final UnirestException ex)
		{
			throw new EarthERPException(url, ex, EarthERPExceptionType.ERP_SERVER_ISSUE);
		}

		if (httpResponse.getStatus() >= 200 && httpResponse.getStatus() < 300)
		{
			return httpResponse.getBody();
		}
		if (Objects.isNull(httpResponse.getBody()) && httpResponse.getParsingError().isPresent())
		{
			throw new EarthERPException(url, httpResponse.getParsingError().get(), EarthERPExceptionType.ERP_PARSING_ISSUE);
		}
		return null;
	}


	public static String httpPost(final String url, final Map<String, Object> params, final String body) throws EarthERPException
	{
		HttpResponse<String> httpResponseString = null;

		try
		{
			httpResponseString = Unirest.post(url).queryString(params).body(body).header("Accept", "application/json")
					.contentType("application/json").asString();
		}
		catch (final UnirestException ex)
		{
			throw new EarthERPException(url, ex, EarthERPExceptionType.ERP_SERVER_ISSUE);
		}

		if (httpResponseString.isSuccess())
		{
			return httpResponseString.getBody();
		}

		if (!httpResponseString.isSuccess())
		{
			throw new EarthERPException(url, httpResponseString.getBody(), EarthERPExceptionType.ERP_SERVER_ISSUE);
		}

		Optional<UnirestParsingException> parsingError = httpResponseString.getParsingError();
		if (Objects.isNull(httpResponseString.getBody()) && parsingError.isPresent())
		{
			throw new EarthERPException(url, parsingError.get(), EarthERPExceptionType.ERP_PARSING_ISSUE);
		}
		return Strings.EMPTY;
	}


}
