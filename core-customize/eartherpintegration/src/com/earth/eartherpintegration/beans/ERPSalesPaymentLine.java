/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ERPSalesPaymentLine
{

	@Expose
	@SerializedName("Amount")
	private double amount;

	@Expose
	@SerializedName("LineNo")
	private int lineNo;

	@Expose
	@SerializedName("PaymMethod")
	private String paymMethod;

	/**
	 * @return the amount
	 */
	public double getAmount()
	{
		return amount;
	}

	/**
	 * @param amount
	 *           the amount to set
	 */
	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the lineNo
	 */
	public int getLineNo()
	{
		return lineNo;
	}

	/**
	 * @param lineNo
	 *           the lineNo to set
	 */
	public void setLineNo(final int lineNo)
	{
		this.lineNo = lineNo;
	}

	/**
	 * @return the paymMethod
	 */
	public String getPaymMethod()
	{
		return paymMethod;
	}

	/**
	 * @param paymMethod
	 *           the paymMethod to set
	 */
	public void setPaymMethod(final String paymMethod)
	{
		this.paymMethod = paymMethod;
	}

	@Override
	public String toString()
	{
		return "ERPSalesPaymentLine [amount=" + amount + ", lineNo=" + lineNo + ", paymMethod=" + paymMethod + "]";
	}




}
