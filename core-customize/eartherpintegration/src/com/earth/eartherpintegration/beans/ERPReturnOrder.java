/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.beans;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ERPReturnOrder
{
	@Expose
	@SerializedName("Amount")
	private double amount;

	@Expose
	@SerializedName("AmountIncVat")
	private double amountIncVat;

	@Expose
	@SerializedName("CompanyCode")
	private String companyCode;

	@Expose
	@SerializedName("CurrencyCode")
	private String currencyCode;

	@Expose
	@SerializedName("DeliveryCharges")
	private double deliveryCharges;

	@Expose
	@SerializedName("DeliveryStatus")
	private int deliveryStatus;

	@Expose
	@SerializedName("OrderDate")
	private String orderDate;

	@Expose
	@SerializedName("OrderNo")
	private String orderNo;

	@Expose
	@SerializedName("StoreCode")
	private String storeCode;

	@Expose
	@SerializedName("NoOfItems")
	private long noOfItems;

	@Expose
	@SerializedName("SalesLine")
	private List<ERPReturnSalesLine> salesLines = new ArrayList<>();


	@Expose
	@SerializedName("SalesPaymLine")
	private List<ERPReturnSalesPaymentLine> salesPayLines = new ArrayList<>();


	/**
	 * @return the deliveryCharges
	 */
	public double getDeliveryCharges()
	{
		return deliveryCharges;
	}

	/**
	 * @param deliveryCharges
	 *           the deliveryCharges to set
	 */
	public void setDeliveryCharges(final double deliveryCharges)
	{
		this.deliveryCharges = deliveryCharges;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public double getAmountIncVat()
	{
		return amountIncVat;
	}

	public void setAmountIncVat(final double amountIncVat)
	{
		this.amountIncVat = amountIncVat;
	}

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(final String companyCode)
	{
		this.companyCode = companyCode;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public int getDeliveryStatus()
	{
		return deliveryStatus;
	}

	public void setDeliveryStatus(final int deliveryStatus)
	{
		this.deliveryStatus = deliveryStatus;
	}

	public String getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(final String orderDate)
	{
		this.orderDate = orderDate;
	}

	public String getOrderNo()
	{
		return orderNo;
	}

	public void setOrderNo(final String orderNo)
	{
		this.orderNo = orderNo;
	}

	public String getStoreCode()
	{
		return storeCode;
	}

	public void setStoreCode(final String storeCode)
	{
		this.storeCode = storeCode;
	}

	public long getNoOfItems()
	{
		return noOfItems;
	}

	public void setNoOfItems(final long noOfItems)
	{
		this.noOfItems = noOfItems;
	}

	public List<ERPReturnSalesLine> getSalesLines()
	{
		return salesLines;
	}

	public void setSalesLines(final List<ERPReturnSalesLine> salesLines)
	{
		this.salesLines = salesLines;
	}



	/**
	 * @return the salesPayLines
	 */
	public List<ERPReturnSalesPaymentLine> getSalesPayLines()
	{
		return salesPayLines;
	}

	/**
	 * @param salesPayLines
	 *           the salesPayLines to set
	 */
	public void setSalesPayLines(final List<ERPReturnSalesPaymentLine> salesPayLines)
	{
		this.salesPayLines = salesPayLines;
	}

	@Override
	public String toString()
	{
		return "ERPSalesOrder [amount=" + amount + ", amountIncVat=" + amountIncVat + ", companyCode=" + companyCode
				+ ", currencyCode=" + currencyCode + ", deliveryCharges=" + deliveryCharges + ", deliveryStatus=" + deliveryStatus
				+ ", orderDate=" + orderDate + ", orderNo=" + orderNo + ", storeCode=" + storeCode + ", noOfItems=" + noOfItems
				+ ", salesLines=" + salesLines + ", salesPayLines=" + salesPayLines + "]";
	}
}
