package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ERPItemInventory {

	@SerializedName("DateTime")
	@Expose
	private String dateTime;
	@SerializedName("ItemCode")
	@Expose
	private String itemCode;
	@SerializedName("ItemName")
	@Expose
	private String itemName;
	@SerializedName("LocationCode")
	@Expose
	private String locationCode;
	@SerializedName("LocationName")
	@Expose
	private String locationName;
	@SerializedName("Quantity")
	@Expose
	private double quantity;
	@SerializedName("UnitID")
	@Expose
	private String unitID;

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(final String dateTime) {
		this.dateTime = dateTime;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(final String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(final String itemName) {
		this.itemName = itemName;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(final String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(final String locationName) {
		this.locationName = locationName;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(final double quantity) {
		this.quantity = quantity;
	}

	public String getUnitID() {
		return unitID;
	}

	public void setUnitID(final String unitID) {
		this.unitID = unitID;
	}

}
