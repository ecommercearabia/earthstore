/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ERPReturnSalesLine
{

	@Expose
	@SerializedName("Amount")
	private double amount;

	@Expose
	@SerializedName("AmountIncVat")
	private double amountIncVat;

	@Expose
	@SerializedName("ItemNo")
	private String itemNo;

	@Expose
	@SerializedName("LineDiscAmount")
	private double lineDiscAmount;

	@Expose
	@SerializedName("LineNo")
	private int lineNo;

	@Expose
	@SerializedName("Quantity")
	private double quantity;

	@Expose
	@SerializedName("TaxAmount")
	private double taxAmount;

	@Expose
	@SerializedName("TaxCode")
	private String taxCode;

	@Expose
	@SerializedName("TaxIncludePrice")
	private double taxIncludePrice;

	@Expose
	@SerializedName("SalesPrice")
	private double salesPrice;

	@Expose
	@SerializedName("Unitofmeasurecode")
	private String unitofmeasurecode;


	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public double getAmountIncVat()
	{
		return amountIncVat;
	}

	public void setAmountIncVat(final double amountIncVat)
	{
		this.amountIncVat = amountIncVat;
	}

	public String getItemNo()
	{
		return itemNo;
	}

	public void setItemNo(final String itemNo)
	{
		this.itemNo = itemNo;
	}

	public double getLineDiscAmount()
	{
		return lineDiscAmount;
	}

	public void setLineDiscAmount(final double lineDiscAmount)
	{
		this.lineDiscAmount = lineDiscAmount;
	}

	public int getLineNo()
	{
		return lineNo;
	}

	public void setLineNo(final int lineNo)
	{
		this.lineNo = lineNo;
	}

	public double getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final double quantity)
	{
		this.quantity = quantity;
	}

	public double getTaxAmount()
	{
		return taxAmount;
	}

	public void setTaxAmount(final double taxAmount)
	{
		this.taxAmount = taxAmount;
	}

	public String getTaxCode()
	{
		return taxCode;
	}

	public void setTaxCode(final String taxCode)
	{
		this.taxCode = taxCode;
	}

	public double getTaxIncludePrice()
	{
		return taxIncludePrice;
	}

	public void setTaxIncludePrice(final double taxIncludePrice)
	{
		this.taxIncludePrice = taxIncludePrice;
	}

	public double getSalesPrice()
	{
		return salesPrice;
	}

	public void setSalesPrice(final double salesPrice)
	{
		this.salesPrice = salesPrice;
	}

	public String getUnitofmeasurecode()
	{
		return unitofmeasurecode;
	}

	public void setUnitofmeasurecode(final String unitofmeasurecode)
	{
		this.unitofmeasurecode = unitofmeasurecode;
	}

	@Override
	public String toString()
	{
		return "SalesLine [amount=" + amount + ", amountIncVat=" + amountIncVat + ", itemNo=" + itemNo + ", lineDiscAmount="
				+ lineDiscAmount + ", lineNo=" + lineNo + ", quantity=" + quantity + ", taxAmount=" + taxAmount + ", taxCode="
				+ taxCode + ", taxIncludePrice=" + taxIncludePrice + ", salesPrice=" + salesPrice + ", unitofmeasurecode="
				+ unitofmeasurecode + "]";
	}


}
