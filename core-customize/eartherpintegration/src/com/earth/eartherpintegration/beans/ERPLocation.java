package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ERPLocation {
	@SerializedName("Address")
	@Expose
	private String address;
	@SerializedName("CompanyCode")
	@Expose
	private String companyCode;
	@SerializedName("Country")
	@Expose
	private String country;
	@SerializedName("FSHStore")
	@Expose
	private int fSHStore;
	@SerializedName("LocationCode")
	@Expose
	private String locationCode;
	@SerializedName("LocationName")
	@Expose
	private String locationName;
	@SerializedName("WHSEnabled")
	@Expose
	private int wHSEnabled;
	@SerializedName("WMSLocationIdDefaultIssue")
	@Expose
	private String wMSLocationIdDefaultIssue;
	@SerializedName("WMSLocationIdDefaultReceipt")
	@Expose
	private String wMSLocationIdDefaultReceipt;

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(final String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public int getFSHStore() {
		return fSHStore;
	}

	public void setFSHStore(final int fSHStore) {
		this.fSHStore = fSHStore;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(final String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(final String locationName) {
		this.locationName = locationName;
	}

	public int getWHSEnabled() {
		return wHSEnabled;
	}

	public void setWHSEnabled(final int wHSEnabled) {
		this.wHSEnabled = wHSEnabled;
	}

	public String getWMSLocationIdDefaultIssue() {
		return wMSLocationIdDefaultIssue;
	}

	public void setWMSLocationIdDefaultIssue(final String wMSLocationIdDefaultIssue) {
		this.wMSLocationIdDefaultIssue = wMSLocationIdDefaultIssue;
	}

	public String getWMSLocationIdDefaultReceipt() {
		return wMSLocationIdDefaultReceipt;
	}

	public void setWMSLocationIdDefaultReceipt(final String wMSLocationIdDefaultReceipt) {
		this.wMSLocationIdDefaultReceipt = wMSLocationIdDefaultReceipt;
	}

}
