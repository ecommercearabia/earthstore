/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class ERPSalesLine
{

	@SerializedName("Amount")
	@Expose
	private double amount;
	@SerializedName("AmountIncVat")
	@Expose
	private double amountIncVat;
	@SerializedName("ItemNo")
	@Expose
	private String itemNo;
	@SerializedName("LineDiscAmount")
	@Expose
	private double lineDiscAmount;
	@SerializedName("LineNo")
	@Expose
	private int lineNo;
	@SerializedName("Quantity")
	@Expose
	private double quantity;
	@SerializedName("TaxAmount")
	@Expose
	private double taxAmount;
	@SerializedName("TaxCode")
	@Expose
	private String taxCode;
	@SerializedName("TaxIncludePrice")
	@Expose
	private double taxIncludePrice;
	@SerializedName("SalesPrice")
	@Expose
	private double salesPrice;
	@SerializedName("Unitofmeasurecode")
	@Expose
	private String unitofmeasurecode;

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public double getAmountIncVat()
	{
		return amountIncVat;
	}

	public void setAmountIncVat(final double amountIncVat)
	{
		this.amountIncVat = amountIncVat;
	}

	public String getItemNo()
	{
		return itemNo;
	}

	public void setItemNo(final String itemNo)
	{
		this.itemNo = itemNo;
	}

	public double getLineDiscAmount()
	{
		return lineDiscAmount;
	}

	public void setLineDiscAmount(final double lineDiscAmount)
	{
		this.lineDiscAmount = lineDiscAmount;
	}

	public int getLineNo()
	{
		return lineNo;
	}

	public void setLineNo(final int lineNo)
	{
		this.lineNo = lineNo;
	}

	public double getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final double quantity)
	{
		this.quantity = quantity;
	}

	public double getTaxAmount()
	{
		return taxAmount;
	}

	public void setTaxAmount(final double taxAmount)
	{
		this.taxAmount = taxAmount;
	}

	public String getTaxCode()
	{
		return taxCode;
	}

	public void setTaxCode(final String taxCode)
	{
		this.taxCode = taxCode;
	}

	public double getTaxIncludePrice()
	{
		return taxIncludePrice;
	}

	public void setTaxIncludePrice(final double taxIncludePrice)
	{
		this.taxIncludePrice = taxIncludePrice;
	}

	public double getSalesPrice()
	{
		return salesPrice;
	}

	public void setSalesPrice(final double salesPrice)
	{
		this.salesPrice = salesPrice;
	}

	public String getUnitofmeasurecode()
	{
		return unitofmeasurecode;
	}

	public void setUnitofmeasurecode(final String unitofmeasurecode)
	{
		this.unitofmeasurecode = unitofmeasurecode;
	}

	@Override
	public String toString()
	{
		return "SalesLine [amount=" + amount + ", amountIncVat=" + amountIncVat + ", itemNo=" + itemNo + ", lineDiscAmount="
				+ lineDiscAmount + ", lineNo=" + lineNo + ", quantity=" + quantity + ", taxAmount=" + taxAmount + ", taxCode="
				+ taxCode + ", taxIncludePrice=" + taxIncludePrice + ", salesPrice=" + salesPrice + ", unitofmeasurecode="
				+ unitofmeasurecode + "]";
	}


}
