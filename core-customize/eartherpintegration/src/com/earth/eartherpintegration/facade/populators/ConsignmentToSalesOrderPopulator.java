/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.beans.ERPSalesPaymentLine;
import com.earth.eartherpintegration.model.EarthERPProviderModel;
import com.earth.eartherpintegration.model.EarthERPSalesOrderProviderModel;
import com.earth.eartherpintegration.service.EarthERPProviderService;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentToSalesOrderPopulator implements Populator<ConsignmentModel, ERPSalesOrder>
{

	@Resource(name = "erpConsignmentEntryToSalesLineConverter")
	Converter<ConsignmentEntryModel, ERPSalesLine> erpConsignmentEntryToSalesLineConverter;

	@Resource(name = "erpConsignmentEntryToSalesPaymentLineConverter")
	Converter<ConsignmentModel, ERPSalesPaymentLine> erpConsignmentEntryToSalesPaymentLineConverter;

	@Resource(name = "earthERPProviderService")
	private EarthERPProviderService earthERPProviderService;

	private static String CASH_CODE = "CASH";
	private static String CREDIT_CART_CODE = "creditCard";
	private static String LOYALTY_CODE = "loyalty";
	private static String STORE_CREDIT_CODE = "storeCredit";


	@Override
	public void populate(final ConsignmentModel source, final ERPSalesOrder target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "source Is null");
		Preconditions.checkNotNull(target, "target Is null");

		target.setDeliveryStatus(1);
		if (Objects.nonNull(source.getWarehouse()))
		{
			target.setStoreCode(source.getWarehouse().getErpLocationCode());

		}
		if (CollectionUtils.isNotEmpty(source.getConsignmentEntries()))
		{
			target.setNoOfItems(source.getConsignmentEntries().size());
			target.getSalesLines().addAll(getErpConsignmentEntryToSalesLineConverter().convertAll(source.getConsignmentEntries()));
			target.getSalesPayLines().addAll(getSalesPayLines(source));
		}
		if (Objects.isNull(source.getOrder()))
		{
			return;
		}

		final AbstractOrderModel order = source.getOrder();

		final Optional<EarthERPProviderModel> provider = getEarthERPProviderService().getActiveProvider(order.getStore(),
				EarthERPSalesOrderProviderModel.class);

		if (provider.isPresent())
		{
			target.setCompanyCode(provider.get().getCompanyCode());
		}

		final double storeCreditAmount = Objects.nonNull(order.getStoreCreditAmountSelected())
				? order.getStoreCreditAmountSelected()
				: 0.0;
		target.setAmount(order.getTotalPrice() + storeCreditAmount);
		target.setAmountIncVat(order.getTotalPrice() + storeCreditAmount);
		if (Objects.nonNull(order.getCurrency()))
		{
			target.setCurrencyCode(order.getCurrency().getIsocode());
		}
		if (Objects.nonNull(order.getDate()))
		{
			target.setOrderDate(getDate(order.getDate()));
		}
		target.setOrderNo(order.getCode());

		target.setDeliveryCharges(order.getDeliveryCost() != null ? String.valueOf(order.getDeliveryCost().doubleValue()) : "0.0");
	}

	/**
	 *
	 */
	protected List<ERPSalesPaymentLine> getSalesPayLines(final ConsignmentModel source)
	{
		if (Objects.isNull(source.getOrder()))
		{
			return Collections.emptyList();
		}

		final AbstractOrderModel order = source.getOrder();
		final List<ERPSalesPaymentLine> erpErpSalesLines = new LinkedList<>();
		final ERPSalesPaymentLine storeCreditSalesLine = getERPSalesPaymentLine(order.getStoreCreditAmountSelected(),
				erpErpSalesLines.size(), STORE_CREDIT_CODE);

		if (Objects.nonNull(storeCreditSalesLine))
		{
			erpErpSalesLines.add(storeCreditSalesLine);
		}
		final ERPSalesPaymentLine loyaltySalesLine = getERPSalesPaymentLine(order.getLoyaltyAmount(), erpErpSalesLines.size(),
				LOYALTY_CODE);

		if (Objects.nonNull(loyaltySalesLine))
		{
			erpErpSalesLines.add(loyaltySalesLine);
		}
		if (Objects.isNull(order.getPaymentMode()))
		{
			return erpErpSalesLines;
		}
		final String paymentMode = order.getPaymentMode().getCode();

		ERPSalesPaymentLine totalPriceSalesLine = null;
		if ("COD".equalsIgnoreCase(paymentMode))
		{
			totalPriceSalesLine = getERPSalesPaymentLine(order.getTotalPrice(), erpErpSalesLines.size(), CASH_CODE);
		}
		else if ("CARD".equalsIgnoreCase(paymentMode))
		{
			totalPriceSalesLine = getERPSalesPaymentLine(order.getTotalPrice(), erpErpSalesLines.size(), CREDIT_CART_CODE);
		}

		if (Objects.nonNull(totalPriceSalesLine))
		{
			erpErpSalesLines.add(totalPriceSalesLine);
		}

		return erpErpSalesLines;
	}

	/**
	 *
	 */
	protected ERPSalesPaymentLine getERPSalesPaymentLine(final Double amount, final int index, final String paymentMethod)
	{
		final ERPSalesPaymentLine erpSalesPaymentLine = new ERPSalesPaymentLine();
		if (amount <= 0)
		{
			return null;
		}
		erpSalesPaymentLine.setAmount(amount);
		erpSalesPaymentLine.setLineNo(index);
		erpSalesPaymentLine.setPaymMethod(paymentMethod);
		return erpSalesPaymentLine;
	}

	/**
	 * @return the erpConsignmentEntryToSalesLineConverter
	 */
	protected Converter<ConsignmentEntryModel, ERPSalesLine> getErpConsignmentEntryToSalesLineConverter()
	{
		return erpConsignmentEntryToSalesLineConverter;
	}

	/**
	 * @return the earthERPProviderService
	 */
	protected EarthERPProviderService getEarthERPProviderService()
	{
		return earthERPProviderService;
	}

	protected String getDate(final Date date)
	{
		final SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return formater.format(date);
	}

	/**
	 * @return the erpConsignmentEntryToSalesPaymentLineConverter
	 */
	public Converter<ConsignmentModel, ERPSalesPaymentLine> getErpConsignmentEntryToSalesPaymentLineConverter()
	{
		return erpConsignmentEntryToSalesPaymentLineConverter;
	}

}
