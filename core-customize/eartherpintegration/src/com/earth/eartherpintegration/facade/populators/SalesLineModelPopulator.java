/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DecimalFormat;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.earth.eartherpintegration.model.ERPSalesLineModel;
import com.google.common.base.Preconditions;


/**
 *
 */
public class SalesLineModelPopulator implements Populator<ERPSalesLineModel, ERPSalesLine>
{

	@Override
	public void populate(final ERPSalesLineModel source, final ERPSalesLine target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "source Is null");
		Preconditions.checkNotNull(target, "target Is null");

		target.setAmount(source.getAmount());
		target.setAmountIncVat(source.getAmountIncVat());

		target.setLineDiscAmount(source.getLineDiscAmount());
		target.setLineNo(source.getLineNo());
		//target.setQuantity(source.getQuantity());
		target.setTaxCode(source.getTaxCode());
		target.setUnitofmeasurecode(source.getUnitofmeasurecode());
		target.setTaxAmount(source.getTaxAmount());
		target.setTaxIncludePrice(source.getTaxIncludePrice());
		target.setSalesPrice(source.getSalesPrice());
		populateQuantity(source, target);
		populateItemNo(source, target);

	}



	/**
	 *
	 */
	private void populateQuantity(final ERPSalesLineModel source, final ERPSalesLine target)
	{
		final Double quantity = Double.valueOf(source.getQuantity());
		if ("KG".equalsIgnoreCase(source.getUnitofmeasurecode()))
		{
			final double formatDouble = formatDouble(quantity / 1000.0);
			target.setQuantity(formatDouble);
		}
		else
		{
			target.setQuantity(quantity);
		}

	}



	private void populateItemNo(final ERPSalesLineModel source, final ERPSalesLine target)
	{
		String itemNo = source.getItemNo();
		if (itemNo.split("_").length == 2)
		{
			itemNo = itemNo.split("_")[0];
		}

		target.setItemNo(itemNo);
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.##");
		final String format = df.format(val);
		return Double.valueOf(format);
	}



}
