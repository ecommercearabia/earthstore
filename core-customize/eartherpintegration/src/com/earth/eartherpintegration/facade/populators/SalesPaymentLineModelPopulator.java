/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.earth.eartherpintegration.beans.ERPSalesPaymentLine;
import com.earth.eartherpintegration.model.ERPSalesPaymentLineModel;
import com.google.common.base.Preconditions;


/**
 *
 */
public class SalesPaymentLineModelPopulator implements Populator<ERPSalesPaymentLineModel, ERPSalesPaymentLine>
{


	@Override
	public void populate(final ERPSalesPaymentLineModel source, final ERPSalesPaymentLine target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "source Is null");
		Preconditions.checkNotNull(target, "target Is null");

		target.setAmount(source.getAmount());
		target.setLineNo(source.getLineNo());
		target.setPaymMethod(source.getPaymMethod());
	}

}
