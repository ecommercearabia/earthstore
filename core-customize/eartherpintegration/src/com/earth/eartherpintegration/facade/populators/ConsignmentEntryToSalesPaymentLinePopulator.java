/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.

 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.earth.eartherpintegration.beans.ERPSalesPaymentLine;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentEntryToSalesPaymentLinePopulator implements Populator<ConsignmentModel, ERPSalesPaymentLine>
{

	@Resource(name = "ordermanagementOrderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	@Override
	public void populate(final ConsignmentModel source, final ERPSalesPaymentLine target) throws ConversionException
	{

		Preconditions.checkNotNull(source, "Consignment Is null");
		Preconditions.checkNotNull(source.getWarehouse(), "Warehouse Is null");
		final AbstractOrderModel order = source.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + source.getCode() + "] consignemnt");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + source.getCode() + "] consignemnt");

		target.setAmount(order.getTotalPrice());
		target.setLineNo(0);
		target.setPaymMethod(order.getPaymentMode() != null ? order.getPaymentMode().getCode() : "");
	}

	/**
	 * @return the orderConverter
	 */
	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

}
