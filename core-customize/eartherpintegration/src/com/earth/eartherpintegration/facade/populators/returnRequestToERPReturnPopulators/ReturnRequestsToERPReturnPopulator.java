/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators.returnRequestToERPReturnPopulators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.eartherpintegration.beans.ERPReturnOrder;
import com.earth.eartherpintegration.beans.ERPReturnSalesLine;
import com.earth.eartherpintegration.beans.ERPReturnSalesPaymentLine;
import com.earth.eartherpintegration.model.EarthERPProviderModel;
import com.earth.eartherpintegration.model.EarthERPSalesOrderProviderModel;
import com.earth.eartherpintegration.service.EarthERPProviderService;


/**
 *
 */
public class ReturnRequestsToERPReturnPopulator implements Populator<Collection<ReturnRequestModel>, ERPReturnOrder>
{
	private static final Logger LOG = LoggerFactory.getLogger(ReturnRequestsToERPReturnPopulator.class);
	private static final String ORDER_NUMBER_DATE_FORMAT = "dd/MM/yyyy";
	private static final String ORDER_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";


	@Resource(name = "earthERPProviderService")
	private EarthERPProviderService earthERPProviderService;

	@Override
	public void populate(final Collection<ReturnRequestModel> source, final ERPReturnOrder target) throws ConversionException
	{
		if (Collections.isEmpty(source) || target == null)
		{
			LOG.info("No ReturnRequests found, nothing will be populated");
			return;
		}

		populateBasicInformation(source.iterator().next(), target);

		// For each ReturnRequestEntry in each ReturnRequest
		// Convert to both ReturnSalesLine & ReturnSalesPayment Line

		final long entryCount = source.stream().flatMap(e -> e.getReturnEntries().stream()).count();
		final List<ERPReturnSalesLine> salesLines = new ArrayList<>((int) entryCount);

		final List<ERPReturnSalesPaymentLine> salesPaymentLines = new ArrayList<>(source.size());

		int counter = 0;
		for (final ReturnRequestModel request : source)
		{
			final List<ERPReturnSalesLine> salesLinesForRequest = convertEntries(request, request.getReturnEntries(), counter);
			salesLines.addAll(salesLinesForRequest);

			final ERPReturnSalesPaymentLine salesPaymentLine = convertReturnRequestToSalesPaymentLine(request, counter);
			salesPaymentLines.add(salesPaymentLine);

			counter++;
		}
		target.setSalesLines(salesLines);
		target.setSalesPayLines(salesPaymentLines);
		populateAmount(source, target, entryCount);

	}


	/**
	 *
	 */
	private ERPReturnSalesPaymentLine convertReturnRequestToSalesPaymentLine(final ReturnRequestModel request, final int counter)
	{
		final ERPReturnSalesPaymentLine target = new ERPReturnSalesPaymentLine();
		target.setAmount(request.getSubtotal().doubleValue() * -1);
		target.setLineNo(counter);
		target.setPaymMethod(request.getOrder().getPaymentMode().getCode());
		return target;
	}


	/**
	 * @param request
	 *
	 */
	private List<ERPReturnSalesLine> convertEntries(final ReturnRequestModel request, final List<ReturnEntryModel> returnEntries,
			final int counter)
	{
		final List<ERPReturnSalesLine> result = new ArrayList<>(returnEntries.size());

		for (final ReturnEntryModel returnEntry : returnEntries)
		{

			final ERPReturnSalesLine target = new ERPReturnSalesLine();

			if (!(returnEntry instanceof RefundEntryModel))
			{
				LOG.warn("ReturnEntry[{}] in ReturnRequest[{}] is not an instance of RefundEntryModel", returnEntry.getPk(),
						request.getCode());
				return null;
			}
			final AbstractOrderEntryModel orderEntry = returnEntry.getOrderEntry();
			final ProductModel product = orderEntry.getProduct();
			final RefundEntryModel source = (RefundEntryModel) returnEntry;

			final Double totalTax = orderEntry.getTaxValues().stream().map(TaxValue::getAppliedValue).reduce(0.d, Double::sum);
			final Double taxPerEntry = totalTax / orderEntry.getQuantity();
			final Double totalTaxForReturn = taxPerEntry * returnEntry.getExpectedQuantity();

			final Double totalDiscount = orderEntry.getDiscountValues().stream().map(DiscountValue::getAppliedValue).reduce(0.d,
					Double::sum);
			final Double discountPerEntry = totalDiscount / orderEntry.getQuantity();

			target.setAmount(source.getAmount().doubleValue() * -1);
			target.setAmountIncVat((source.getAmount().doubleValue()) * -1);
			//target.setItemNo(product.getCode());
			populateItemNo(product, target);
			target.setLineDiscAmount(discountPerEntry * source.getExpectedQuantity());
			target.setLineNo(counter);
			target.setQuantity(getQuantityForEntry(source, product) * -1);
			target.setTaxAmount(totalTaxForReturn);
			target.setTaxCode("5PCT");
			target.setTaxIncludePrice(orderEntry.getBasePrice());
			target.setSalesPrice(orderEntry.getBasePrice());
			target.setUnitofmeasurecode(orderEntry.getUnit().getCode());

			result.add(target);
		}

		return result;
	}

	private void populateItemNo(final ProductModel product, final ERPReturnSalesLine target)
	{
		String itemNo = product.getCode();
		if (itemNo.split("_").length == 2)
		{
			itemNo = itemNo.split("_")[0];
		}

		target.setItemNo(itemNo);
	}





	/**
	 *
	 */
	private double getQuantityForEntry(final RefundEntryModel source, final ProductModel product)
	{
		final double unitFactor = getUnitFactorForProduct(product);

		return source.getExpectedQuantity() / unitFactor;

	}


	/**
	 *
	 */
	private double getUnitFactorForProduct(final ProductModel product)
	{
		if (product == null || product.getUnit() == null || Strings.isBlank(product.getUnit().getCode()))
		{
			return 1.0d;
		}
		final String code = product.getUnit().getCode();

		if ("KG".equalsIgnoreCase(code))
		{
			return 1000.0d;
		}

		return 1.0d;
	}


	/**
	 *
	 */
	private void populateBasicInformation(final ReturnRequestModel source, final ERPReturnOrder target)
	{
		final Date date = new Date();

		final OrderModel order = source.getOrder();
		final WarehouseModel returnWarehouse = source.getReturnWarehouse();

		final Optional<EarthERPProviderModel> provider = getEarthERPProviderService().getActiveProvider(order.getStore(),
				EarthERPSalesOrderProviderModel.class);

		target.setOrderDate(getFormattedDate(date, ORDER_DATE_FORMAT));
		target.setOrderNo("SR" + getFormattedDate(date, ORDER_NUMBER_DATE_FORMAT) + returnWarehouse.getErpLocationCode());
		target.setStoreCode(returnWarehouse.getErpLocationCode());
		target.setCompanyCode(provider.get().getCompanyCode());
		target.setCurrencyCode(order.getCurrency().getIsocode());


	}

	private double getDeliveryCostForReturnRequest(final ReturnRequestModel returnRequest)
	{
		if (returnRequest == null || returnRequest.getOrder() == null || returnRequest.getOrder().getDeliveryCost() == null)
		{
			return 0.0d;
		}
		return returnRequest.getOrder().getDeliveryCost().doubleValue();
	}

	/**
	 * @param source
	 * @param target
	 *
	 */
	private void populateAmount(final Collection<ReturnRequestModel> source, final ERPReturnOrder target, final long entriesCount)
	{
		final double amount = source.stream().map(e -> e.getSubtotal()).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue();
		final Double deliveryCharges = source.stream().filter(e -> e.getRefundDeliveryCost())
				.map(this::getDeliveryCostForReturnRequest).reduce(0.0d, Double::sum);

		target.setAmount(amount * -1);
		target.setAmountIncVat(amount * -1);
		target.setDeliveryCharges(deliveryCharges == null ? 0.0d : deliveryCharges.doubleValue());
		target.setDeliveryStatus(1);
		target.setNoOfItems(entriesCount);
	}

	private String getFormattedDate(final Date date, final String format)
	{
		final SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}


	/**
	 * @return the earthERPProviderService
	 */
	protected EarthERPProviderService getEarthERPProviderService()
	{
		return earthERPProviderService;
	}
}
