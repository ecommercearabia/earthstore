/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.beans.ERPSalesPaymentLine;
import com.earth.eartherpintegration.model.ERPSalesLineModel;
import com.earth.eartherpintegration.model.ERPSalesOrderModel;
import com.earth.eartherpintegration.model.ERPSalesPaymentLineModel;
import com.google.common.base.Preconditions;


/**
 *
 */
public class SalesOrderModelPopulator implements Populator<ERPSalesOrderModel, ERPSalesOrder>
{
	@Resource(name = "salesLineModelConverter")
	private Converter<ERPSalesLineModel, ERPSalesLine> salesLineModelConverter;

	@Resource(name = "salesPaymentLineModelConverter")
	private Converter<ERPSalesPaymentLineModel, ERPSalesPaymentLine> salesPaymentLineModelConverter;

	@Override
	public void populate(final ERPSalesOrderModel source, final ERPSalesOrder target) throws ConversionException
	{
		Preconditions.checkNotNull(source, "source Is null");
		Preconditions.checkNotNull(target, "target Is null");

		target.setStoreCode(source.getStoreCode());
		target.setAmount(source.getAmount());
		target.setAmountIncVat(source.getAmountIncVat());
		target.setCompanyCode(source.getCompanyCode());
		target.setCurrencyCode(source.getCurrencyCode());
		target.setDeliveryCharges(source.getDeliveryCharges());
		target.setDeliveryStatus(source.getDeliveryStatus());
		target.setNoOfItems(source.getNoOfItems());
		target.setOrderDate(source.getOrderDate());
		target.setOrderNo(source.getOrderNo());
		populateSalesLines(source, target);
		populateSalesPaymentLines(source, target);

	}




	private void populateSalesPaymentLines(final ERPSalesOrderModel source, final ERPSalesOrder target)
	{
		final List<ERPSalesPaymentLineModel> erpSalesPaymentLineModels = source.getSalesPaymentLines();
		if (!CollectionUtils.isEmpty(erpSalesPaymentLineModels))
		{
			final List<ERPSalesPaymentLine> erpSalesPaymentLines = getSalesPaymentLineModelConverter()
					.convertAll(erpSalesPaymentLineModels);
			target.setSalesPayLines(erpSalesPaymentLines);
		}

	}


	private void populateSalesLines(final ERPSalesOrderModel source, final ERPSalesOrder target)
	{
		final List<ERPSalesLineModel> erpSalesLineModels = source.getSalesLines();
		if (!CollectionUtils.isEmpty(erpSalesLineModels))
		{
			final List<ERPSalesLine> erpSalesLines = getSalesLineModelConverter().convertAll(erpSalesLineModels);
			target.setSalesLines(erpSalesLines);
		}

	}




	protected Converter<ERPSalesLineModel, ERPSalesLine> getSalesLineModelConverter()
	{
		return salesLineModelConverter;
	}



	protected Converter<ERPSalesPaymentLineModel, ERPSalesPaymentLine> getSalesPaymentLineModelConverter()
	{
		return salesPaymentLineModelConverter;
	}



}
