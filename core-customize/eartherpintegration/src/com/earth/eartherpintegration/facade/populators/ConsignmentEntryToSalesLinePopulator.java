/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.

 */
package com.earth.eartherpintegration.facade.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import javax.annotation.Resource;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 */
public class ConsignmentEntryToSalesLinePopulator implements Populator<ConsignmentEntryModel, ERPSalesLine>
{

	@Resource(name = "ordermanagementConsignmentEntryConverter")
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Override
	public void populate(final ConsignmentEntryModel source, final ERPSalesLine target) throws ConversionException
	{
		final ConsignmentModel consignment = source.getConsignment();
		Preconditions.checkNotNull(consignment, "Consignment Is null");
		Preconditions.checkNotNull(consignment.getWarehouse(), "Warehouse Is null");
		final AbstractOrderModel order = consignment.getOrder();
		Preconditions.checkNotNull(order, "Order Is null for [" + consignment.getCode() + "] consignemnt");
		Preconditions.checkNotNull(order.getUser(), "User Is null for [" + consignment.getCode() + "] consignemnt");

		final ConsignmentEntryData consignmentEntryData = getConsignmentEntryConverter().convert(source);


		if (Objects.isNull(consignmentEntryData))
		{
			return;
		}

		final OrderEntryData orderEntryData = consignmentEntryData.getOrderEntry();
		if (Objects.isNull(consignmentEntryData.getOrderEntry()))
		{
			return;
		}

		if (Objects.nonNull(orderEntryData.getTotalPrice()) && Objects.nonNull(orderEntryData.getTotalPrice().getValue()))
		{
			final double price = orderEntryData.getTotalPrice().getValue().doubleValue();
			final BigDecimal bd = BigDecimal.valueOf(price).setScale(2, RoundingMode.DOWN);
			target.setAmount(bd.doubleValue());
			target.setAmountIncVat(bd.doubleValue());

		}


		target.setLineDiscAmount(orderEntryData.getTotalDiscounts());
		target.setLineNo(orderEntryData.getEntryNumber());
		target.setQuantity(orderEntryData.getQuantity());
		target.setTaxAmount(orderEntryData.getTotalTaxAmount());
		target.setTaxCode("5Pct");

		if (Objects.nonNull(orderEntryData.getBasePrice()) && Objects.nonNull(orderEntryData.getBasePrice().getValue()))
		{
			target.setTaxIncludePrice(orderEntryData.getBasePrice().getValue().doubleValue());
			target.setSalesPrice(orderEntryData.getBasePrice().getValue().doubleValue());

		}

		if (Objects.isNull(orderEntryData.getProduct()))
		{
			return;
		}
		target.setItemNo(orderEntryData.getProduct().getCode());

		if (Objects.nonNull(orderEntryData.getProduct().getUnit()))
		{
			target.setUnitofmeasurecode(orderEntryData.getProduct().getUnit().getUnitType());
		}
	}

	/**
	 * @return the consignmentEntryConverter
	 */
	protected Converter<ConsignmentEntryModel, ConsignmentEntryData> getConsignmentEntryConverter()
	{
		return consignmentEntryConverter;
	}

}
