/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.earth.eartherpintegration.enums.ERPSalesOrderSendStatus;
import com.earth.eartherpintegration.exception.ERPSalesOrderException;
import com.earth.eartherpintegration.model.ERPSalesOrderModel;


/**
 *
 */
public interface ERPSalesOrderService
{

	List<ERPSalesOrderModel> getByStatus(ERPSalesOrderSendStatus status) throws ERPSalesOrderException;

	List<ERPSalesOrderModel> getAll();




	void send(BaseStoreModel store) throws ERPSalesOrderException;

	void send(List<ConsignmentModel> consignments, BaseStoreModel store) throws ERPSalesOrderException;

}
