package com.earth.eartherpintegration.service;

import java.util.List;
import java.util.Set;

import com.earth.eartherpintegration.beans.ERPItemBarcode;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.beans.ERPLocation;
import com.earth.eartherpintegration.beans.ERPReturnOrder;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.exception.EarthERPException;


public interface EarthERPIntegrationService
{

	public List<ERPItemPrice> getItemPriceList(String baseURL, String companyCode, String modifiedDateTime, final String itemId)
			throws EarthERPException;

	public List<ERPItemPrice> getItemsPriceList(String baseURL, String companyCode, int pageNo) throws EarthERPException;

	public List<ERPItemPrice> getAllItemsPriceList(String baseURL, String companyCode) throws EarthERPException;

	public Set<String> getAllItemsPriceListCodes(String baseURL, String companyCode) throws EarthERPException;


	public List<ERPItemPrice> getItemsPriceList(String baseURL, String companyCode, String modifiedDateTime)
			throws EarthERPException;

	public List<ERPItemBarcode> getItemBarcodeList(String baseURL, String companyCode, String modifiedDateTime,
			final String itemId) throws EarthERPException;

	public List<ERPItemBarcode> getItemsBarcodeList(String baseURL, String companyCode, String modifiedDateTime)
			throws EarthERPException;

	public List<ERPItemInventory> getItemInventoryList(String baseURL, String companyCode, String inventoryLocation,
			final String itemId) throws EarthERPException;

	public List<ERPItemInventory> getItemsInventoryList(String baseURL, String companyCode, String inventoryLocation)
			throws EarthERPException;

	public List<ERPLocation> getLocations(String baseURL, String companyCode) throws EarthERPException;


	public ERPSalesOrder sendSalesOrder(final String baseURL, final String companyCode, final String salesOrderJson)
			throws EarthERPException;


	public ERPReturnOrder sendReturnOrder(String baseURL, String companyCode, String returnOrderJson) throws EarthERPException;

}
