package com.earth.eartherpintegration.service.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.earth.eartherpintegration.beans.ERPItemBarcode;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.beans.ERPLocation;
import com.earth.eartherpintegration.beans.ERPReturnOrder;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.enums.ERPProviderEnvironment;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.model.EarthERPProductProviderModel;
import com.earth.eartherpintegration.model.EarthERPProviderModel;
import com.earth.eartherpintegration.model.EarthERPReturnOrderProviderModel;
import com.earth.eartherpintegration.model.EarthERPSalesOrderProviderModel;
import com.earth.eartherpintegration.service.ERPIntegrationService;
import com.earth.eartherpintegration.service.EarthERPIntegrationService;
import com.earth.eartherpintegration.service.EarthERPProviderService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Service
public class DefaultERPIntegrationService implements ERPIntegrationService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPIntegrationService.class);

	private static final String PROD_BASE_URL_EMPTY_MSG = "prodBaseUrl is empty";
	private static final String STG_BASE_URL_EMPTY_MSG = "stgBaseUrl is empty";
	private static final String EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG = "earthERPProductProviderModel is null";
	private static final String BASESTORE_MODEL_NULL_MSG = "baseStoreModel is null";


	@Resource(name = "earthERPIntegrationService")
	private EarthERPIntegrationService earthERPIntegrationService;

	@Resource(name = "earthERPProviderService")
	private EarthERPProviderService earthERPProviderService;

	@Resource(name = "erpConsignmentToSalesOrderConverter")
	private Converter<ConsignmentModel, ERPSalesOrder> erpConsignmentToSalesOrderConverter;



	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public List<ERPItemPrice> getItemPriceList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime, final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getItemPriceList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), modifiedDateTime, itemId);
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return getEarthERPIntegrationService().getItemsPriceList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), modifiedDateTime);
	}

	@Override
	public List<ERPItemBarcode> getItemBarcodeList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime, final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getItemBarcodeList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), modifiedDateTime, itemId);
	}

	@Override
	public List<ERPItemBarcode> getItemsBarcodeList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String modifiedDateTime) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getItemsBarcodeList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), modifiedDateTime);
	}

	@Override
	public List<ERPItemInventory> getItemInventoryList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String inventoryLocation, final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getItemInventoryList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), inventoryLocation, itemId);
	}

	@Override
	public List<ERPItemInventory> getItemsInventoryList(final EarthERPProductProviderModel earthERPProductProviderModel,
			final String inventoryLocation) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getItemsInventoryList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), inventoryLocation);
	}

	@Override
	public List<ERPLocation> getLocations(final EarthERPProductProviderModel earthERPProductProviderModel) throws EarthERPException
	{
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		return getEarthERPIntegrationService().getLocations(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode());
	}


	/**
	 *
	 */
	private String getBaseUrl(final EarthERPProviderModel earthERPProviderModel)
	{
		if (ERPProviderEnvironment.STG.equals(earthERPProviderModel.getErpProviderEnvironment()))
		{
			Preconditions.checkArgument(Strings.isNotBlank(earthERPProviderModel.getStgBaseUrl()), STG_BASE_URL_EMPTY_MSG);
			return earthERPProviderModel.getStgBaseUrl();
		}
		Preconditions.checkArgument(Strings.isNotBlank(earthERPProviderModel.getProdBaseUrl()), PROD_BASE_URL_EMPTY_MSG);
		return earthERPProviderModel.getProdBaseUrl();
	}

	@Override
	public List<ERPItemPrice> getItemPriceList(final BaseStoreModel baseStoreModel, final String itemId) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getItemPriceList(earthERPProductProvider, earthERPProductProvider.getModifiedDateTime(), itemId);
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final BaseStoreModel baseStoreModel) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getItemsPriceList(earthERPProductProvider, earthERPProductProvider.getModifiedDateTime());
	}

	@Override
	public List<ERPItemBarcode> getItemBarcodeList(final BaseStoreModel baseStoreModel, final String itemId)
			throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getItemBarcodeList(earthERPProductProvider, earthERPProductProvider.getModifiedDateTime(), itemId);
	}

	@Override
	public List<ERPItemBarcode> getItemsBarcodeList(final BaseStoreModel baseStoreModel) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getItemsBarcodeList(earthERPProductProvider, earthERPProductProvider.getModifiedDateTime());
	}

	@Override
	public List<ERPItemInventory> getItemInventoryList(final BaseStoreModel baseStoreModel, final String inventoryLocation,
			final String itemId) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getEarthERPIntegrationService().getItemInventoryList(getBaseUrl(earthERPProductProvider),
				earthERPProductProvider.getCompanyCode(), inventoryLocation, itemId);
	}

	@Override
	public List<ERPItemInventory> getItemsInventoryList(final BaseStoreModel baseStoreModel, final String inventoryLocation)
			throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getItemsInventoryList(earthERPProductProvider, inventoryLocation);
	}

	@Override
	public List<ERPLocation> getLocations(final BaseStoreModel baseStoreModel) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);

		return getLocations(earthERPProductProvider);
	}

	protected EarthERPProductProviderModel getEarthERPProductProvider(final BaseStoreModel baseStoreModel)
	{

		Preconditions.checkArgument(Objects.nonNull(baseStoreModel), BASESTORE_MODEL_NULL_MSG);

		final Optional<EarthERPProviderModel> activeProvider = getEarthERPProviderService().getActiveProvider(baseStoreModel,
				EarthERPProductProviderModel.class);
		Preconditions.checkArgument(activeProvider.isPresent(), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		Preconditions.checkArgument(activeProvider.get() instanceof EarthERPProductProviderModel,
				EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return (EarthERPProductProviderModel) activeProvider.get();

	}

	/**
	 * @return the earthERPProviderService
	 */
	protected EarthERPProviderService getEarthERPProviderService()
	{
		return earthERPProviderService;
	}

	/**
	 * @return the earthERPIntegrationService
	 */
	protected EarthERPIntegrationService getEarthERPIntegrationService()
	{
		return earthERPIntegrationService;
	}

	@Override
	public boolean sendSalesOrder(final BaseStoreModel baseStoreModel, final ConsignmentModel consignment) throws EarthERPException
	{
		final ERPSalesOrder convert = getErpConsignmentToSalesOrderConverter().convert(consignment);

		return sendSalesOrder(baseStoreModel, convert).isPresent();
	}

	@Override
	public Optional<ERPSalesOrder> sendSalesOrder(final BaseStoreModel baseStoreModel, final ERPSalesOrder salesOrder)
			throws EarthERPException
	{
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();

		final String jsonData = gson.toJson(List.of(salesOrder));
		final EarthERPSalesOrderProviderModel earthERPSalesOrderProvider = getEarthERPSalesOrderProvider(baseStoreModel);
		return Optional.ofNullable(getEarthERPIntegrationService().sendSalesOrder(getBaseUrl(earthERPSalesOrderProvider),
				earthERPSalesOrderProvider.getCompanyCode(), jsonData));
	}

	protected EarthERPSalesOrderProviderModel getEarthERPSalesOrderProvider(final BaseStoreModel baseStoreModel)
	{

		Preconditions.checkArgument(Objects.nonNull(baseStoreModel), BASESTORE_MODEL_NULL_MSG);

		final Optional<EarthERPProviderModel> activeProvider = getEarthERPProviderService().getActiveProvider(baseStoreModel,
				EarthERPSalesOrderProviderModel.class);
		Preconditions.checkArgument(activeProvider.isPresent(), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		Preconditions.checkArgument(activeProvider.get() instanceof EarthERPSalesOrderProviderModel,
				EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return (EarthERPSalesOrderProviderModel) activeProvider.get();

	}

	protected EarthERPReturnOrderProviderModel getEarthERPReturnOrderProvider(final BaseStoreModel baseStoreModel)
	{

		Preconditions.checkArgument(Objects.nonNull(baseStoreModel), BASESTORE_MODEL_NULL_MSG);

		final Optional<EarthERPProviderModel> activeProvider = getEarthERPProviderService().getActiveProvider(baseStoreModel,
				EarthERPReturnOrderProviderModel.class);
		Preconditions.checkArgument(activeProvider.isPresent(), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);
		Preconditions.checkArgument(activeProvider.get() instanceof EarthERPReturnOrderProviderModel,
				EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return (EarthERPReturnOrderProviderModel) activeProvider.get();

	}

	/**
	 * @return the erpConsignmentToSalesOrderConverter
	 */
	public Converter<ConsignmentModel, ERPSalesOrder> getErpConsignmentToSalesOrderConverter()
	{
		return erpConsignmentToSalesOrderConverter;
	}

	@Override
	public boolean sendSalesOrderByCurrentBaseStore(final ConsignmentModel consignment) throws EarthERPException
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return sendSalesOrder(currentBaseStore, consignment);
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public List<ERPItemPrice> getItemPriceList(final BaseStoreModel baseStoreModel, final String itemId,
			final String modifiedDateTime) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);
		return getItemPriceList(earthERPProductProvider, modifiedDateTime, itemId);
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final BaseStoreModel baseStoreModel, final String modifiedDateTime)
			throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProvider = getEarthERPProductProvider(baseStoreModel);
		return getItemsPriceList(earthERPProductProvider, modifiedDateTime);
	}

	@Override
	public void sendReturnOrder(final BaseStoreModel baseStore, final ERPReturnOrder returnOrder) throws EarthERPException
	{
		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();

		final String jsonData = gson.toJson(List.of(returnOrder));
		final EarthERPReturnOrderProviderModel earthERPSalesOrderProvider = getEarthERPReturnOrderProvider(baseStore);


		LOG.info(jsonData);

		getEarthERPIntegrationService().sendReturnOrder(getBaseUrl(earthERPSalesOrderProvider),
				earthERPSalesOrderProvider.getCompanyCode(), jsonData);
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final BaseStoreModel baseStoreModel, final int pageNo) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProviderModel = getEarthERPProductProvider(baseStoreModel);
		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return getEarthERPIntegrationService().getItemsPriceList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode(), pageNo);

	}

	@Override
	public List<ERPItemPrice> getAllItemsPriceList(final BaseStoreModel baseStoreModel) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProviderModel = getEarthERPProductProvider(baseStoreModel);

		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return getEarthERPIntegrationService().getAllItemsPriceList(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode());
	}

	@Override
	public Set<String> getAllItemsPriceListCodes(final BaseStoreModel baseStoreModel) throws EarthERPException
	{
		final EarthERPProductProviderModel earthERPProductProviderModel = getEarthERPProductProvider(baseStoreModel);

		Preconditions.checkArgument(Objects.nonNull(earthERPProductProviderModel), EARTH_ERP_PRODUCT_PROVIDER_MODEL_NULL_MSG);

		return getEarthERPIntegrationService().getAllItemsPriceListCodes(getBaseUrl(earthERPProductProviderModel),
				earthERPProductProviderModel.getCompanyCode());
	}
}
