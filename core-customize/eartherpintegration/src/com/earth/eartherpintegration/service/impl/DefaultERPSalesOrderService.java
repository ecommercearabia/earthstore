/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.eartherpintegration.beans.ERPSalesLine;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.beans.ERPSalesPaymentLine;
import com.earth.eartherpintegration.dao.ERPSalesOrderDao;
import com.earth.eartherpintegration.enums.ERPSalesOrderSendStatus;
import com.earth.eartherpintegration.exception.ERPSalesOrderException;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.exception.type.ERPSalesOrderExceptionType;
import com.earth.eartherpintegration.filter.ConsignmentsERPLocationFilter;
import com.earth.eartherpintegration.model.ERPSalesLineHistoryModel;
import com.earth.eartherpintegration.model.ERPSalesLineModel;
import com.earth.eartherpintegration.model.ERPSalesOrderHistoryModel;
import com.earth.eartherpintegration.model.ERPSalesOrderModel;
import com.earth.eartherpintegration.model.ERPSalesPaymentLineModel;
import com.earth.eartherpintegration.service.ERPIntegrationService;
import com.earth.eartherpintegration.service.ERPSalesOrderService;
import com.earth.earthfulfillment.service.CustomConsignmentService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class DefaultERPSalesOrderService implements ERPSalesOrderService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPSalesOrderService.class);

	private static final String ORDER_NUMBER_DATE_FORMAT = "dd/MM/yyyy";
	private static final String ORDER_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

	@Resource(name = "erpConsignmentToSalesOrderConverter")
	private Converter<ConsignmentModel, ERPSalesOrder> erpConsignmentToSalesOrderConverter;

	@Resource(name = "salesOrderModelConverter")
	private Converter<ERPSalesOrderModel, ERPSalesOrder> salesOrderModelConverter;

	@Resource(name = "erpSalesOrderDao")
	private ERPSalesOrderDao erpSalesOrderDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "consignmentsERPLocationFilter")
	private ConsignmentsERPLocationFilter consignmentsERPLocationFilter;

	@Resource(name = "erpIntegrationService")
	ERPIntegrationService erpIntegrationService;

	@Resource(name = "customConsignmentService")
	CustomConsignmentService customConsignmentService;




	@Override
	public List<ERPSalesOrderModel> getByStatus(final ERPSalesOrderSendStatus status) throws ERPSalesOrderException
	{
		if (status == null)
		{
			throw new ERPSalesOrderException(ERPSalesOrderExceptionType.BAD_REQUEST, "ERP sales order status cannot be null");
		}

		final List<ERPSalesOrderModel> salesOrders = getErpSalesOrderDao().getByStatus(status);

		if (salesOrders != null)
		{
			return salesOrders;
		}
		return new ArrayList<>();
	}

	@Override
	public List<ERPSalesOrderModel> getAll()
	{
		final List<ERPSalesOrderModel> salesOrders = getErpSalesOrderDao().getAll();

		if (salesOrders != null)
		{
			return salesOrders;
		}
		return new ArrayList<>();
	}


	protected ERPSalesOrderModel convert(final List<ConsignmentModel> consignments) throws ERPSalesOrderException
	{
		if (CollectionUtils.isEmpty(consignments))
		{
			throw new ERPSalesOrderException(ERPSalesOrderExceptionType.BAD_REQUEST, "Consignments cannot be null");
		}

		final ERPSalesOrderModel erpSalesOrderModel = getModelService().create(ERPSalesOrderModel.class);
		boolean firstTime = true;
		final List<String> consignmentCodes = new ArrayList<>();
		final Map<String, ERPSalesLineModel> salesLineMap = new HashMap<>();
		final Map<String, ERPSalesPaymentLineModel> salesPaymentLineMap = new HashMap<>();


		for (final ConsignmentModel consignmentModel : consignments)
		{


			final ERPSalesOrder erpSalesOrder = getErpConsignmentToSalesOrderConverter().convert(consignmentModel);
			if (erpSalesOrder == null)
			{
				continue;
			}

			consignmentCodes.add(consignmentModel.getCode());

			if (firstTime)
			{
				setBasicInformation(erpSalesOrderModel, erpSalesOrder);
				firstTime = false;
			}

			calculateAmount(erpSalesOrderModel, erpSalesOrder, consignmentModel);
			calculateAmountIncVat(erpSalesOrderModel, erpSalesOrder, consignmentModel);
			calculateDeliveryCharges(erpSalesOrderModel, erpSalesOrder);
			calculateSalesOrderLine(erpSalesOrderModel, erpSalesOrder, consignmentModel, salesLineMap);
			calculateSalesPaymentLine(erpSalesOrderModel, erpSalesOrder, salesPaymentLineMap);
		}
		setSalesLines(erpSalesOrderModel, salesLineMap);
		setSalesPaymentLines(erpSalesOrderModel, salesPaymentLineMap);
		erpSalesOrderModel.setConsginments(consignments);
		erpSalesOrderModel.setConsignmentCodes(consignmentCodes);
		erpSalesOrderModel.setNoOfItems(erpSalesOrderModel.getSalesLines().size());

		return erpSalesOrderModel;
	}

	/**
	 *
	 */


	private void setBasicInformation(final ERPSalesOrderModel erpSalesOrderModel, final ERPSalesOrder erpSalesOrder)
	{
		final Date date = new Date();

		erpSalesOrderModel.setOrderDate(getFormattedDate(date, ORDER_DATE_FORMAT));
		erpSalesOrderModel.setOrderNo(getFormattedDate(date, ORDER_NUMBER_DATE_FORMAT) + erpSalesOrder.getStoreCode());
		erpSalesOrderModel.setStoreCode(erpSalesOrder.getStoreCode());
		erpSalesOrderModel.setCompanyCode(erpSalesOrder.getCompanyCode());
		erpSalesOrderModel.setCurrencyCode(erpSalesOrder.getCurrencyCode());
		erpSalesOrderModel.setDeliveryStatus(erpSalesOrder.getDeliveryStatus());
	}

	/**
	 *
	 */
	private String getFormattedDate(final Date date, final String format)
	{
		final SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}

	private void calculateAmount(final ERPSalesOrderModel salesOrderModel, final ERPSalesOrder salesOrder,
			final ConsignmentModel consignment)
	{
		final double salesOrderModelAmount = salesOrderModel.getAmount() == null ? 0 : salesOrderModel.getAmount();
		final double salesOrderAmount = salesOrder.getAmount();
		final double loyaltyAmount = consignment.getOrder().getLoyaltyAmount();
		salesOrderModel.setAmount(salesOrderModelAmount + salesOrderAmount + loyaltyAmount);
	}

	private void calculateAmountIncVat(final ERPSalesOrderModel salesOrderModel, final ERPSalesOrder salesOrder,
			final ConsignmentModel consignment)
	{
		final double salesOrderModelAmountIncVat = salesOrderModel.getAmountIncVat() == null ? 0
				: salesOrderModel.getAmountIncVat();
		final double salesOrderAmountIncVat = salesOrder.getAmountIncVat();
		final double loyaltyAmountIncVat = consignment.getOrder().getLoyaltyAmount();
		salesOrderModel.setAmountIncVat(salesOrderModelAmountIncVat + salesOrderAmountIncVat + loyaltyAmountIncVat);

	}

	private void calculateDeliveryCharges(final ERPSalesOrderModel salesOrderModel, final ERPSalesOrder salesOrder)
	{
		final String salesOrderModelDeliveryString = salesOrderModel.getDeliveryCharges() == null ? "0"
				: salesOrderModel.getDeliveryCharges();


		try
		{
			final double salesOrderModelDelivery = Double.parseDouble(salesOrderModelDeliveryString);
			final double salesOrderDelivery = Double
					.parseDouble(salesOrder.getDeliveryCharges() == null ? "0" : salesOrder.getDeliveryCharges());
			salesOrderModel.setDeliveryCharges(String.valueOf(salesOrderModelDelivery + salesOrderDelivery));

		}
		catch (final NumberFormatException ex)
		{
			LOG.error(ex.getMessage());
		}

	}





	private void calculateSalesOrderLine(final ERPSalesOrderModel salesOrderModel, final ERPSalesOrder salesOrder,
			final ConsignmentModel consignmentModel, final Map<String, ERPSalesLineModel> salesLineMap)
	{
		if (salesOrderModel == null || salesOrder == null || CollectionUtils.isEmpty(salesOrder.getSalesLines()))
		{
			return;
		}

		final List<ERPSalesLine> salesLines = salesOrder.getSalesLines();


		for (final ERPSalesLine erpSalesLine : salesLines)
		{

			if (Strings.isBlank(erpSalesLine.getItemNo()))
			{
				continue;
			}
			final String itemNumber = erpSalesLine.getItemNo();

			ERPSalesLineModel temp;
			if (salesLineMap.containsKey(itemNumber))
			{
				temp = salesLineMap.get(itemNumber);
				editSalesLineHistory(temp, erpSalesLine);
			}
			else
			{
				temp = getModelService().create(ERPSalesLineModel.class);
				addNewSalesOrderLine(temp, erpSalesLine, itemNumber);
			}
			addNewSalesLineHistory(erpSalesLine, temp, consignmentModel);
			getModelService().save(temp);
			salesLineMap.put(itemNumber, temp);

		}
	}



	private void addNewSalesOrderLine(final ERPSalesLineModel temp, final ERPSalesLine erpSalesLine, final String itemNumber)
	{
		temp.setItemNo(itemNumber);
		temp.setTaxCode(erpSalesLine.getTaxCode());
		temp.setTaxIncludePrice(erpSalesLine.getTaxIncludePrice());
		temp.setSalesPrice(erpSalesLine.getSalesPrice());
		temp.setUnitofmeasurecode(erpSalesLine.getUnitofmeasurecode());
		temp.setAmount(erpSalesLine.getAmount());
		temp.setAmountIncVat(erpSalesLine.getAmountIncVat());
		temp.setLineDiscAmount(erpSalesLine.getLineDiscAmount());
		temp.setQuantity(erpSalesLine.getQuantity());
		temp.setTaxAmount(erpSalesLine.getTaxAmount());
	}

	private void editSalesLineHistory(final ERPSalesLineModel temp, final ERPSalesLine erpSalesLine)
	{
		temp.setAmount(temp.getAmount() + erpSalesLine.getAmount());
		temp.setAmountIncVat(temp.getAmountIncVat() + erpSalesLine.getAmountIncVat());
		temp.setLineDiscAmount(temp.getLineDiscAmount() + erpSalesLine.getLineDiscAmount());
		temp.setQuantity(temp.getQuantity() + erpSalesLine.getQuantity());
		temp.setTaxAmount(temp.getTaxAmount() + erpSalesLine.getTaxAmount());
	}

	private void calculateSalesPaymentLine(final ERPSalesOrderModel salesOrderModel, final ERPSalesOrder salesOrder,
			final Map<String, ERPSalesPaymentLineModel> salesPaymentLineMap)
	{
		if (salesOrderModel == null || salesOrder == null || CollectionUtils.isEmpty(salesOrder.getSalesPayLines()))
		{
			return;
		}

		final List<ERPSalesPaymentLine> salesPaymentLines = salesOrder.getSalesPayLines();


		for (final ERPSalesPaymentLine erpSalesPaymentLine : salesPaymentLines)
		{

			if (Strings.isBlank(erpSalesPaymentLine.getPaymMethod()))
			{
				continue;
			}
			final String paymentMethod = erpSalesPaymentLine.getPaymMethod();
			ERPSalesPaymentLineModel temp;

			if (salesPaymentLineMap.containsKey(paymentMethod))
			{
				temp = salesPaymentLineMap.get(paymentMethod);
				temp.setAmount(temp.getAmount() + erpSalesPaymentLine.getAmount());
			}
			else
			{
				temp = getModelService().create(ERPSalesPaymentLineModel.class);
				temp.setPaymMethod(erpSalesPaymentLine.getPaymMethod());
				temp.setAmount(erpSalesPaymentLine.getAmount());

			}
			getModelService().save(temp);
			salesPaymentLineMap.put(paymentMethod, temp);
		}


	}

	private void addNewSalesLineHistory(final ERPSalesLine erpSalesLine, final ERPSalesLineModel erpSalesLineModel,
			final ConsignmentModel consignmentModel)
	{
		final ERPSalesLineHistoryModel salesLineHistoryModel = getModelService().create(ERPSalesLineHistoryModel.class);
		salesLineHistoryModel.setConsignmentCode(consignmentModel.getCode());
		salesLineHistoryModel.setPrice(erpSalesLine.getAmount());
		salesLineHistoryModel.setQuantity(erpSalesLine.getQuantity());
		salesLineHistoryModel.setProductCode(erpSalesLine.getItemNo());

		salesLineHistoryModel.setErpSalesLine(erpSalesLineModel);

		getModelService().save(salesLineHistoryModel);
	}


	private void setSalesLines(final ERPSalesOrderModel salesOrderModel, final Map<String, ERPSalesLineModel> salesLineMap)
	{
		final List<ERPSalesLineModel> erpSalesLineModels = salesOrderModel.getSalesLines() == null ? new ArrayList<>()
				: salesOrderModel.getSalesLines();



		salesLineMap.forEach((k, v) -> erpSalesLineModels.add(v));

		int index = 0;
		for (final ERPSalesLineModel model : erpSalesLineModels)
		{
			model.setLineNo(index++);
		}
		salesOrderModel.setSalesLines(erpSalesLineModels);

	}


	private void setSalesPaymentLines(final ERPSalesOrderModel salesOrderModel,
			final Map<String, ERPSalesPaymentLineModel> salesPaymentLineMap)
	{

		final List<ERPSalesPaymentLineModel> erpSalesPaymentLineModels = salesOrderModel.getSalesPaymentLines() == null
				? new ArrayList<>()
				: salesOrderModel.getSalesPaymentLines();

		salesPaymentLineMap.forEach((k, v) -> erpSalesPaymentLineModels.add(v));

		int index = 0;
		for (final ERPSalesPaymentLineModel model : erpSalesPaymentLineModels)
		{
			model.setLineNo(index++);
		}

		salesOrderModel.setSalesPaymentLines(erpSalesPaymentLineModels);

	}

	@Override
	public void send(final BaseStoreModel store) throws ERPSalesOrderException
	{

		final ConsignmentStatus status = ConsignmentStatus.SHIPPED;
		final List<ConsignmentModel> consignments = customConsignmentService.getByStoreAndByShipmentStatusAndBySentStatus(store,
				status, false);


		send(consignments, store);
	}

	@Override
	public void send(final List<ConsignmentModel> consignments, final BaseStoreModel store) throws ERPSalesOrderException
	{
		Preconditions.checkArgument(consignments != null, "Consignments are null ");
		Preconditions.checkArgument(store != null, "Store is null");

		final Map<String, List<ConsignmentModel>> consignmentsPerWarehouse = getConsignmentsERPLocationFilter().filter(consignments,
				store);

		final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

		for (final String erpLocationCode : consignmentsPerWarehouse.keySet())
		{
			final List<ConsignmentModel> list = consignmentsPerWarehouse.get(erpLocationCode);
			final ERPSalesOrderModel erpSalesOrderModel = convert(list);
			final ERPSalesOrder erpSalesOrder = getSalesOrderModelConverter().convert(erpSalesOrderModel);
			final String consignmentsCodes = String.join(",",
					list.stream().map(ConsignmentModel::getCode).collect(Collectors.toList()));
			LOG.info("Here is the list of consignments for OrderNo:[{}]-> {}", erpSalesOrder.getOrderNo(), consignmentsCodes);
			final String request = gson.toJson(erpSalesOrder);
			LOG.info("finished converting list of consignments to sales order model");
			try

			{
				final Optional<ERPSalesOrder> optionalResponse = getErpIntegrationService().sendSalesOrder(store, erpSalesOrder);
				if (optionalResponse.isEmpty())
				{
					LOG.error("There was no response after sending this sales order: " + erpSalesOrder);
					addNewSalesOrderHistory(erpSalesOrderModel, request, Strings.EMPTY, ERPSalesOrderSendStatus.FAILED);
					erpSalesOrderModel.setSendStatus(ERPSalesOrderSendStatus.FAILED);
					return;
				}

				final String response = gson.toJson(List.of(optionalResponse.get()));

				addNewSalesOrderHistory(erpSalesOrderModel, request, response, ERPSalesOrderSendStatus.SUCCESS);
				erpSalesOrderModel.setSendStatus(ERPSalesOrderSendStatus.SUCCESS);
				LOG.info("erp sales order : " + erpSalesOrder + " was sent successfuly");
			}
			catch (final EarthERPException e)
			{
				LOG.error("Couldn't send erp sales order : " + erpSalesOrder + " because of the following exception: \n" + "type: "
						+ e.getExceptionType() + " \n" + "message: " + e.getMessage());

				final String response = gson.toJson(List.of(e.getData()));

				addNewSalesOrderHistory(erpSalesOrderModel, request, response, ERPSalesOrderSendStatus.FAILED);
				erpSalesOrderModel.setSendStatus(ERPSalesOrderSendStatus.FAILED);

			}
			LOG.info("saving sales order model");
			getModelService().save(erpSalesOrderModel);
			changeConsignmentsSentStatus(erpSalesOrderModel);
		}


	}



	private void changeConsignmentsSentStatus(final ERPSalesOrderModel erpSalesOrderModel)
	{
		if (ERPSalesOrderSendStatus.FAILED.equals(erpSalesOrderModel.getSendStatus()))
		{
			return;
		}

		final List<ConsignmentModel> consignments = erpSalesOrderModel.getConsginments();

		for (final ConsignmentModel consignmentModel : consignments)
		{
			consignmentModel.setSent(true);
			getModelService().save(consignmentModel);

		}


	}

	private void addNewSalesOrderHistory(final ERPSalesOrderModel erpSalesOrderModel, final String request, final String response,
			final ERPSalesOrderSendStatus status)
	{
		LOG.info("Creating sales order history model for sales order with request: {}\n", request);
		final ERPSalesOrderHistoryModel erpSalesOrderHistoryModel = getModelService().create(ERPSalesOrderHistoryModel.class);
		erpSalesOrderHistoryModel.setDate(new Date());
		erpSalesOrderHistoryModel.setRequestBody(request);
		erpSalesOrderHistoryModel.setResponseBody(response);
		erpSalesOrderHistoryModel.setSendStatus(status);
		erpSalesOrderHistoryModel.setSalesOrder(erpSalesOrderModel);

		getModelService().save(erpSalesOrderHistoryModel);
	}


	/**
	 * @return the salesOrderModelConverter
	 */
	protected Converter<ERPSalesOrderModel, ERPSalesOrder> getSalesOrderModelConverter()
	{
		return salesOrderModelConverter;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the erpIntegrationService
	 */
	protected ERPIntegrationService getErpIntegrationService()
	{
		return erpIntegrationService;
	}



	/**
	 * @return the consignmentsERPLocationFilter
	 */
	protected ConsignmentsERPLocationFilter getConsignmentsERPLocationFilter()
	{
		return consignmentsERPLocationFilter;
	}




	public ERPSalesOrderDao getErpSalesOrderDao()
	{
		return erpSalesOrderDao;
	}


	public Converter<ConsignmentModel, ERPSalesOrder> getErpConsignmentToSalesOrderConverter()
	{
		return erpConsignmentToSalesOrderConverter;
	}


}
