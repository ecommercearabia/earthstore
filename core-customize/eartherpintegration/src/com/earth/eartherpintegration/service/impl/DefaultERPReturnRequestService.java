/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service.impl;

import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthcore.service.CustomReturnService;
import com.earth.eartherpintegration.beans.ERPReturnOrder;
import com.earth.eartherpintegration.exception.ERPReturnRequestException;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.exception.type.ERPReturnRequestExceptionType;
import com.earth.eartherpintegration.service.ERPIntegrationService;
import com.earth.eartherpintegration.service.ERPReturnRequestService;
import com.google.common.base.Preconditions;



/**
 * The Class DefaultERPReturnRequestService.
 *
 * @author Tuqa
 * @author husam.dababneh@erabia.com
 */
public class DefaultERPReturnRequestService implements ERPReturnRequestService
{
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultERPReturnRequestService.class);

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The erp integration service. */
	@Resource(name = "erpIntegrationService")
	ERPIntegrationService erpIntegrationService;

	@Resource(name = "customReturnService")
	private CustomReturnService customReturnService;


	@Resource(name = "returnRequestsToERPReturnConverter")
	private Converter<Collection<ReturnRequestModel>, ERPReturnOrder> returnRequestsToERPReturnConverter;

	/**
	 * Send.
	 *
	 * @param requestsR
	 *           the requests
	 * @param store
	 *           the store
	 * @throws ERPReturnRequestException
	 *            the ERP return request exception
	 */
	@Override
	public void send(final Set<ReturnRequestModel> requests, final BaseStoreModel store, final String formatDate)
			throws ERPReturnRequestException
	{
		Preconditions.checkArgument(store != null, "Store is null");

		if (requests == null || requests.isEmpty())
		{
			LOG.info("No Data to Send");
			return;
		}

		final Map<WarehouseModel, List<ReturnRequestModel>> returnRequestsByWarehouse = requests.stream()
				.filter(e -> e.getReturnWarehouse() != null).collect(Collectors.groupingBy(ReturnRequestModel::getReturnWarehouse));

		reportNullWarehousesForReturnRequests(requests);
		sendReturnSalesOrderToErp(store, formatDate, returnRequestsByWarehouse);
	}


	private void sendReturnSalesOrderToErp(final BaseStoreModel store, final String formatDate,
			final Map<WarehouseModel, List<ReturnRequestModel>> returnRequestsByWarehouse) throws ERPReturnRequestException
	{
		for (final WarehouseModel warehouse : returnRequestsByWarehouse.keySet())
		{
			final List<ReturnRequestModel> returnRequests = returnRequestsByWarehouse.get(warehouse);

			final ERPReturnOrder erpReturnOrder = getReturnRequestsToERPReturnConverter().convert(returnRequests);
			try
			{
				getErpIntegrationService().sendReturnOrder(store, erpReturnOrder);
			}
			catch (final EarthERPException e)
			{
				throw new ERPReturnRequestException(ERPReturnRequestExceptionType.SERVER_ERROR, e.getMessage());
			}

			returnRequests.stream().forEach(e -> e.setSentToERP(true));
			getModelService().saveAll(returnRequests);

			LOG.info("ERPReturnRequestSenderCronJob[{}] processed [{}] ReturnRequests and finished successfully", formatDate,
					returnRequests.size());
		}
	}


	/**
	 *
	 */
	private void reportNullWarehousesForReturnRequests(final Set<ReturnRequestModel> collect)
	{
		final Set<String> listOfCodes = collect.stream().filter(e -> e.getReturnWarehouse() == null)
				.map(ReturnRequestModel::getCode).collect(Collectors.toSet());

		if (listOfCodes.isEmpty())
		{
			return;
		}
		final String returnRequests = String.join(", ", listOfCodes);
		LOG.error("Return Warehouse is null for the following Return Requests [{}]", returnRequests);
	}


	/**
	 * Gets the model service.
	 *
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the customReturnService
	 */
	protected CustomReturnService getCustomReturnService()
	{
		return customReturnService;
	}

	/**
	 * Gets the erp integration service.
	 *
	 * @return the erpIntegrationService
	 */
	protected ERPIntegrationService getErpIntegrationService()
	{
		return erpIntegrationService;
	}

	/**
	 * @return the returnRequestsToERPReturnConverter
	 */
	public Converter<Collection<ReturnRequestModel>, ERPReturnOrder> getReturnRequestsToERPReturnConverter()
	{
		return returnRequestsToERPReturnConverter;
	}


}
