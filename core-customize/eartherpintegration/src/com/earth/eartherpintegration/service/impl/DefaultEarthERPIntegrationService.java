package com.earth.eartherpintegration.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.earth.eartherpintegration.beans.ERPItemBarcode;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.beans.ERPLocation;
import com.earth.eartherpintegration.beans.ERPReturnOrder;
import com.earth.eartherpintegration.beans.ERPSalesOrder;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.exception.type.EarthERPExceptionType;
import com.earth.eartherpintegration.service.EarthERPIntegrationService;
import com.earth.eartherpintegration.util.EartErpConnectionUtil;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


/**
 *
 * @author husam.dababneh@erabia.com
 * @author mbaker
 */

@Service
public class DefaultEarthERPIntegrationService implements EarthERPIntegrationService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultEarthERPIntegrationService.class);//NONSONAR

	private static final String BASE_URL_EMPTY_MSG = "baseURL is empty";
	private static final String INVENTORY_LOCATION_EMPTY_MSG = "inventoryLocation is empty";
	private static final String MODIFIED_DATE_TIME_EMPTY_MSG = "modifiedDateTime is empty";
	private static final String COMPANY_CODE_EMPTY_MSG = "companyCode is empty";
	private static final String ITEM_ID_EMPTY_MSG = "itemId is empty";

	private static final String INVENTORY_LOCATION_KEY = "inventLocation";
	private static final String ITEM_ID_KEY = "itemId";
	private static final String MODIFIED_DATE_TIME_KEY = "modifiedDateTime";
	private static final String PAGE_NO_TIME_KEY = "pageNo";

	private static final String COMPANY_CODE_KEY = "companyCode";

	private static final String ITEMS_PRICE_URL = "getEcommerceItemsPrice";
	private static final String ITEMS_PRICE_BY_PAGE_URL = "getEcommerceItemsPriceByPage";

	private static final String ITEM_PRICE_URL = "getEcommerceItemPrice";
	private static final String ITEM_BARCODE_URL = "getEcommerceItemBarcode";
	private static final String ITEMS_BARCODE_URL = "getEcommerceItemsBarcode";
	private static final String ITEM_INVENTORY_URL = "getEcommerceItemOnhandQty";
	private static final String ITEMS_INVENTORY_URL = "getEcommerceItemsOnhandQty";
	private static final String LOCATIONS_URL = "getLocations";

	private static final String POST_ECOMMERCE_SALES = "postEcommerceSales";


	@Override
	public List<ERPItemPrice> getItemPriceList(final String baseURL, final String companyCode, final String modifiedDateTime,
			final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(itemId), ITEM_ID_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(modifiedDateTime), MODIFIED_DATE_TIME_EMPTY_MSG);
		final Map<String, Object> params = getParamsMap(companyCode, modifiedDateTime);
		params.put(ITEM_ID_KEY, itemId);
		final ERPItemPrice[] erpItemsPrices = EartErpConnectionUtil.httpGet(baseURL.concat(ITEM_PRICE_URL), params,
				ERPItemPrice[].class);
		return Objects.nonNull(erpItemsPrices) ? Arrays.asList(erpItemsPrices) : Collections.emptyList();
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final String baseURL, final String companyCode, final String modifiedDateTime)
			throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(modifiedDateTime), MODIFIED_DATE_TIME_EMPTY_MSG);
		final Map<String, Object> params = getParamsMap(companyCode, modifiedDateTime);
		final ERPItemPrice[] erpItemsPrices = EartErpConnectionUtil.httpGet(baseURL.concat(ITEMS_PRICE_URL), params,
				ERPItemPrice[].class);
		return Objects.nonNull(erpItemsPrices) ? Arrays.asList(erpItemsPrices) : Collections.emptyList();
	}

	@Override
	public List<ERPItemBarcode> getItemBarcodeList(final String baseURL, final String companyCode, final String modifiedDateTime,
			final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(itemId), ITEM_ID_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(modifiedDateTime), MODIFIED_DATE_TIME_EMPTY_MSG);
		final Map<String, Object> params = getParamsMap(companyCode, modifiedDateTime);
		params.put(ITEM_ID_KEY, itemId);
		final ERPItemBarcode[] erpItemsBarcodes = EartErpConnectionUtil.httpGet(baseURL.concat(ITEM_BARCODE_URL), params,
				ERPItemBarcode[].class);
		return Objects.nonNull(erpItemsBarcodes) ? Arrays.asList(erpItemsBarcodes) : Collections.emptyList();
	}

	@Override
	public List<ERPItemBarcode> getItemsBarcodeList(final String baseURL, final String companyCode, final String modifiedDateTime)
			throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(modifiedDateTime), MODIFIED_DATE_TIME_EMPTY_MSG);
		final Map<String, Object> params = getParamsMap(companyCode, modifiedDateTime);
		final ERPItemBarcode[] erpItemBarcodes = EartErpConnectionUtil.httpGet(baseURL.concat(ITEMS_BARCODE_URL), params,
				ERPItemBarcode[].class);
		return Objects.nonNull(erpItemBarcodes) ? Arrays.asList(erpItemBarcodes) : Collections.emptyList();
	}

	@Override
	public List<ERPItemInventory> getItemInventoryList(final String baseURL, final String companyCode,
			final String inventoryLocation, final String itemId) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(itemId), ITEM_ID_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(inventoryLocation), MODIFIED_DATE_TIME_EMPTY_MSG);
		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		params.put(INVENTORY_LOCATION_KEY, inventoryLocation);
		params.put(ITEM_ID_KEY, itemId);
		final ERPItemInventory[] erpItemsInventory = EartErpConnectionUtil.httpGet(baseURL.concat(ITEM_INVENTORY_URL), params,
				ERPItemInventory[].class);
		return Objects.nonNull(erpItemsInventory) ? Arrays.asList(erpItemsInventory) : Collections.emptyList();
	}

	@Override
	public List<ERPItemInventory> getItemsInventoryList(final String baseURL, final String companyCode,
			final String inventoryLocation) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		Preconditions.checkArgument(Objects.nonNull(inventoryLocation), INVENTORY_LOCATION_EMPTY_MSG);
		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		params.put(INVENTORY_LOCATION_KEY, inventoryLocation);
		final ERPItemInventory[] erpItemInventory = EartErpConnectionUtil.httpGet(baseURL.concat(ITEMS_INVENTORY_URL), params,
				ERPItemInventory[].class);
		return Objects.nonNull(erpItemInventory) ? Arrays.asList(erpItemInventory) : Collections.emptyList();
	}

	@Override
	public List<ERPLocation> getLocations(final String baseURL, final String companyCode) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		final ERPLocation[] erpLocations = EartErpConnectionUtil.httpGet(baseURL.concat(LOCATIONS_URL), params,
				ERPLocation[].class);
		return Objects.nonNull(erpLocations) ? Arrays.asList(erpLocations) : Collections.emptyList();
	}

	private Map<String, Object> getParamsMap(final String companyCode, final String modifiedDateTime)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		params.put(MODIFIED_DATE_TIME_KEY, modifiedDateTime);

		return params;
	}

	private Map<String, Object> getParamsMapByPage(final String companyCode, final int pageNo)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		params.put(PAGE_NO_TIME_KEY, pageNo);
		return params;
	}

	@Override
	public ERPSalesOrder sendSalesOrder(final String baseURL, final String companyCode, final String salesOrderJson)
			throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);

		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		final String result = EartErpConnectionUtil.httpPost(baseURL.concat(POST_ECOMMERCE_SALES), params, salesOrderJson);

		try
		{
			final ERPSalesOrder[] erpSalesOrders = (new Gson()).fromJson(String.valueOf(result), ERPSalesOrder[].class);
			if (ArrayUtils.isNotEmpty(erpSalesOrders))
			{
				return erpSalesOrders[0];
			}
		}
		catch (final JsonSyntaxException ex)
		{
			LOG.error("failed to send order the response is {}", String.valueOf(result));
			throw new EarthERPException(EarthERPExceptionType.SEND_ORDER_ISSUE.getMsg(), String.valueOf(result),
					EarthERPExceptionType.SEND_ORDER_ISSUE);
		}
		return null;

	}

	@Override
	public ERPReturnOrder sendReturnOrder(final String baseURL, final String companyCode, final String returnOrderJson)
			throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);

		final Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_CODE_KEY, companyCode);
		final String result = EartErpConnectionUtil.httpPost(baseURL.concat(POST_ECOMMERCE_SALES), params, returnOrderJson);

		try
		{
			final ERPReturnOrder[] erpReturnOrders = (new Gson()).fromJson(String.valueOf(result), ERPReturnOrder[].class);
			if (ArrayUtils.isNotEmpty(erpReturnOrders))
			{
				return erpReturnOrders[0];
			}
		}
		catch (final JsonSyntaxException ex)
		{
			LOG.error("failed to send order the response is {}", String.valueOf(result));
			throw new EarthERPException(EarthERPExceptionType.SEND_RETURN_ORDER_ISSUE.getMsg(), String.valueOf(result),
					EarthERPExceptionType.SEND_RETURN_ORDER_ISSUE);
		}
		return null;
	}

	@Override
	public List<ERPItemPrice> getItemsPriceList(final String baseURL, final String companyCode, final int pageNo)
			throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		final Map<String, Object> params = getParamsMapByPage(companyCode, pageNo);
		final ERPItemPrice[] erpItemsPrices = EartErpConnectionUtil.httpGet(baseURL.concat(ITEMS_PRICE_BY_PAGE_URL), params,
				ERPItemPrice[].class);
		return Objects.nonNull(erpItemsPrices) ? Arrays.asList(erpItemsPrices) : Collections.emptyList();
	}

	@Override
	public List<ERPItemPrice> getAllItemsPriceList(final String baseURL, final String companyCode) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		int pageNo = 1;

		final List<ERPItemPrice> allItems = new ArrayList<>();
		boolean hasNext = true;

		do
		{
			final List<ERPItemPrice> itemsPriceList = getItemsPriceList(baseURL, companyCode, pageNo);

			if (!CollectionUtils.isEmpty(itemsPriceList))
			{
				allItems.addAll(itemsPriceList);
				++pageNo;
			}
			else
			{
				hasNext = false;
			}
		}
		while (hasNext);

		return allItems;
	}

	@Override
	public Set<String> getAllItemsPriceListCodes(final String baseURL, final String companyCode) throws EarthERPException
	{
		Preconditions.checkArgument(Strings.isNotBlank(baseURL), BASE_URL_EMPTY_MSG);
		Preconditions.checkArgument(Strings.isNotBlank(companyCode), COMPANY_CODE_EMPTY_MSG);
		int pageNo = 1;

		final Set<String> allItems = new HashSet<>();
		boolean hasNext = true;

		do
		{
			LOG.error("start to getItemsPriceList companyCode {} pageNo {}", companyCode, String.valueOf(pageNo));

			final List<ERPItemPrice> itemsPriceList = getItemsPriceList(baseURL, companyCode, pageNo);

			if (!CollectionUtils.isEmpty(itemsPriceList))
			{
				LOG.error("ItemsPriceList size is {}", String.valueOf(itemsPriceList.size()));

				allItems.addAll(itemsPriceList.stream().map(ERPItemPrice::getItemId).collect(Collectors.toSet()));
				++pageNo;
			}
			else
			{
				LOG.error("ItemsPriceList size is empty, the last page is {}", String.valueOf(pageNo - 1));
				hasNext = false;
			}
		}
		while (hasNext);

		return allItems;
	}




}
