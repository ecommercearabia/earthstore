/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.eartherpintegration.dao.EarthERPProviderDao;
import com.earth.eartherpintegration.model.EarthERPProviderModel;
import com.earth.eartherpintegration.service.EarthERPProviderService;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultLoyaltyProgramProviderService.
 */
public class DefaultEarthERPProviderService implements EarthERPProviderService
{

	/** The Constant CODE_MUSTN_T_BE_NULL. */
	private static final String CODE_MUSTN_T_BE_NULL = "code mustn't be null or empty";

	/** The Constant BASESTORE_UID_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_UID_MUSTN_T_BE_NULL = "baseStoreUid mustn't be null or empty";

	/** The Constant BASESTORE_MUSTN_T_BE_NULL. */
	private static final String BASESTORE_MUSTN_T_BE_NULL = "baseStoreModel mustn't be null or empty";

	/** The Constant PROVIDER_CLASS_MUSTN_T_BE_NULL. */
	private static final String PROVIDER_CLASS_MUSTN_T_BE_NULL = "providerClass mustn't be null";

	/** The Constant PROVIDER_DAO_NOT_FOUND. */
	private static final String PROVIDER_DAO_NOT_FOUND = "dao not found";

	/** The earth ERP provider dao map. */
	@Resource(name = "earthERPProviderDaoMap")
	private Map<Class<?>, EarthERPProviderDao> earthERPProviderDaoMap;

	/**
	 * Gets the loyalty program provider dao map.
	 *
	 * @return the loyalty program provider dao map
	 */
	protected Map<Class<?>, EarthERPProviderDao> getEarthProviderDaoMap()
	{
		return earthERPProviderDaoMap;
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	@Override
	public Optional<EarthERPProviderModel> get(final String code, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(code), CODE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EarthERPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().get(code);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @param providerClass
	 *           the provider class
	 * @return the active provider
	 */
	@Override
	public Optional<EarthERPProviderModel> getActiveProvider(final String baseStoreUid, final Class<?> providerClass)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(baseStoreUid), BASESTORE_UID_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EarthERPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProvider(baseStoreUid);
	}

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param providerClass
	 *           the provider class
	 * @return the active provider
	 */
	@Override
	public Optional<EarthERPProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel,
			final Class<?> providerClass)
	{
		Preconditions.checkArgument(baseStoreModel != null, BASESTORE_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EarthERPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProvider(baseStoreModel);
	}

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current base store
	 */
	@Override
	public Optional<EarthERPProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass)
	{
		Preconditions.checkArgument(providerClass != null, PROVIDER_CLASS_MUSTN_T_BE_NULL);
		final Optional<EarthERPProviderDao> dao = getDao(providerClass);
		Preconditions.checkArgument(dao.isPresent(), PROVIDER_DAO_NOT_FOUND);

		return dao.get().getActiveProviderByCurrentBaseStore();
	}

	/**
	 * Gets the dao.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the dao
	 */
	protected Optional<EarthERPProviderDao> getDao(final Class<?> providerClass)
	{
		final EarthERPProviderDao dao = getEarthProviderDaoMap().get(providerClass);

		return Optional.ofNullable(dao);
	}
}
