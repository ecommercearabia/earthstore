/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.eartherpintegration.model.EarthERPProviderModel;


/**
 *
 */
public interface EarthERPProviderService
{
	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @param providerClass
	 *           the provider class
	 * @return the optional
	 */
	public Optional<EarthERPProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid
	 *           the base store uid
	 * @param providerClass
	 *           the provider class
	 * @return the active provider
	 */
	public Optional<EarthERPProviderModel> getActiveProvider(String baseStoreUid, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel
	 *           the base store model
	 * @param providerClass
	 *           the provider class
	 * @return the active provider
	 */
	public Optional<EarthERPProviderModel> getActiveProvider(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass
	 *           the provider class
	 * @return the active provider by current base store
	 */
	public Optional<EarthERPProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass);
}
