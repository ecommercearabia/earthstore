/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.service;

import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;

import com.earth.eartherpintegration.exception.ERPReturnRequestException;


/**
 * The Interface ERPReturnRequestService.
 *
 * @author Tuqa
 */
public interface ERPReturnRequestService
{
	/**
	 * Send.
	 *
	 * @param requests
	 *           the requests
	 * @param store
	 *           the store
	 * @throws ERPReturnRequestException
	 *            the ERP return request exception
	 */
	void send(Set<ReturnRequestModel> requests, BaseStoreModel store, final String formatDate) throws ERPReturnRequestException;

}
