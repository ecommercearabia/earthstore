/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.exception;

import com.earth.eartherpintegration.exception.type.ERPReturnRequestExceptionType;


/**
 *
 */
public class ERPReturnRequestException extends Exception
{
	private final ERPReturnRequestExceptionType type;



	public ERPReturnRequestException(final ERPReturnRequestExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}


	public ERPReturnRequestExceptionType getType()
	{
	return type;
}


}
