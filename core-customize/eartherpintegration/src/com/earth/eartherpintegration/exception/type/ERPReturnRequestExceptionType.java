/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.exception.type;

/**
 *
 */
public enum ERPReturnRequestExceptionType
{
	BAD_REQUEST("Bad Request"),
	SERVER_ERROR("Server Error");

	private String message;

	ERPReturnRequestExceptionType(final String message)
	{
		this.message = message;
	}


	public String getMessage()
	{
		return message;
	}




}
