/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.eartherpintegration.exception;

import com.earth.eartherpintegration.exception.type.ERPSalesOrderExceptionType;


/**
 *
 */
public class ERPSalesOrderException extends Exception
{
	private final ERPSalesOrderExceptionType type;



	public ERPSalesOrderException(final ERPSalesOrderExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}


	public ERPSalesOrderExceptionType getType()
	{
	return type;
}


}
