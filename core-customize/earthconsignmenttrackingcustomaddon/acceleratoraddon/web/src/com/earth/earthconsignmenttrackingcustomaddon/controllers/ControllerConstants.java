/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthconsignmenttrackingcustomaddon.controllers;

/**
 */
public interface ControllerConstants
{
	interface Views
	{
		String _AddonPrefix = "addon:/earthconsignmenttrackingcustomaddon/";

		interface Pages
		{

			interface Consignment
			{
				String TrackPackagePage = _AddonPrefix + "pages/consignment/trackPackage";
			}
		}
	}
}
