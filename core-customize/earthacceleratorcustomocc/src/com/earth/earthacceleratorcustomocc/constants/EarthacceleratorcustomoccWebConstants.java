/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthacceleratorcustomocc.constants;

/**
 * Global class for all Earthacceleratorcustomocc web constants. You can add global constants for your extension into this class.
 */
public final class EarthacceleratorcustomoccWebConstants
{
	//Dummy field to avoid pmd error - delete when you add the first real constant!
	public static final String deleteThisDummyField = "DELETE ME";

	private EarthacceleratorcustomoccWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
