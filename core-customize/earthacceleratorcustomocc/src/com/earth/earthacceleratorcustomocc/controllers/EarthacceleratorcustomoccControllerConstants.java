/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthacceleratorcustomocc.controllers;

public interface EarthacceleratorcustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
