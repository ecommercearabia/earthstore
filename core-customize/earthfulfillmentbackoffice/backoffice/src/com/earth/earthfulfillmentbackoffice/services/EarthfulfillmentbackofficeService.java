/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthfulfillmentbackoffice.services;

/**
 * Hello World EarthfulfillmentbackofficeService
 */
public class EarthfulfillmentbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
