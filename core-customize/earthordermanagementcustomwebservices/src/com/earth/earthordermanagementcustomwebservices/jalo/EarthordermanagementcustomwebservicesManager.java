/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthordermanagementcustomwebservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthordermanagementcustomwebservices.constants.EarthordermanagementcustomwebservicesConstants;

@SuppressWarnings("PMD")
public class EarthordermanagementcustomwebservicesManager extends GeneratedEarthordermanagementcustomwebservicesManager
{
	public static final EarthordermanagementcustomwebservicesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthordermanagementcustomwebservicesManager) em.getExtension(EarthordermanagementcustomwebservicesConstants.EXTENSIONNAME);
	}
	
}
