/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomaddon.jalo;

import com.earth.earthcustomerinterestscustomaddon.constants.EarthcustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustomerinterestscustomaddonManager extends GeneratedEarthcustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustomerinterestscustomaddonManager.class.getName() );
	
	public static final EarthcustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustomerinterestscustomaddonManager) em.getExtension(EarthcustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
