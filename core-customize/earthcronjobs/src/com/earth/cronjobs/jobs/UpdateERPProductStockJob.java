/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.warehousing.model.AllocationEventModel;
import de.hybris.platform.warehousing.model.InventoryEventModel;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.model.ERPProductCronJobModel;
import com.earth.eartherpintegration.beans.ERPItemInventory;
import com.earth.eartherpintegration.exception.EarthERPException;


/**
 *
 */
public class UpdateERPProductStockJob extends AbstractERPProductJob
{

	private static final String UPDATE_QUANTITY_MSG = "updateQuantity={}";
	protected static final Logger LOG = LoggerFactory.getLogger(UpdateERPProductStockJob.class);

	@Override
	protected PerformResult performProductJob(final ERPProductCronJobModel cronjob)
	{

		Map<String, Set<String>> codesByInventory;

		try
		{
			codesByInventory = getERPUpdatedProductInventoryCodes(cronjob);
		}

		catch (final Exception e)
		{
			LOG.error("Cannot Update stock due to excetpion {}", e.getMessage());
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		}

		if (MapUtils.isEmpty(codesByInventory))
		{
			LOG.info("No items to update stocks");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}

		for (final Map.Entry<String, Set<String>> mapEntry : codesByInventory.entrySet())
		{
			try
			{
				updateWarehouse(cronjob, mapEntry.getKey(), mapEntry.getValue());
			}
			catch (final Exception e)
			{
				LOG.error("Cannot Update stock due to excetpion {}", e.getMessage());
			}
		}



		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * @param set
	 * @param string
	 * @param cronjob
	 *
	 */
	private void updateWarehouse(final ERPProductCronJobModel cronjob, final String warehouseCode, final Set<String> productCodes)
	{
		if (CollectionUtils.isEmpty(productCodes))
		{
			LOG.info("Cannot Update Warehouse {} because no productCodes found", warehouseCode);
			return;

		}

		Optional<WarehouseModel> warehouseForCode = Optional.empty();
		try
		{
			warehouseForCode = getWarehouseService().getWarehouseByERPLocationCode(warehouseCode);

		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.error("NO warehouse found for code {}", warehouseCode);
			return;
		}

		if (warehouseForCode.isEmpty())
		{
			LOG.info("Cannot Update Warehouse {} because no warehouse exist", warehouseCode);
			return;
		}

		for (final String code : productCodes)
		{
			updateProductStockInWarehouse(cronjob, warehouseForCode.get(), code);
		}

	}

	/**
	 * @param cronjob
	 *
	 */
	private void updateProductStockInWarehouse(final ERPProductCronJobModel cronjob, final WarehouseModel warehouseForCode,
			final String code)
	{
		final ProductModel product = getProduct(cronjob, code);
		if (Objects.isNull(product))
		{
			LOG.warn("Cannot update stock becasue no product exist with code {}", code);
			return;
		}

		if (Objects.isNull(product.getUnit()))
		{
			LOG.warn("Cannot update stock becasue  product with code {} doesn't have a unit", code);
			return;
		}

		List<ERPItemInventory> itemInventoryList;
		try
		{
			itemInventoryList = getErpIntegrationService().getItemInventoryList(cronjob.getStore(),
					warehouseForCode.getErpLocationCode(), code);
		}
		catch (final EarthERPException e)
		{
			LOG.error("Cannot Update stock due to exception {}", e.getMessage());
			return;
		}

		if (CollectionUtils.isEmpty(itemInventoryList))
		{
			LOG.error("Cannot Update stock {} because no items available", code);
			return;
		}
		for (final ERPItemInventory erpItemInventory : itemInventoryList)
		{
			updateProductStocks(cronjob, warehouseForCode, product, erpItemInventory);
		}

	}

	/**
	 *
	 */
	private void updateProductStocks(final ERPProductCronJobModel cronjob, final WarehouseModel warehouse,
			final ProductModel product, final ERPItemInventory erpItemInventory)
	{
		UnitModel unitForCode = null;
		try
		{
			unitForCode = getUnitService().getUnitForCode(erpItemInventory.getUnitID());
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("No unit with code {} exist", erpItemInventory.getUnitID());
		}
		if (Objects.isNull(unitForCode) || !unitForCode.equals(product.getUnit()))
		{
			LOG.info("Product {} has unit {} and ERP unit is {}", product.getCode(), product.getUnit().getCode(),
					erpItemInventory.getUnitID());
		}

		final Collection<VariantProductModel> variants = product.getVariants();

		if (CollectionUtils.isEmpty(variants))
		{

			LOG.info("No variant products for {} with unit {}", product.getCode(), erpItemInventory.getUnitID());
			updateQuantityForProduct(cronjob, warehouse, (int) erpItemInventory.getQuantity(), product);
			return;

		}
		final List<VariantProductModel> approvedVariants = variants.stream()
				.filter(v -> ArticleApprovalStatus.APPROVED.equals(v.getApprovalStatus())).collect(Collectors.toList());

		if (!validateInventoryAllocation(approvedVariants))
		{
			LOG.warn("Cannot Update Product Stocks for {} becaue inventory Allocation is not valid", product.getCode());
			return;
		}

		final List<VariantProductModel> productVaraintsWithUnit = approvedVariants.stream()
				.filter(p -> Objects.nonNull(p.getUnit())).collect(Collectors.toList());
		for (final VariantProductModel productVariant : productVaraintsWithUnit)
		{
			final double unitConverstion = productVariant.getUnitConvertion();
			if (unitConverstion <= 0)
			{
				LOG.warn("Cannot Update Product Stocks for {} with unit {} becaue unit converstion {} is not valid",
						productVariant.getCode(), productVariant.getUnit().getCode(), unitConverstion);
				continue;

			}

			final double invetoryAllocationPer = productVariant.getInventoryAllocationPercentage() / 100.0;
			final double qty = (erpItemInventory.getQuantity() * invetoryAllocationPer / unitConverstion);
			LOG.warn("product {} has unitConverstion {}, invetoryAllocation {}, qty {} ", productVariant.getCode(), unitConverstion,
					invetoryAllocationPer, qty);
			updateQuantityForProduct(cronjob, warehouse, (int) qty, productVariant);
		}

	}

	/**
	 *
	 */
	private boolean validateInventoryAllocation(final Collection<VariantProductModel> variants)
	{
		double inventoryAllocation = 0.0;
		for (final VariantProductModel product : variants)
		{
			inventoryAllocation += product.getInventoryAllocationPercentage();
		}
		return inventoryAllocation == 100.0;
	}

	private void updateQuantityForProduct(final ERPProductCronJobModel cronjob, final WarehouseModel warehouse, final int qty,
			final ProductModel variantProduct)
	{
		final int quantity = updateQuantity(variantProduct, cronjob, qty);

		final StockLevelModel stockLevel = getStockService().getStockLevel(variantProduct, warehouse);
		if (Objects.isNull(stockLevel))
		{
			createStockLevel(variantProduct, quantity, warehouse);
		}
		else
		{
			updateStockLevel(cronjob, variantProduct, quantity, stockLevel);
			updateInventoryEvents(cronjob, stockLevel);

		}
	}

	/**
	 * @param cronjob
	 * @param quantity
	 *
	 */
	private void updateStockLevel(final ERPProductCronJobModel cronjob, final ProductModel product, final int quantity,
			final StockLevelModel stockLevel)
	{
		if (cronjob.isResetStockReserved())
		{
			stockLevel.setReserved(0);
		}
		stockLevel.setAvailable(quantity);
		if (product.isIsForceInStock())
		{

			stockLevel.setInStockStatus(InStockStatus.FORCEINSTOCK);
		}
		else
		{
			stockLevel.setInStockStatus(InStockStatus.NOTSPECIFIED);
		}
		getModelService().save(stockLevel);
		LOG.info("STOCK UPDATED FOR PRODUCT CODE: {}", product.getCode());

	}

	/**
	 * @param quantity
	 *
	 */
	private void createStockLevel(final ProductModel product, final int quantity, final WarehouseModel warehouse)
	{
		final StockLevelModel newStock = getModelService().create(StockLevelModel.class);
		newStock.setWarehouse(warehouse);

		newStock.setAvailable(quantity);
		newStock.setProduct(product);
		newStock.setReserved(0);
		newStock.setProductCode(product.getCode());
		newStock.setInStockStatus(InStockStatus.NOTSPECIFIED);
		getModelService().save(newStock);
		LOG.info("STOCK CREATED FOR PRODUCT CODE:{} ", product.getCode());

	}


	protected int updateQuantity(final ProductModel product, final ERPProductCronJobModel cronJob, final int quantity)
	{
		LOG.info("isUpdateFixedStock={}", cronJob.isUpdateFixedStock());
		if (quantity < 0)
		{
			LOG.info(UPDATE_QUANTITY_MSG, 0);
			return 0;
		}
		if (cronJob.isUpdateFixedStock())
		{
			LOG.info(UPDATE_QUANTITY_MSG, cronJob.getFixedStock());

			return (int) cronJob.getFixedStock();
		}
		else if (KG_UNIT_SYMBOL.equalsIgnoreCase(product.getUnit().getCode()))
		{
			LOG.info(UPDATE_QUANTITY_MSG, quantity * 1000);
			return quantity * 1000;
		}
		else
		{
			LOG.info(UPDATE_QUANTITY_MSG, quantity);
			return quantity;
		}
	}

	protected void updateInventoryEvents(final ERPProductCronJobModel cronJob, final StockLevelModel stockLevel)
	{
		if (cronJob.getRemoveInventoryEvents() == null || Boolean.TRUE.equals(cronJob.getRemoveInventoryEvents()))
		{
			LOG.info("Delete inventory events where product code={}", stockLevel.getProductCode());
			final Collection<InventoryEventModel> inventoryEvents = stockLevel.getInventoryEvents();
			final List<InventoryEventModel> collect = inventoryEvents.stream().filter(e -> e instanceof AllocationEventModel)
					.map(e -> (AllocationEventModel) e).filter(UpdateERPProductStockJob::isAllocationEventNeeded)
					.collect(Collectors.toList());
			stockLevel.setInventoryEvents(collect);
			getModelService().save(stockLevel);
		}
	}

	private static boolean isAllocationEventNeeded(final AllocationEventModel ae)
	{

		if (ae.getConsignmentEntry() == null || ae.getConsignmentEntry().getConsignment() == null
				|| ae.getConsignmentEntry().getConsignment().getStatus() == null)
		{
			return true;
		}
		return !ae.getConsignmentEntry().getConsignment().getStatus().equals(ConsignmentStatus.DELIVERY_COMPLETED);
	}
}
