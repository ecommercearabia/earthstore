/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Set;

import com.earth.cronjobs.model.ERPProductCronJobModel;


/**
 *
 */
public class UpdateERPProductJob extends AbstractERPProductJob
{

	@Override
	protected PerformResult performProductJob(final ERPProductCronJobModel cronjob)
	{
		try
		{
			final Set<String> codes = getERPUpdatedProductInfoCodes(cronjob);
		}
		catch (final Exception e)
		{
			// XXX: handle exception
		}

		//		getERPUpdatedProductInfosByCode(cronjob, itemCode);

		return null;
	}


}
