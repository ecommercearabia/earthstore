package com.earth.cronjobs.jobs;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.model.SalesOrderSenderCronJobModel;
import com.earth.eartherpintegration.exception.ERPSalesOrderException;
import com.earth.eartherpintegration.service.ERPSalesOrderService;
import com.earth.earthfulfillment.service.CustomConsignmentService;
import com.google.common.base.Preconditions;


public class SalesOrderSenderJob extends AbstractJobPerformable<SalesOrderSenderCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SalesOrderSenderJob.class);


	@Resource(name = "erpSalesOrderService")
	private ERPSalesOrderService erpSalesOrderService;

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Override
	public PerformResult perform(final SalesOrderSenderCronJobModel cronJobModel)
	{

		Preconditions.checkArgument(cronJobModel != null, "cronJobModel is null");

		final List<ConsignmentStatus> consignmentStatuses = cronJobModel.getConsignmentStatuses();


		if (CollectionUtils.isEmpty(consignmentStatuses))
		{
			LOG.warn("There is no consignmet status assosiated with the following cron job: " + cronJobModel);

			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		try
		{
			final List<ConsignmentModel> consignments = getCustomConsignmentService()
					.getByStoreAndByShipmentStatusesAndBySentStatus(cronJobModel.getStore(), consignmentStatuses, false);

			if (CollectionUtils.isEmpty(consignments))
			{
				LOG.info("There is no consignment to send");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}
			getErpSalesOrderService().send(consignments, cronJobModel.getStore());
		}
		catch (final ERPSalesOrderException | IllegalArgumentException e)
		{
			LOG.error(
					"Error while performing sales order sender job with the following message: \n" + e.getCause() + e.getMessage());

			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @return the customConsignmentService
	 */
	protected CustomConsignmentService getCustomConsignmentService()
	{
		return customConsignmentService;
	}

	/**
	 * @return the erpSalesOrderService
	 */
	protected ERPSalesOrderService getErpSalesOrderService()
	{
		return erpSalesOrderService;
	}




}