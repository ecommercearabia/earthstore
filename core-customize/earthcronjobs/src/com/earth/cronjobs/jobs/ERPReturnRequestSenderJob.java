package com.earth.cronjobs.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.model.ERPReturnRequestSenderCronJobModel;
import com.earth.earthcore.service.CustomReturnService;
import com.earth.eartherpintegration.service.ERPReturnRequestService;
import com.google.common.base.Preconditions;


public class ERPReturnRequestSenderJob extends AbstractJobPerformable<ERPReturnRequestSenderCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ERPReturnRequestSenderJob.class);

	@Resource(name = "customReturnService")
	private CustomReturnService customReturnService;

	@Resource(name = "erpReturnRequestService")
	private ERPReturnRequestService erpReturnRequestService;


	private static final String ORDER_NUMBER_DATE_FORMAT = "dd/MM/yyyy";

	@Override
	public PerformResult perform(final ERPReturnRequestSenderCronJobModel cronJobModel)
	{

		Preconditions.checkNotNull(cronJobModel, "cronJobModel is null");
		Preconditions.checkNotNull(cronJobModel.getStore(), "store model is null");


		final boolean valid = validateCronJobModel(cronJobModel);

		if (!valid)
		{
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		PerformResult result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		final String formatDate = formatDate(new Date(), ORDER_NUMBER_DATE_FORMAT);
		try
		{
			final Set<ReturnRequestModel> returnList = getCustomReturnService()
					.findByStoreAndBySetOfStatusAndBySentStatus(cronJobModel.getStore(), cronJobModel.getStatuses(), false);

			if (CollectionUtils.isEmpty(returnList))
			{
				LOG.info("There is no ReturnRequests to send");
				return result;
			}

			getErpReturnRequestService().send(returnList, cronJobModel.getStore(), formatDate);


		}
		catch (final Exception e)
		{
			result = new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	/**
	 *
	 */
	private boolean validateCronJobModel(final ERPReturnRequestSenderCronJobModel cronJobModel)
	{
		boolean result = true;
		if (cronJobModel.getStore() == null)
		{
			LOG.error("Please assign a BaseStore for the cronJob Model");
			result = false;
		}

		if (CollectionUtils.isEmpty(cronJobModel.getStatuses()))
		{
			LOG.error("Please select one or more ReturnRquestStatus");
			result = false;
		}

		return result;
	}

	protected String formatDate(final Date date, final String format)
	{
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * @return the customReturnService
	 */
	protected CustomReturnService getCustomReturnService()
	{
		return customReturnService;
	}



	/**
	 * @return the erpReturnRequestService
	 */
	public ERPReturnRequestService getErpReturnRequestService()
	{
		return erpReturnRequestService;
	}

}