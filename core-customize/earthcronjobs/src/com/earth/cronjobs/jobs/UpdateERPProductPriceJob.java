/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.cronjobs.jobs;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.StandardDateRange;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.cronjobs.model.ERPProductCronJobModel;
import com.earth.eartherpintegration.beans.ERPItemPrice;
import com.earth.eartherpintegration.exception.EarthERPException;
import com.google.common.base.Preconditions;


/**
 *
 */
public class UpdateERPProductPriceJob extends AbstractERPProductJob
{
	protected static final Logger LOG = LoggerFactory.getLogger(UpdateERPProductPriceJob.class);
	private static final String ONLINE = "ONLINE";
	private static final String ALL = "ALL";
	private Date currentDate;

	@SuppressWarnings("removal")
	@Override
	protected PerformResult performProductJob(final ERPProductCronJobModel cronjob)
	{
		Set<String> codes;
		try
		{
			codes = getERPUpdatedProductPriceCodes(cronjob);
			currentDate = new SimpleDateFormat(DATE_FORMAT_STRING).parse(getModifiedDateTime(cronjob));
			if (CollectionUtils.isEmpty(codes))
			{
				LOG.info("No items available to update price");
				return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}


		}
		catch (final Exception e)
		{
			LOG.info("Cannot Update Prices due to exception {}", e.getMessage());
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}


		for (final String code : codes)
		{
			try
			{
				updatePrice(cronjob, code);
			}
			catch (final Exception ex)
			{
				LOG.info("Cannot Update Prices for code {} due to exception {}", code, ex.getMessage());
			}
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	/**
	 * @throws EarthERPException
	 *
	 */
	private void updatePrice(final ERPProductCronJobModel cronjob, final String code) throws EarthERPException
	{

		final ProductModel product = getProduct(cronjob, code);

		if (Objects.isNull(product))
		{
			LOG.info("No Product with code {} available to update price", code);
			return;
		}

		final List<ERPItemPrice> itemPriceList = getErpIntegrationService().getItemPriceList(cronjob.getStore(), code,
				getModifiedDateTime(cronjob));

		if (CollectionUtils.isEmpty(itemPriceList))
		{
			LOG.warn("No item prices for product {}", code);
			return;
		}


		final Map<UnitModel, List<ERPItemPrice>> map = new LinkedHashMap<>();


		for (final ERPItemPrice erpItemPrice : itemPriceList)
		{

			UnitModel unitForCode = null;
			try
			{
				unitForCode = getUnitService().getUnitForCode(erpItemPrice.getUnitId());
			}
			catch (final UnknownIdentifierException ex)
			{
				LOG.warn("No unit with code {} exist", erpItemPrice.getUnitId());
			}
			if (Objects.isNull(unitForCode))
			{
				LOG.warn("Cannot Update product {} because unit {} is not found", code, erpItemPrice.getUnitId());
				continue;
			}

			final List<ERPItemPrice> itemList = map.getOrDefault(unitForCode, new LinkedList<>());
			itemList.add(erpItemPrice);
			map.put(unitForCode, itemList);
		}


		for (final Entry<UnitModel, List<ERPItemPrice>> entry : map.entrySet())
		{
			updatePriceRow(cronjob, product, entry.getValue(), entry.getKey());
		}

	}


	/**
	 *
	 */
	private void updatePriceRow(final ERPProductCronJobModel cronjob, final ProductModel product,
			final List<ERPItemPrice> itemList, final UnitModel unit)
	{

		try
		{
			final Optional<ProductModel> variantProduct = getVariantProduct(product, unit.getCode(), cronjob);
			if (variantProduct.isEmpty())
			{
				LOG.warn("Cannot update price for product {} with unit {}", product.getCode(), unit.getCode());
				return;
			}

			final Map<String, List<ERPItemPrice>> priceMap = getPriceMap(itemList);
			removeExpiredAndFuturePriceLines(priceMap, currentDate);

			if (MapUtils.isEmpty(priceMap))
			{
				LOG.warn("Cannot update price for product {} with unit {} because priceMap is empty (No Valid Price date exists)",
						product.getCode(), unit.getCode());
				return;
			}
			if (!priceMap.containsKey(ALL))
			{
				LOG.warn("Cannot update price for product {} with unit {} because all price is empty (No Valid Price date exists)",
						product.getCode(), unit.getCode());
				return;

			}

			variantProduct.get().setEurope1Prices(Collections.emptyList());
			getModelService().save(variantProduct.get());
			getModelService().refresh(variantProduct.get());
			createOnlineAndAllPriceRow(cronjob, variantProduct.get(), priceMap, unit);
		}
		catch (final Exception ex)
		{
			LOG.error("Cannot update price for product {} with unit {} ex: {}", product.getCode(), unit.getCode(), ex.getMessage());
		}
	}


	private boolean isActiveDate(final Date validatorDate, final String from, final String to)
	{
		final Date fromDate = getDateFromString(from);
		final Date toDate = getDateFromString(to);
		if (fromDate == null || toDate == null || validatorDate == null)
		{
			return false;
		}

		return (isSameDayFromDate(validatorDate, fromDate) || validatorDate.after(fromDate))
				&& (validatorDate.before(toDate) || isSameDayFromDate(validatorDate, toDate));
	}


	private boolean isSameDayFromDate(final Date d1, final Date d2)
	{
		return d1.getYear() == d2.getYear() && d1.getMonth() == d2.getMonth() && d1.getDay() == d2.getDay(); // NOSONAR
	}

	/**
	 *
	 */
	private void removeExpiredAndFuturePriceLines(final Map<String, List<ERPItemPrice>> priceMap, final Date currentDate)
	{
		if (MapUtils.isEmpty(priceMap))
		{
			return;
		}

		for (final String key : priceMap.keySet())
		{
			priceMap.put(key, priceMap.get(key).stream().filter(e -> isActiveDate(currentDate, e.getFromDate(), e.getToDate()))
					.collect(Collectors.toList()));
		}
	}


	/**
	 *
	 */
	private void createOnlineAndAllPriceRow(final ERPProductCronJobModel cronjob, final ProductModel productModel,
			final Map<String, List<ERPItemPrice>> priceMap, final UnitModel unit)
	{
		final List<ERPItemPrice> allList = priceMap.get(ALL);
		final double allPrice = allList.size() > 0 ? allList.get(allList.size() - 1).getAmount() : 0.0d;

		if (!priceMap.containsKey(ONLINE))
		{
			LOG.info("no online price for product {} with unit {}", productModel.getCode(), unit.getCode());
			allList.stream().forEach(e -> createPriceRow(e, cronjob, productModel, unit));
			return;
		}

		final List<ERPItemPrice> onlinePrices = priceMap.get(ONLINE);
		for (final ERPItemPrice onlinePrice : onlinePrices)
		{
			final List<ERPItemPrice> collect = allList.stream().filter(price -> price.getFromDate().equals(onlinePrice.getFromDate())
					&& price.getToDate().equals(onlinePrice.getToDate())).collect(Collectors.toList());

			final double allAmount = CollectionUtils.isEmpty(collect) ? allPrice : collect.get(0).getAmount();
			final PriceRowModel priceRow = createPriceRow(onlinePrice, cronjob, productModel, unit);

			if (onlinePrice.getAmount() < allAmount)
			{
				priceRow.setOriginalPrice(allAmount);
				priceRow.setPrice(onlinePrice.getAmount());
				getModelService().save(priceRow);
				getModelService().refresh(priceRow);
			}

			if (!CollectionUtils.isEmpty(collect))
			{
				allList.remove(collect.get(0));
			}
		}
		allList.stream().forEach(e -> createPriceRow(e, cronjob, productModel, unit));
	}


	/**
	 *
	 */
	private PriceRowModel createPriceRow(final ERPItemPrice allPrice, final ERPProductCronJobModel cronjob,
			final ProductModel productModel, final UnitModel unit)
	{
		final PriceRowModel priceRowModel = getModelService().create(PriceRowModel.class);
		priceRowModel.setPrice(allPrice.getAmount());

		final StandardDateRange standardDateRange = new StandardDateRange(
				DateUtils.addDays(formatDateFromString(allPrice.getFromDate(), DATE_FORMAT_START_OF_DAY), -1),
				formatDateFromString(allPrice.getToDate(), DATE_FORMAT_END_OF_DAY));

		priceRowModel.setStartDate(standardDateRange.getStart());
		priceRowModel.setEndDate(standardDateRange.getEnd());

		priceRowModel.setStartTime(standardDateRange.getStart());
		priceRowModel.setEndTime(standardDateRange.getEnd());

		priceRowModel.setDateRange(standardDateRange);

		priceRowModel.setCurrency(cronjob.getStore().getDefaultCurrency());
		priceRowModel.setProduct(productModel);
		priceRowModel.setUnitFactor(KG_UNIT_SYMBOL.equalsIgnoreCase(unit.getCode()) ? 1000 : 1);
		getModelService().save(priceRowModel);
		return priceRowModel;

	}


	/**
	 *
	 */
	private void createPriceRowForProduct(final ERPProductCronJobModel cronjob, final ProductModel product,
			final Map<String, ERPItemPrice> priceMap, final UnitModel unit)
	{
		LOG.info("Create a new price row for product {}", product.getCode());
		Preconditions.checkArgument(MapUtils.isNotEmpty(priceMap),
				String.format("prices are empty for product %s", product.getCode()));

		final PriceRowModel priceRowModel = getModelService().create(PriceRowModel.class);
		priceRowModel.setUnit(unit);
		final ERPItemPrice onlinePrice = priceMap.getOrDefault(ONLINE, null);
		final ERPItemPrice allPrice = priceMap.getOrDefault(ALL, null);


		if (Objects.isNull(onlinePrice) && Objects.nonNull(allPrice))
		{
			priceRowModel.setPrice(allPrice.getAmount());
			priceRowModel.setStartDate(getDateFromString(allPrice.getFromDate()));
			priceRowModel.setEndDate(getDateFromString(allPrice.getToDate()));
			priceRowModel.setDateRange(
					new StandardDateRange(getDateFromString(allPrice.getFromDate()), getDateFromString(allPrice.getToDate())));


		}
		else if ((Objects.nonNull(onlinePrice) && Objects.nonNull(allPrice) && onlinePrice.getAmount() >= allPrice.getAmount())
				|| (Objects.nonNull(onlinePrice) && Objects.isNull(allPrice)))
		{
			priceRowModel.setPrice(onlinePrice.getAmount());
			priceRowModel.setStartDate(getDateFromString(onlinePrice.getFromDate()));
			priceRowModel.setEndDate(getDateFromString(onlinePrice.getToDate()));
			priceRowModel.setDateRange(
					new StandardDateRange(getDateFromString(onlinePrice.getFromDate()), getDateFromString(onlinePrice.getToDate())));

		}
		else if (Objects.nonNull(onlinePrice) && onlinePrice.getAmount() < allPrice.getAmount())
		{
			priceRowModel.setOriginalPrice(allPrice.getAmount());
			priceRowModel.setPrice(onlinePrice.getAmount());
			priceRowModel.setStartDate(getDateFromString(onlinePrice.getFromDate()));
			priceRowModel.setEndDate(getDateFromString(onlinePrice.getToDate()));
			priceRowModel.setDateRange(
					new StandardDateRange(getDateFromString(onlinePrice.getFromDate()), getDateFromString(onlinePrice.getToDate())));

		}



		priceRowModel.setCurrency(cronjob.getStore().getDefaultCurrency());
		priceRowModel.setProduct(product);
		priceRowModel.setUnitFactor(KG_UNIT_SYMBOL.equalsIgnoreCase(unit.getCode()) ? 1000 : 1);
		getModelService().save(priceRowModel);

	}


	/**
	 *
	 */
	private Map<String, List<ERPItemPrice>> getPriceMap(final List<ERPItemPrice> itemList)
	{
		return itemList.stream().collect(Collectors.groupingBy(ERPItemPrice::getPriceGroup));
	}

	/**
	 *
	 */
	private void updatePriceRow(final ERPProductCronJobModel cronJobModel, final ProductModel product,
			final ERPItemPrice erpItemPrice, final UnitModel unitForCode)
	{
		try
		{
			final Optional<ProductModel> variantProduct = getVariantProduct(product, erpItemPrice.getUnitId(), cronJobModel);
			if (variantProduct.isEmpty())
			{
				LOG.warn("Cannot update price for product {} with unit {}", product.getCode(), erpItemPrice.getUnitId());
				return;
			}
			variantProduct.get().setEurope1Prices(Collections.emptyList());

			getModelService().save(variantProduct.get());
			getModelService().refresh(variantProduct.get());
			createPriceRowForProduct(cronJobModel, variantProduct.get(), erpItemPrice, unitForCode);
		}
		catch (final Exception ex)
		{
			LOG.error("Cannot update price for product {} with unit {} ex: {}", product.getCode(), erpItemPrice.getUnitId(),
					ex.getMessage());

		}
	}



	/**
	 *
	 */
	private void createPriceRowForProduct(final ERPProductCronJobModel cronJobModel, final ProductModel product,
			final ERPItemPrice erpItemPrice, final UnitModel unitForCode)
	{
		LOG.info("Create a new price row for product {}", product.getCode());
		final PriceRowModel priceRowModel = getModelService().create(PriceRowModel.class);
		priceRowModel.setUnit(unitForCode);
		priceRowModel.setPrice(erpItemPrice.getAmount());
		priceRowModel.setCurrency(cronJobModel.getStore().getDefaultCurrency());
		priceRowModel.setStartDate(getDateFromString(erpItemPrice.getFromDate()));
		priceRowModel.setEndDate(getDateFromString(erpItemPrice.getToDate()));
		priceRowModel.setProduct(product);
		priceRowModel.setUnitFactor(KG_UNIT_SYMBOL.equalsIgnoreCase(unitForCode.getCode()) ? 1000 : 1);
		getModelService().save(priceRowModel);
	}

}
