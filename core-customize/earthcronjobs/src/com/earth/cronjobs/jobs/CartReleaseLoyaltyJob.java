package com.earth.cronjobs.jobs;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.util.List;

import javax.annotation.Resource;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.cronjobs.model.CartReleaseLoyaltyCronJobModel;
import com.earth.earthfacades.facade.CustomAcceleratorCheckoutFacade;
import com.earth.earthloyaltyprogramprovider.service.CartReleaseLoyaltyService;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.google.common.base.Preconditions;


public class CartReleaseLoyaltyJob extends AbstractJobPerformable<CartReleaseLoyaltyCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CartReleaseLoyaltyJob.class);
	@Resource(name = "cartReleaseLoyaltyService")
	private CartReleaseLoyaltyService cartReleaseLoyaltyService;

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	private ModelService modelService;

	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	/**
	 * @return the checkoutFacade
	 */
	protected CustomAcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public PerformResult perform(final CartReleaseLoyaltyCronJobModel arg0)
	{

		Preconditions.checkNotNull(arg0, "CartReleaseLoyaltyCronJobModel is null");

		PerformResult result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

		try
		{
			setLoyaltyPaymentMode(arg0);
		}
		catch (final Exception e)
		{
			result = new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * @return the cartReleaseLoyaltyService
	 */
	protected CartReleaseLoyaltyService getCartReleaseLoyaltyService()
	{
		return cartReleaseLoyaltyService;
	}

	public void setLoyaltyPaymentMode(final CartReleaseLoyaltyCronJobModel arg0) throws ParseException
	{
		final List<CartModel> cartList = cartReleaseLoyaltyService
				.getAllCartsAfterTimeAmount(arg0.getSite().getCartLoyaltyReleaseMinTime());

		if (Collections.isEmpty(cartList))
		{

			return;
		}

		for (final CartModel cart : cartList)
		{

			getCheckoutFacade().setLoyaltyPaymentMode(cart, "REDEEM_NONE", cart.getLoyaltyAmount());
		}
	}


}