package com.earth.cronjobs.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class EarthcronjobsConstants extends GeneratedEarthcronjobsConstants
{
	public static final String EXTENSIONNAME = "earthcronjobs";
	
	private EarthcronjobsConstants()
	{
		//empty
	}
	
	
}
