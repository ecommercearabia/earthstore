/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.customer.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.event.ChangeUIDEvent;
import de.hybris.platform.commerceservices.event.UpdatedProfileEvent;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.NoCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthpayment.customer.service.CustomCustomerAccountService;
import com.earth.earthpayment.customer.service.dao.CustomCustomerAccountDao;
import com.earth.earthpayment.enums.NoCardType;


/**
 * @author mnasro
 *
 *         The Class DefaultCustomCustomerAccountService.
 */
public class DefaultCustomCustomerAccountService extends DefaultCustomerAccountService implements CustomCustomerAccountService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomCustomerAccountService.class);

	private static final String CUSTOMER_VALIDATION_NONULL = "Customer model cannot be null";

	/** The sefam customer account dao. */
	@Resource(name = "customerAccountDao")
	private CustomCustomerAccountDao customerAccountDao;

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	/**
	 * Creates the payment subscription.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param billingInfo
	 *           the billing info
	 * @param titleCode
	 *           the title code
	 * @param paymentProvider
	 *           the payment provider
	 * @param saveInAccount
	 *           the save in account
	 * @param noCardTypeCode
	 *           the no card type code
	 * @return the no card payment info model
	 */
	@Override
	public Optional<NoCardPaymentInfoModel> createPaymentSubscription(final CustomerModel customerModel,
			final BillingInfo billingInfo, final String titleCode, final String paymentProvider, final boolean saveInAccount,
			final String noCardTypeCode)
	{
		validateParameterNotNull(customerModel, "Customer cannot be null");
		validateParameterNotNull(titleCode, "titleCode cannot be null");
		validateParameterNotNull(billingInfo, "billingInfo cannot be null");
		validateParameterNotNull(paymentProvider, "PaymentProvider cannot be null");
		validateParameterNotNull(noCardTypeCode, "noCardTypeCode cannot be null");
		final CurrencyModel currencyModel = getCurrency(customerModel);
		validateParameterNotNull(currencyModel, "Customer session currency cannot be null");

		final AddressModel billingAddress = getModelService().create(AddressModel.class);
		if (StringUtils.isNotBlank(titleCode))
		{
			final TitleModel title = new TitleModel();
			title.setCode(titleCode);
			billingAddress.setTitle(getFlexibleSearchService().getModelByExample(title));
		}
		billingAddress.setFirstname(billingInfo.getFirstName());
		billingAddress.setLastname(billingInfo.getLastName());
		billingAddress.setLine1(billingInfo.getStreet1());
		billingAddress.setLine2(billingInfo.getStreet2());
		billingAddress.setTown(billingInfo.getCity());
		billingAddress.setPostalcode(billingInfo.getPostalCode());
		billingAddress.setCountry(getCommonI18NService().getCountry(billingInfo.getCountry()));
		if (billingInfo.getRegion() != null)
		{
			billingAddress.setRegion(getCommonI18NService().getRegion(billingAddress.getCountry(), billingInfo.getRegion()));
		}
		billingAddress.setPhone1(billingInfo.getPhoneNumber());
		final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
		billingAddress.setEmail(email);
		billingAddress.setOwner(customerModel);
		getModelService().save(billingAddress);

		try
		{
			final NoCardPaymentInfoModel noCardPaymentInfoModel = getModelService().create(NoCardPaymentInfoModel.class);
			noCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
			noCardPaymentInfoModel.setUser(customerModel);
			noCardPaymentInfoModel.setBillingAddress(billingAddress);
			noCardPaymentInfoModel.setType(NoCardType.valueOf(noCardTypeCode));

			noCardPaymentInfoModel.setSaved(saveInAccount);

			billingAddress.setOwner(noCardPaymentInfoModel);
			getModelService().save(noCardPaymentInfoModel);
			getModelService().refresh(customerModel);

			addPaymentInfo(customerModel, noCardPaymentInfoModel);

			return Optional.ofNullable(noCardPaymentInfoModel);
		}
		catch (final AdapterException ae)
		{
			LOG.error("Failed to create subscription for customer. Customer PK: " + customerModel.getPk() + " Exception: "
					+ ae.getClass().getName());

			return Optional.empty();
		}

	}

	/**
	 * Gets the no card payment infos.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param saved
	 *           the saved
	 * @return the no card payment infos
	 */
	@Override
	public Optional<List<NoCardPaymentInfoModel>> getNoCardPaymentInfos(final CustomerModel customerModel, final boolean saved)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfosByCustomer(customerModel, saved));
	}

	@Override
	protected CustomCustomerAccountDao getCustomerAccountDao()
	{
		return customerAccountDao;
	}


	/**
	 * Gets the no card payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the no card payment info for code
	 */
	@Override
	public Optional<NoCardPaymentInfoModel> getNoCardPaymentInfoForCode(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		validateParameterNotNull(code, "code cannot be null");
		return Optional.ofNullable(getCustomerAccountDao().findNoCardPaymentInfoByCustomer(customerModel, code));
	}

	/**
	 * Gets the payment info for code.
	 *
	 * @param customerModel
	 *           the customer model
	 * @param code
	 *           the code
	 * @return the payment info for code
	 */
	@Override
	public Optional<PaymentInfoModel> getPaymentInfoForCode(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, CUSTOMER_VALIDATION_NONULL);
		validateParameterNotNull(code, "code cannot be null");
		return Optional.ofNullable(getCustomerAccountDao().findPaymentInfoByCustomer(customerModel, code));
	}




	@Override
	public void changeUid(final String newUid, final String currentPassword)
			throws DuplicateUidException, PasswordMismatchException
	{
		Assert.hasText(newUid, "The field [newEmail] cannot be empty");
		Assert.hasText(currentPassword, "The field [currentPassword] cannot be empty");

		final String newUidLower = newUid.toLowerCase();
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final ChangeUIDEvent event = new ChangeUIDEvent(currentUser.getOriginalUid(), newUid);

		try
		{
			if (currentUser.isInvolvedInLoyaltyProgram() && loyaltyProgramContext.isLoyaltyEnabledByCurrentBaseStore(currentUser))
			{
				loyaltyProgramContext.updateEmail(currentUser, baseStoreService.getCurrentBaseStore(), newUidLower);
			}
		}
		catch (final EarthLoyaltyException e)
		{
			throw new IllegalArgumentException("Loyalty error has occured when trying to update email, message is: " + e.getMessage()
					+ ", please contact customer support.");
		}
		currentUser.setOriginalUid(newUid);
		// check uniqueness only if the uids are case insensitive different
		if (!currentUser.getUid().equalsIgnoreCase(newUid))
		{
			checkUidUniqueness(newUidLower);
		}
		adjustPassword(currentUser, newUidLower, currentPassword);
		getEventService().publishEvent(initializeEvent(event, currentUser));

	}

	@Override
	public void updateProfile(final CustomerModel customerModel, final String titleCode, final String name, final String login)
			throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final String phoneNum = customerModel.getMobileNumber();
		currentUser.setUid(login);
		currentUser.setName(name);
		if (StringUtils.isNotBlank(titleCode))
		{
			currentUser.setTitle(getUserService().getTitleForCode(titleCode));
		}
		else
		{
			currentUser.setTitle(null);
		}

		try
		{
			if (currentUser.isInvolvedInLoyaltyProgram() && loyaltyProgramContext.isLoyaltyEnabledByCurrentBaseStore(currentUser))
			{
				loyaltyProgramContext.updateMobileNumber(currentUser, baseStoreService.getCurrentBaseStore(), phoneNum);
			}
		}
		catch (final EarthLoyaltyException e)
		{
			throw new IllegalArgumentException("Loyalty error has occured when trying to update phone number, message is: "
					+ e.getMessage() + ", please contact customer support.");
		}

		populateCustomer(currentUser, customerModel);
		internalSaveCustomer(currentUser);
		getEventService().publishEvent(initializeEvent(new UpdatedProfileEvent(), customerModel));
	}

	private void populateCustomer(final CustomerModel currentUser, final CustomerModel customerModel)
	{

		currentUser.setMobileNumber(customerModel.getMobileNumber());
		currentUser.setNationality(customerModel.getNationality());
		currentUser.setNationalityID(customerModel.getNationalityID());
		currentUser.setMaritalStatus(customerModel.getMaritalStatus());
		currentUser.setBirthOfDate(customerModel.getBirthOfDate());
	}

}
