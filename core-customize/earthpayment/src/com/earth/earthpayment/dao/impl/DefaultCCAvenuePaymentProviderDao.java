/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.dao.impl;

import com.earth.earthpayment.dao.PaymentProviderDao;
import com.earth.earthpayment.model.CCAvenuePaymentProviderModel;


/**
 * @author mnasro
 *
 *         The Class DefaultCCAvenuePaymentProviderDao.
 */
public class DefaultCCAvenuePaymentProviderDao extends DefaultPaymentProviderDao implements PaymentProviderDao
{

	/**
	 * Instantiates a new default CC avenue payment provider dao.
	 */
	public DefaultCCAvenuePaymentProviderDao()
	{
		super(CCAvenuePaymentProviderModel._TYPECODE);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	@Override
	protected String getModelName()
	{
		return CCAvenuePaymentProviderModel._TYPECODE;
	}

}
