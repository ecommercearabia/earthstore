/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.dao.records.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;

import com.earth.earthpayment.dao.records.PaymentTransactionRecordsDao;
import com.earth.earthpayment.model.PaymentTransactionRecordsModel;
import com.google.common.base.Preconditions;



/**
 *
 */
public class DefaultPaymentTransactionRecordsDao implements PaymentTransactionRecordsDao
{

	private static final String BASE_QUERY = "SELECT {" + PaymentTransactionRecordsModel.PK + "} from {"
			+ PaymentTransactionRecordsModel._TYPECODE + "}";

	private static final String WHERE_ORDER_CODE = " WHERE {" + PaymentTransactionRecordsModel.ORDERCODE + "}=?"
			+ PaymentTransactionRecordsModel.ORDERCODE;

	private static final String AND_PAYMENT_ID = " AND {" + PaymentTransactionRecordsModel.PAYMENTID + "}=?"
			+ PaymentTransactionRecordsModel.PAYMENTID;

	private static final String AND_CART_CODE = " AND {" + PaymentTransactionRecordsModel.CARTCODE + "}=?"
			+ PaymentTransactionRecordsModel.CARTCODE;

	private static final String AND_RESULT_CODE = " AND {" + PaymentTransactionRecordsModel.RESULTCODE + "}=?"
			+ PaymentTransactionRecordsModel.RESULTCODE;

	private static final String AND_USER_EMAIL = " AND {" + PaymentTransactionRecordsModel.USEREMAIL + "}=?"
			+ PaymentTransactionRecordsModel.USEREMAIL;

	private static final String AND_PROVIDER = " AND {" + PaymentTransactionRecordsModel.PROVIDER + "}=?"
			+ PaymentTransactionRecordsModel.PROVIDER;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public void createPaymentRecord(final PaymentTransactionRecordsModel model)
	{
		Preconditions.checkArgument(model != null, "Payment Record Model is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getOrderCode()), "Order Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getCartCode()), "Cart Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getResultCode()), "Result Code is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getUserEmail()), "Use Email is null");
		modelService.save(model);
	}

	@Override
	public List<PaymentTransactionRecordsModel> getAllPaymentRecords()
	{
		final String query = BASE_QUERY;
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);

		return flexibleSearchService.<PaymentTransactionRecordsModel> search(searchQuery).getResult();
	}

	@Override
	public List<PaymentTransactionRecordsModel> getAllPaymentRecordsForQuery(final PaymentTransactionRecordsModel model)
	{
		Preconditions.checkArgument(model != null, "Payment Record Model is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(model.getOrderCode()), "Order Code is null");

		final Map<String, Object> paramMap = new HashedMap();
		final String query = buildQuery(model, paramMap);

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameters(paramMap);

		return flexibleSearchService.<PaymentTransactionRecordsModel> search(searchQuery).getResult();
	}

	/**
	 *
	 */
	private String buildQuery(final PaymentTransactionRecordsModel model, final Map<String, Object> paramMap)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append(BASE_QUERY);
		builder.append(WHERE_ORDER_CODE);
		paramMap.put(PaymentTransactionRecordsModel.ORDERCODE, model.getOrderCode());

		if (StringUtils.isNotBlank(model.getCartCode()))
		{
			builder.append(AND_CART_CODE);
			paramMap.put(PaymentTransactionRecordsModel.CARTCODE, model.getCartCode());
		}
		if (StringUtils.isNotBlank(model.getPaymentId()))
		{
			builder.append(AND_PAYMENT_ID);
			paramMap.put(PaymentTransactionRecordsModel.PAYMENTID, model.getPaymentId());
		}
		if (StringUtils.isNotBlank(model.getResultCode()))
		{
			builder.append(AND_RESULT_CODE);
			paramMap.put(PaymentTransactionRecordsModel.RESULTCODE, model.getResultCode());
		}
		if (StringUtils.isNotBlank(model.getUserEmail()))
		{
			builder.append(AND_USER_EMAIL);
			paramMap.put(PaymentTransactionRecordsModel.USEREMAIL, model.getUserEmail());
		}
		if (StringUtils.isNotBlank(model.getProvider()))
		{
			builder.append(AND_PROVIDER);
			paramMap.put(PaymentTransactionRecordsModel.PROVIDER, model.getProvider());
		}
		return builder.toString();
	}

}
