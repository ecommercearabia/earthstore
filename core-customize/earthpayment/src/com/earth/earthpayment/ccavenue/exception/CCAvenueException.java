/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.ccavenue.exception;

import com.earth.earthpayment.exception.PaymentException;
import com.earth.earthpayment.exception.type.PaymentExceptionType;

/**
 *
 */
public class CCAvenueException extends PaymentException
{
	

	public CCAvenueException(final PaymentExceptionType type)
	{
		super(type.getMessage(), type);
	}

	public CCAvenueException(final String message, final PaymentExceptionType type)
	{
		super(message, type);
	}



}
