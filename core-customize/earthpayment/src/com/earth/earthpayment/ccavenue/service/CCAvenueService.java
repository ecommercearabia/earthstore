/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.ccavenue.service;

import java.util.Map;
import java.util.Optional;

import com.earth.earthpayment.ccavenue.entry.RequestData;
import com.earth.earthpayment.exception.PaymentException;


/**
 * The Interface CCAvenueService.
 *
 * @author mnasro
 * @author alshati
 * @author abu-muhasien
 */
public interface CCAvenueService
{


	/**
	 * Gets the script src.
	 *
	 * @param requestData
	 *           the request data
	 * @return the script src
	 */
	public Optional<String> getScriptSrc(final RequestData requestData);

	/**
	 * Gets the response data.
	 *
	 * @param source
	 *           the source
	 * @param workingKey
	 *           the working key
	 * @return the response data
	 */
	public Optional<Map<String, Object>> getResponseData(String source, final String workingKey);

	/**
	 * Gets the response data.
	 *
	 * @param source
	 *           the source
	 * @param workingKey
	 *           the working key
	 * @return the response data
	 */
	public Optional<Map<String, Object>> getOrderStatusData(final String workingKey, final String accessCode,
			final String referanceNo, final String orderNo) throws PaymentException;


	public Optional<Map<String, Object>> getOrderCancelData(String workingKey, String accessCode, String referanceNo,
			String amount) throws PaymentException;

	public Optional<Map<String, Object>> getOrderRefundData(String workingKey, String accessCode, String referanceNo,
			String refundAmount, String refundRefNo) throws PaymentException;

	public Optional<Map<String, Object>> getOrderConfiemData(String workingKey, String accessCode, String referanceNo,
			String amount) throws PaymentException;


}