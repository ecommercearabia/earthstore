package com.earth.earthpayment.mpgs.service.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.earth.earthpayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.CreateCheckoutSessionResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.Initiate3DAuthenticationResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.MpgsResponse.MpgsResultEnum;
import com.earth.earthpayment.mpgs.beans.incoming.OrderStatusResponse;
import com.earth.earthpayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.earth.earthpayment.mpgs.beans.incoming.PayOrderResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Authentication;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.BrowserDetails;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Device;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Order;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SessionBean;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.earth.earthpayment.mpgs.beans.outgoing.AuthenticatePayerRequestBean;
import com.earth.earthpayment.mpgs.beans.outgoing.CaptureOrderBean;
import com.earth.earthpayment.mpgs.beans.outgoing.CreateCheckoutSessionBean;
import com.earth.earthpayment.mpgs.beans.outgoing.Initiate3DAuthenticationBean;
import com.earth.earthpayment.mpgs.beans.outgoing.PayOrderRequestBean;
import com.earth.earthpayment.mpgs.beans.outgoing.RefundTransactionBean;
import com.earth.earthpayment.mpgs.beans.outgoing.VoidTransactionBean;
import com.earth.earthpayment.mpgs.beans.serviceparams.PayServiceParams;
import com.earth.earthpayment.mpgs.beans.serviceparams._3DSecureParams;
import com.earth.earthpayment.mpgs.constants.MpgsConstants;
import com.earth.earthpayment.mpgs.exception.MpgsException;
import com.earth.earthpayment.mpgs.exception.type.MpgsExceptionType;
import com.earth.earthpayment.mpgs.service.MpgsService;
import com.earth.earthpayment.mpgs.utils.WebServiceApiUtil;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class DefaultMpgsServiceImpl implements MpgsService
{

	/**
	 *
	 */
	private static final String TRANSACTION_STRING = "/transaction/";

	private static final String CANCELLED = "CANCELLED";

	private static final String ORDER_STRING = "/order/";

	private static final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

	private static final Gson GSON = _builder.setPrettyPrinting().create();

	static
	{
		_builder.serializeNulls();
	}

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public Optional<CreateCheckoutSessionResponseBean> createCheckoutSession(
			final CreateCheckoutSessionBean createCheckoutSessionBean, final MpgsConstants constants) throws MpgsException
	{
		createCheckoutSessionBean.getInteraction().setOperation("PURCHASE");
		final String url = constants.getBaseURL() + "/session";

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(createCheckoutSessionBean).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPost(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final CreateCheckoutSessionResponseBean result = GSON.fromJson(response.getBody(), CreateCheckoutSessionResponseBean.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(result.getResult()))
		{
			throw MpgsException.getExceptionFromCode(result.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(result);

	}

	@Override
	public Optional<OuterTransactionResponse> voidOrder(final String orderId, final MpgsConstants constants) throws MpgsException
	{

		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, "Order Doesn't Exist", null);
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			return Optional.empty();
		}


		final VoidTransactionBean voidData = new VoidTransactionBean();
		voidData.setTransactionId("1");

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/voidtransaction_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(voidData).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);
	}

	@Override
	public Optional<OuterTransactionResponse> authorizeOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			return Optional.empty();
		}

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/captureOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final CaptureOrderBean captureBody = new CaptureOrderBean();
		captureBody.setTransactionAmount(amount);
		captureBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(captureBody).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);

	}

	@Override
	public Optional<OuterTransactionResponse> captureOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		if (CANCELLED.equals(order.getStatus()))
		{
			return Optional.empty();
		}

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/captureOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final CaptureOrderBean captureBody = new CaptureOrderBean();
		captureBody.setTransactionAmount(amount);
		captureBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(captureBody).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);
		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);

	}


	private MultiValueMap<String, String> createAuthHeader(final MpgsConstants constants)
	{
		final String header = WebServiceApiUtil.getBasicAuthHeader(constants.getAuthUsername(), constants.getPassword());
		final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Authorization", header);
		return headers;
	}


	@Override
	public Optional<OrderStatusResponse> getOrder(final String orderId, final MpgsConstants constants) throws MpgsException
	{
		final String url = constants.getBaseURL() + ORDER_STRING + orderId;
		final MultiValueMap<String, String> headers = createAuthHeader(constants);
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpGet(url, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			return Optional.empty();
		}
		final OrderStatusResponse orderStatusResponse = GSON.fromJson(response.getBody(), OrderStatusResponse.class);

		if (!MpgsResultEnum.SUCCESS.getResult().equals(orderStatusResponse.getResult()))
		{
			throw MpgsException.getExceptionFromCode(orderStatusResponse.getError().getCause(), response.getBody(), null);
		}
		return Optional.ofNullable(orderStatusResponse);

	}

	@Override
	public Optional<OuterTransactionResponse> getTransaction(final String orderId, final String transactonId,
			final MpgsConstants constants) throws MpgsException
	{
		final String url = constants.getBaseURL() + ORDER_STRING + orderId + TRANSACTION_STRING + transactonId;
		final MultiValueMap<String, String> headers = createAuthHeader(constants);
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpGet(url, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), url);
		}
		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);

		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}
		return Optional.ofNullable(transaction);

	}

	@Override
	public Optional<OuterTransactionResponse> refundOrder(final String orderId, final double amount, final String currency,
			final MpgsConstants constants) throws MpgsException
	{
		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(orderId, constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();
		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + "/transaction/refundOrder_"
				+ order.getTransactions().get(0).getTransaction().getId();

		final RefundTransactionBean refundBody = new RefundTransactionBean();
		refundBody.setTransactionAmount(amount);
		refundBody.setTransactionCurrency(currency);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(refundBody).toString();
		ResponseEntity<String> response = null;

		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final OuterTransactionResponse transaction = GSON.fromJson(response.getBody(), OuterTransactionResponse.class);

		if (!MpgsResultEnum.SUCCESS.getResult().equals(transaction.getResult()))
		{
			throw MpgsException.getExceptionFromCode(transaction.getError().getCause(), response.getBody(), null);
		}

		return Optional.ofNullable(transaction);


	}

	@Override
	public Optional<OuterTransactionResponse> getLastTransaction(final String orderId, final MpgsConstants constants)
			throws MpgsException
	{
		final Optional<OrderStatusResponse> order = this.getOrder(orderId, constants);
		if (order.isEmpty())
		{
			return Optional.empty();
		}
		final List<OuterTransactionResponse> transactions = order.get().getTransactions();
		return Optional.ofNullable(transactions.get(transactions.size() - 1));
	}

	@Override
	public Optional<List<OuterTransactionResponse>> getTransactions(final String orderId, final MpgsConstants constants)
			throws MpgsException
	{
		final Optional<OrderStatusResponse> order = this.getOrder(orderId, constants);
		if (order.isEmpty())
		{
			return Optional.empty();
		}
		return Optional.ofNullable(order.get().getTransactions());
	}

	@Override
	public Optional<OuterTransactionResponse> getLastTransaction(final OrderStatusResponse order) throws MpgsException
	{
		final List<OuterTransactionResponse> transactions = order.getTransactions();
		if (transactions == null || transactions.size() == 0)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(transactions.get(transactions.size() - 1));
	}

	@Override
	public Optional<Initiate3DAuthenticationResponseBean> initiate3DSecureAuthentication(final _3DSecureParams params,
			final MpgsConstants constants) throws MpgsException
	{
		// no need to get the order since the order has not been created yet on MPGS
		Preconditions.checkArgument(params != null, "Initiate 3DSecure parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getCurrency()), "Initiate 3DSecure currency parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getSessionId()),
				"Initiate 3DSecure session id parameter is empty");
		Preconditions.checkArgument(constants != null, "Initiate 3DSecure Mpgs Constants parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getAuthUsername()),
				"Initiate 3DSecure Mpgs Constants Auth username parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getPassword()),
				"Initiate 3DSecure Mpgs Constants Auth password parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getBaseURL()),
				"Initiate 3DSecure Mpgs Constants base URL parameter is empty");

		final String url = constants.getBaseURL() + ORDER_STRING + params.getOrderId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		final Initiate3DAuthenticationBean request = populateInitiate3DSecureRequest(params);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final Initiate3DAuthenticationResponseBean transaction = GSON.fromJson(response.getBody(),
				Initiate3DAuthenticationResponseBean.class);

		return Optional.ofNullable(transaction);
	}

	@Override
	public Optional<AuthenticatePayerResponseBean> authenticate3DSecurePayer(final _3DSecureParams params,
			final MpgsConstants constants) throws MpgsException
	{
		// no need to get the order since the order has not been created yet on MPGS
		Preconditions.checkArgument(params != null, "Initiate 3DSecure parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getOrderId()), "Initiate 3DSecure order ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getCurrency()), "Initiate 3DSecure currency parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getUniqueTransactionId()),
				"Initiate 3DSecure transaction ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getUniqueTransactionId()),
				"Initiate 3DSecure transaction ID parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(params.getSessionId()),
				"Initiate 3DSecure session id parameter is empty");
		Preconditions.checkArgument(constants != null, "Initiate 3DSecure Mpgs Constants parameters are empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getAuthUsername()),
				"Initiate 3DSecure Mpgs Constants Auth username parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getPassword()),
				"Initiate 3DSecure Mpgs Constants Auth password parameter is empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(constants.getBaseURL()),
				"Initiate 3DSecure Mpgs Constants base URL parameter is empty");

		final String url = constants.getBaseURL() + ORDER_STRING + params.getOrderId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		final AuthenticatePayerRequestBean request = populate3DSecureAuthRequest(params);

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (!response.getStatusCode().is2xxSuccessful())
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.getBody(), requestBody);
		}

		final AuthenticatePayerResponseBean transaction = GSON.fromJson(response.getBody(), AuthenticatePayerResponseBean.class);

		return Optional.ofNullable(transaction);
	}

	/**
	 *
	 */
	private Initiate3DAuthenticationBean populateInitiate3DSecureRequest(final _3DSecureParams params)
	{
		// populate authentication param
		final Authentication authentication = new Authentication();
		authentication.setAcceptVersions("3DS1,3DS2");
		authentication.setChannel("PAYER_BROWSER");
		authentication.setPurpose("PAYMENT_TRANSACTION");

		// populating order param
		final Order order = new Order();
		order.setCurrency(params.getCurrency());

		// populate session param
		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final Initiate3DAuthenticationBean request = new Initiate3DAuthenticationBean();

		request.setAuthentication(authentication);
		request.setOrder(order);
		request.setSession(session);

		return request;
	}

	private AuthenticatePayerRequestBean populate3DSecureAuthRequest(final _3DSecureParams params)
	{
		// populate authentication param
		final Authentication authentication = new Authentication();
		authentication.setRedirectResponseUrl(params.getRedirectUrl());

		// populating order param
		final Order order = new Order();
		order.setCurrency(params.getCurrency());
		order.setAmount(String.valueOf(params.getAmount()));

		// populate session param
		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final Device device = new Device();
		device.setBrowser("PHONE");
		device.setBrowserDetails(new BrowserDetails());

		final AuthenticatePayerRequestBean request = new AuthenticatePayerRequestBean();
		request.setOrder(order);
		request.setSession(session);
		request.setDevice(device);
		request.setAuthentication(authentication);
		return request;
	}

	@Override
	public Optional<PayOrderResponseBean> pay(final PayServiceParams params, final MpgsConstants constants) throws MpgsException
	{
		// Get order first
		final Optional<OrderStatusResponse> orderOpt = getOrder(params.getOrderId(), constants);
		if (orderOpt.isEmpty())
		{
			return Optional.empty();
		}

		final OrderStatusResponse order = orderOpt.get();

		// Prepare Pay Request
		final PayOrderRequestBean request = createPayOrderRequest(order, params);

		final String url = constants.getBaseURL() + ORDER_STRING + order.getId() + TRANSACTION_STRING
				+ params.getUniqueTransactionId();

		final MultiValueMap<String, String> headers = createAuthHeader(constants);

		final String requestBody = GSON.toJson(request).toString();
		ResponseEntity<String> response = null;
		response = WebServiceApiUtil.httpPut(url, requestBody, headers, String.class);
		if (response != null && (!response.getStatusCode().is2xxSuccessful() || !response.hasBody()))
		{
			throw new MpgsException(MpgsExceptionType.UNKNOWN, response.hasBody() ? response.getBody() : "null", requestBody);
		}

		final PayOrderResponseBean payResponse = GSON.fromJson(response.getBody(), PayOrderResponseBean.class); // NOSONAR Checked above

		if (payResponse.getError() != null || payResponse.getResponse() == null
				|| !"APPROVED".equals(payResponse.getResponse().getGatewayCode()))
		{
			final String error = payResponse.getResponse() == null ? "Empty" : payResponse.getResponse().getGatewayCode();
			final String errorMessage = payResponse.getError() == null ? "Transaction status was " + error
					: payResponse.getError().getCause();
			throw MpgsException.getExceptionFromCode(errorMessage, response.getBody(), null);
		}

		return Optional.ofNullable(payResponse);
	}

	/**
	 *
	 */
	private PayOrderRequestBean createPayOrderRequest(final OrderStatusResponse order, final PayServiceParams params)
	{
		final Authentication authentication = new Authentication();
		authentication.setTransactionId(params.getAuthenticatedTransactioinId());

		final Order orderParam = new Order();
		orderParam.setAmount(String.valueOf(order.getAmount()));
		orderParam.setCurrency(order.getCurrency());

		final SessionBean session = new SessionBean();
		session.setId(params.getSessionId());

		final SourceOfFunds source = new SourceOfFunds();
		source.setType("CARD");

		final PayOrderRequestBean payRequest = new PayOrderRequestBean();
		payRequest.setAuthentication(authentication);
		payRequest.setOrder(orderParam);
		payRequest.setSession(session);
		payRequest.setSourceOfFunds(source);
		return payRequest;
	}

}

