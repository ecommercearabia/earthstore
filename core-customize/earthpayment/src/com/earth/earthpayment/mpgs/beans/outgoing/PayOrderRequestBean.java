/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.outgoing;

import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Authentication;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Order;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SessionBean;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.SourceOfFunds;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class PayOrderRequestBean extends MpgsRequest
{

	@Expose
	@SerializedName("authentication")
	private Authentication authentication;

	@Expose
	@SerializedName("order")
	private Order order;

	@Expose
	@SerializedName("session")
	private SessionBean session;

	@Expose
	@SerializedName("sourceOfFunds")
	private SourceOfFunds sourceOfFunds;

	/**
	 *
	 */
	public PayOrderRequestBean()
	{
		super();
		this.setApiOperation(ApiOperationEnum.PAY.getOperation());
	}

	/**
	 * @return the authentication
	 */
	public Authentication getAuthentication()
	{
		return authentication;
	}

	/**
	 * @param authentication
	 *           the authentication to set
	 */
	public void setAuthentication(final Authentication authentication)
	{
		this.authentication = authentication;
	}

	/**
	 * @return the order
	 */
	public Order getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	public void setOrder(final Order order)
	{
		this.order = order;
	}

	/**
	 * @return the session
	 */
	public SessionBean getSession()
	{
		return session;
	}

	/**
	 * @param session
	 *           the session to set
	 */
	public void setSession(final SessionBean session)
	{
		this.session = session;
	}

	/**
	 * @return the sourceOfFunds
	 */
	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	/**
	 * @param sourceOfFunds
	 *           the sourceOfFunds to set
	 */
	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

}
