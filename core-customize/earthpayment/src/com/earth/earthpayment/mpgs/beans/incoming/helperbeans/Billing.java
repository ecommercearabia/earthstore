package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Billing {
	@Expose
	@SerializedName("address")
	private Address address;

	public Billing() {
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}