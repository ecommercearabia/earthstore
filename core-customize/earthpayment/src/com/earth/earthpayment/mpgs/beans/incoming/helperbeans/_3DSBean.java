/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class _3DSBean
{

	@Expose
	@SerializedName("methodPostData")
	private String methodPostData;

	@Expose
	@SerializedName("methodUrl")
	private String methodUrl;

	/**
	 *
	 */
	public _3DSBean()
	{
	}

	/**
	 * @return the methodPostData
	 */
	public String getMethodPostData()
	{
		return methodPostData;
	}

	/**
	 * @param methodPostData
	 *           the methodPostData to set
	 */
	public void setMethodPostData(final String methodPostData)
	{
		this.methodPostData = methodPostData;
	}

	/**
	 * @return the methodUrl
	 */
	public String getMethodUrl()
	{
		return methodUrl;
	}

	/**
	 * @param methodUrl
	 *           the methodUrl to set
	 */
	public void setMethodUrl(final String methodUrl)
	{
		this.methodUrl = methodUrl;
	}

}
