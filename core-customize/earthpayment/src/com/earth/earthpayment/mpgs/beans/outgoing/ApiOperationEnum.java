package com.earth.earthpayment.mpgs.beans.outgoing;

public enum ApiOperationEnum {
	CREATE_CHECKOUT_SESSION("CREATE_CHECKOUT_SESSION"),
	CAPTURE("CAPTURE"),
	REFUND("REFUND"),
	CHECK_3DS_ENROLLMENT("CHECK_3DS_ENROLLMENT"),
	PAY("PAY"),
	VOID("VOID"),
	AUTHORIZE("AUTHORIZE"),
	INITIATE_AUTHENTICATION("INITIATE_AUTHENTICATION"),
	AUTHENTICATE_PAYER("AUTHENTICATE_PAYER");

	private ApiOperationEnum(final String operation) {
		this.operation = operation;
	}

	private String operation;

	public String getOperation() {
		return operation;
	}

}
