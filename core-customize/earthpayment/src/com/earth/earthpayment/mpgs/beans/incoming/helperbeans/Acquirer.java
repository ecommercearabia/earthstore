package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Acquirer
{
	@Expose
	@SerializedName("batch")
	private int batch;
	@Expose
	@SerializedName("date")
	private String date;
	@Expose
	@SerializedName("id")
	private String id;
	@Expose
	@SerializedName("merchantId")
	private String merchantId;
	@Expose
	@SerializedName("transactionId")
	private String transactionId;
	@Expose
	@SerializedName("settlementDate")
	private String settlementDate;
	@Expose
	@SerializedName("timeZone")
	private String timeZone;

	public Acquirer()
	{
	}

	public int getBatch()
	{
		return batch;
	}

	public void setBatch(final int batch)
	{
		this.batch = batch;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(final String date)
	{
		this.date = date;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getMerchantId()
	{
		return merchantId;
	}

	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

}
