/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * @author monzer
 */
public class Authentication
{

	@Expose
	@SerializedName("acceptVersions")
	private String acceptVersions;

	@Expose
	@SerializedName("channel")
	private String channel;

	@Expose
	@SerializedName("purpose")
	private String purpose;

	@Expose
	@SerializedName("3ds1")
	private _3DSecure _3ds1;

	@Expose
	@SerializedName("redirect")
	private RedirectBean redirect;

	@Expose
	@SerializedName("redirectHtml")
	private String redirectHtml;

	@Expose
	@SerializedName("version")
	private String version;

	@Expose
	@SerializedName("3ds")
	private _3DSecure _3ds;

	@Expose
	@SerializedName("3ds2")
	private _3DSecure _3ds2;

	@Expose
	@SerializedName("payerInteraction")
	private String payerInteraction;

	@Expose
	@SerializedName("redirectResponseUrl")
	private String redirectResponseUrl;

	@Expose
	@SerializedName("transactionId")
	private String transactionId;

	/**
	 *
	 */
	public Authentication()
	{
	}

	/**
	 * @return the acceptVersions
	 */
	public String getAcceptVersions()
	{
		return acceptVersions;
	}

	/**
	 * @param acceptVersions
	 *           the acceptVersions to set
	 */
	public void setAcceptVersions(final String acceptVersions)
	{
		this.acceptVersions = acceptVersions;
	}

	/**
	 * @return the channel
	 */
	public String getChannel()
	{
		return channel;
	}

	/**
	 * @param channel
	 *           the channel to set
	 */
	public void setChannel(final String channel)
	{
		this.channel = channel;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose()
	{
		return purpose;
	}

	/**
	 * @param purpose
	 *           the purpose to set
	 */
	public void setPurpose(final String purpose)
	{
		this.purpose = purpose;
	}

	/**
	 * @return the _3ds1
	 */
	public _3DSecure get_3ds1()
	{
		return _3ds1;
	}

	/**
	 * @param _3ds1
	 *           the _3ds1 to set
	 */
	public void set_3ds1(final _3DSecure _3ds1)
	{
		this._3ds1 = _3ds1;
	}

	/**
	 * @return the redirect
	 */
	public RedirectBean getRedirect()
	{
		return redirect;
	}

	/**
	 * @param redirect
	 *           the redirect to set
	 */
	public void setRedirect(final RedirectBean redirect)
	{
		this.redirect = redirect;
	}

	/**
	 * @return the redirectHtml
	 */
	public String getRedirectHtml()
	{
		return redirectHtml;
	}

	/**
	 * @param redirectHtml
	 *           the redirectHtml to set
	 */
	public void setRedirectHtml(final String redirectHtml)
	{
		this.redirectHtml = redirectHtml;
	}

	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}

	/**
	 * @param version
	 *           the version to set
	 */
	public void setVersion(final String version)
	{
		this.version = version;
	}

	/**
	 * @return the _3ds
	 */
	public _3DSecure get_3ds()
	{
		return _3ds;
	}

	/**
	 * @param _3ds
	 *           the _3ds to set
	 */
	public void set_3ds(final _3DSecure _3ds)
	{
		this._3ds = _3ds;
	}

	/**
	 * @return the _3ds2
	 */
	public _3DSecure get_3ds2()
	{
		return _3ds2;
	}

	/**
	 * @param _3ds2
	 *           the _3ds2 to set
	 */
	public void set_3ds2(final _3DSecure _3ds2)
	{
		this._3ds2 = _3ds2;
	}

	/**
	 * @return the payerInteraction
	 */
	public String getPayerInteraction()
	{
		return payerInteraction;
	}

	/**
	 * @param payerInteraction
	 *           the payerInteraction to set
	 */
	public void setPayerInteraction(final String payerInteraction)
	{
		this.payerInteraction = payerInteraction;
	}

	/**
	 * @return the redirectResponseUrl
	 */
	public String getRedirectResponseUrl()
	{
		return redirectResponseUrl;
	}

	/**
	 * @param redirectResponseUrl
	 *           the redirectResponseUrl to set
	 */
	public void setRedirectResponseUrl(final String redirectResponseUrl)
	{
		this.redirectResponseUrl = redirectResponseUrl;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId()
	{
		return transactionId;
	}

	/**
	 * @param transactionId
	 *           the transactionId to set
	 */
	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

}
