package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Device
{
	@Expose
	@SerializedName("browser")
	private String browser;
	@Expose
	@SerializedName("ipAddress")
	private String ipAddress;
	@Expose
	@SerializedName("browserDetails")
	private BrowserDetails browserDetails;

	public Device()
	{
	}

	public String getBrowser()
	{
		return browser;
	}

	public void setBrowser(final String browser)
	{
		this.browser = browser;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress(final String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the browserDetails
	 */
	public BrowserDetails getBrowserDetails()
	{
		return browserDetails;
	}

	/**
	 * @param browserDetails
	 *           the browserDetails to set
	 */
	public void setBrowserDetails(final BrowserDetails browserDetails)
	{
		this.browserDetails = browserDetails;
	}

}