/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.mpgs.beans.incoming.helperbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class CustomizedBean
{

	@Expose
	@SerializedName("3DS")
	private _3DSBean _3DS;

	/**
	 *
	 */
	public CustomizedBean()
	{
	}

	/**
	 * @return the _3DS
	 */
	public _3DSBean get_3DS()
	{
		return _3DS;
	}

	/**
	 * @param _3ds
	 *           the _3DS to set
	 */
	public void set_3DS(final _3DSBean _3ds)
	{
		_3DS = _3ds;
	}

}
