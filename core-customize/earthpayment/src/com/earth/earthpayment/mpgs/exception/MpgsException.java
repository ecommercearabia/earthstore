package com.earth.earthpayment.mpgs.exception;

import com.earth.earthpayment.exception.PaymentException;
import com.earth.earthpayment.exception.type.PaymentExceptionType;
import com.earth.earthpayment.mpgs.exception.type.MpgsExceptionType;


public class MpgsException extends PaymentException
{

	private final MpgsExceptionType type;
	private final Object data;

	public MpgsException(final MpgsExceptionType type, final String msg, final Object data) {
		super(msg, null);
		this.type = type;
		this.data = data;
	}

	public MpgsException(final MpgsExceptionType type, final String msg, final Object data,
			final PaymentExceptionType paymentExceptionType)
	{
		super(msg, paymentExceptionType);
		this.type = type;
		this.data = data;
	}


	public MpgsExceptionType getType() {
		return type;
	}

	public Object getData() {
		return data;
	}

	public static MpgsException getExceptionFromCode(final String code, final String msg, final Object obj) {
		return new MpgsException(MpgsExceptionType.getExceptionOrDefault(code), msg, obj);
	}

	@Override
	public String toString()
	{
		return "MpgsException [type=" + type + ", data=" + data + ", Message=" + this.getMessage() + "]";
	}



}
