/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.exception;

import com.earth.earthpayment.exception.type.PaymentExceptionType;


/**
 *
 */
public class PaymentException extends Exception
{
	private final PaymentExceptionType exceptionType;


	public PaymentException(final String message, final PaymentExceptionType type )
	{
		super(message);
		this.exceptionType = type;
	}


	/**
	 * @return the type
	 */
	public PaymentExceptionType getExceptionType()
	{
		return exceptionType;
	}

}
