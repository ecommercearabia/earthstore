/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
 package com.earth.earthpayment.hyperpay.service;

import com.earth.earthpayment.hyperpay.enums.HyperpayPaymentDataParameters;


/**
 * The Interface HyperpayPaymentPatternValidationService.
 *
 * @author monzer
 */
public interface HyperpayPaymentPatternValidationService {

	/**
	 * Validate.
	 *
	 * @param param the param
	 * @param pattern the pattern
	 * @return true, if param matches pattern
	 */
	boolean validate(String param, HyperpayPaymentDataParameters hyperpayPaymentDataParameters);

}
