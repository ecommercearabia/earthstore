/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.hyperpay.enums;

/**
 *
 * @author monzer
 *
 */
public enum TransactionCategory {
	EC, MO, TO, RC, IN, PO, PM;
}
