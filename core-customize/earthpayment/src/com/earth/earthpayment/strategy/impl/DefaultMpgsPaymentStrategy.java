/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthpayment.strategy.impl;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.earth.earthpayment.entry.PaymentRequestData;
import com.earth.earthpayment.entry.PaymentResponseData;
import com.earth.earthpayment.enums.PaymentResponseStatus;
import com.earth.earthpayment.exception.PaymentException;
import com.earth.earthpayment.exception.type.PaymentExceptionType;
import com.earth.earthpayment.model.MpgsPaymentProviderModel;
import com.earth.earthpayment.model.PaymentProviderModel;
import com.earth.earthpayment.model.PaymentRefundedTransactionModel;
import com.earth.earthpayment.mpgs.beans.MpgsPaymentRequestData;
import com.earth.earthpayment.mpgs.beans.ReturnCallbackResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.AuthenticatePayerResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.CreateCheckoutSessionResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.Initiate3DAuthenticationResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.OrderStatusResponse;
import com.earth.earthpayment.mpgs.beans.incoming.OuterTransactionResponse;
import com.earth.earthpayment.mpgs.beans.incoming.PayOrderResponseBean;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Address;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Billing;
import com.earth.earthpayment.mpgs.beans.incoming.helperbeans.Shipping;
import com.earth.earthpayment.mpgs.beans.outgoing.CreateCheckoutSessionBean;
import com.earth.earthpayment.mpgs.beans.outgoing.InteractionBean;
import com.earth.earthpayment.mpgs.beans.outgoing.OrderInfoBean;
import com.earth.earthpayment.mpgs.beans.serviceparams.PayServiceParams;
import com.earth.earthpayment.mpgs.beans.serviceparams._3DSecureParams;
import com.earth.earthpayment.mpgs.constants.MpgsConstants;
import com.earth.earthpayment.mpgs.enums.MpgsOrderResult;
import com.earth.earthpayment.mpgs.exception.MpgsException;
import com.earth.earthpayment.mpgs.exception.type.MpgsExceptionType;
import com.earth.earthpayment.mpgs.service.MpgsService;
import com.earth.earthpayment.service.records.PaymentTransactionRecordService;
import com.earth.earthpayment.strategy.CustomPaymentTransactionStrategy;
import com.earth.earthpayment.strategy.PaymentStrategy;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * The Class DefaultMpgsPaymentStrategy.
 *
 * @author husam.dababneh@erabia.com
 *
 *         The Class DefaultMpgsPaymentStrategy.
 */
public class DefaultMpgsPaymentStrategy implements PaymentStrategy
{

	/**
	 *
	 */
	private static final String GET_PAYMENT_STATUS = "GET_PAYMENT_STATUS";

	/** The Constant RESPONSE_PARAMS_MUSTN_T_BE_NULL. */
	private static final String RESPONSE_PARAMS_MUSTN_T_BE_NULL = "responseParams mustn't be null";

	/** The Constant PAYMENT_PROVIDER_MUSTN_T_BE_NULL. */
	private static final String PAYMENT_PROVIDER_MUSTN_T_BE_NULL = "paymentProviderModel mustn't be null";

	/** The Constant ABSTRACTORDER_MUSTN_T_BE_NULL. */
	private static final String ABSTRACTORDER_MUSTN_T_BE_NULL = "abstractOrder mustn't be null";

	private static final String BASE_URL_PROPERTY = "website.earth.https";

	private static final String API_BASE_URL_PROPERTY = "api.earth.https";

	/** The Constant PAYMENT_ID. */
	private static final String PAYMENT_ID = "Payment Id= [";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultMpgsPaymentStrategy.class);

	/** The payment provider service. */
	@Resource(name = "mpgsService")
	private MpgsService mpgsService;

	/** The key generator. */
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/** The key generator. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The payment transaction strategy. */
	@Resource(name = "paymentTransactionStrategy")
	private CustomPaymentTransactionStrategy paymentTransactionStrategy;

	/** The payment transaction record service. */
	@Resource(name = "paymentTransactionRecordService")
	private PaymentTransactionRecordService paymentTransactionRecordService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/** The create subscription result converter. */
	@Resource(name = "mpgsCreateSubscriptionResultConverter")
	private Converter<Map<String, Object>, CreateSubscriptionResult> createSubscriptionResultConverter;

	private static final GsonBuilder _builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();

	private static final Gson GSON = _builder.setPrettyPrinting().create();

	private MpgsConstants mpgsConstants = null;

	static
	{
		_builder.serializeNulls();
	}

	private MpgsConstants getMpgsConstants(final MpgsPaymentProviderModel provider)
	{
		mpgsConstants = new MpgsConstants(provider.getMerchantId(), provider.getApiKey(), provider.getApiVersion(),
				provider.getJavaScriptCodeUrl(), provider.getBaseURL());
		return mpgsConstants;

	}

	/**
	 * Gets the creates the subscription result converter.
	 *
	 * @return the createSubscriptionResultConverter
	 */
	protected Converter<Map<String, Object>, CreateSubscriptionResult> getCreateSubscriptionResultConverter()
	{
		return createSubscriptionResultConverter;
	}

	@Override
	public Optional<PaymentRequestData> buildPaymentRequestData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderModel;

		String request = null;
		String response = null;
		String successIndicator = null;
		CreateCheckoutSessionResponseBean responseBean;
		try
		{
			if (StringUtils.isEmpty(abstractOrderModel.getOrderCode()))
			{
				final String generateOrderCode = generateOrderCode();
				abstractOrderModel.setOrderCode(generateOrderCode);
				abstractOrderModel.setPaymentTransactionCode(generateOrderCode);
				modelService.save(abstractOrderModel);
			}
			else
			{
				checkIfOrderPaid(paymentProviderModel, abstractOrderModel);

			}

			final CreateCheckoutSessionBean requestBean = populateCreateCheckoutSessionBean(abstractOrderModel, provider);
			request = GSON.toJson(requestBean);
			final Optional<CreateCheckoutSessionResponseBean> createCheckoutSession = mpgsService.createCheckoutSession(requestBean,
					getMpgsConstants(provider));

			if (createCheckoutSession.isPresent())
			{
				responseBean = createCheckoutSession.get();
				successIndicator = responseBean.getSuccessIndicator();
				final MpgsPaymentRequestData mpgsData = new MpgsPaymentRequestData(getMpgsConstants(provider).getJavaScriptCode(),
						provider.getMerchantName(), provider.getMerchantAddressLine1(), provider.getMerchantAddressLine2(),
						provider.getMerchantId(), provider.getApiKey(), responseBean.getSession().getId(), provider.getApiVersion());

				final PaymentRequestData data = new PaymentRequestData(responseBean.getSession().getId(),
						MpgsPaymentProviderModel._TYPECODE, mpgsData, provider.getApiVersion());


				response = GSON.toJson(responseBean);
				saveRequestOnOrder(request, response, successIndicator, abstractOrderModel);

				return Optional.ofNullable(data);
			}
		}
		catch (final PaymentException e)
		{
			response = e.toString();
			saveRequestOnOrder(request, response, successIndicator, abstractOrderModel);
			throw e;
		}

		return Optional.empty();
	}

	/**
	 * @param abstractOrderModel2
	 * @param paymentProviderModel
	 *
	 */
	private void checkIfOrderPaid(final PaymentProviderModel paymentProviderModel, final AbstractOrderModel abstractOrderModel)
			throws PaymentException
	{
		if (isSuccessfulPaidOrder(paymentProviderModel, abstractOrderModel, null))
		{

			throw new PaymentException(PaymentExceptionType.ORDER_IS_ALREADY_PAID.getMessage(),
					PaymentExceptionType.ORDER_IS_ALREADY_PAID);
		}
		else
		{
			refundOrder(paymentProviderModel, abstractOrderModel);
			appendNumberToOrderCode(abstractOrderModel);
		}
	}

	private void appendNumberToOrderCode(final AbstractOrderModel abstractOrderModel)
	{
		if (Strings.isNullOrEmpty(abstractOrderModel.getPaymentTransactionCode()))
		{
			abstractOrderModel.setPaymentTransactionCode(abstractOrderModel.getOrderCode());
			modelService.save(abstractOrderModel);
		}
		String newOrderCode = null;
		final int index = abstractOrderModel.getPaymentTransactionCode().indexOf('_');
		if (index > -1)
		{
			final int a = Integer.parseInt(abstractOrderModel.getPaymentTransactionCode().substring(index + 1));
			newOrderCode = abstractOrderModel.getPaymentTransactionCode().substring(0, index + 1).concat(String.valueOf(a + 1));
		}
		else
		{
			newOrderCode = abstractOrderModel.getPaymentTransactionCode() + "_1";
		}
		abstractOrderModel.setPaymentTransactionCode(newOrderCode);
		modelService.save(abstractOrderModel);
	}

	/**
	 *
	 */
	private void saveRequestOnOrder(final String request, final String response, final String successIndicator,
			final AbstractOrderModel abstractOrderModel)
	{
		abstractOrderModel.setRequestPaymentBody(request);
		abstractOrderModel.setResponsePaymentBody(response);
		abstractOrderModel.setMpgsSuccessIndicator(successIndicator);
		modelService.save(abstractOrderModel);

	}

	/**
	 *
	 */
	private CreateCheckoutSessionBean populateCreateCheckoutSessionBean(final AbstractOrderModel abstractOrderModel,
			final MpgsPaymentProviderModel provider)
	{


		final CreateCheckoutSessionBean bean = new CreateCheckoutSessionBean();

		final InteractionBean interaction = new InteractionBean();

		final String baseURL = getSiteBaseURL(provider, abstractOrderModel);

		interaction.setOperation("PURCHASE");
		interaction.setReturnUrl(baseURL + provider.getReturnCallbackURL());
		interaction.setCancelUrl(baseURL + provider.getCancelCallbackURL());
		bean.setInteraction(interaction);

		final OrderInfoBean orderInfo = new OrderInfoBean();
		orderInfo.setAmount(getOrderTotalWithTax(abstractOrderModel).toString());
		orderInfo.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		orderInfo
				.setDescription(abstractOrderModel.getDescription() != null ? abstractOrderModel.getDescription() : "No description");
		orderInfo.setId(abstractOrderModel.getPaymentTransactionCode());
		bean.setOrder(orderInfo);


		final Billing billing = new Billing();
		billing.setAddress(getAddress(abstractOrderModel));
		//bean.setBilling(billing);

		final Address shippingAddress = getAddress(abstractOrderModel);
		final Shipping shipping = new Shipping();
		shipping.setAddress(shippingAddress);
		//	bean.setShipping(shipping);

		final String sameAsBilling = provider.isShippingSameAsBilling() ? "SAME" : "DIFFERENT";
		//	bean.getShipping().getAddress().setSameAsBilling(sameAsBilling);
		return bean;
	}

	private Address getAddress(final AbstractOrderModel abstractOrderModel)
	{
		final Address address = new Address();
		final AddressModel deliveryAddress = abstractOrderModel.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			address.setCity(deliveryAddress.getCity() == null ? null : deliveryAddress.getCity().getName());
			address.setCountry(deliveryAddress.getCountry() == null ? null : deliveryAddress.getCountry().getIsocode3());
			address.setStateProvince(deliveryAddress.getCity() == null ? null : deliveryAddress.getCity().getName());
			address.setPostcodeZip(deliveryAddress.getPostalcode());
			address.setStreet(deliveryAddress.getStreetname());
		}
		return address;
	}

	/**
	 * @param abstractOrderModel
	 * @param provider
	 *
	 */
	private String getSiteBaseURL(final MpgsPaymentProviderModel provider, final AbstractOrderModel abstractOrderModel)
	{
		if (!provider.isBuildSiteBaseUrlFromProperty() && !Strings.isNullOrEmpty(abstractOrderModel.getBaseUrl()))
		{
			return abstractOrderModel.getBaseUrl() + "/";
		}
		final String lang = commonI18NService.getCurrentLanguage() == null ? "en"
				: commonI18NService.getCurrentLanguage().getIsocode();
		final String site = cmsSiteService.getCurrentSite() == null ? "" : cmsSiteService.getCurrentSite().getUid();
		final String siteFormat = Strings.isNullOrEmpty(site) ? "" : site + "/";

		return configurationService.getConfiguration().getString(BASE_URL_PROPERTY) + "/" + siteFormat + lang + "/";
	}


	/**
	 * Generate order code.
	 *
	 * @return the string
	 */
	protected String generateOrderCode()
	{
		final Object generatedValue = keyGenerator.generate();
		if (generatedValue instanceof String)
		{
			return (String) generatedValue;
		}
		else
		{
			return String.valueOf(generatedValue);
		}
	}


	@Override
	public Optional<PaymentResponseData> buildPaymentResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		final ReturnCallbackResponseBean callbackResponse = (ReturnCallbackResponseBean) data;

		LOG.info("DefaultMpgsPaymentStrategy : Building Payment Response Data via Checkout Transaction and SuccessIndicator");
		final String successIndicator = abstractOrderModel.getMpgsSuccessIndicator();

		PaymentResponseStatus status = PaymentResponseStatus.FAILURE;
		if (successIndicator != null)
		{
			status = abstractOrderModel.getMpgsSuccessIndicator().equals(callbackResponse.getResultIndicator())
					? PaymentResponseStatus.SUCCESS
					: PaymentResponseStatus.FAILURE;
		}

		try
		{
			final Optional<OuterTransactionResponse> transaction = getLatestTransaction(paymentProviderModel, abstractOrderModel,
					data);
			if (transaction.isPresent()) // NOSONAR WHAT ?? (Husam Dababneh)
			{
				if (status == null)
				{
					status = getPaymentResponseStatus(transaction.get().getResponse().getGatewayCode());
				}
				paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, status.toString(),
						"[Result Indicator = " + abstractOrderModel.getMpgsSuccessIndicator() + "]",
						"[Success Indicator = " + abstractOrderModel.getMpgsSuccessIndicator() + "]", "resultCode", abstractOrderModel,
						paymentProviderModel);

				LOG.info("DefaultMpgsPaymentStrategy : Payment Response Status is : " + status);
				final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = PaymentResponseStatus.FAILURE
						.equals(status) ? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
								: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
				LOG.info("DefaultMpgsPaymentStrategy : Transaction Status is : " + transactionStatus);

				final TransactionStatusDetails transactionStatusDetails = PaymentResponseStatus.FAILURE.equals(status)
						? TransactionStatusDetails.INVALID_REQUEST
						: TransactionStatusDetails.REVIEW_NEEDED;
				LOG.info("DefaultMpgsPaymentStrategy : Transaction Status Details is : " + transactionStatusDetails);

				abstractOrderModel.setPaymentReferenceId(abstractOrderModel.getPaymentTransactionCode());
				modelService.save(abstractOrderModel);

				final String responseJsonTransaction = GSON.toJson(transaction.get());
				final Map<String, Object> response = GSON.fromJson(responseJsonTransaction, Map.class);

				final String requestPaymentBody = String.format("Payment Id= [%s]", abstractOrderModel.getPaymentReferenceId());
				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
						responseJsonTransaction, null);

				modelService.refresh(abstractOrderModel);
				abstractOrderModel.setResponsePaymentBody(responseJsonTransaction);
				modelService.save(abstractOrderModel);

				final PaymentResponseData paymentResponseData = new PaymentResponseData(response, MpgsPaymentProviderModel._TYPECODE,
						status);
				saveRequestOnOrder(requestPaymentBody, responseJsonTransaction, successIndicator, abstractOrderModel);
				return Optional.ofNullable(paymentResponseData);
			}
			else
			{
				LOG.error("DefaultMpgsPaymentStrategy : checkout response is empty or does nothave an id");
				final String requestPaymentBody = String.format("Order Code=[%s], Transaction Id=[%s]",
						abstractOrderModel.getPaymentTransactionCode(), "1");
				final String responsePaymentBody = "Data Returned is empty";

				paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, status.toString(),
						"[Result Indicator = " + abstractOrderModel.getMpgsSuccessIndicator() + "]",
						"[Success Indicator = " + abstractOrderModel.getMpgsSuccessIndicator() + "]", "resultCode", abstractOrderModel,
						paymentProviderModel);

				paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
						PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
						"checkoutStatus Map is null", requestPaymentBody, responsePaymentBody, null);
				saveRequestOnOrder(requestPaymentBody, responsePaymentBody, successIndicator, abstractOrderModel);
				return Optional.empty();
			}
		}
		catch (final PaymentException e)
		{
			final String requestPaymentBody = String.format("Order Code=[%s], Transaction Id=[%s]",
					abstractOrderModel.getPaymentTransactionCode(), "1");
			final String responsePaymentBody = e.toString();

			paymentTransactionRecordService.savePaymentRecords(GET_PAYMENT_STATUS, requestPaymentBody, requestPaymentBody,
					status != null ? status.toString() : "null", responsePaymentBody, abstractOrderModel, paymentProviderModel);

			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR, e.getMessage(),
					requestPaymentBody, responsePaymentBody, null);

			saveRequestOnOrder(requestPaymentBody, responsePaymentBody, successIndicator, abstractOrderModel);
			LOG.error("DefaultMpgsPaymentStrategy : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	@Override
	public Optional<CreateSubscriptionResult> interpretResponse(final Map<String, Object> responseParams,
			final PaymentProviderModel paymentProviderModel)
	{
		Preconditions.checkArgument(responseParams != null, RESPONSE_PARAMS_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		return Optional.ofNullable(getCreateSubscriptionResultConverter().convert(responseParams));
	}

	@Override
	public boolean isSuccessfulPaidOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws MpgsException
	{
		final Optional<OrderStatusResponse> order = mpgsService.getOrder(abstractOrderModel.getPaymentTransactionCode(),
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));
		if (order.isEmpty())
		{
			return false;
		}

		final OrderStatusResponse orderStatusResponse = order.get();

		return (MpgsOrderResult.CAPTURED.getCode().equalsIgnoreCase(orderStatusResponse.getStatus())
				&& (orderStatusResponse.getTotalCapturedAmount()
						- orderStatusResponse.getTotalRefundedAmount()) == getOrderTotalWithTax(abstractOrderModel))
				|| (MpgsOrderResult.AUTHORIZED.getCode().equalsIgnoreCase(orderStatusResponse.getStatus())
						&& (orderStatusResponse.getTotalAuthorizedAmount()
								- orderStatusResponse.getTotalRefundedAmount()) == getOrderTotalWithTax(abstractOrderModel))
						&& orderStatusResponse.getAmount() == getOrderTotalWithTax(abstractOrderModel);
	}

	private Optional<OuterTransactionResponse> getLatestTransaction(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws MpgsException
	{
		final Optional<OrderStatusResponse> order = mpgsService.getOrder(abstractOrderModel.getPaymentTransactionCode(),
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));

		if (order.isEmpty())
		{
			throw new MpgsException(MpgsExceptionType.REQUEST_REJECTED, "No order found on MPGS Gateway", data);
		}
		final OuterTransactionResponse latestTransaction = order.get().getTransactions()
				.get(order.get().getTransactions().size() - 1);
		if (latestTransaction == null || latestTransaction.getTransaction() == null || latestTransaction.getResponse() == null
				|| getPaymentResponseStatus(latestTransaction.getResponse().getGatewayCode()).equals(PaymentResponseStatus.FAILURE))
		{
			throw new MpgsException(MpgsExceptionType.REQUEST_REJECTED,
					"Last Transaction for order " + abstractOrderModel.getOrderCode() + " is a Failed transaction ", data);
		}
		return Optional.ofNullable(latestTransaction);
	}

	@Override
	public Optional<PaymentResponseData> getPaymentOrderStatusResponseData(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderModel;

		final Optional<OuterTransactionResponse> optCheckoutTrasnaction = getLatestTransaction(paymentProviderModel,
				abstractOrderModel, data);

		if (optCheckoutTrasnaction.isPresent()) // NOSONAR WHAT ?? (HUSAM DABABNEH)
		{
			final OuterTransactionResponse checkoutTrasnaction = optCheckoutTrasnaction.get();
			abstractOrderModel.setPaymentReferenceId(abstractOrderModel.getPaymentTransactionCode());
			modelService.save(abstractOrderModel);
			final PaymentResponseStatus paymentResponseStatus = getPaymentResponseStatus(
					checkoutTrasnaction.getResponse().getGatewayCode());

			final de.hybris.platform.payment.dto.TransactionStatus transactionStatus = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus)
							? de.hybris.platform.payment.dto.TransactionStatus.REJECTED
							: de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;

			final TransactionStatusDetails transactionStatusDetails = paymentResponseStatus == null
					|| PaymentResponseStatus.FAILURE.equals(paymentResponseStatus) ? TransactionStatusDetails.INVALID_REQUEST
							: TransactionStatusDetails.REVIEW_NEEDED;
			final String requestPaymentBody = PAYMENT_ID + abstractOrderModel.getPaymentReferenceId() + "]";
			final String responsePaymentBody = GSON.toJson(checkoutTrasnaction);
			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, transactionStatus, transactionStatusDetails, requestPaymentBody,
					responsePaymentBody, null);

			modelService.refresh(abstractOrderModel);
			abstractOrderModel.setResponsePaymentBody(responsePaymentBody);
			modelService.save(abstractOrderModel);
			final PaymentResponseData paymentResponseData = new PaymentResponseData(GSON.fromJson(responsePaymentBody, Map.class),
					MpgsPaymentProviderModel._TYPECODE, null);
			return Optional.ofNullable(paymentResponseData);
		}
		else
		{
			LOG.error("DefaultMpgsPaymentStrategy : checkout response is empty or does nothave an id");
			final String requestPaymentBody = PAYMENT_ID + abstractOrderModel.getPaymentReferenceId() + "]";


			paymentTransactionStrategy.savePaymentTransactionEntry(abstractOrderModel, abstractOrderModel.getPaymentReferenceId(),
					PaymentTransactionType.REVIEW_DECISION, de.hybris.platform.payment.dto.TransactionStatus.ERROR,
					"checkoutStatus Map is null", requestPaymentBody, StringUtils.EMPTY, null);

			return Optional.empty();
		}


	}

	@Override
	public Optional<PaymentResponseData> captureOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		PaymentResponseData result = null;


		final Optional<OuterTransactionResponse> captureOrder = mpgsService.captureOrder(abstractOrderModel.getCode(), 0, null,
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));

		if (captureOrder.isPresent())
		{
			final PaymentResponseStatus status = getPaymentResponseStatus(captureOrder.get().getResponse().getGatewayCode());
			final Map responseMap = GSON.fromJson(GSON.toJson(captureOrder.get()), Map.class);
			result = new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status);

		}

		return Optional.ofNullable(result);
	}

	private PaymentResponseStatus getPaymentResponseStatus(final String responseGatewayCode)
	{
		if (Strings.isNullOrEmpty(responseGatewayCode))
		{
			return PaymentResponseStatus.FAILURE;
		}

		switch (responseGatewayCode)
		{
			case "APPROVED":
			case "APPROVED_AUTO":
				return PaymentResponseStatus.SUCCESS;
			default:
				return PaymentResponseStatus.FAILURE;
		}

	}

	@Override
	public Optional<PaymentResponseData> cancelOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		final Optional<OuterTransactionResponse> voidOrder = mpgsService.voidOrder(abstractOrderModel.getPaymentTransactionCode(),
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));

		PaymentResponseData result = null;
		if (voidOrder.isPresent())
		{

			final PaymentResponseStatus status = getPaymentResponseStatus(voidOrder.get().getResponse().getGatewayCode());

			final Map responseMap = GSON.fromJson(GSON.toJson(voidOrder.get()), Map.class);

			result = new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status);

		}

		return Optional.ofNullable(result);
	}

	@Override
	public Optional<PaymentResponseData> refundOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);

		final Optional<OrderStatusResponse> order = mpgsService.getOrder(abstractOrderModel.getPaymentTransactionCode(),
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));

		if (order.isEmpty() || order.get().getTotalAuthorizedAmount() == 0
				|| (!MpgsOrderResult.CAPTURED.getCode().equalsIgnoreCase(order.get().getStatus())
						&& !MpgsOrderResult.AUTHORIZED.getCode().equalsIgnoreCase(order.get().getStatus())))
		{
			return Optional.empty();
		}

		setRefundRecord(abstractOrderModel, order);


		if (!((MpgsPaymentProviderModel) paymentProviderModel).isRefundEnabled())
		{

			return Optional.empty();
		}

		final Optional<OuterTransactionResponse> refundOrder = mpgsService.refundOrder(
				abstractOrderModel.getPaymentTransactionCode(), order.get().getTotalCapturedAmount(),
				abstractOrderModel.getCurrency().getIsocode(), getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));

		PaymentResponseData result = null;

		if (refundOrder.isPresent())
		{
			final PaymentResponseStatus status = getPaymentResponseStatus(refundOrder.get().getResponse().getGatewayCode());

			final Map responseMap = GSON.fromJson(GSON.toJson(refundOrder.get()), Map.class);

			result = new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status);

		}


		return Optional.ofNullable(result);
	}

	private void setRefundRecord(final AbstractOrderModel abstractOrderModel, final Optional<OrderStatusResponse> order)
	{
		final Set<PaymentRefundedTransactionModel> paymentRefundedTransactions = new HashSet<>(
				abstractOrderModel.getPaymentRefundedTransactions());

		final PaymentRefundedTransactionModel paymentRefundedTransactionModel = modelService
				.create(PaymentRefundedTransactionModel.class);
		paymentRefundedTransactionModel.setTransactionId(order.get().getId());
		paymentRefundedTransactionModel.setAmount(order.get().getAmount());
		paymentRefundedTransactionModel.setRefunded(false);
		paymentRefundedTransactions.add(paymentRefundedTransactionModel);
		abstractOrderModel.setPaymentRefundedTransactions(paymentRefundedTransactions);
		modelService.save(abstractOrderModel);
	}


	/**
	 * Gets the total price.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @return the total price
	 */
	protected Double getOrderTotalWithTax(final AbstractOrderModel abstractOrderModel)
	{
		final CurrencyModel cartCurrency = abstractOrderModel.getCurrency();
		final CurrencyModel baseCurrency = commonI18NService.getBaseCurrency();
		final double calcTotalWithTax = calcTotalWithTax(abstractOrderModel);

		return commonI18NService.convertAndRoundCurrency(cartCurrency.getConversion().doubleValue(),
				baseCurrency.getConversion().doubleValue(), cartCurrency.getDisplayDigits(), calcTotalWithTax);
	}

	@Override
	public Optional<PaymentResponseData> initiate3DSecureCheck(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof MpgsPaymentProviderModel,
				"Payment provider is " + paymentProviderModel.getClass() + ", it has to be Mpgs Payment Provider");
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, "Session ID cannot be empty");

		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderModel;
		final _3DSecureParams params = new _3DSecureParams();
		params.setAmount(getOrderTotalWithTax(abstractOrderModel));
		params.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		params.setOrderId(abstractOrderModel.getPaymentTransactionCode());
		params.setSessionId((String) data);
		params.setUniqueTransactionId(getMpgs3DSTransactionId(abstractOrderModel));

		final Optional<Initiate3DAuthenticationResponseBean> initiate3DSecureAuth = mpgsService
				.initiate3DSecureAuthentication(params, getMpgsConstants(provider));

		if (initiate3DSecureAuth.isEmpty())
		{
			return Optional.empty();
		}

		final PaymentResponseStatus status = getPaymentResponseStatus(initiate3DSecureAuth.get().getResponse().getGatewayCode());

		final Map<String, Object> responseMap = GSON.fromJson(GSON.toJson(initiate3DSecureAuth.get()), HashMap.class);


		return Optional.ofNullable(new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status));
	}

	@Override
	public Optional<PaymentResponseData> authenticate3DSecurePayer(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object data, final String redirectUrl) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof MpgsPaymentProviderModel,
				"Payment provider is " + paymentProviderModel.getClass() + ", it has to be Mpgs Payment Provider");
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(data != null, "Session ID cannot be empty");

		final MpgsPaymentProviderModel provider = (MpgsPaymentProviderModel) paymentProviderModel;
		final _3DSecureParams params = new _3DSecureParams();
		params.setAmount(getOrderTotalWithTax(abstractOrderModel));
		params.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		params.setOrderId(abstractOrderModel.getPaymentTransactionCode());
		params.setSessionId((String) data);
		params.setUniqueTransactionId(getMpgs3DSTransactionId(abstractOrderModel));
		params.setRedirectUrl(provider.getPaymentRedirectUrl() + redirectUrl);


		final Optional<AuthenticatePayerResponseBean> payerAuth = mpgsService.authenticate3DSecurePayer(params,
				getMpgsConstants(provider));
		if (payerAuth.isEmpty())
		{
			return Optional.empty();
		}

		final PaymentResponseStatus status = getPaymentResponseStatus(payerAuth.get().getResponse().getGatewayCode());

		final Map<String, Object> responseMap = GSON.fromJson(GSON.toJson(payerAuth.get()), HashMap.class);


		return Optional.ofNullable(new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status));
	}

	private String getMpgs3DSTransactionId(final AbstractOrderModel order)
	{
		if (StringUtils.isBlank(order.getMpgs3dsTransactionId()))
		{
			order.setMpgs3dsTransactionId(generateUniqueTransactionId(order));
			modelService.save(order);
		}
		return order.getMpgs3dsTransactionId();
	}

	/**
	 *
	 */
	private String generateUniqueTransactionId(final AbstractOrderModel abstractOrderModel)
	{
		abstractOrderModel.setMpgsTransactionIdCounter(abstractOrderModel.getMpgsTransactionIdCounter() + 1);
		final String transactionId = abstractOrderModel.getPaymentTransactionCode() + "_"
				+ (abstractOrderModel.getMpgsTransactionIdCounter());
		abstractOrderModel.setMpgsLastTransactionId(transactionId);
		modelService.save(abstractOrderModel);
		return transactionId;
	}

	@Override
	public Optional<PaymentResponseData> payOrder(final PaymentProviderModel paymentProviderModel,
			final AbstractOrderModel abstractOrderModel, final Object sessionId, final Object authenticatedTransactionId,
			final String threeDSResponse) throws PaymentException
	{
		Preconditions.checkArgument(paymentProviderModel != null, PAYMENT_PROVIDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(paymentProviderModel instanceof MpgsPaymentProviderModel,
				"Payment provider is " + paymentProviderModel.getClass() + ", it has to be Mpgs Payment Provider");
		Preconditions.checkArgument(abstractOrderModel != null, ABSTRACTORDER_MUSTN_T_BE_NULL);
		Preconditions.checkArgument(sessionId != null, "Session ID cannot be empty");

		abstractOrderModel.setThreeDSResponse(threeDSResponse);
		modelService.save(abstractOrderModel);

		final PayServiceParams params = new PayServiceParams();
		params.setAmount(getOrderTotalWithTax(abstractOrderModel));
		params.setCurrency(abstractOrderModel.getCurrency().getIsocode());
		params.setOrderId(abstractOrderModel.getPaymentTransactionCode());
		params.setSessionId((String) sessionId);
		params.setUniqueTransactionId(generateUniqueTransactionId(abstractOrderModel));
		params.setAuthenticatedTransactioinId((String) authenticatedTransactionId);
		final Optional<PayOrderResponseBean> payResponse = mpgsService.pay(params,
				getMpgsConstants((MpgsPaymentProviderModel) paymentProviderModel));
		if (payResponse.isEmpty())
		{
			return Optional.empty();
		}

		final PaymentResponseStatus status = getPaymentResponseStatus(payResponse.get().getResponse().getGatewayCode());

		final Map<String, Object> responseMap = GSON.fromJson(GSON.toJson(payResponse.get()), HashMap.class);

		return Optional.ofNullable(new PaymentResponseData(responseMap, MpgsPaymentProviderModel._TYPECODE, status));
	}

	private double calcTotalWithTax(final AbstractOrderModel source)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (source.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());

		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(source.getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0 && source.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
		}
		return totalPrice.doubleValue();
	}

}

