package com.earth.earthpcmbackofficecustomaddon.jalo;

import com.earth.earthpcmbackofficecustomaddon.constants.EarthpcmbackofficecustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthpcmbackofficecustomaddonManager extends GeneratedEarthpcmbackofficecustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthpcmbackofficecustomaddonManager.class.getName() );
	
	public static final EarthpcmbackofficecustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthpcmbackofficecustomaddonManager) em.getExtension(EarthpcmbackofficecustomaddonConstants.EXTENSIONNAME);
	}
	
}
