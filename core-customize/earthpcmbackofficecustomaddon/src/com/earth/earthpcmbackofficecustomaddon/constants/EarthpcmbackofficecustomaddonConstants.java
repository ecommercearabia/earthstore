/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthpcmbackofficecustomaddon.constants;

/**
 * Global class for all Earthpcmbackofficecustomaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class EarthpcmbackofficecustomaddonConstants extends GeneratedEarthpcmbackofficecustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthpcmbackofficecustomaddon";

	private EarthpcmbackofficecustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
