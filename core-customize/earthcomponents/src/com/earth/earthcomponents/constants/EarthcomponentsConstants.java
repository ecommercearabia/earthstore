/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcomponents.constants;

/**
 * Global class for all Earthcomponents constants. You can add global constants for your extension into this class.
 */
public final class EarthcomponentsConstants extends GeneratedEarthcomponentsConstants
{
	public static final String EXTENSIONNAME = "earthcomponents";

	private EarthcomponentsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthcomponentsPlatformLogo";
}
