/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthordermanagement.events;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * Listener for order confirmation events.
 */
public class OrderDeliveryCompleteConfirmationEventListener
		extends AbstractAcceleratorSiteEventListener<OrderDeliveryCompleteConfirmationEvent>
{

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected void onSiteEvent(final OrderDeliveryCompleteConfirmationEvent orderPlacedEvent)
	{
		final AbstractOrderModel order = orderPlacedEvent.getOrder();
		if (order instanceof OrderModel)
		{
			final OrderModel orderModel = (OrderModel) order;
			final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
					"orderDeliveryCompleteConfirmationEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
					"orderDeliveryCompleteConfirmationEmailProcess");
			orderProcessModel.setOrder(orderModel);
			getModelService().save(orderProcessModel);
			getBusinessProcessService().startProcess(orderProcessModel);
		}

	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final OrderDeliveryCompleteConfirmationEvent event)
	{
		final AbstractOrderModel order = event.getOrder();
		if (order instanceof OrderModel)
		{
			final OrderModel orderModel = (OrderModel) event.getOrder();
			ServicesUtil.validateParameterNotNullStandardMessage("event.order", orderModel);
			final BaseSiteModel site = orderModel.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}
	return null;
	}
}
