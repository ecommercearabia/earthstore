package com.earth.earthordermanagement.events;
/**
 *
 */

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author tuqa
 *
 */
public class OrderDeliveryCompleteConfirmationEvent extends AbstractEvent
{
	private AbstractOrderModel order;

	/**
	 * @param process
	 */

	public OrderDeliveryCompleteConfirmationEvent(final AbstractOrderModel order)
	{

		this.order = order;

	}

	/**
	 * @return the order
	 */
	protected AbstractOrderModel getOrder()
	{
		return order;
	}

	/**
	 * @param order
	 *           the order to set
	 */
	protected void setOrder(final OrderModel order)
	{
		this.order = order;
	}



}
