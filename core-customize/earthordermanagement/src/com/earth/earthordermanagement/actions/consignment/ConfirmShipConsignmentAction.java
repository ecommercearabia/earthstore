/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthordermanagement.actions.consignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;


/**
 * Update the {@link ConsignmentModel} status to {@link ConsignmentStatus#SHIPPED}.
 */
public class ConfirmShipConsignmentAction extends AbstractConsignmentAction
{
	private static final Logger LOG = LoggerFactory.getLogger(ConfirmShipConsignmentAction.class);

	@Override
	public void executeAction(final ConsignmentProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final ConsignmentModel consignment = process.getConsignment();
		consignment.setStatus(ConsignmentStatus.SHIPPED);
		consignment.setShipped(true);
		save(consignment);

		getEventService().publishEvent(getEvent(process));
	}

}
