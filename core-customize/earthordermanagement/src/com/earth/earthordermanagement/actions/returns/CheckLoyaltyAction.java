/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */

package com.earth.earthordermanagement.actions.returns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;


public class CheckLoyaltyAction extends AbstractSimpleDecisionAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CheckLoyaltyAction.class);
   
	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;
	
	/**
	 * @return the loyaltyProgramContext
	 */
	protected LoyaltyProgramContext getLoyaltyProgramContext()
	{
		return loyaltyProgramContext;
	}

	@Override
	public Transition executeAction(final ReturnProcessModel process)
	{
		LOG.debug("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		final ReturnRequestModel returnRequest = process.getReturnRequest();

	
	return isLoyaltyEnabled(returnRequest)?Transition.OK:Transition.NOK;
		
		
	}

	private boolean isLoyaltyEnabled(ReturnRequestModel returnRequest)
	{
		if(returnRequest ==null || returnRequest.getOrder()==null || returnRequest.getOrder().getStore() ==null|| returnRequest.getOrder().getUser() ==null || !(returnRequest.getOrder().getUser() instanceof CustomerModel) )
		{
			//TODO ADD LOG
			return false;
		}
		return getLoyaltyProgramContext().isLoyaltyEnabled(returnRequest.getOrder().getStore(), (CustomerModel)returnRequest.getOrder().getUser());
	}


}
