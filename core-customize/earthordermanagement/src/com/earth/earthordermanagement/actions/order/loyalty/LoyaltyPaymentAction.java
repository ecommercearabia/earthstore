/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthordermanagement.actions.order.loyalty;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentType;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;


/**
 * Action node responsible for sourcing the order and allocating the consignments. After the consignments are created,
 * the consignment sub-process is started for every consignment.
 */
public class LoyaltyPaymentAction extends AbstractProceduralAction<OrderProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(LoyaltyPaymentAction.class);

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
//
//		final OrderModel order = process.getOrder();
//
//		try
//		{
//			if (loyaltyProgramContext.isLoyaltyEnabled(order) && loyaltyProgramContext.createTransaction(order) && !LoyaltyPaymentType.CAPTURED.equals(order.getLoyaltyPaymentType()))
//			{
//				getModelService().refresh(order);
//				order.setLoyaltyPaymentType(LoyaltyPaymentType.CAPTURED);
//				getModelService().save(order);
//			}
//		}
//		catch (EarthLoyaltyException e)
//		{
//			LOG.error(e.getMessage());
//		}
//

	}

}
