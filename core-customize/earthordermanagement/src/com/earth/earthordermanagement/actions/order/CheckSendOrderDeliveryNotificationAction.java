/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthordermanagement.actions.order;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;



/**
 * The Class CheckSendOrderDeliveryNotificationAction.
 * @author Tuqa
 */
public class CheckSendOrderDeliveryNotificationAction  extends AbstractOrderAction<OrderProcessModel>
{
	
	private static final Logger LOG = LoggerFactory.getLogger(CheckSendOrderDeliveryNotificationAction.class);

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	@Override
	public final String execute(final OrderProcessModel process)
	{
		return executeAction(process).toString();
	}

	protected Transition executeAction(final OrderProcessModel process)
	{
		validateParameterNotNullStandardMessage("process", process);

		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());
		final OrderModel order = process.getOrder();

		if (isSendOrderDeliveryNotificationEnable(order))
		{
			LOG.info("Send Order Delivery Notification is Enable");
			return Transition.OK;
		}
		else
		{
			LOG.info("Send Order Delivery Notification is Disable");
			return Transition.NOK;
		}
	}
	
	/**
	 * Checks if is send order delivery notification enable.
	 *
	 * @param order the order
	 * @return true, if is send order delivery notification enable
	 */
	private boolean isSendOrderDeliveryNotificationEnable(OrderModel order)
	{
		if (order == null)
		{
			LOG.info("Missing the order, ordeorder.getStore()r is null");
			return false;
		}
		
		if (order.getStore() == null)
		{
			LOG.info("Store is Null");
			return false;
		}
		return order.getStore().isSendOrderDeliveryNotification();
	}
	

	protected enum Transition
	{
		OK, NOK;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}
}
