/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 */
package com.earth.earthordermanagement.actions.order;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthordermanagement.CheckOrderService;
import com.earth.earthordermanagement.events.OrderDeliveryCompleteConfirmationEvent;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.event.EventService;



/**
 * The Class SendOrderDeliveryNotificationAction.
 * @author Tuqa
 */
public class SendOrderDeliveryNotificationAction  extends AbstractOrderAction<OrderProcessModel>
{
	private EventService eventService;

	private static final Logger LOG = LoggerFactory.getLogger(CheckSendOrderDeliveryNotificationAction.class);

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	@Override
	public final String execute(final OrderProcessModel process)
	{
		return executeAction(process).toString();
	}

	protected Transition executeAction(final OrderProcessModel process)
	{
		validateParameterNotNullStandardMessage("process", process);

		LOG.info("Process: {} in step {}", process.getCode(), getClass().getSimpleName());

		LOG.info("Sending Order Delivery Complete Confirmation Email......");
		getEventService().publishEvent(new OrderDeliveryCompleteConfirmationEvent(process.getOrder()));
		return Transition.OK;

	}

	protected EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	protected enum Transition
	{
		OK;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}
}