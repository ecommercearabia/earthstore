/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthmessagecentercscustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthmessagecentercscustomocc.constants.EarthmessagecentercscustomoccConstants;
import org.apache.log4j.Logger;

public class EarthmessagecentercscustomoccManager extends GeneratedEarthmessagecentercscustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthmessagecentercscustomoccManager.class.getName() );
	
	public static final EarthmessagecentercscustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthmessagecentercscustomoccManager) em.getExtension(EarthmessagecentercscustomoccConstants.EXTENSIONNAME);
	}
	
}
