/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthtimeslotfacades.validation;

import com.earth.earthtimeslotfacades.TimeSlotInfoData;
import com.earth.earthtimeslotfacades.exception.TimeSlotException;


/**
 * @author amjad.shati@erabia.com
 */
public interface TimeSlotValidationService
{
	public boolean validate(final TimeSlotInfoData timeSlotInfo) throws TimeSlotException;
}
