/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthorderconfirmation.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class EarthorderconfirmationConstants extends GeneratedEarthorderconfirmationConstants
{
	public static final String EXTENSIONNAME = "earthorderconfirmation";

	private EarthorderconfirmationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
