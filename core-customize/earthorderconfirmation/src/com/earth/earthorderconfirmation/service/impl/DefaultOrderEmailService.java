/**
 *
 */
package com.earth.earthorderconfirmation.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.earth.earthorderconfirmation.dao.OrderEmailDao;
import com.earth.earthorderconfirmation.model.OrderConfirmationEmailModel;
import com.earth.earthorderconfirmation.service.OrderEmailService;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultOrderEmailService implements OrderEmailService
{
	@Resource(name = "orderEmailDao")
	private OrderEmailDao orderEmailDao;

	public OrderEmailDao getOrderEmailDao()
	{
		return orderEmailDao;
	}

	@Override
	public List<OrderConfirmationEmailModel> findByStoreUid(final String storeUid)
	{
		ServicesUtil.validateParameterNotNull(storeUid, "storeUid must not be null");

		return getOrderEmailDao().findByStoreUid(storeUid);
	}
}
