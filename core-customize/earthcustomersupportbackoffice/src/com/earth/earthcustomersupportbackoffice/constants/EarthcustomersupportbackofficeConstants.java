/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthcustomersupportbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class EarthcustomersupportbackofficeConstants extends GeneratedEarthcustomersupportbackofficeConstants
{
	public static final String EXTENSIONNAME = "earthcustomersupportbackoffice";

	private EarthcustomersupportbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
