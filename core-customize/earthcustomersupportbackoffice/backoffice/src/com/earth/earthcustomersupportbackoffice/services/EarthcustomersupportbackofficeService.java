/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthcustomersupportbackoffice.services;

/**
 * Hello World EarthcustomersupportbackofficeService
 */
public class EarthcustomersupportbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}
