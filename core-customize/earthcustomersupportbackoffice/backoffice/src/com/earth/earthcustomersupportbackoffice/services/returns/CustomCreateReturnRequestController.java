package com.earth.earthcustomersupportbackoffice.services.returns;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.omsbackoffice.widgets.returns.createreturnrequest.CreateReturnRequestController;
import de.hybris.platform.omsbackoffice.widgets.returns.dtos.ReturnEntryToCreateDto;

import org.apache.log4j.Logger;
import org.zkoss.zul.Row;

import com.hybris.cockpitng.annotations.SocketEvent;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCreateReturnRequestController extends CreateReturnRequestController
{
	private static final Logger LOG = Logger.getLogger(CustomCreateReturnRequestController.class.getName());

	@Override
	@SocketEvent(socketId = "inputObject")
	public void initCreateReturnRequestForm(final OrderModel inputOrder)
	{
		LOG.info("CustomCreateReturnRequestController -> initCreateReturnRequestForm");
		super.initCreateReturnRequestForm(inputOrder);
	}

	@Override
	protected void calculateRowAmount(final Row myRow, final ReturnEntryToCreateDto myReturnEntry, final int qtyEntered)
	{
		LOG.info("CustomCreateReturnRequestController -> CustomCreateReturnRequestController");
		super.calculateRowAmount(myRow, myReturnEntry, qtyEntered);
	}
}
