package com.earth.earthcustomersupportbackoffice.renderers;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;

import com.hybris.cockpitng.components.Editor;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.AbstractPanel;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.Attribute;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.CustomPanel;
import com.hybris.cockpitng.core.model.ModelObserver;
import com.hybris.cockpitng.core.model.WidgetModel;
import com.hybris.cockpitng.dataaccess.facades.type.DataAttribute;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.dataaccess.facades.type.TypeFacade;
import com.hybris.cockpitng.dataaccess.facades.type.exceptions.TypeNotFoundException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.labels.LabelService;
import com.hybris.cockpitng.util.YTestTools;
import com.hybris.cockpitng.widgets.editorarea.renderer.impl.DefaultEditorAreaPanelRenderer;


public class TotalLoyaltyRenderer extends DefaultEditorAreaPanelRenderer
{
	private static final Logger LOG = LoggerFactory.getLogger(TotalLoyaltyRenderer.class);
	private static final String ORDER = "Order";
	private static final String QUALIFIER = "loyaltyReturned";
	private TypeFacade typeFacade;
	private Double totalLoyaltyReturnedAmount;
	private LabelService labelService;

	@Override
	public void render(final Component component, final AbstractPanel abstractPanelConfiguration, final Object object,
			final DataType dataType, final WidgetInstanceManager widgetInstanceManager)
	{
		if (abstractPanelConfiguration instanceof CustomPanel)
		{
			this.totalLoyaltyReturnedAmount = this.getOrderTotalDiscount((ReturnRequestModel) object);

			try
			{
				final Attribute attribute = new Attribute();
				attribute.setLabel("customersupportbackoffice.returnentry.loyaltyreturned");
				attribute.setQualifier("loyaltyReturned");
				attribute.setReadonly(Boolean.TRUE);
				final DataType order = this.getTypeFacade().load("Order");
				final boolean canReadObject = this.getPermissionFacade().canReadInstanceProperty(order.getClazz(), "loyaltyReturned");
				if (canReadObject)
				{
					this.createAttributeRenderer().render(component, attribute, order.getClazz(), order, widgetInstanceManager);
				}
				else
				{
					final Div attributeContainer = new Div();
					attributeContainer.setSclass("yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-ed");
					this.renderNotReadableLabel(attributeContainer, attribute, dataType,
							this.getLabelService().getAccessDeniedLabel(attribute));
					attributeContainer.setParent(component);
				}
			}
			catch (final TypeNotFoundException var10)
			{
				if (LOG.isWarnEnabled())
				{
					LOG.warn(var10.getMessage(), var10);
				}
			}
			catch (final Exception e)
			{
				LOG.info("[TotalLoyaltyRenderer]", e.getMessage());
			}
		}

	}

	@Override
	protected Editor createEditor(final DataType genericType, final WidgetInstanceManager widgetInstanceManager,
			final Attribute attribute, final Object object)
	{
		final DataAttribute genericAttribute = genericType.getAttribute(attribute.getQualifier());
		if (genericAttribute == null)
		{
			return null;
		}
		else
		{
			final String qualifier = genericAttribute.getQualifier();
			final String referencedModelProperty = "Order." + attribute.getQualifier();
			final Editor editor = this.createEditor(genericAttribute, widgetInstanceManager.getModel(), referencedModelProperty);
			editor.setReadOnly(Boolean.TRUE);
			editor.setLocalized(Boolean.FALSE);
			editor.setWidgetInstanceManager(widgetInstanceManager);
			editor.setType(this.resolveEditorType(genericAttribute));
			editor.setOptional(!genericAttribute.isMandatory());
			YTestTools.modifyYTestId(editor, "editor_Order." + qualifier);
			editor.setAttribute("parentObject", object);
			editor.setWritableLocales(this.getPermissionFacade().getWritableLocalesForInstance(object));
			editor.setReadableLocales(this.getPermissionFacade().getReadableLocalesForInstance(object));
			if (genericAttribute.isLocalized())
			{
				editor.addParameter("localizedEditor.attributeDescription", this.getAttributeDescription(genericType, attribute));
			}

			editor.setProperty(referencedModelProperty);
			if (StringUtils.isNotBlank(attribute.getEditor()))
			{
				editor.setDefaultEditor(attribute.getEditor());
			}

			editor.setPartOf(genericAttribute.isPartOf());
			editor.setOrdered(genericAttribute.isOrdered());
			editor.afterCompose();
			editor.setSclass("ye-default-editor-readonly");
			editor.setInitialValue(this.totalLoyaltyReturnedAmount);
			return editor;
		}
	}

	protected Editor createEditor(final DataAttribute genericAttribute, final WidgetModel model,
			final String referencedModelProperty)
	{
		if (this.isReferenceEditor(genericAttribute))
		{
			final ModelObserver referenceObserver = new ModelObserver()
			{
				public void modelChanged()
				{
				}
			};
			model.addObserver(referencedModelProperty, referenceObserver);
			return new Editor()
			{
				@Override
				public void destroy()
				{
					super.destroy();
					model.removeObserver(referencedModelProperty, referenceObserver);
				}
			};
		}
		else
		{
			return new Editor();
		}
	}

	protected Double getOrderTotalDiscount(final ReturnRequestModel returnRequest)
	{
		final OrderModel order = returnRequest.getOrder();
		//		Double totalDiscount = order.getTotalDiscounts() != null ? order.getTotalDiscounts() : 0.0D;
		//		totalDiscount = totalDiscount + order.getEntries().stream().mapToDouble((entry) -> {
		//			return entry.getDiscountValues().stream().mapToDouble((discount) -> {
		//				return discount.getAppliedValue();
		//			}).sum();
		//		}).sum();
		//		return totalDiscount;
		return Double.valueOf(10d);
	}

	protected boolean isReferenceEditor(final DataAttribute genericAttribute)
	{
		return genericAttribute.getValueType() != null && !genericAttribute.getValueType().isAtomic();
	}

	protected TypeFacade getTypeFacade()
	{
		return this.typeFacade;
	}

	@Required
	public void setTypeFacade(final TypeFacade typeFacade)
	{
		this.typeFacade = typeFacade;
	}

	@Override
	protected LabelService getLabelService()
	{
		return this.labelService;
	}

	@Override
	@Required
	public void setLabelService(final LabelService labelService)
	{
		this.labelService = labelService;
	}
}
