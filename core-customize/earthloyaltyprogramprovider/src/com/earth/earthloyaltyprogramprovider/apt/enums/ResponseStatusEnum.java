package com.earth.earthloyaltyprogramprovider.apt.enums;

public enum ResponseStatusEnum {

	SUCCESS("0"),
	FAILED("5001");

	private String code;

	private ResponseStatusEnum(final String code) {
		this.code=code;
	}

	public String getCode() {
		return code;
	}

}
