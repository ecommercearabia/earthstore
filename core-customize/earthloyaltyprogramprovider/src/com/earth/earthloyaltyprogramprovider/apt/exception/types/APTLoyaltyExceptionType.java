package com.earth.earthloyaltyprogramprovider.apt.exception.types;

public enum APTLoyaltyExceptionType {

	FAILED("FAILED"),
	BAD_REQUEST("Bad request"),
	ERROR_RESPONSE("response error"),
	USER_NOT_FOUND("No user Found"),
	UNKNOWN("");

	private String message;

	private APTLoyaltyExceptionType(final String message) {
		this.message=message;
	}

	public String getMessage() {
		return message;
	}

}
