package com.earth.earthloyaltyprogramprovider.apt.exception;

import com.earth.earthloyaltyprogramprovider.apt.exception.types.APTLoyaltyExceptionType;

public class APTLoyaltyException extends Exception{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private final APTLoyaltyExceptionType type;
	private final String responseData;
	private final String requestData;


	public APTLoyaltyException(final APTLoyaltyExceptionType type, final String requestData, final String responseData,
			final String message) {
		super(message);
		this.type = type;
		this.responseData = responseData;
		this.requestData = requestData;
	}


	public APTLoyaltyExceptionType getType() {
		return type;
	}

	public String getResponseData() {
		return responseData;
	}

	public String getRequestData() {
		return requestData;
	}

}
