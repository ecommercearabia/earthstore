package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import java.util.ArrayList;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.RedeemVoucherBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.TransactionPointHistory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CustomerTransactionHistoryResponseBean
{

	@SerializedName("transactionPointHistory")
	@JsonProperty(value = "transactionPointHistory")
	@Expose
	public ArrayList<TransactionPointHistory> transactionPointHistory;
	@SerializedName("redeemedVouchers")
	@JsonProperty(value = "redeemedVouchers")
	@Expose
	public ArrayList<RedeemVoucherBean> redeemedVouchers;

	public ArrayList<TransactionPointHistory> getTransactionPointHistory()
	{
		return transactionPointHistory;
	}

	public ArrayList<RedeemVoucherBean> getRedeemedVouchers()
	{
		return redeemedVouchers;
	}


	public CustomerTransactionHistoryResponseBean()
	{

	}

}
