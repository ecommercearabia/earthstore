package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import java.util.List;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.RedeemVoucherBean;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedemptionResponseBean {

	@SerializedName("RedeemVouchers")
	@JsonProperty(value = "RedeemVouchers")
	@Expose
	private List<RedeemVoucherBean> redeemVouchers;

	@SerializedName("returnCode")
	@JsonProperty(value = "returnCode")
	@Expose
	private String returnCode;

	@SerializedName("returnMessage")
	@JsonProperty(value = "returnMessage")
	@Expose
	private String returnMessage;

	@SerializedName("memberId")
	@JsonProperty(value = "memberId")
	@Expose
	private String memberId;

	@SerializedName("response")
	@JsonProperty(value = "response")
	@Expose
	private String response;

	@SerializedName("custFirstName")
	@JsonProperty(value = "custFirstName")
	@Expose
	private String custFirstName;

	@SerializedName("custLastName")
	@JsonProperty(value = "custLastName")
	@Expose
	private String custLastName;

	@SerializedName("custAvailableAmount")
	@JsonProperty(value = "custAvailableAmount")
	@Expose
	private String custAvailableAmount;

	public List<RedeemVoucherBean> getRedeemVouchers() {
		return redeemVouchers;
	}

	public void setRedeemVouchers(final List<RedeemVoucherBean> redeemVouchers) {
		this.redeemVouchers = redeemVouchers;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(final String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(final String memberId) {
		this.memberId = memberId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(final String response) {
		this.response = response;
	}

	public String getCustFirstName() {
		return custFirstName;
	}

	public void setCustFirstName(final String custFirstName) {
		this.custFirstName = custFirstName;
	}

	public String getCustLastName() {
		return custLastName;
	}

	public void setCustLastName(final String custLastName) {
		this.custLastName = custLastName;
	}

	public String getCustAvailableAmount() {
		return custAvailableAmount;
	}

	public void setCustAvailableAmount(final String custAvailableAmount) {
		this.custAvailableAmount = custAvailableAmount;
	}

}