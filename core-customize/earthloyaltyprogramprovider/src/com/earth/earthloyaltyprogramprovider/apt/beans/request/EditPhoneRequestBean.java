/*
 * Copyright (c) 2023 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.apt.beans.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class EditPhoneRequestBean
{

	@SerializedName("countryId")
	@JsonProperty(value = "countryId")
	@Expose
	String countryId;

	@SerializedName("NewMobileNumber")
	@JsonProperty(value = "NewMobileNumber")
	@Expose
	String newMobileNumber;

	@SerializedName("OldMobileNumber")
	@JsonProperty(value = "OldMobileNumber")
	@Expose
	String oldMobileNumber;

	/**
	 * @return the countryId
	 */
	public String getCountryId()
	{
		return countryId;
	}

	/**
	 * @param countryId
	 *           the countryId to set
	 */
	public void setCountryId(final String countryId)
	{
		this.countryId = countryId;
	}

	/**
	 * @return the newMobileNumber
	 */
	public String getNewMobileNumber()
	{
		return newMobileNumber;
	}

	/**
	 * @param newMobileNumber
	 *           the newMobileNumber to set
	 */
	public void setNewMobileNumber(final String newMobileNumber)
	{
		this.newMobileNumber = newMobileNumber;
	}

	/**
	 * @return the oldMobileNumber
	 */
	public String getOldMobileNumber()
	{
		return oldMobileNumber;
	}

	/**
	 * @param oldMobileNumber
	 *           the oldMobileNumber to set
	 */
	public void setOldMobileNumber(final String oldMobileNumber)
	{
		this.oldMobileNumber = oldMobileNumber;
	}


}
