package com.earth.earthloyaltyprogramprovider.apt.beans.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedemptionRequestBean {

    @SerializedName("barCode")
    @JsonProperty(value="barCode")
    @Expose
    private String barCode;

    @SerializedName("store")
    @JsonProperty(value="store")
    @Expose
    private String store;

    @SerializedName("terminal")
    @JsonProperty(value="terminal")
    @Expose
    private String terminal;

    @SerializedName("transactionNo")
    @JsonProperty(value="transactionNo")
    @Expose
    private String transactionNo;

    @SerializedName("receiptNo")
    @JsonProperty(value="receiptNo")
    @Expose
    private String receiptNo;

    @SerializedName("userId")
    @JsonProperty(value="userId")
    @Expose
    private int userId;

    @SerializedName("availablePoints")
    @JsonProperty(value="availablePoints")
    @Expose
    private int availablePoints;

    @SerializedName("redeemedPoints")
    @JsonProperty(value="redeemedPoints")
    @Expose
    private int redeemedPoints;

    @SerializedName("memberTypeId")
    @JsonProperty(value="memberTypeId")
    @Expose
    private int memberTypeId;

    @SerializedName("countryId")
    @JsonProperty(value="countryId")
    @Expose
    private int countryId;

    @SerializedName("amount")
    @JsonProperty(value="amount")
    @Expose
	 private double amount;

    @SerializedName("invoiceNo")
    @JsonProperty(value="invoiceNo")
    @Expose
    private String invoiceNo;


	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(final String barCode) {
		this.barCode = barCode;
	}

	public String getStore() {
		return store;
	}

	public void setStore(final String store) {
		this.store = store;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(final String terminal) {
		this.terminal = terminal;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(final String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(final String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	public int getAvailablePoints() {
		return availablePoints;
	}

	public void setAvailablePoints(final int availablePoints) {
		this.availablePoints = availablePoints;
	}

	public int getRedeemedPoints() {
		return redeemedPoints;
	}

	public void setRedeemedPoints(final int redeemedPoints) {
		this.redeemedPoints = redeemedPoints;
	}

	public int getMemberTypeId() {
		return memberTypeId;
	}

	public void setMemberTypeId(final int memberTypeId) {
		this.memberTypeId = memberTypeId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(final int countryId) {
		this.countryId = countryId;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(final String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

}