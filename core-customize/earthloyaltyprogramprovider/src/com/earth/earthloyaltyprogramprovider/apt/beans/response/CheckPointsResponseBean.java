package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckPointsResponseBean {

	@SerializedName("loyaltyPointId")
	@JsonProperty(value="loyaltyPointId")
	    @Expose
	    private int loyaltyPointId;

	    @SerializedName("barCode")
	    @JsonProperty(value="barCode")
	    @Expose
	    private String barCode;

	    @SerializedName("loyaltyPoints")
	    @JsonProperty(value="loyaltyPoints")
	    @Expose
	    private double loyaltyPoints;

	    @SerializedName("availableAmount")
	    @JsonProperty(value="availableAmount")
	    @Expose
	    private double availableAmount;

	    @SerializedName("firstName")
	    @JsonProperty(value="firstName")
	    @Expose
	    private String firstName;

	    @SerializedName("lastName")
	    @JsonProperty(value="lastName")
	    @Expose
	    private String lastName;

	    @SerializedName("mobileNo")
	    @JsonProperty(value="mobileNo")
	    @Expose
	    private String mobileNo;

	    @SerializedName("returnCode")
	    @JsonProperty(value="returnCode")
	    @Expose
	    private String returnCode;

	    @SerializedName("returnMessage")
	    @JsonProperty(value="returnMessage")
	    @Expose
	    private String returnMessage;

	    @SerializedName("memberId")
	    @JsonProperty(value="memberId")
	    @Expose
	    private String memberId;

	    @SerializedName("response")
	    @JsonProperty(value="response")
	    @Expose
	    private String response;

	    @SerializedName("custFirstName")
	    @JsonProperty(value="custFirstName")
	    @Expose
	    private String custFirstName;

	    @SerializedName("custLastName")
	    @JsonProperty(value="custLastName")
	    @Expose
	    private String custLastName;

	    @SerializedName("custAvailableAmount")
	    @JsonProperty(value="custAvailableAmount")
	    @Expose
	    private String custAvailableAmount;

	    @SerializedName("totalRedeemedAmountForThisTrans")
	    @JsonProperty(value="totalRedeemedAmountForThisTrans")
	    @Expose
	    private double totalRedeemedAmountForThisTrans;

	    @SerializedName("totalRedeemedPointsForThisTrans")
	    @JsonProperty(value="totalRedeemedPointsForThisTrans")
	    @Expose
	    private double totalRedeemedPointsForThisTrans;


	public int getLoyaltyPointId() {
		return loyaltyPointId;
	}

	public void setLoyaltyPointId(final int loyaltyPointId) {
		this.loyaltyPointId = loyaltyPointId;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(final String barCode) {
		this.barCode = barCode;
	}

	public double getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(final double loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public double getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(final double availableAmount) {
		this.availableAmount = availableAmount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(final String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(final String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(final String memberId) {
		this.memberId = memberId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(final String response) {
		this.response = response;
	}

	public String getCustFirstName() {
		return custFirstName;
	}

	public void setCustFirstName(final String custFirstName) {
		this.custFirstName = custFirstName;
	}

	public String getCustLastName() {
		return custLastName;
	}

	public void setCustLastName(final String custLastName) {
		this.custLastName = custLastName;
	}

	public String getCustAvailableAmount() {
		return custAvailableAmount;
	}

	public void setCustAvailableAmount(final String custAvailableAmount) {
		this.custAvailableAmount = custAvailableAmount;
	}

	public double getTotalRedeemedAmountForThisTrans() {
		return totalRedeemedAmountForThisTrans;
	}

	public void setTotalRedeemedAmountForThisTrans(final double totalRedeemedAmountForThisTrans) {
		this.totalRedeemedAmountForThisTrans = totalRedeemedAmountForThisTrans;
	}

	public double getTotalRedeemedPointsForThisTrans() {
		return totalRedeemedPointsForThisTrans;
	}

	public void setTotalRedeemedPointsForThisTrans(final double totalRedeemedPointsForThisTrans) {
		this.totalRedeemedPointsForThisTrans = totalRedeemedPointsForThisTrans;
	}

}