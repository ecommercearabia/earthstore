/*
 * Copyright (c) 2023 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.apt.beans.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class EditEmailRequestBean
{

	@SerializedName("countryId")
	@JsonProperty(value = "countryId")
	@Expose
	String countryId;

	@SerializedName("NewEmail")
	@JsonProperty(value = "NewEmail")
	@Expose
	String newEmail;

	@SerializedName("OldEmail")
	@JsonProperty(value = "OldEmail")
	@Expose
	String oldEmail;

	/**
	 * @return the countryId
	 */
	public String getCountryId()
	{
		return countryId;
	}

	/**
	 * @param countryId
	 *           the countryId to set
	 */
	public void setCountryId(final String countryId)
	{
		this.countryId = countryId;
	}

	/**
	 * @return the newEmail
	 */
	public String getNewEmail()
	{
		return newEmail;
	}

	/**
	 * @param newEmail
	 *           the newEmail to set
	 */
	public void setNewEmail(final String newEmail)
	{
		this.newEmail = newEmail;
	}

	/**
	 * @return the oldEmail
	 */
	public String getOldEmail()
	{
		return oldEmail;
	}

	/**
	 * @param oldEmail
	 *           the oldEmail to set
	 */
	public void setOldEmail(final String oldEmail)
	{
		this.oldEmail = oldEmail;
	}

}
