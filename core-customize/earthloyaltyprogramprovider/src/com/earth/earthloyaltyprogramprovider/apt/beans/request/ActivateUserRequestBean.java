package com.earth.earthloyaltyprogramprovider.apt.beans.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivateUserRequestBean implements Serializable {

	@SerializedName("userId")
	@Expose
	@JsonProperty(value = "userId")
	private int userId;

	@SerializedName("isActive")
	@Expose
	@JsonProperty(value = "isActive")
	private boolean isActive;

	@SerializedName("countryId")
	@Expose
	@JsonProperty(value = "countryId")
	private int countryId;

	private final static long serialVersionUID = 745598425973624099L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ActivateUserRequestBean() {
	}

	public int getUserId() {
		return userId;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	public boolean isIsActive() {
		return isActive;
	}

	public void setIsActive(final boolean isActive) {
		this.isActive = isActive;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(final int countryId) {
		this.countryId = countryId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(ActivateUserRequestBean.class.getName()).append('@')
				.append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("userId");
		sb.append('=');
		sb.append(this.userId);
		sb.append(',');
		sb.append("isActive");
		sb.append('=');
		sb.append(this.isActive);
		sb.append(',');
		sb.append("countryId");
		sb.append('=');
		sb.append(this.countryId);
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}