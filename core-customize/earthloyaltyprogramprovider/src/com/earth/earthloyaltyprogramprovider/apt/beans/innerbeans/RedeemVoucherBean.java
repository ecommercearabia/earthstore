package com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedeemVoucherBean {

	@SerializedName("redeemVoucherId")
	@JsonProperty(value="redeemVoucherId")
	    @Expose
	    private int redeemVoucherId;

	    @SerializedName("barcode")
	    @JsonProperty(value="barcode")
	    @Expose
	    private String barcode;

	    @SerializedName("voucherBarcode")
	    @JsonProperty(value="voucherBarcode")
	    @Expose
	    private String voucherBarcode;

	    @SerializedName("redeemedPoints")
	    @JsonProperty(value="redeemedPoints")
	    @Expose
	    private double redeemedPoints;

	    @SerializedName("isActive")
	    @JsonProperty(value="isActive")
	    @Expose
	    private boolean isActive;

	    @SerializedName("isUsed")
	    @JsonProperty(value="isUsed")
	    @Expose
	    private boolean isUsed;

	    @SerializedName("amount")
	    @JsonProperty(value="amount")
	    @Expose
	    private double amount;

	    @SerializedName("createdDate")
	    @JsonProperty(value="createdDate")
	    @Expose
	    private String createdDate;

	    @SerializedName("invoiceNo")
	    @JsonProperty(value="invoiceNo")
	    @Expose
	    private String invoiceNo;

	    @SerializedName("redeemInvoiceNo")
	    @JsonProperty(value="redeemInvoiceNo")
	    @Expose
	    private String redeemInvoiceNo;

	    @SerializedName("redeemedDate")
	    @JsonProperty(value="redeemedDate")
	    @Expose
	    private String redeemedDate;


	public int getRedeemVoucherId() {
		return redeemVoucherId;
	}

	public void setRedeemVoucherId(final int redeemVoucherId) {
		this.redeemVoucherId = redeemVoucherId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	public String getVoucherBarcode() {
		return voucherBarcode;
	}

	public void setVoucherBarcode(final String voucherBarcode) {
		this.voucherBarcode = voucherBarcode;
	}

	public double getRedeemedPoints() {
		return redeemedPoints;
	}

	public void setRedeemedPoints(final double redeemedPoints) {
		this.redeemedPoints = redeemedPoints;
	}

	public boolean isIsActive() {
		return isActive;
	}

	public void setIsActive(final boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isIsUsed() {
		return isUsed;
	}

	public void setIsUsed(final boolean isUsed) {
		this.isUsed = isUsed;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(final double amount) {
		this.amount = amount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(final String createdDate) {
		this.createdDate = createdDate;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(final String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getRedeemInvoiceNo() {
		return redeemInvoiceNo;
	}

	public void setRedeemInvoiceNo(final String redeemInvoiceNo) {
		this.redeemInvoiceNo = redeemInvoiceNo;
	}

	public String getRedeemedDate() {
		return redeemedDate;
	}

	public void setRedeemedDate(final String redeemedDate) {
		this.redeemedDate = redeemedDate;
	}

}