package com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchaseDetailBean {

    @SerializedName("invoiceNo")
    @JsonProperty(value="invoiceNo")
    @Expose
    private String invoiceNo;

    @SerializedName("productId")
    @JsonProperty(value="productId")
    @Expose
    private String productId;

    @SerializedName("productCode")
    @JsonProperty(value="productCode")
    @Expose
    private String productCode;

    @SerializedName("descriptionEng")
    @JsonProperty(value="descriptionEng")
    @Expose
    private String descriptionEng;

    @SerializedName("descriptionArab")
    @JsonProperty(value="descriptionArab")
    @Expose
    private String descriptionArab;

    @SerializedName("quantity")
    @JsonProperty(value="quantity")
    @Expose
    private int quantity;

    @SerializedName("barCode")
    @JsonProperty(value="barCode")
    @Expose
    private String barCode;

    @SerializedName("amount")
    @JsonProperty(value="amount")
    @Expose
	 private double amount;

    @SerializedName("terminal")
    @JsonProperty(value="terminal")
    @Expose
    private String terminal;

    @SerializedName("store")
    @JsonProperty(value="store")
    @Expose
    private String store;

    @SerializedName("isPromotion")
    @JsonProperty(value="isPromotion")
    @Expose
    private boolean isPromotion;

    @SerializedName("staffId")
    @JsonProperty(value="staffId")
    @Expose
    private String staffId;

    @SerializedName("staffName")
    @JsonProperty(value="staffName")
    @Expose
    private String staffName;

    @SerializedName("receiptNo")
    @JsonProperty(value="receiptNo")
    @Expose
    private String receiptNo;

    @SerializedName("totalBillAmount")
    @JsonProperty(value="totalBillAmount")
    @Expose
	 private double totalBillAmount;

    @SerializedName("itemAmount")
    @JsonProperty(value="itemAmount")
    @Expose
	 private double itemAmount;

    @SerializedName("isWelcomeBonus")
    @JsonProperty(value="isWelcomeBonus")
    @Expose
    private boolean isWelcomeBonus;

    @SerializedName("isReferralBonus")
    @JsonProperty(value="isReferralBonus")
    @Expose
    private boolean isReferralBonus;

    @SerializedName("categoryCode")
    @JsonProperty(value="categoryCode")
    @Expose
    private String categoryCode;

    @SerializedName("groupCode")
    @JsonProperty(value="groupCode")
    @Expose
    private String groupCode;

    @SerializedName("SpecialAmount")
    @JsonProperty(value="SpecialAmount")
    @Expose
    private int specialAmount;

    @SerializedName("SpecialPoint")
    @JsonProperty(value="SpecialPoint")
    @Expose
    private int specialPoint;

    @SerializedName("DiscountCode")
    @JsonProperty(value="DiscountCode")
    @Expose
    private String discountCode;

    @SerializedName("NetLineDiscountAmount")
    @JsonProperty(value="NetLineDiscountAmount")
    @Expose
    private String netLineDiscountAmount;

    @SerializedName("reconcile")
    @JsonProperty(value="reconcile")
    @Expose
    private boolean reconcile;


	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(final String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(final String productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(final String productCode) {
		this.productCode = productCode;
	}

	public String getDescriptionEng() {
		return descriptionEng;
	}

	public void setDescriptionEng(final String descriptionEng) {
		this.descriptionEng = descriptionEng;
	}

	public String getDescriptionArab() {
		return descriptionArab;
	}

	public void setDescriptionArab(final String descriptionArab) {
		this.descriptionArab = descriptionArab;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(final int quantity) {
		this.quantity = quantity;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(final String barCode) {
		this.barCode = barCode;
	}

	public double getAmount()
	{
		return amount;
	}

	public void setAmount(final double amount)
	{
		this.amount = amount;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(final String terminal) {
		this.terminal = terminal;
	}

	public String getStore() {
		return store;
	}

	public void setStore(final String store) {
		this.store = store;
	}

	public boolean isIsPromotion() {
		return isPromotion;
	}

	public void setIsPromotion(final boolean isPromotion) {
		this.isPromotion = isPromotion;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(final String staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(final String staffName) {
		this.staffName = staffName;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(final String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public double getTotalBillAmount()
	{
		return totalBillAmount;
	}

	public void setTotalBillAmount(final double totalBillAmount)
	{
		this.totalBillAmount = totalBillAmount;
	}

	public double getItemAmount()
	{
		return itemAmount;
	}

	public void setItemAmount(final double itemAmount)
	{
		this.itemAmount = itemAmount;
	}

	public boolean isIsWelcomeBonus() {
		return isWelcomeBonus;
	}

	public void setIsWelcomeBonus(final boolean isWelcomeBonus) {
		this.isWelcomeBonus = isWelcomeBonus;
	}

	public boolean isIsReferralBonus() {
		return isReferralBonus;
	}

	public void setIsReferralBonus(final boolean isReferralBonus) {
		this.isReferralBonus = isReferralBonus;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(final String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(final String groupCode) {
		this.groupCode = groupCode;
	}

	public int getSpecialAmount() {
		return specialAmount;
	}

	public void setSpecialAmount(final int specialAmount) {
		this.specialAmount = specialAmount;
	}

	public int getSpecialPoint() {
		return specialPoint;
	}

	public void setSpecialPoint(final int specialPoint) {
		this.specialPoint = specialPoint;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(final String discountCode) {
		this.discountCode = discountCode;
	}

	public String getNetLineDiscountAmount() {
		return netLineDiscountAmount;
	}

	public void setNetLineDiscountAmount(final String netLineDiscountAmount) {
		this.netLineDiscountAmount = netLineDiscountAmount;
	}

	public boolean isReconcile() {
		return reconcile;
	}

	public void setReconcile(final boolean reconcile) {
		this.reconcile = reconcile;
	}

}