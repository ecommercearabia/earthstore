
package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RegisterNewUserResponseBean implements Serializable
{

	@SerializedName("userId")
	@Expose
	@JsonProperty(value = "userId")
	private int userId;

	@SerializedName("memberId")
	@Expose
	@JsonProperty(value = "memberId")
	private String memberId;

	@SerializedName("aspUserId")
	@Expose
	@JsonProperty(value = "aspUserId")
	private String aspUserId;

	@SerializedName("custFirstName")
	@Expose
	@JsonProperty(value = "custFirstName")
	private String custFirstName;

	@SerializedName("lastName")
	@Expose
	@JsonProperty(value = "lastName")
	private String lastName;

	@SerializedName("gender")
	@Expose
	@JsonProperty(value = "gender")
	private String gender;

	@SerializedName("isActive")
	@Expose
	@JsonProperty(value = "isActive")
	private boolean isActive;

	@SerializedName("digitalReceipt")
	@Expose
	@JsonProperty(value = "digitalReceipt")
	private boolean digitalReceipt;

	@SerializedName("countryId")
	@Expose
	@JsonProperty(value = "countryId")
	private int countryId;

	@SerializedName("createdDate")
	@Expose
	@JsonProperty(value = "createdDate")
	private String createdDate;

	@SerializedName("response")
	@Expose
	@JsonProperty(value = "response")
	private String response;

	@SerializedName("lastLogin")
	@Expose
	@JsonProperty(value = "lastLogin")
	private String lastLogin;

	@SerializedName("returnMessage")
	@Expose
	@JsonProperty(value = "returnMessage")
	private String returnMessage;

	@SerializedName("barcode")
	@Expose
	@JsonProperty(value = "barcode")
	private int barCode;

	@SerializedName("returnCode")
	@Expose
	@JsonProperty(value = "returnCode")
	private String returnCode;

	@SerializedName("nationality")
	@Expose
	@JsonProperty(value = "nationality")
	private String nationality;

	@SerializedName("firstName")
	@Expose
	@JsonProperty(value = "firstName")
	private String firstName;

	@SerializedName("custLastName")
	@Expose
	@JsonProperty(value = "custLastName")
	private String custLastName;

	@SerializedName("custAvailableAmount")
	@Expose
	@JsonProperty(value = "custAvailableAmount")
	private String custAvailableAmount;

	@SerializedName("totalRedeemedAmountForThisTrans")
	@Expose
	@JsonProperty(value = "totalRedeemedAmountForThisTrans")
	private int totalRedeemedAmountForThisTrans;

	@SerializedName("totalRedeemedPointsForThisTrans")
	@Expose
	@JsonProperty(value = "totalRedeemedPointsForThisTrans")
	private int totalRedeemedPointsForThisTrans;

	public String getMemberId()
	{
		return memberId;
	}

	public String getCustFirstName()
	{
		return custFirstName;
	}

	public boolean isActive()
	{
		return isActive;
	}

	public boolean isDigitalReceipt()
	{
		return digitalReceipt;
	}

	public String getResponse()
	{
		return response;
	}

	public String getLastLogin()
	{
		return lastLogin;
	}

	public String getReturnCode()
	{
		return returnCode;
	}

	public String getNationality()
	{
		return nationality;
	}

	public String getCustLastName()
	{
		return custLastName;
	}

	public String getCustAvailableAmount()
	{
		return custAvailableAmount;
	}

	public int getTotalRedeemedAmountForThisTrans()
	{
		return totalRedeemedAmountForThisTrans;
	}

	public int getTotalRedeemedPointsForThisTrans()
	{
		return totalRedeemedPointsForThisTrans;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public int getBarCode()
	{
		return barCode;
	}

	private final static long serialVersionUID = 2780911050995237342L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public RegisterNewUserResponseBean()
	{
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(final int userId)
	{
		this.userId = userId;
	}

	public String getAspUserId()
	{
		return aspUserId;
	}

	public void setAspUserId(final String aspUserId)
	{
		this.aspUserId = aspUserId;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(final String gender)
	{
		this.gender = gender;
	}

	public boolean isIsActive()
	{
		return isActive;
	}

	public void setIsActive(final boolean isActive)
	{
		this.isActive = isActive;
	}

	public int getCountryId()
	{
		return countryId;
	}

	public void setCountryId(final int countryId)
	{
		this.countryId = countryId;
	}

	public String getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(final String createdDate)
	{
		this.createdDate = createdDate;
	}

	public String getReturnMessage()
	{
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage)
	{
		this.returnMessage = returnMessage;
	}

}