/*
 * Copyright (c) 2023 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class EditBean
{
	@SerializedName("returnCode")
	@JsonProperty(value = "returnCode")
	@Expose
	String returnCode;

	@SerializedName("returnMessage")
	@JsonProperty(value = "returnMessage")
	@Expose
	String returnMessage;

	/**
	 * @return the returnCode
	 */
	public String getReturnCode()
	{
		return returnCode;
	}

	/**
	 * @param returnCode
	 *           the returnCode to set
	 */
	public void setReturnCode(final String returnCode)
	{
		this.returnCode = returnCode;
	}

	/**
	 * @return the returnMessage
	 */
	public String getReturnMessage()
	{
		return returnMessage;
	}

	/**
	 * @param returnMessage
	 *           the returnMessage to set
	 */
	public void setReturnMessage(final String returnMessage)
	{
		this.returnMessage = returnMessage;
	}


}
