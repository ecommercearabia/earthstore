package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPointsResponseBean {

    @SerializedName("barCode")
    @JsonProperty(value="barCode")
    @Expose
    private String barCode;

    @SerializedName("userId")
    @JsonProperty(value="userId")
    @Expose
    private int userId;

    @SerializedName("isReferralUser")
    @JsonProperty(value="isReferralUser")
    @Expose
    private boolean isReferralUser;

    @SerializedName("referralPoint")
    @JsonProperty(value="referralPoint")
    @Expose
    private int referralPoint;

    @SerializedName("pointsEared")
    @JsonProperty(value="pointsEared")
    @Expose
    private double pointsEared;

    @SerializedName("speicalPoints")
    @JsonProperty(value="speicalPoints")
    @Expose
    private double speicalPoints;

    @SerializedName("returnCode")
    @JsonProperty(value="returnCode")
    @Expose
    private String returnCode;

    @SerializedName("returnMessage")
    @JsonProperty(value="returnMessage")
    @Expose
    private String returnMessage;

    @SerializedName("memberId")
    @JsonProperty(value="memberId")
    @Expose
    private String memberId;

    @SerializedName("response")
    @JsonProperty(value="response")
    @Expose
    private String response;

    @SerializedName("custFirstName")
    @JsonProperty(value="custFirstName")
    @Expose
    private String custFirstName;

    @SerializedName("custLastName")
    @JsonProperty(value="custLastName")
    @Expose
    private String custLastName;

    @SerializedName("custAvailableAmount")
    @JsonProperty(value="custAvailableAmount")
    @Expose
    private String custAvailableAmount;


	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(final String barCode) {
		this.barCode = barCode;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	public boolean isIsReferralUser() {
		return isReferralUser;
	}

	public void setIsReferralUser(final boolean isReferralUser) {
		this.isReferralUser = isReferralUser;
	}

	public int getReferralPoint() {
		return referralPoint;
	}

	public void setReferralPoint(final int referralPoint) {
		this.referralPoint = referralPoint;
	}

	public double getPointsEared() {
		return pointsEared;
	}

	public void setPointsEared(final double pointsEared) {
		this.pointsEared = pointsEared;
	}

	public double getSpeicalPoints() {
		return speicalPoints;
	}

	public void setSpeicalPoints(final double speicalPoints) {
		this.speicalPoints = speicalPoints;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(final String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(final String memberId) {
		this.memberId = memberId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(final String response) {
		this.response = response;
	}

	public String getCustFirstName() {
		return custFirstName;
	}

	public void setCustFirstName(final String custFirstName) {
		this.custFirstName = custFirstName;
	}

	public String getCustLastName() {
		return custLastName;
	}

	public void setCustLastName(final String custLastName) {
		this.custLastName = custLastName;
	}

	public String getCustAvailableAmount() {
		return custAvailableAmount;
	}

	public void setCustAvailableAmount(final String custAvailableAmount) {
		this.custAvailableAmount = custAvailableAmount;
	}

}