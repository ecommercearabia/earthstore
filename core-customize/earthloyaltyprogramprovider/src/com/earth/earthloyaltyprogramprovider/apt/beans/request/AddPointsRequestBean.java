package com.earth.earthloyaltyprogramprovider.apt.beans.request;

import java.io.Serializable;
import java.util.List;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPointsRequestBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@SerializedName(value = "MobileNumber")
	@Expose
	@JsonProperty(value = "MobileNumber")
	private String mobileNumber;

	@SerializedName("LoyaltyCardNo")
	@Expose
	@JsonProperty(value = "LoyaltyCardNo")
	private String loyaltyCardNo;

	@SerializedName("TransactionNo")
	@Expose
	@JsonProperty(value = "TransactionNo")
	private String transactionNo;

	@SerializedName("ReceiptNo")
	@Expose
	@JsonProperty(value = "ReceiptNo")
	private String receiptNo;

	@SerializedName("Store")
	@Expose
	@JsonProperty(value = "Store")
	private String store;

	@SerializedName("Terminal")
	@Expose
	@JsonProperty(value = "Terminal")
	private String terminal;

	@SerializedName("StaffCode")
	@Expose
	@JsonProperty(value = "StaffCode")
	private String staffCode;

	@SerializedName("StaffName")
	@Expose
	@JsonProperty(value = "StaffName")
	private String staffName;

	@SerializedName("TotalBillAmount")
	@Expose
	@JsonProperty(value = "TotalBillAmount")
	private double totalBillAmount;

	@SerializedName("TotalBillAmountInclVAT")
	@Expose
	@JsonProperty(value = "TotalBillAmountInclVAT")
	private double totalBillAmountInclVAT;

	@SerializedName("RedeemVoucherAmount")
	@Expose
	@JsonProperty(value = "RedeemVoucherAmount")
	private int redeemVoucherAmount;

	@SerializedName("NetAmount")
	@Expose
	@JsonProperty(value = "NetAmount")
	private int netAmount;

	@SerializedName("AvailablePoints")
	@Expose
	@JsonProperty(value = "AvailablePoints")
	private int availablePoints;

	@SerializedName("reconcile")
	@Expose
	@JsonProperty(value = "reconcile")
	private boolean reconcile;

	@SerializedName("PurchaseDetails")
	@Expose
	@JsonProperty(value = "PurchaseDetails")
	private List<PurchaseDetailBean> purchaseDetails;

	public AddPointsRequestBean() {
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLoyaltyCardNo() {
		return loyaltyCardNo;
	}

	public void setLoyaltyCardNo(final String loyaltyCardNo) {
		this.loyaltyCardNo = loyaltyCardNo;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(final String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(final String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getStore() {
		return store;
	}

	public void setStore(final String store) {
		this.store = store;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(final String terminal) {
		this.terminal = terminal;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(final String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(final String staffName) {
		this.staffName = staffName;
	}

	public double getTotalBillAmount()
	{
		return totalBillAmount;
	}

	public void setTotalBillAmount(final double totalBillAmount)
	{
		this.totalBillAmount = totalBillAmount;
	}

	public double getTotalBillAmountInclVAT()
	{
		return totalBillAmountInclVAT;
	}

	public void setTotalBillAmountInclVAT(final double totalBillAmountInclVAT)
	{
		this.totalBillAmountInclVAT = totalBillAmountInclVAT;
	}

	public int getRedeemVoucherAmount() {
		return redeemVoucherAmount;
	}

	public void setRedeemVoucherAmount(final int redeemVoucherAmount) {
		this.redeemVoucherAmount = redeemVoucherAmount;
	}

	public int getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(final int netAmount) {
		this.netAmount = netAmount;
	}

	public int getAvailablePoints() {
		return availablePoints;
	}

	public void setAvailablePoints(final int availablePoints) {
		this.availablePoints = availablePoints;
	}

	public boolean isReconcile() {
		return reconcile;
	}

	public void setReconcile(final boolean reconcile) {
		this.reconcile = reconcile;
	}

	public List<PurchaseDetailBean> getPurchaseDetails() {
		return purchaseDetails;
	}

	public void setPurchaseDetails(final List<PurchaseDetailBean> purchaseDetails) {
		this.purchaseDetails = purchaseDetails;
	}

}