package com.earth.earthloyaltyprogramprovider.apt.beans.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivateUserResponseBean implements Serializable {



	@SerializedName("memberId")
	@Expose
	@JsonProperty(value = "memberId")
	private String memberId;

	@SerializedName("aspUserId")
	@Expose
	@JsonProperty(value = "aspUserId")
	private String aspUserId;

	@SerializedName("custFirstName")
	@Expose
	@JsonProperty(value = "custFirstName")
	private String custFirstName;

	@SerializedName("lastName")
	@Expose
	@JsonProperty(value = "lastName")
	private String lastName;

	@SerializedName("gender")
	@Expose
	@JsonProperty(value = "gender")
	private String gender;

	@SerializedName("isActive")
	@Expose
	@JsonProperty(value = "isActive")
	private boolean isActive;

	@SerializedName("digitalReceipt")
	@Expose
	@JsonProperty(value = "digitalReceipt")
	private boolean digitalReceipt;

	@SerializedName("countryId")
	@Expose
	@JsonProperty(value = "countryId")
	private int countryId;

	@SerializedName("createdDate")
	@Expose
	@JsonProperty(value = "createdDate")
	private String createdDate;

	@SerializedName("response")
	@Expose
	@JsonProperty(value = "response")
	private String response;

	@SerializedName("lastLogin")
	@Expose
	@JsonProperty(value = "lastLogin")
	private String lastLogin;

	@SerializedName("returnMessage")
	@Expose
	@JsonProperty(value = "returnMessage")
	private String returnMessage;

	@SerializedName("returnCode")
	@Expose
	@JsonProperty(value = "returnCode")
	private String returnCode;

	@SerializedName("nationality")
	@Expose
	@JsonProperty(value = "nationality")
	private String nationality;

	@SerializedName("firstName")
	@Expose
	@JsonProperty(value = "firstName")
	private String firstName;

	@SerializedName("custLastName")
	@Expose
	@JsonProperty(value = "custLastName")
	private String custLastName;

	@SerializedName("custAvailableAmount")
	@Expose
	@JsonProperty(value = "custAvailableAmount")
	private String custAvailableAmount;

	@SerializedName("totalRedeemedAmountForThisTrans")
	@Expose
	@JsonProperty(value = "totalRedeemedAmountForThisTrans")
	private int totalRedeemedAmountForThisTrans;

	@SerializedName("totalRedeemedPointsForThisTrans")
	@Expose
	@JsonProperty(value = "totalRedeemedPointsForThisTrans")
	private int totalRedeemedPointsForThisTrans;

	@SerializedName("userId")
	@Expose
	private int userId;
	@SerializedName("barcode")
	@Expose
	@JsonProperty(value = "barcode")
	private String barcode;



	public String getReturnMessage() {
		return returnMessage;
	}

	public String getReturnCode() {
		return returnCode;
	}

	private final static long serialVersionUID = -8196153947443476019L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ActivateUserResponseBean() {
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(ActivateUserResponseBean.class.getName()).append('@')
				.append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("userId");
		sb.append('=');
		sb.append(this.userId);
		sb.append(',');
		sb.append("barcode");
		sb.append('=');
		sb.append(((this.barcode == null) ? "<null>" : this.barcode));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}