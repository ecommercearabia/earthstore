package com.earth.earthloyaltyprogramprovider.apt.beans.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterNewUserRequestBean implements Serializable {

	@SerializedName("email")
	@Expose
	@JsonProperty(value = "email")
	private String email;

	@SerializedName("phoneNumber")
	@Expose
	@JsonProperty(value = "phoneNumber")
	private String phoneNumber;

	@SerializedName("password")
	@Expose
	@JsonProperty(value = "password")
	private String password;

	@SerializedName("confirmPassword")
	@Expose
	@JsonProperty(value = "confirmPassword")
	private String confirmPassword;

	@SerializedName("countryId")
	@Expose
	@JsonProperty(value = "countryId")
	private int countryId;

	@SerializedName("firstName")
	@Expose
	@JsonProperty(value = "firstName")
	private String firstName;

	@SerializedName("lastName")
	@Expose
	@JsonProperty(value = "lastName")
	private String lastName;

	@SerializedName("gender")
	@Expose
	@JsonProperty(value = "gender")
	private String gender;

	@SerializedName("dob")
	@Expose
	@JsonProperty(value = "dob")
	private String dob;


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(final String dob) {
		this.dob = dob;
	}

	private final static long serialVersionUID = 3151913368929699177L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public RegisterNewUserRequestBean() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(final String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(final int countryId) {
		this.countryId = countryId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}
}