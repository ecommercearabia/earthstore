
package com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionPointHistory implements Serializable {

	@SerializedName("LoyaltyPointId")
	@Expose
	@JsonProperty(value = "loyaltyPointId")
	private int loyaltyPointId;

	@SerializedName("BarCode")
	@Expose
	@JsonProperty(value = "barCode")
	private String barCode;

	@SerializedName("LoyaltyPoints")
	@Expose
	@JsonProperty(value = "loyaltyPoints")
	private double loyaltyPoints;

	@SerializedName("LoyaltyStatus")
	@Expose
	@JsonProperty(value = "loyaltyStatus")
	private boolean loyaltyStatus;

	@SerializedName("PurchaseId")
	@Expose
	@JsonProperty(value = "purchaseId")
	private int purchaseId;

	@SerializedName("UserId")
	@Expose
	@JsonProperty(value = "userId")
	private int userId;

	@SerializedName("ProgramId")
	@Expose
	@JsonProperty(value = "programId")
	private int programId;

	@SerializedName("CreatedDate")
	@Expose
	@JsonProperty(value = "createdDate")
	private String createdDate;

	@SerializedName("RedeemedPoints")
	@Expose
	@JsonProperty(value = "redeemedPoints")
	private int redeemedPoints;

	@SerializedName("ReceiptNo")
	@Expose
	@JsonProperty(value = "receiptNo")
	private String receiptNo;

	@SerializedName("PurchaseDate")
	@Expose
	@JsonProperty(value = "purchaseDate")
	private String purchaseDate;

	@SerializedName("InvoiceNo")
	@Expose
	@JsonProperty(value = "invoiceNo")
	private String invoiceNo;

	@SerializedName("Terminal")
	@Expose
	@JsonProperty(value = "terminal")
	private String terminal;

	@SerializedName("Store")
	@Expose
	@JsonProperty(value = "store")
	private String store;

	@SerializedName("StaffId")
	@Expose
	@JsonProperty(value = "staffId")
	private String staffId;

	@SerializedName("StaffName")
	@Expose
	@JsonProperty(value = "staffName")
	private String staffName;

	@SerializedName("Amount")
	@Expose
	@JsonProperty(value = "amount")
	private double amount;

	@SerializedName("TotalBillAmount")
	@Expose
	@JsonProperty(value = "totalBillAmount")
	private double totalBillAmount;
	private final static long serialVersionUID = 4776924244157973921L;

/**
* No args constructor for use in serialization
*
*/
public TransactionPointHistory() {
}

	public int getLoyaltyPointId() {
		return loyaltyPointId;
	}

	public void setLoyaltyPointId(final int loyaltyPointId) {
		this.loyaltyPointId = loyaltyPointId;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(final String barCode) {
		this.barCode = barCode;
	}

	public double getLoyaltyPoints() {
		return loyaltyPoints;
	}

	public void setLoyaltyPoints(final double loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	public boolean isLoyaltyStatus() {
		return loyaltyStatus;
	}

	public void setLoyaltyStatus(final boolean loyaltyStatus) {
		this.loyaltyStatus = loyaltyStatus;
	}

	public int getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(final int purchaseId) {
		this.purchaseId = purchaseId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(final int userId) {
		this.userId = userId;
	}

	public int getProgramId() {
		return programId;
	}

	public void setProgramId(final int programId) {
		this.programId = programId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(final String createdDate) {
		this.createdDate = createdDate;
	}

	public int getRedeemedPoints() {
		return redeemedPoints;
	}

	public void setRedeemedPoints(final int redeemedPoints) {
		this.redeemedPoints = redeemedPoints;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(final String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(final String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(final String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(final String terminal) {
		this.terminal = terminal;
	}

	public String getStore() {
		return store;
	}

	public void setStore(final String store) {
		this.store = store;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(final String staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(final String staffName) {
		this.staffName = staffName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(final double amount) {
		this.amount = amount;
	}

	public double getTotalBillAmount() {
		return totalBillAmount;
	}

	public void setTotalBillAmount(final double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(TransactionPointHistory.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this)))
				.append('[');
		sb.append("loyaltyPointId");
		sb.append('=');
		sb.append(this.loyaltyPointId);
		sb.append(',');
		sb.append("barCode");
		sb.append('=');
		sb.append(((this.barCode == null) ? "<null>" : this.barCode));
		sb.append(',');
		sb.append("loyaltyPoints");
		sb.append('=');
		sb.append(this.loyaltyPoints);
		sb.append(',');
		sb.append("loyaltyStatus");
		sb.append('=');
		sb.append(this.loyaltyStatus);
		sb.append(',');
		sb.append("purchaseId");
		sb.append('=');
		sb.append(this.purchaseId);
		sb.append(',');
		sb.append("userId");
		sb.append('=');
		sb.append(this.userId);
		sb.append(',');
		sb.append("programId");
		sb.append('=');
		sb.append(this.programId);
		sb.append(',');
		sb.append("createdDate");
		sb.append('=');
		sb.append(((this.createdDate == null) ? "<null>" : this.createdDate));
		sb.append(',');
		sb.append("redeemedPoints");
		sb.append('=');
		sb.append(this.redeemedPoints);
		sb.append(',');
		sb.append("receiptNo");
		sb.append('=');
		sb.append(((this.receiptNo == null) ? "<null>" : this.receiptNo));
		sb.append(',');
		sb.append("purchaseDate");
		sb.append('=');
		sb.append(((this.purchaseDate == null) ? "<null>" : this.purchaseDate));
		sb.append(',');
		sb.append("invoiceNo");
		sb.append('=');
		sb.append(((this.invoiceNo == null) ? "<null>" : this.invoiceNo));
		sb.append(',');
		sb.append("terminal");
		sb.append('=');
		sb.append(((this.terminal == null) ? "<null>" : this.terminal));
		sb.append(',');
		sb.append("store");
		sb.append('=');
		sb.append(((this.store == null) ? "<null>" : this.store));
		sb.append(',');
		sb.append("staffId");
		sb.append('=');
		sb.append(((this.staffId == null) ? "<null>" : this.staffId));
		sb.append(',');
		sb.append("staffName");
		sb.append('=');
		sb.append(((this.staffName == null) ? "<null>" : this.staffName));
		sb.append(',');
		sb.append("amount");
		sb.append('=');
		sb.append(this.amount);
		sb.append(',');
		sb.append("totalBillAmount");
		sb.append('=');
		sb.append(this.totalBillAmount);
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

}