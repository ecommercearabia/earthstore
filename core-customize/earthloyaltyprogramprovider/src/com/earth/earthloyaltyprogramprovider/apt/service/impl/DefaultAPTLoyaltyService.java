package com.earth.earthloyaltyprogramprovider.apt.service.impl;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.apt.beans.request.ActivateUserRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditEmailRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditPhoneRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RedemptionRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RegisterNewUserRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.ActivateUserResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.AddPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CheckPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CustomerTransactionHistoryResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.EditBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.RedemptionResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.RegisterNewUserResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.enums.ResponseStatusEnum;
import com.earth.earthloyaltyprogramprovider.apt.exception.APTLoyaltyException;
import com.earth.earthloyaltyprogramprovider.apt.exception.types.APTLoyaltyExceptionType;
import com.earth.earthloyaltyprogramprovider.apt.service.APTLoyaltyService;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


public class DefaultAPTLoyaltyService implements APTLoyaltyService
{

	//	private static final String BASE_URL = "https://services.earthretail.ae/UATSites/APTPOSAPIUAT/api/APTLoyaltyPOSConnectorController/";
	private static final String CHECK_LOYALTY_POINTS_PATH = "CheckLoyaltyPoints";
	private static final String ADD_POINTS_BY_PRODUCTS_PATH = "AddPointsByProducts";
	private static final String CREATE_REDEEM_VOUCHER_BY_AMOUNT_SINGLE_VOUCHER_AND_USE_PATH = "CreateRedeemVoucherByAmountSingleVoucherAndUse";
	private static final String COUNTRY_ID_PARAM = "CountryId";
	private static final String SEARCH_TYPE_PARAM = "SearchType";
	private static final String MOBILE_LOYALTY_NUMBER_PARAM = "MobileLoyaltyNumber";
	private static final String TRANSACTION_NO_PARAM = "TransactionNo";
	private static final String REGISTER_NEW_USER_PATH = "RegisterNewCustomer";
	private static final String ACTIVATE_NEW_CUSTOMER_PATH = "ActivateNewCustomer";
	private static final String GET_CUSTOMER_TRANSACTION_PATH = "GetCustomerTransaction";
	private static final String EDIT_MOBILE_PATH = "UpdateCustomerMobileNumber";
	private static final String EDIT_EMAIL_PATH = "UpdateCustomerEmail";
	private static final String CARD_OR_MOBILE_NO = "CardOrMobileNo";
	private static final String TYPE_PARAM = "Type";

	private static Gson gson;
	static
	{
		gson = new GsonBuilder().setPrettyPrinting().create();
	}

	@Override
	public CheckPointsResponseBean checkLoyaltyPoints(final String baseURL, final String mobileLoyaltyNumber,
			final String transactionNo) throws APTLoyaltyException
	{

		final String requestUrl = baseURL + CHECK_LOYALTY_POINTS_PATH;
		HttpResponse<String> response;
		try
		{
			response = Unirest.get(requestUrl).queryString(COUNTRY_ID_PARAM, "1").queryString(SEARCH_TYPE_PARAM, "mobile")
					.queryString(MOBILE_LOYALTY_NUMBER_PARAM, mobileLoyaltyNumber).queryString(TRANSACTION_NO_PARAM, transactionNo)
					.header("accept", "application/json").asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty", e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty", "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty",
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty",
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}
		final CheckPointsResponseBean checkBean = gson.fromJson(response.getBody(), CheckPointsResponseBean.class);

		if (checkBean == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, "body is empty", "Returned response is empty",
					"Returned response is empty");
		}

		final String responseBody = gson.toJson(response.getBody());

		if (!ResponseStatusEnum.SUCCESS.getCode().equals(checkBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, "body is empty", responseBody,
					checkBean.getReturnMessage());
		}

		return checkBean;
	}

	@Override
	public AddPointsResponseBean addLoyaltyPoints(final String baseURL, final AddPointsRequestBean addPoinntsRequest)
			throws APTLoyaltyException
	{
		final String requestUrl = baseURL + ADD_POINTS_BY_PRODUCTS_PATH;

		final String requestBody = gson.toJson(addPoinntsRequest);

		HttpResponse<String> response;
		try
		{
			response = Unirest.post(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final AddPointsResponseBean addBean = gson.fromJson(response.getBody(), AddPointsResponseBean.class);

		if (addBean == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, "Returned response is empty",
					"Returned response is empty");
		}
		final String responseBody = gson.toJson(response.getBody());
		if (!ResponseStatusEnum.SUCCESS.getCode().equals(addBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, responseBody,
					addBean.getReturnMessage());
		}

		return addBean;
	}

	@Override
	public RedemptionResponseBean redeemLoyaltyPoints(final String baseURL, final RedemptionRequestBean redemptionRequest)
			throws APTLoyaltyException
	{
		final String requestUrl = baseURL + CREATE_REDEEM_VOUCHER_BY_AMOUNT_SINGLE_VOUCHER_AND_USE_PATH;

		final String requestBody = gson.toJson(redemptionRequest);

		HttpResponse<String> response;
		try
		{
			response = Unirest.put(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final RedemptionResponseBean redeemBean = gson.fromJson(response.getBody(), RedemptionResponseBean.class);

		if (redeemBean == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, "Returned response is empty",
					"Returned response is empty");
		}
		final String responseBody = gson.toJson(redeemBean);
		if (!ResponseStatusEnum.SUCCESS.getCode().equals(redeemBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, responseBody,
					redeemBean.getReturnMessage());
		}

		return redeemBean;
	}

	@Override
	public RegisterNewUserResponseBean registerNewUserResponse(final String baseURL,
			final RegisterNewUserRequestBean registerNewUserRequest) throws APTLoyaltyException
	{
		Preconditions.checkArgument(!Strings.isBlank(baseURL), "baseURL is blank");

		Preconditions.checkArgument(!Objects.isNull(registerNewUserRequest), "registerNewUserRequest is null");

		final String requestUrl = baseURL + REGISTER_NEW_USER_PATH;

		final String requestBody = gson.toJson(registerNewUserRequest);

		HttpResponse<String> response;
		try
		{
			response = Unirest.post(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final RegisterNewUserResponseBean registerNewUserResponseBean = gson.fromJson(response.getBody(),
				RegisterNewUserResponseBean.class);

		if (registerNewUserResponseBean == null)
		{

			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, "Returned response is empty",
					"Returned response is empty");

		}
		final String responseBody = gson.toJson(registerNewUserResponseBean);

		if (!ResponseStatusEnum.SUCCESS.getCode().equals(registerNewUserResponseBean.getReturnCode())
				|| registerNewUserResponseBean.getUserId() == 0)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, requestBody, responseBody,
					split(registerNewUserResponseBean.getReturnMessage()));

		}

		return registerNewUserResponseBean;
	}

	/**
	 *
	 */
	private String split(final String returnMessage)
	{
		if (returnMessage.contains("message"))
		{
			final Type listTamaraPaymentTypeClass = new TypeToken<HashMap<String, Object>>()
			{}.getType();
			final Map jsonJavaRootObject = new Gson().fromJson(returnMessage, listTamaraPaymentTypeClass);
			return jsonJavaRootObject.get("message").toString();
		}
		else
		{
			return returnMessage;
		}

	}

	@Override
	public ActivateUserResponseBean activateUserResponse(final String baseURL,
			final ActivateUserRequestBean activateUserRequestBean) throws APTLoyaltyException
	{
		Preconditions.checkArgument(!Strings.isBlank(baseURL), "baseURL is blank");

		Preconditions.checkArgument(!Objects.isNull(activateUserRequestBean), "activateUserRequestBean is null");

		final String requestUrl = baseURL + ACTIVATE_NEW_CUSTOMER_PATH;

		final String requestBody = gson.toJson(activateUserRequestBean);

		HttpResponse<String> response;
		try
		{
			response = Unirest.post(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final ActivateUserResponseBean activateUserResponseBean = gson.fromJson(response.getBody(), ActivateUserResponseBean.class);
		final String responseBody = gson.toJson(activateUserResponseBean);
		if (!ResponseStatusEnum.SUCCESS.getCode().equals(activateUserResponseBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.USER_NOT_FOUND, requestBody, responseBody,
					activateUserResponseBean.getReturnMessage());

		}

		return activateUserResponseBean;
	}

	@Override
	public ActivateUserResponseBean registerAndActivateNewUserResponse(final String baseURL,
			final RegisterNewUserRequestBean registerNewUserRequest) throws APTLoyaltyException
	{

		final RegisterNewUserResponseBean registerNewUserResponse = registerNewUserResponse(baseURL, registerNewUserRequest);

		if (registerNewUserResponse == null)
		{
			final String requestBody = gson.toJson(registerNewUserRequest);
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "registerNewUserResponse is null",
					"registerNewUserResponse is null");
		}

		final ActivateUserRequestBean activateUserRequestBean = generatActivateUserRequestBean(registerNewUserResponse);

		return activateUserResponse(baseURL, activateUserRequestBean);
	}

	private ActivateUserRequestBean generatActivateUserRequestBean(final RegisterNewUserResponseBean registerNewUserResponse)
	{

		final ActivateUserRequestBean activateUserRequestBean = new ActivateUserRequestBean();

		activateUserRequestBean.setUserId(registerNewUserResponse.getUserId());
		activateUserRequestBean.setIsActive(true);
		activateUserRequestBean.setCountryId(1);

		return activateUserRequestBean;
	}


	@Override
	public CustomerTransactionHistoryResponseBean customerTransactionHistory(final String baseURL, final String cardOrMobileNo)
			throws APTLoyaltyException
	{
		final String requestUrl = baseURL + GET_CUSTOMER_TRANSACTION_PATH;
		HttpResponse<String> response;
		try
		{
			response = Unirest.get(requestUrl).queryString(COUNTRY_ID_PARAM, "1").queryString(TYPE_PARAM, "mobile")
					.queryString(CARD_OR_MOBILE_NO, cardOrMobileNo).header("accept", "application/json").asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty", e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty", "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty",
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, "body is empty",
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final CustomerTransactionHistoryResponseBean customerTransactionHistoryResponseBean = gson.fromJson(response.getBody(),
				CustomerTransactionHistoryResponseBean.class);
		if (customerTransactionHistoryResponseBean == null)
		{

			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, "body is empty", "Returned response is empty",
					"Returned response is empty");

		}

		if (CollectionUtils.isEmpty(customerTransactionHistoryResponseBean.getTransactionPointHistory()))
		{

			throw new APTLoyaltyException(APTLoyaltyExceptionType.ERROR_RESPONSE, cardOrMobileNo, response.getBody(),
					"Returned response is empty");

		}
		return customerTransactionHistoryResponseBean;
	}

	@Override
	public EditBean editMobileNumber(final String baseURL, final EditPhoneRequestBean editPhoneRequestBean)
			throws APTLoyaltyException
	{
		Preconditions.checkArgument(!Strings.isBlank(baseURL), "baseURL is blank");

		Preconditions.checkArgument(!Objects.isNull(editPhoneRequestBean), "editPhoneRequestBean is null");

		final String requestUrl = baseURL + EDIT_MOBILE_PATH;

		final String requestBody = gson.toJson(editPhoneRequestBean);

		HttpResponse<String> response;

		try
		{
			response = Unirest.post(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final EditBean editResponseBean = gson.fromJson(response.getBody(), EditBean.class);
		final String responseBody = gson.toJson(editResponseBean);
		if (!ResponseStatusEnum.SUCCESS.getCode().equals(editResponseBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.USER_NOT_FOUND, requestBody, responseBody,
					editResponseBean.getReturnMessage());

		}

		return editResponseBean;
	}

	@Override
	public EditBean editEmail(final String baseURL, final EditEmailRequestBean editEmailRequestBean) throws APTLoyaltyException
	{
		Preconditions.checkArgument(!Strings.isBlank(baseURL), "baseURL is blank");

		Preconditions.checkArgument(!Objects.isNull(editEmailRequestBean), "editPhoneRequestBean is null");

		final String requestUrl = baseURL + EDIT_EMAIL_PATH;

		final String requestBody = gson.toJson(editEmailRequestBean);

		HttpResponse<String> response;

		try
		{
			response = Unirest.post(requestUrl).header("Accept", "application/json").header("Content-Type", "application/json")
					.queryString(COUNTRY_ID_PARAM, "1").body(requestBody).asString();
		}
		catch (final UnirestException e)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, e.getMessage(), e.getMessage());
		}
		if (response == null)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody, "Response is empty",
					"Response is empty");
		}
		if (response.getStatus() != 200)
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response status code is " + response.getStatus(), "Response status code is " + response.getStatus());
		}
		if (Strings.isBlank(response.getBody()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.BAD_REQUEST, requestBody,
					"Response body is empty" + response.getStatus(), "Response body is empty" + response.getStatus());
		}

		final EditBean editResponseBean = gson.fromJson(response.getBody(), EditBean.class);
		final String responseBody = gson.toJson(editResponseBean);
		if (!ResponseStatusEnum.SUCCESS.getCode().equals(editResponseBean.getReturnCode()))
		{
			throw new APTLoyaltyException(APTLoyaltyExceptionType.USER_NOT_FOUND, requestBody, responseBody,
					editResponseBean.getReturnMessage());

		}

		return editResponseBean;
	}

}
