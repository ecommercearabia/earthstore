package com.earth.earthloyaltyprogramprovider.apt.service;


import com.earth.earthloyaltyprogramprovider.apt.beans.request.ActivateUserRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditEmailRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditPhoneRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RedemptionRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RegisterNewUserRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.ActivateUserResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.AddPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CheckPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CustomerTransactionHistoryResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.EditBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.RedemptionResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.RegisterNewUserResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.exception.APTLoyaltyException;


public interface APTLoyaltyService
{

	public RegisterNewUserResponseBean registerNewUserResponse(String baseURL, RegisterNewUserRequestBean registerNewUserRequest)
			throws APTLoyaltyException;

	public ActivateUserResponseBean activateUserResponse(String baseURL, ActivateUserRequestBean activateUserRequestBean)
			throws APTLoyaltyException;

	public CheckPointsResponseBean checkLoyaltyPoints(String baseURL, String mobileLoyaltyNumber, String transactionNo)
			throws APTLoyaltyException;

	public AddPointsResponseBean addLoyaltyPoints(String baseURL, AddPointsRequestBean addPoinntsRequest)
			throws APTLoyaltyException;

	public RedemptionResponseBean redeemLoyaltyPoints(String baseURL, RedemptionRequestBean redemptionRequest)
			throws APTLoyaltyException;

	public CustomerTransactionHistoryResponseBean customerTransactionHistory(String baseURL, String cardOrMobileNo)
			throws APTLoyaltyException;

	public EditBean editMobileNumber(String baseURL, EditPhoneRequestBean editPhoneRequestBean) throws APTLoyaltyException;

	public EditBean editEmail(String baseURL, EditEmailRequestBean editEmailRequestBean) throws APTLoyaltyException;

	public ActivateUserResponseBean registerAndActivateNewUserResponse(String baseURL,
			RegisterNewUserRequestBean registerNewUserRequest) throws APTLoyaltyException;

}
