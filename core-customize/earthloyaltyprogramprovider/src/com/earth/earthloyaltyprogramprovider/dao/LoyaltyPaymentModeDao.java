/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.dao;

import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;


/**
 *
 */
public interface LoyaltyPaymentModeDao
{
	public LoyaltyPaymentModeModel getLoyaltyPaymentMode(LoyaltyPaymentModeType loyaltyPaymentModeType);

}
