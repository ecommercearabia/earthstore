package com.earth.earthloyaltyprogramprovider.dao.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.earth.earthloyaltyprogramprovider.constants.GeneratedEarthloyaltyprogramproviderConstants.Enumerations.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.dao.CartReleaseLoyaltyDao;


public class DefaultCartReleaseLoyaltyDao implements CartReleaseLoyaltyDao
{


	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<CartModel> getAllCartsAfterTimeAmount(final int timeAmount)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public List<CartModel> getAllCartsByLoyaltyMode()
	{
		final StringBuilder queryString = new StringBuilder(
				"select {c.pk} from {Cart as c join LoyaltyPaymentMode as lpm on {c.loyaltyPaymentMode} = {lpm.pk} join LoyaltyPaymentModeType as t on {lpm.loyaltyPaymentModeType} = {t.pk}} where {lpm.loyaltyPaymentModeType} IS NOT NULL and {t.code}!='")
						.append(LoyaltyPaymentModeType.REDEEM_NONE).append("'");
		final SearchResult<CartModel> result = getFlexibleSearchService().search(queryString.toString());
		return result == null ? Collections.emptyList() : result.getResult();
	}

	/**
	 * @return the flexibleSearchService
	 */
	protected FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

}
