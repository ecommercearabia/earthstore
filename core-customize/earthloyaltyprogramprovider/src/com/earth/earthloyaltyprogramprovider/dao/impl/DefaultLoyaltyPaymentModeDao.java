/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import com.earth.earthloyaltyprogramprovider.dao.LoyaltyPaymentModeDao;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;


/**
 *
 */
public class DefaultLoyaltyPaymentModeDao extends AbstractItemDao implements LoyaltyPaymentModeDao
{

	@Override
	public LoyaltyPaymentModeModel getLoyaltyPaymentMode(final LoyaltyPaymentModeType loyaltyPaymentModeType)
	{
		final String query = "SELECT {PK} FROM {LoyaltyPaymentMode} WHERE {loyaltyPaymentModeType}=?loyaltyPaymentModeType";

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		fQuery.addQueryParameter("loyaltyPaymentModeType", loyaltyPaymentModeType);

		final SearchResult<LoyaltyPaymentModeModel> result = getFlexibleSearchService().search(fQuery);

		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

}
