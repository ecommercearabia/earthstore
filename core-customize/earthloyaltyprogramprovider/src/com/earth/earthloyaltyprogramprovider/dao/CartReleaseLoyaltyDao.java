package com.earth.earthloyaltyprogramprovider.dao;

import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


public interface CartReleaseLoyaltyDao
{
	public List<CartModel> getAllCartsAfterTimeAmount(int timeAmount);

	public List<CartModel> getAllCartsByLoyaltyMode();
}
