package com.earth.earthloyaltyprogramprovider.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

/**
 * @author monzer
 *
 */
public class AbstractOrderLoyaltyAmountToBeDeductedAttributeHandler
		implements DynamicAttributeHandler<Double, AbstractOrderModel>
{

	@Override
	public Double get(final AbstractOrderModel abstractOrderModel)
	{

		final double deductedLoyaltyAmount = abstractOrderModel.getDeductedLoyaltyAmount();
		final double loyaltyAmount = abstractOrderModel.getLoyaltyAmount();

		if (loyaltyAmount <= 0 || loyaltyAmount - deductedLoyaltyAmount <= 0)
		{
			return Double.valueOf(0.00);
		}

		return Double.valueOf(loyaltyAmount - deductedLoyaltyAmount);

	}

	@Override
	public void set(final AbstractOrderModel abstractOrderModel, final Double val)
	{
		throw new UnsupportedOperationException();
	}



}
