package com.earth.earthloyaltyprogramprovider.giift.util;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthloyaltyprogramprovider.giift.beans.ErrorResult;
import com.earth.earthloyaltyprogramprovider.giift.exception.GiiftException;
import com.earth.earthloyaltyprogramprovider.giift.exception.type.GiiftExceptionType;


public class ErrorHandlingUtil
{

	private static final Logger LOG = LoggerFactory.getLogger(ErrorHandlingUtil.class);

	private ErrorHandlingUtil()
	{

	}

	public static GiiftException getException(final ErrorResult errorResult)
	{
		if (Objects.isNull(errorResult))
		{
			final GiiftException ex = new GiiftException(GiiftExceptionType.EMPTY_ERROR_RESPONSE_EXCEPTION,
					GiiftExceptionType.EMPTY_ERROR_RESPONSE_EXCEPTION.getMsg(), null);
			LOG.error(ex.getMessage());
			return ex;
		}

		final GiiftExceptionType exceptionFromCode = GiiftExceptionType.getExceptionFromCode(errorResult.getCode());
		return new GiiftException(exceptionFromCode, errorResult.getMessage(), errorResult);
	}

}
