package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class QueryMerchantCampaignStatusData {

	@SerializedName("MerchantID")
	private String merchantID;

	@SerializedName("Status")
	private String status;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(final String merchantID) {
		this.merchantID = merchantID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

}
