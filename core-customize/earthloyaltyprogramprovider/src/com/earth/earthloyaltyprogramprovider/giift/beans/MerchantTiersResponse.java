package com.earth.earthloyaltyprogramprovider.giift.beans;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MerchantTiersResponse {

	@SerializedName("ResultCode")
	private String code;

	@SerializedName("Result")
	private List<MerchantTier> result;

	@SerializedName("ErrorResult")
	private ErrorResult errorResult;

	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}

	public List<MerchantTier> getResult() {
		return result;
	}

	public void setResult(final List<MerchantTier> result) {
		this.result = result;
	}

	public ErrorResult getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(final ErrorResult errorResult) {
		this.errorResult = errorResult;
	}

}
