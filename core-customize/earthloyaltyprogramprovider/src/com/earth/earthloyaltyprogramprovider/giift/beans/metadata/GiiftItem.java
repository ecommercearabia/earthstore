/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.giift.beans.metadata;

import com.google.gson.annotations.SerializedName;


/**
 *
 */
public class GiiftItem
{

	@SerializedName("ItemCode")
	private String code;

	@SerializedName("UOMCode")
	private String uomCode;

	@SerializedName("Sign")
	private long sign;

	@SerializedName("SalesQty")
	private long quantity;

	@SerializedName("UnitPrice")
	private double unitPrice;

	@SerializedName("LineTotal")
	private double totalPrice;

	@SerializedName("VATCode")
	private String vatCode;

	@SerializedName("VATPercent")
	private double vatPercent;

	@SerializedName("VATAmount")
	private double vatAmount;

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the uomCode
	 */
	public String getUomCode()
	{
		return uomCode;
	}

	/**
	 * @param uomCode
	 *           the uomCode to set
	 */
	public void setUomCode(final String uomCode)
	{
		this.uomCode = uomCode;
	}

	/**
	 * @return the sign
	 */
	public long getSign()
	{
		return sign;
	}

	/**
	 * @param sign
	 *           the sign to set
	 */
	public void setSign(final long sign)
	{
		this.sign = sign;
	}

	/**
	 * @return the quantity
	 */
	public long getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *           the quantity to set
	 */
	public void setQuantity(final long quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the unitPrice
	 */
	public double getUnitPrice()
	{
		return unitPrice;
	}

	/**
	 * @param unitPrice
	 *           the unitPrice to set
	 */
	public void setUnitPrice(final double unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice()
	{
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *           the totalPrice to set
	 */
	public void setTotalPrice(final double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the vatCode
	 */
	public String getVatCode()
	{
		return vatCode;
	}

	/**
	 * @param vatCode
	 *           the vatCode to set
	 */
	public void setVatCode(final String vatCode)
	{
		this.vatCode = vatCode;
	}

	/**
	 * @return the vatPercent
	 */
	public double getVatPercent()
	{
		return vatPercent;
	}

	/**
	 * @param vatPercent
	 *           the vatPercent to set
	 */
	public void setVatPercent(final double vatPercent)
	{
		this.vatPercent = vatPercent;
	}

	/**
	 * @return the vatAmount
	 */
	public double getVatAmount()
	{
		return vatAmount;
	}

	/**
	 * @param vatAmount
	 *           the vatAmount to set
	 */
	public void setVatAmount(final double vatAmount)
	{
		this.vatAmount = vatAmount;
	}

}
