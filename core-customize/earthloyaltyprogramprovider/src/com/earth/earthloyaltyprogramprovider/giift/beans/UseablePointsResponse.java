package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class UseablePointsResponse {

	@SerializedName("MerchantID")
	private String merchantID;

	@SerializedName("CustomerID")
	private String customerID;

	@SerializedName("InitialAmount")
	private double initialAmount;

	@SerializedName("Reward")
	private Reward reward;

	@SerializedName("Points")
	private Points points;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(final String merchantID) {
		this.merchantID = merchantID;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(final String customerID) {
		this.customerID = customerID;
	}

	public double getInitialAmount()
	{
		return initialAmount;
	}

	public void setInitialAmount(final double initialAmount)
	{
		this.initialAmount = initialAmount;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(final Reward reward) {
		this.reward = reward;
	}

	public Points getPoints() {
		return points;
	}

	public void setPoints(final Points points) {
		this.points = points;
	}

}
