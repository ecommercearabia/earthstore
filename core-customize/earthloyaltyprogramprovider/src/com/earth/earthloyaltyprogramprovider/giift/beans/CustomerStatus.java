package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class CustomerStatus {

	@SerializedName("CustomerID")
	private String customerID;

	@SerializedName("RegisterStatus")
	private boolean register;

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(final String customerID) {
		this.customerID = customerID;
	}

	public boolean isRegister() {
		return register;
	}

	public void setRegister(final boolean register) {
		this.register = register;
	}

}
