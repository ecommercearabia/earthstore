/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.giift.beans;

/**
 *
 */
public class GiiftPagination
{

	private int pageIndex;

	private int pageSize;

	/**
	 * @return the pageIndex
	 */
	public int getPageIndex()
	{
		return pageIndex;
	}

	/**
	 * @param pageIndex
	 *           the pageIndex to set
	 */
	public void setPageIndex(final int pageIndex)
	{
		this.pageIndex = pageIndex;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize()
	{
		return pageSize;
	}

	/**
	 * @param pageSize
	 *           the pageSize to set
	 */
	public void setPageSize(final int pageSize)
	{
		this.pageSize = pageSize;
	}



}
