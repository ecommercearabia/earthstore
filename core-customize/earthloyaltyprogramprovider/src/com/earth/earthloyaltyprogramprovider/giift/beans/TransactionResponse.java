package com.earth.earthloyaltyprogramprovider.giift.beans;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class TransactionResponse {

	@SerializedName("Created")
	private Date created;

	@SerializedName("MerchantID")
	private String merchantId;

	@SerializedName("MerchantName")
	private String merchantName;

	@SerializedName("CustomerID")
	private String customerId;

	@SerializedName("InitialAmount")
	private double initialAmount;

	@SerializedName("FinalAmount")
	private double finalAmount;

	@SerializedName("ValidateID")
	private String validateId;

	@SerializedName("TransactionID")
	private String transactionId;

	@SerializedName("Points")
	private Points points;

	public Date getCreated() {
		return created;
	}

	public void setCreated(final Date created) {
		this.created = created;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(final String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(final String merchantName) {
		this.merchantName = merchantName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(final String customerId) {
		this.customerId = customerId;
	}

	public double getInitialAmount() {
		return initialAmount;
	}

	public void setInitialAmount(final double initialAmount) {
		this.initialAmount = initialAmount;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(final double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public String getValidateId() {
		return validateId;
	}

	public void setValidateId(final String validateId) {
		this.validateId = validateId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(final String transactionId) {
		this.transactionId = transactionId;
	}

	public Points getPoints() {
		return points;
	}

	public void setPoints(final Points points) {
		this.points = points;
	}

}
