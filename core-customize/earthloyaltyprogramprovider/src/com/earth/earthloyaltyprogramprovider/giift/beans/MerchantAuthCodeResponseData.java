package com.earth.earthloyaltyprogramprovider.giift.beans;

import com.google.gson.annotations.SerializedName;

public class MerchantAuthCodeResponseData {
	@SerializedName("MerchantID")
	private String merchantID;

	@SerializedName("AuthorizationCode")
	private String authorizationCode;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(final String merchantID) {
		this.merchantID = merchantID;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(final String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

}
