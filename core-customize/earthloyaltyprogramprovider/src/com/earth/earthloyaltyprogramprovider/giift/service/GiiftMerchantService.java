package com.earth.earthloyaltyprogramprovider.giift.service;

import java.util.List;
import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantAuthCodeResponseData;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantData;
import com.earth.earthloyaltyprogramprovider.giift.beans.MerchantTier;
import com.earth.earthloyaltyprogramprovider.giift.beans.QueryMerchantCampaignStatusData;
import com.earth.earthloyaltyprogramprovider.giift.exception.GiiftException;

/**
 *
 * @author husam.dababneh@erabia.com
 *
 * @apiNote This is subject to change at anytime during development
 *
 */

public interface GiiftMerchantService {

	public Optional<List<MerchantTier>> retrieveMerchantTiers(String key, String vector) throws GiiftException;

	public Optional<MerchantData> merchantOnboarding(String key, String vector, MerchantData merchant)
			throws GiiftException;

	public Optional<MerchantData> retrieveMerchantInformation(String key, String vector, String merchantID)
			throws GiiftException;

	public Optional<MerchantData> updateMerchant(String key, String vector, MerchantData merchant)
			throws GiiftException;

	public Optional<QueryMerchantCampaignStatusData> checkRewardsProgramStatus(String key, String vector,
			String merchantID) throws GiiftException;

	public Optional<MerchantAuthCodeResponseData> retrieveMerchantAccountAuthorizationCode(String key, String vector,
			String merchantID) throws GiiftException;

}
