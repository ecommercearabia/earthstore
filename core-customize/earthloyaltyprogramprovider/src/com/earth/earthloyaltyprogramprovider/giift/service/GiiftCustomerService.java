package com.earth.earthloyaltyprogramprovider.giift.service;

import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.giift.beans.Customer;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerPointLogs;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerPointSummary;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.earth.earthloyaltyprogramprovider.giift.beans.ExchangeRate;
import com.earth.earthloyaltyprogramprovider.giift.beans.GiiftCredential;
import com.earth.earthloyaltyprogramprovider.giift.beans.Pagination;
import com.earth.earthloyaltyprogramprovider.giift.beans.Points;
import com.earth.earthloyaltyprogramprovider.giift.beans.Transaction;
import com.earth.earthloyaltyprogramprovider.giift.beans.TransactionResponse;
import com.earth.earthloyaltyprogramprovider.giift.beans.VaildateTransactionResponse;
import com.earth.earthloyaltyprogramprovider.giift.enums.CancelStatusOption;
import com.earth.earthloyaltyprogramprovider.giift.exception.GiiftException;


public interface GiiftCustomerService
{

	public Optional<Points> getUsablePoints(GiiftCredential giiftCredential, Transaction transaction)
			throws GiiftException;

	public Optional<ExchangeRate> getPointExchangeRate(GiiftCredential giiftCredential) throws GiiftException;

	public Optional<VaildateTransactionResponse> validateTransaction(GiiftCredential giiftCredential, Transaction transaction)
			throws GiiftException;

	public Optional<TransactionResponse> createTransaction(GiiftCredential giiftCredential, Transaction transaction)
			throws GiiftException;

	public Optional<CustomerPointSummary> getCustomerPointSummary(GiiftCredential giiftCredential, String customerId)
			throws GiiftException;

	public Optional<CustomerPointLogs> getCustomerPointLogs(GiiftCredential giiftCredential, String customerId,
			final Pagination pagination) throws GiiftException;

	public Optional<CustomerQRCode> getCustomerQRCode(GiiftCredential giiftCredential, String customerId) throws GiiftException;

	public boolean isCustomerRegistered(GiiftCredential giiftCredential, String customerId) throws GiiftException;

	public boolean registerCustomer(GiiftCredential giiftCredential, Customer customer) throws GiiftException;

	public boolean cancelTransaction(GiiftCredential giiftCredential, String validateId, CancelStatusOption cancelStatusOption)
			throws GiiftException;


}
