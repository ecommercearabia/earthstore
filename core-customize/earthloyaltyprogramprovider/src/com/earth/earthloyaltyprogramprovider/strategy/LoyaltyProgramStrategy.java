/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.strategy;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.text.ParseException;
import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 *
 */
public interface LoyaltyProgramStrategy
{


	public Optional<LoyaltyUsablePoints> getUsablePoints(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<ValidateTransactionResult> validateTransactionByOrder(AbstractOrderModel order,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public boolean createTransaction(AbstractOrderModel order, LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyPagination pagination,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public boolean cancelTranscation(AbstractOrderModel order, LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public boolean editLoyaltyMobileNumber(CustomerModel customer, LoyaltyProgramProviderModel provider, String newNumber)
			throws EarthLoyaltyException;

	public boolean editLoyaltyEmail(CustomerModel customer, LoyaltyProgramProviderModel provider, String newEmail)
			throws EarthLoyaltyException;


	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	public void reserve(AbstractOrderModel order, double orderTotalAmount, LoyaltyProgramProviderModel loyaltyProgramProviderModel)
			throws EarthLoyaltyException;

	/**
	 *
	 */
	Optional<ValidateTransactionResult> validateTransaction(AbstractOrderModel order, double orderTotalAmount,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;




	public boolean isRegister(CustomerModel customer, LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public boolean registerCustomer(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException, ParseException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			BaseStoreModel baseStoreModel) throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> addPoints(ConsignmentModel consignmentModel,
			LoyaltyProgramProviderModel loyaltyProgramProviderModel) throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> redeemPoints(AbstractOrderModel abstractOrderModel, double newRedeemPoints,
			LoyaltyProgramProviderModel provider) throws EarthLoyaltyException;

	public Optional<LoyaltyBalance> updateRedeemPoints(final AbstractOrderModel abstractOrderModel, double newRedeemPoints,
			final LoyaltyProgramProviderModel loyaltyProgramProviderModel) throws EarthLoyaltyException;

	public Optional<Object> getLoyaltyCustomerInfo(CustomerModel customer, LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException;

	public Optional<Object> getTransactionHistory(final CustomerModel customerModel,
			final LoyaltyProgramProviderModel loyaltyProgramProviderModel) throws EarthLoyaltyException;

	/**
	 *
	 */
	public void orderCancelRefund(LoyaltyProgramProviderModel provider, OrderCancelResponse orderCancelResponse)
			throws EarthLoyaltyException;


	/**
	 *
	 */
	public void orderReturnRefund(LoyaltyProgramProviderModel provider, ReturnRequestModel returnRequestModel)
			throws EarthLoyaltyException;


}
