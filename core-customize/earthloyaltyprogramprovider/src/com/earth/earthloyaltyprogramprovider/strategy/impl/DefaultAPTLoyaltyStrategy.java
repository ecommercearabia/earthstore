/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.DiscountValue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import javax.annotation.Resource;
import javax.ws.rs.NotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditEmailRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.EditPhoneRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RedemptionRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.RegisterNewUserRequestBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.ActivateUserResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.AddPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CheckPointsResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CustomerTransactionHistoryResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.EditBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.RedemptionResponseBean;
import com.earth.earthloyaltyprogramprovider.apt.exception.APTLoyaltyException;
import com.earth.earthloyaltyprogramprovider.apt.service.APTLoyaltyService;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyPagination;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyUsablePoints;
import com.earth.earthloyaltyprogramprovider.beans.ValidateTransactionResult;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyRecordOperationStatus;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyRecordOperationType;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.earth.earthloyaltyprogramprovider.exception.type.EarthLoyaltyExceptionType;
import com.earth.earthloyaltyprogramprovider.giift.beans.CustomerQRCode;
import com.earth.earthloyaltyprogramprovider.model.APTLoyaltyProgramProviderModel;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyRecordOperationHistoryEntryModel;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyValidationService;
import com.earth.earthloyaltyprogramprovider.strategy.LoyaltyProgramStrategy;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



/**
 *
 */
public class DefaultAPTLoyaltyStrategy implements LoyaltyProgramStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultAPTLoyaltyStrategy.class);
	private static final String LOYALTY_PROGRAM_PROVIDER_NULL_MSG = "loyalty program provider is null";
	private static final String ABSTRACT_ORDER_NULL_MSG = "order is null";
	private static final String CUSTOMER_ID_EMPTY_MSG = "customerId  is null or empty";
	private static final String TRANSACTION_VAILDATE_ID = "transactionVaildateId  is null or empty";
	private static final String BASE_STORE_NULL_MSG = "baseStore  is null";
	private static final String CUSTOMER_NULL_MSG = "customer is null";
	private static final String BASESTORE_NULL_MSG = "basestore is null";

	private static final String CONSIGNMENT_NULL_MSG = "consignment  is null";
	private static final String ORDER_CONSIGNMENT_NULL_MSG = "Order on the consignment  is null";
	private static final String USER_ORDER_CONSIGNMENT_NULL_MSG = "User on the Order on the consignment  is null";
	private static final String USER_IS_NOT_CUSTOMER = "User on the Order on the consignment is not a customer";


	private static final String LOG_ERROR_MSG = "[DefaultAPTLoyaltyStrategy]: [{}]";
	private static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	private static final String DATE_FORMAT_DD_MM_YYYY = "dd/mm/yyyy";
	private static final String TRANSACTION_NO = "test";
	private static final String STATUS_SUCCESS = "0";
	private static final String OPERATION_IS_NOT_SUPPORTED = "Operation is not supported";
	private static final String ORDER_CANCEL_RESPONSE_NULL_MSG = "Order cancel response is null";



	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "loyaltyValidationService")
	private LoyaltyValidationService loyaltyValidationService;

	@Resource(name = "aptLoyaltyService")
	private APTLoyaltyService aptLoyaltyService;


	@Resource(name = "aptConsignmentAddPointsRequestBeanConverter")
	private Converter<ConsignmentModel, AddPointsRequestBean> aptConsignmentAddPointsRequestBeanConverter;

	@Resource(name = "aptOrderCancelResponseAddPointsRequestBeanConverter")
	private Converter<OrderCancelResponse, AddPointsRequestBean> aptOrderCancelResponseAddPointsRequestBeanConverter;

	@Resource(name = "aptOrderReturnResponseAddPointsRequestBeanConverter")
	private Converter<ReturnRequestModel, AddPointsRequestBean> aptOrderReturnResponseAddPointsRequestBeanConverter;

	@Resource(name = "aptOrderReturnFromResponseAddPointsRequestBeanConverter")
	private Converter<ReturnRequestModel, AddPointsRequestBean> aptOrderReturnFromResponseAddPointsRequestBeanConverter;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;



	@Resource(name = "aptAbstractOrderModelRedemptionRequestBeanConverter")
	private Converter<AbstractOrderModel, RedemptionRequestBean> aptAbstractOrderModelRedemptionRequestBeanConverter;

	@Resource(name = "aptCustomerRegisterNewUserRequestBeanConverter")
	private Converter<CustomerModel, RegisterNewUserRequestBean> aptCustomerRegisterNewUserRequestBeanConverter;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	@Resource(name = "aptOrderAddPointsRequestBeanConverter")
	private Converter<AbstractOrderModel, AddPointsRequestBean> aptOrderAddPointsRequestBeanConverter;


	private static Gson gson;
	static
	{
		gson = new GsonBuilder().setPrettyPrinting().create();
	}




	/**
	 * @return the aptConsignmentAddPointsRequestBeanConverter
	 */
	protected Converter<ConsignmentModel, AddPointsRequestBean> getAptConsignmentAddPointsRequestBeanConverter()
	{
		return aptConsignmentAddPointsRequestBeanConverter;
	}

	/**
	 * @return the aptLoyaltyService
	 */
	protected APTLoyaltyService getAptLoyaltyService()
	{
		return aptLoyaltyService;
	}

	/**
	 * @return the aptOrderAddPointsRequestBeanConverter
	 */
	protected Converter<AbstractOrderModel, AddPointsRequestBean> getAptOrderAddPointsRequestBeanConverter()
	{
		return aptOrderAddPointsRequestBeanConverter;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @return the cmsSiteService
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the aptOrderReturnResponseAddPointsRequestBeanConverter
	 */
	protected Converter<ReturnRequestModel, AddPointsRequestBean> getAptOrderReturnResponseAddPointsRequestBeanConverter()
	{
		return aptOrderReturnResponseAddPointsRequestBeanConverter;
	}

	/**
	 * @return the aptOrderReturnFromResponseAddPointsRequestBeanPopulator
	 */
	protected Converter<ReturnRequestModel, AddPointsRequestBean> getAptOrderReturnFromResponseAddPointsRequestBeanConverter()
	{
		return aptOrderReturnFromResponseAddPointsRequestBeanConverter;
	}

	/**
	 * @return the aptOrderCancelResponseAddPointsRequestBeanConverter
	 */
	protected Converter<OrderCancelResponse, AddPointsRequestBean> getAptOrderCancelResponseAddPointsRequestBeanConverter()
	{
		return aptOrderCancelResponseAddPointsRequestBeanConverter;
	}

	@Override
	public boolean isRegister(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		LOG.info("chceking if the customer is registered");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);

		CheckPointsResponseBean checkPointsResponseBean = null;

		try
		{

			LOG.info("chceking if the customer is registered: customer uid[{}], mobileNumber [{}] , baseUrl [{}]  ...",
					customer.getUid(), customer.getMobileNumber(), provider.getBaseUrl());

			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), TRANSACTION_NO,
					customer.getMobileNumber());

			final String requestBody = gson.toJson(checkPointsResponseBean);

			LOG.info(
					"chceking if the customer is registered result: customer uid[{}], mobileNumber [{}] , baseUrl [{}] , response[{}]  ...",
					customer.getUid(), customer.getMobileNumber(), provider.getBaseUrl(), requestBody);

			final String responseBody = gson.toJson(checkPointsResponseBean);


			if (checkPointsResponseBean != null && STATUS_SUCCESS.equals(checkPointsResponseBean.getReturnCode()))
			{
				LOG.info("The customer is registered result : customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), requestBody, responseBody);
				saveAPTLoyaltyBarCode(customer, checkPointsResponseBean.getBarCode());
				return true;
			}
			else
			{
				LOG.error(
						"Register customer for points Loyalty is Failed: customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), requestBody, responseBody);
				return false;
			}
		}
		catch (final APTLoyaltyException e)
		{

			LOG.error("Error occured while checking if the customer is registered. Error of type [{}], message [{}],  response [{}]",
					e.getType(), e.getMessage(), e.getRequestData(), e.getResponseData());

			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);

		}
	}


	private APTLoyaltyProgramProviderModel getAPTProvider(final LoyaltyProgramProviderModel providerModel)
	{
		if (!(providerModel instanceof APTLoyaltyProgramProviderModel))
		{
			LOG.error("LoyaltyProgramProvider [{}] is not supported", providerModel.getCode());
			throw new NotSupportedException("LoyaltyProgramProvider is not supported");
		}
		return (APTLoyaltyProgramProviderModel) providerModel;
	}

	@Override
	public boolean registerCustomer(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException, ParseException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_ID_EMPTY_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		LOG.info("Registering customer uid[{}] ...", customer.getUid());

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);

		loyaltyValidationService.validateLoyaltyEnabledForRegistrationByCurrentStore(customer);

		if (isAlreadyRregistered(customer, provider))
		{
			saveAlreadyExistingLoyaltyCustomer(customer, provider);
			setRegisterUserGroup(customer);
			return true;
		}

		final RegisterNewUserRequestBean registerNewUserRequestBean = aptCustomerRegisterNewUserRequestBeanConverter
				.convert(customer);
		ActivateUserResponseBean activateUserResponseBean = null;

		try
		{

			final String requestBody = gson.toJson(registerNewUserRequestBean);

			LOG.info("Registering customer for points Loyalty : customer uid[{}] ,baseUrl [{}] ,request [{}] ", customer.getUid(),
					aptProvider.getBaseUrl(), requestBody);


			activateUserResponseBean = getAptLoyaltyService().registerAndActivateNewUserResponse(provider.getBaseUrl(),
					registerNewUserRequestBean);


			final String responseBody = gson.toJson(activateUserResponseBean);

			saveRecord(customer, LoyaltyRecordOperationType.REGISTER_ACTIVATION_CUSTOMER, LoyaltyRecordOperationStatus.SUCCESS,
					requestBody, responseBody, provider.getBaseUrl(), null, null);

			if (activateUserResponseBean != null && STATUS_SUCCESS.equals(activateUserResponseBean.getReturnCode()))
			{
				LOG.info("Registered customer for points Loyalty : customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), requestBody, responseBody);

				saveAPTLoyaltyBarCode(customer, activateUserResponseBean.getBarcode());
				setRegisterUserGroup(customer);

				return true;

			}
			else
			{
				LOG.error(
						"Register customer for points Loyalty is Failed: customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), requestBody, responseBody);
				return false;
			}
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while registering customer. Error of type [{}], message [{}],  response [{}]", e.getType(),
					e.getMessage(), e.getRequestData(), e.getResponseData());
			saveRecord(customer, LoyaltyRecordOperationType.REGISTER_ACTIVATION_CUSTOMER, LoyaltyRecordOperationStatus.FAILED,
					e.getRequestData(), e.getResponseData(), provider.getBaseUrl(), null, null);
			throw new EarthLoyaltyException(e.getMessage(), activateUserResponseBean, EarthLoyaltyExceptionType.APT);
		}

	}

	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	private void saveAlreadyExistingLoyaltyCustomer(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);

		CheckPointsResponseBean checkPointsResponseBean = null;
		try
		{

			LOG.info("Getting new customer info : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] ", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl());

			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), customer.getMobileNumber(),
					TRANSACTION_NO);

			final String responseBody = gson.toJson(checkPointsResponseBean);

			LOG.info("Got new customer info : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			saveRecord(customer, LoyaltyRecordOperationType.REGISTER_ACTIVATION_CUSTOMER, LoyaltyRecordOperationStatus.SUCCESS, null,
					responseBody, provider.getBaseUrl(), null, null);

			if (checkPointsResponseBean != null && STATUS_SUCCESS.equals(checkPointsResponseBean.getReturnCode()))
			{
				LOG.info("Registered customer for points Loyalty : customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), null, responseBody);

				saveAPTLoyaltyBarCode(customer, checkPointsResponseBean.getBarCode());
			}
			else
			{
				LOG.error(
						"Register customer for points Loyalty is Failed: customer uid[{}] ,baseUrl [{}] ,request [{}], response[{}]",
						customer.getUid(), aptProvider.getBaseUrl(), null, responseBody);

			}
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while getting new customer info. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}

	}

	/**
	 *
	 */
	private boolean isAlreadyRregistered(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
	{
		try
		{
			final CheckPointsResponseBean checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(),
					customer.getMobileNumber(), TRANSACTION_NO);
			return (checkPointsResponseBean != null && STATUS_SUCCESS.equals(checkPointsResponseBean.getReturnCode()));



		}
		catch (final Exception e)
		{
			return false;
		}
	}

	/**
	 *
	 */
	private void saveAPTLoyaltyBarCode(final CustomerModel customer, final String barcode)
	{
		LOG.info("saveing APT Loyalty BarCode for a customer: customer uid[{}] ,barcode[{}]", customer.getUid(), barcode);
		try
		{
			//getModelService().refresh(customer.getUid());
			customer.setInvolvedInLoyaltyProgram(true);
			customer.setAptLoyaltyBarCode(barcode);

			getModelService().save(customer);
			LOG.info("APT Loyalty BarCode for a customer is saved: customer uid[{}] ,barcode[{}]", customer.getUid(), barcode);
		}
		catch (final Exception e)
		{
			LOG.error("Error for saveing APT Loyalty BarCode for a customer  error[{}]: customer uid[{}] ,barcode[{}] ",
					e.getMessage(), customer.getUid(), barcode);
		}


	}

	private void saveRecord(final CustomerModel customer, final LoyaltyRecordOperationType type,
			final LoyaltyRecordOperationStatus status, final String request, final String response, final String url,
			final AbstractOrderModel order, final ConsignmentModel consignment)
	{

		final String orderCode = order != null ? order.getCode() : null;
		final String consignmentCode = consignment != null ? consignment.getCode() : null;

		LOG.info(
				"saveing Loyalty Record Operation: type [{}] ,status [{}] , request [{}] , response[{}] ,baseUrl [{}], customer uid[{}] ,order[{}] , consignment[{}] ",
				type, status, request, response, url, customer.getUid(), orderCode, consignmentCode);

		try
		{
			final LoyaltyRecordOperationHistoryEntryModel recordOperation = getModelService()
					.create(LoyaltyRecordOperationHistoryEntryModel.class);

			recordOperation.setRequest(request);
			recordOperation.setResponse(response);
			recordOperation.setUrl(url);
			recordOperation.setType(type);
			recordOperation.setStatus(status);
			recordOperation.setOrder(order);
			recordOperation.setConsignment(consignment);
			recordOperation.setCustomer(customer);

			getModelService().saveAll(recordOperation);
			LOG.info(
					"Loyalty Record Operation is saved: type [{}] ,status [{}] , request [{}] , response[{}] ,baseUrl [{}], customer uid[{}] ,order[{}] , consignment[{}] ",
					type, status, request, response, url, customer.getUid(), orderCode, consignmentCode);
		}
		catch (final Exception e)
		{
			LOG.error(
					"Error for saveing Loyalty Record Operation error[{}]: type [{}] ,status [{}] , request [{}] , response[{}] ,baseUrl [{}], customer uid[{}] ,order[{}] , consignment[{}] ",
					e.getMessage(), type, status, request, response, url, customer.getUid(), orderCode, consignmentCode);
		}

	}

	private LoyaltyRecordOperationHistoryEntryModel generateRecord(final CustomerModel customer,
			final LoyaltyRecordOperationType type, final LoyaltyRecordOperationStatus status, final String request,
			final String response, final String url)
	{

		final LoyaltyRecordOperationHistoryEntryModel recordOperation = getModelService()
				.create(LoyaltyRecordOperationHistoryEntryModel.class);

		recordOperation.setRequest(request);
		recordOperation.setResponse(response);
		recordOperation.setUrl(url);
		recordOperation.setType(type);
		recordOperation.setStatus(status);

		return recordOperation;
	}

	@Override
	public Optional<LoyaltyUsablePoints> getUsablePoints(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	/**
	 * @return the aptAbstractOrderModelRedemptionRequestBeanConverter
	 */
	protected Converter<AbstractOrderModel, RedemptionRequestBean> getAptAbstractOrderModelRedemptionRequestBeanConverter()
	{
		return aptAbstractOrderModelRedemptionRequestBeanConverter;
	}

	/**
	 * @return the aptCustomerRegisterNewUserRequestBeanConverter
	 */
	protected Converter<CustomerModel, RegisterNewUserRequestBean> getAptCustomerRegisterNewUserRequestBeanConverter()
	{
		return aptCustomerRegisterNewUserRequestBeanConverter;
	}

	@Override
	public Optional<ValidateTransactionResult> validateTransactionByOrder(final AbstractOrderModel order,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	@Override
	public boolean createTransaction(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	@Override
	public Optional<LoyaltyCustomerInfo> getLoyaltyCustomerInfo(final CustomerModel customer, final LoyaltyPagination pagination,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	@Override
	public Optional<LoyaltyCustomerCode> getLoyaltyCustomerCode(final CustomerModel customer,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		final Optional<CustomerQRCode> customerQRCode;

		final LoyaltyCustomerCode customerCode = new LoyaltyCustomerCode();
		customerCode.setCustomerCode(customer.getAptLoyaltyBarCode());
		customerCode.setCustomerID(customer.getAptLoyaltyBarCode());
		customerCode.setQrCode(customer.getAptLoyaltyBarCode());
		return Optional.ofNullable(customerCode);
	}

	@Override
	public boolean cancelTranscation(final AbstractOrderModel order, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		LOG.info("Getting current loyalty balance.");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);

		CheckPointsResponseBean checkPointsResponseBean = null;
		try
		{

			LOG.info("Getting current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] ", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl());

			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), customer.getMobileNumber(),
					TRANSACTION_NO);

			final String responseBody = gson.toJson(checkPointsResponseBean);

			LOG.info("Got current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]",
					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			return getLoyaltyBalance(checkPointsResponseBean, provider);
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while getting loyalty balance. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}
	}

	/**
	 *
	 */
	private Optional<LoyaltyBalance> getLoyaltyBalance(final CheckPointsResponseBean checkPointsResponseBean,
			final LoyaltyProgramProviderModel provider)
	{
		if (checkPointsResponseBean == null)
		{
			return Optional.empty();
		}
		final LoyaltyBalance loyaltyBalance = new LoyaltyBalance();

		//TODO Juman
		loyaltyBalance.setPointExchangeRate(0);
		loyaltyBalance.setPoints(checkPointsResponseBean.getLoyaltyPoints());
		loyaltyBalance.setValue(checkPointsResponseBean.getAvailableAmount());

		return Optional.ofNullable(loyaltyBalance);
	}

	@Override
	public void reserve(final AbstractOrderModel abstractOrderModel, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		LOG.info("Redeeming and updating customer points");
		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel.getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((abstractOrderModel.getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);
		final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, abstractOrderModel.getStore());

		getModelService().refresh(abstractOrderModel);

		final Optional<LoyaltyBalance> balance = getLoyaltyBalance(customer, provider, abstractOrderModel.getStore());
		final double loyaltyAmount = abstractOrderModel.getLoyaltyAmount();

		if (balance.isEmpty() || abstractOrderModel.getLoyaltyPaymentMode() == null
				|| abstractOrderModel.getLoyaltyPaymentMode().getLoyaltyPaymentModeType() == null
				|| (LoyaltyPaymentModeType.REDEEM_NONE.equals(abstractOrderModel.getLoyaltyPaymentMode().getLoyaltyPaymentModeType())
						&& loyaltyAmount <= 0))
		{
			return;
		}

		final LoyaltyPaymentModeType loyaltyPaymentModeType = abstractOrderModel.getLoyaltyPaymentMode()
				.getLoyaltyPaymentModeType();

		final double totalBalanceLoyaltyAmount = balance.get().getValue() + abstractOrderModel.getLoyaltyAmount();

		final double loyaltyAmountSelected = getLoyaltyAmountSelected(loyaltyPaymentModeType,
				abstractOrderModel.getLoyaltyAmountSelected(), orderTotalAmount, totalBalanceLoyaltyAmount);

		abstractOrderModel.setLoyaltyAmountSelected(loyaltyAmountSelected);
		getModelService().save(abstractOrderModel);
		getModelService().refresh(abstractOrderModel);
		if (LoyaltyPaymentModeType.REDEEM_NONE.equals(loyaltyPaymentModeType))
		{
			addPoints(abstractOrderModel, loyaltyAmount, aptProvider);
		}
		else
		{
			if (loyaltyAmount == loyaltyAmountSelected)
			{
				return;
			}

			if (loyaltyAmount > loyaltyAmountSelected)
			{
				addPoints(abstractOrderModel, loyaltyAmount - loyaltyAmountSelected, aptProvider);
			}
			else
			{
				redeemPoints(abstractOrderModel, loyaltyAmountSelected - loyaltyAmount, aptProvider);
			}

		}

		abstractOrderModel.setLoyaltyAmount(loyaltyAmountSelected);
		abstractOrderModel.setShouldCalculateLoyalty(false);
		getModelService().save(abstractOrderModel);

	}

	/**
	 * @param orderTotalAmount
	 *
	 */
	private double getLoyaltyAmountSelected(final LoyaltyPaymentModeType loyaltyPaymentModeType, double loyaltyAmountSelected,
			final double orderTotalAmount, final double totalBalanceLoyaltyAmount)
	{

		if (LoyaltyPaymentModeType.REDEEM_NONE.equals(loyaltyPaymentModeType))
		{
			loyaltyAmountSelected = 0.0d;
		}

		if (LoyaltyPaymentModeType.REDEEM_FULL_AMOUNT.equals(loyaltyPaymentModeType))
		{
			loyaltyAmountSelected = orderTotalAmount;
		}


		return calculateLoyaltyAmount(loyaltyAmountSelected, totalBalanceLoyaltyAmount, orderTotalAmount);

	}

	private static double calculateLoyaltyAmount(double loyaltyAmountSelected, final double loyaltyAmount,
			final double totalOrderAmount)
	{

		if (loyaltyAmount == 0 || totalOrderAmount == 0)
		{
			return 0;
		}
		loyaltyAmountSelected = loyaltyAmountSelected <= loyaltyAmount ? loyaltyAmountSelected : loyaltyAmount;

		return loyaltyAmountSelected <= totalOrderAmount ? loyaltyAmountSelected : totalOrderAmount;

	}

	@Override
	public Optional<ValidateTransactionResult> validateTransaction(final AbstractOrderModel order, final double orderTotalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);
	}

	@Override
	public Optional<LoyaltyBalance> addPoints(final ConsignmentModel consignmentModel, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		LOG.info("Adding points");
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel), CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder()), ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder().getStore()), BASE_STORE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder().getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((consignmentModel.getOrder().getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);
		final CustomerModel customer = (CustomerModel) consignmentModel.getOrder().getUser();

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, consignmentModel.getOrder().getStore());

		if (consignmentModel.isLoyaltyPointsAdded())
		{
			//TODO LOG
			return getLoyaltyBalance(consignmentModel, provider);
		}
		final AbstractOrderModel order = consignmentModel.getOrder();
		final double loyaltyAmountToBeDeducted = consignmentModel.getOrder().getLoyaltyAmountToBeDeducted().doubleValue();
		final double totalAmountWithTax = getTotalAmountWithTax(consignmentModel);
		final double deductedLoyaltyAmount = consignmentModel.getOrder().getDeductedLoyaltyAmount();

		if (totalAmountWithTax - loyaltyAmountToBeDeducted <= 0)
		{
			order.setDeductedLoyaltyAmount(deductedLoyaltyAmount + totalAmountWithTax);

			//TODO LOG
			return getLoyaltyBalance(consignmentModel, provider);
		}






		final AddPointsRequestBean addPointsRequest = getAptConsignmentAddPointsRequestBeanConverter().convert(consignmentModel);
		order.setDeductedLoyaltyAmount(deductedLoyaltyAmount + loyaltyAmountToBeDeducted);
		addPointsRangeAdjust(addPointsRequest, provider.getRange());


		AddPointsResponseBean addPointsResponseBean = null;
		try
		{
			final String requestBody = gson.toJson(addPointsRequest);

			LOG.info("Adding points Loyalty : customer uid[{}] ,order[{}] , consignment[{}],baseUrl [{}] ,request [{}] ",
					customer.getUid(), consignmentModel.getOrder().getCode(), consignmentModel.getCode(), aptProvider.getBaseUrl(),
					requestBody);

			addPointsResponseBean = getAptLoyaltyService().addLoyaltyPoints(provider.getBaseUrl(), addPointsRequest);
			final String responseBody = gson.toJson(addPointsResponseBean);

			LOG.info(
					"Added points Loyalty : customer uid[{}] ,order[{}] , consignment[{}] ,baseUrl [{}] ,request [{}] ,response [{}] ",
					customer.getUid(), consignmentModel.getOrder().getCode(), consignmentModel.getCode(), aptProvider.getBaseUrl(),
					requestBody, responseBody);
			updateFlagConsignmentAndOrder(consignmentModel, addPointsResponseBean.getPointsEared(),
					addPointsResponseBean.getPointsEared() / provider.getReverseRange());

			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
					responseBody, provider.getBaseUrl(), consignmentModel.getOrder(), consignmentModel);
		}
		catch (final APTLoyaltyException e)
		{
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), consignmentModel.getOrder(), consignmentModel);

			LOG.error("Error occured while attempting to add points. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), addPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}

		return getLoyaltyBalance(consignmentModel, provider);
	}

	private double getTotalAmountWithTax(final ConsignmentModel source)
	{
		double totalAmountWithTax = 0.0d;

		for (final ConsignmentEntryModel entry : source.getConsignmentEntries())
		{

			if (entry.getQuantity() == null || entry.getQuantity() <= 0 || entry.getOrderEntry() == null)
			{
				continue;
			}
			final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();


			double discount = 0.0;


			if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
			{

				for (final DiscountValue value : orderEntry.getDiscountValues())
				{
					discount += value.getAppliedValue();
				}
				discount = discount / entry.getOrderEntry().getQuantity();

			}

			final double discountVal = discount * entry.getQuantity().doubleValue();
			final double totalPriceVal = (orderEntry.getBasePrice() - discountVal) * entry.getQuantity();

			totalAmountWithTax += totalPriceVal;

		}

		return totalAmountWithTax;
	}


	/**
	 * @param consignmentModel
	 * @param addedLoyaltyPoints
	 * @param addedLoyaltyAmount
	 *
	 */
	private void updateFlagConsignmentAndOrder(final ConsignmentModel consignmentModel, final double addedLoyaltyPoints,
			final double addedLoyaltyAmount)
	{
		consignmentModel.setLoyaltyPointsAdded(true);
		consignmentModel.setAddedLoyaltyPoints(addedLoyaltyPoints + consignmentModel.getAddedLoyaltyPoints());
		consignmentModel.setAddedLoyaltyAmount(addedLoyaltyAmount + consignmentModel.getAddedLoyaltyAmount());
		getModelService().save(consignmentModel);
		updateFlagOrder(consignmentModel.getOrder(), addedLoyaltyPoints, addedLoyaltyAmount);
	}

	/**
	 * @param orderModel
	 * @param addedLoyaltyPoints
	 * @param addedLoyaltyAmount
	 *
	 */
	private void updateFlagOrder(final AbstractOrderModel orderModel, final double addedLoyaltyPoints,
			final double addedLoyaltyAmount)
	{
		orderModel.setLoyaltyPointsAdded(true);
		orderModel.setAddedLoyaltyPoints(addedLoyaltyPoints + orderModel.getAddedLoyaltyPoints());
		orderModel.setAddedLoyaltyAmount(addedLoyaltyAmount + orderModel.getAddedLoyaltyAmount());
		getModelService().save(orderModel);
	}

	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	private Optional<LoyaltyBalance> getLoyaltyBalance(final ConsignmentModel consignmentModel,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		LOG.info("Getting current loyalty balance.");
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel), CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder()), ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder().getStore()), BASE_STORE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(consignmentModel.getOrder().getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((consignmentModel.getOrder().getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		final CustomerModel customer = (CustomerModel) consignmentModel.getOrder().getUser();
		loyaltyValidationService.validateLoyaltyEnabled(customer, consignmentModel.getOrder().getStore());

		CheckPointsResponseBean checkPointsResponseBean = null;
		try
		{

			LOG.info("Getting current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] ", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl());

			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), customer.getMobileNumber(),
					TRANSACTION_NO);

			final String responseBody = gson.toJson(checkPointsResponseBean);

			LOG.info("Got current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]",
					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			return getLoyaltyBalance(checkPointsResponseBean, provider);
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while getting loyalty balance. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}
	}

	/**
	 *
	 */
	private void addPointsRangeAdjust(final AddPointsRequestBean addPointsRequest, final double range)
	{
		final List<PurchaseDetailBean> details = addPointsRequest.getPurchaseDetails();

		if (addPointsRequest == null || details == null || details.isEmpty())
		{
			return;

		}

		addPointsRequest.setTotalBillAmount(addPointsRequest.getTotalBillAmount() * range);
		addPointsRequest.setTotalBillAmountInclVAT(addPointsRequest.getTotalBillAmountInclVAT() * range);

		for (final PurchaseDetailBean bean : details)
		{

			bean.setAmount(bean.getAmount() * range);
			bean.setTotalBillAmount(bean.getTotalBillAmount() * range);
		}

	}

	private Optional<LoyaltyBalance> addPoints(final AbstractOrderModel order, final double totalAmount,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		LOG.info("Adding points");
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order), ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(order.getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((order.getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);
		final CustomerModel customer = (CustomerModel) order.getUser();

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, order.getStore());
		//		final AddPointsRequestBean addPointsRequest = getAptOrderAddPointsRequestBeanConverter().convert(order);


		final AddPointsRequestBean addPointsRequest = getAddPointsRequestBean(order, totalAmount);
		AddPointsResponseBean addPointsResponseBean = null;
		try
		{
			final String requestBody = gson.toJson(addPointsRequest);

			LOG.info(
					"Adding points Loyalty : customer uid[{}] ,order[{}] , consignment[{}] , totalAmount[{}] ,baseUrl [{}] ,request [{}] ",
					customer.getUid(), order.getCode(), order.getCode(), totalAmount, aptProvider.getBaseUrl(), requestBody);

			addPointsResponseBean = getAptLoyaltyService().addLoyaltyPoints(provider.getBaseUrl(), addPointsRequest);
			final String responseBody = gson.toJson(addPointsResponseBean);

			LOG.info(
					"Added points Loyalty : customer uid[{}] ,order[{}] , totalAmount[{}] ,baseUrl [{}] ,request [{}] ,response [{}] ",
					customer.getUid(), order.getCode(), totalAmount, aptProvider.getBaseUrl(), requestBody, responseBody);

			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
					responseBody, provider.getBaseUrl(), order, null);
		}
		catch (final APTLoyaltyException e)
		{
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), order, null);
			LOG.error("Error occured while attempting to add points. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), addPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}

		return getLoyaltyBalance(customer, provider);
	}

	/**
	 *
	 */
	private AddPointsRequestBean getAddPointsRequestBean(final AbstractOrderModel order, final double totalAmount)
	{
		if (order.getUser() == null || !(order.getUser() instanceof CustomerModel))
		{
			return null;
		}

		final AddPointsRequestBean target = new AddPointsRequestBean();

		final String receiptNo = getReceiptNo(order);
		final CustomerModel customer = (CustomerModel) order.getUser();

		target.setLoyaltyCardNo(customer.getAptLoyaltyBarCode());
		target.setMobileNumber(customer.getMobileNumber());
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setStaffCode("WEB");
		target.setReceiptNo(receiptNo);
		target.setTransactionNo(receiptNo);

		final double totalPoints = totalAmount * 100;

		target.setTotalBillAmountInclVAT(totalPoints);

		target.setTotalBillAmount(totalPoints);

		target.setPurchaseDetails(getPurchaseDetails(totalPoints, customer));

		return target;
	}

	private List<PurchaseDetailBean> getPurchaseDetails(final double totalPoints, final CustomerModel customer)
	{
		final PurchaseDetailBean purchaseDetailBean = new PurchaseDetailBean();
		final List<PurchaseDetailBean> purchaseList = new ArrayList<>();

		purchaseDetailBean.setAmount(totalPoints);
		purchaseDetailBean.setBarCode(customer.getAptLoyaltyBarCode());
		purchaseDetailBean.setReceiptNo(" ");
		purchaseDetailBean.setProductId("");
		purchaseDetailBean.setInvoiceNo(" ");
		purchaseDetailBean.setStore("ECOM");
		purchaseDetailBean.setTerminal("WEB");
		purchaseDetailBean.setStaffId("WEB");
		purchaseDetailBean.setDescriptionArab(" ");
		purchaseDetailBean.setDescriptionEng(" ");
		purchaseDetailBean.setQuantity(1);
		purchaseDetailBean.setTotalBillAmount(totalPoints);
		purchaseDetailBean.setItemAmount(totalPoints);
		purchaseList.add(purchaseDetailBean);
		return purchaseList;
	}

	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}

	/**
	 *
	 */

	@Override
	public Optional<LoyaltyBalance> updateRedeemPoints(final AbstractOrderModel abstractOrderModel, final double newRedeemPoints,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		//		LOG.info("Redeeming and updating customer points");
		//		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel), ABSTRACT_ORDER_NULL_MSG);
		//		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		//		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel.getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		//		Preconditions.checkArgument((abstractOrderModel.getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);
		//		final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
		//		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		//		loyaltyValidationService.validateLoyaltyEnabledByCurrentStore(customer);
		//
		//		Optional<LoyaltyBalance> balance = getLoyaltyBalance(customer, provider);
		//		final RedemptionRequestBean redemptionRequestBean = aptAbstractOrderModelRedemptionRequestBeanConverter
		//				.convert(abstractOrderModel);
		//		RedemptionResponseBean redemptionResponseBean = null;
		//		try
		//		{
		//
		//			LOG.info("Redeeming loyalty points : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}], amount[{}] ", customer.getUid(),
		//					customer.getMobileNumber(), aptProvider.getBaseUrl(), newRedeemPoints);
		//
		//			if (abstractOrderModel.getLoyaltyAmount() > 0)
		//			{
		//				balance = addPoints(abstractOrderModel, abstractOrderModel.getTotalPrice(), provider);
		//
		//			}
		//
		//			if (balance.get().getPoints() >= newRedeemPoints)
		//			{
		//				redemptionResponseBean = aptLoyaltyService.redeemLoyaltyPoints(provider.getBaseUrl(), redemptionRequestBean);
		//			}
		//			else
		//			{
		//				throw new EarthLoyaltyException(EarthLoyaltyExceptionType.INSUFFECIENT_AMOUNT.getMsg(), balance,
		//						EarthLoyaltyExceptionType.INSUFFECIENT_AMOUNT);
		//			}
		//
		//			LOG.info("Redeemed loyalty points : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}], amount[{}] , response [{}]",
		//					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), newRedeemPoints, redemptionResponseBean);
		//
		//		}
		//		catch (final APTLoyaltyException e)
		//		{
		//			LOG.error(
		//					"Error occured while attempting to redeem points. Error of type" + e.getType() + " reason is:" + e.getMessage());
		//			throw new EarthLoyaltyException(e.getMessage(), redemptionResponseBean, EarthLoyaltyExceptionType.APT);
		//		}
		//
		//		return balance;
		throw new UnsupportedOperationException(OPERATION_IS_NOT_SUPPORTED);

	}

	@Override
	public Optional<LoyaltyBalance> redeemPoints(final AbstractOrderModel abstractOrderModel, final double newRedeemPoints,
			final LoyaltyProgramProviderModel provider) throws EarthLoyaltyException
	{
		LOG.info("Redeeming customer points");
		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(abstractOrderModel.getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((abstractOrderModel.getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);
		final CustomerModel customer = (CustomerModel) abstractOrderModel.getUser();
		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, abstractOrderModel.getStore());

		final Optional<LoyaltyBalance> balance = getLoyaltyBalance(customer, provider);
		final RedemptionRequestBean redemptionRequestBean = getRedemptionRequestBean(abstractOrderModel, newRedeemPoints);

		final String requestBody = gson.toJson(redemptionRequestBean);

		RedemptionResponseBean redemptionResponseBean = null;
		try
		{
			if (balance.get().getPoints() >= newRedeemPoints)
			{

				LOG.info("Redeeming loyalty points : customer uid[{}]  , baseUrl [{}], amount[{}] ,request [{}]", customer.getUid(),
						aptProvider.getBaseUrl(), newRedeemPoints, requestBody);

				redemptionResponseBean = aptLoyaltyService.redeemLoyaltyPoints(provider.getBaseUrl(), redemptionRequestBean);


				final String responseBody = gson.toJson(redemptionResponseBean);

				LOG.info("Redeemed loyalty points : customer uid[{}], baseUrl [{}], amount[{}] ,request [{}] , response [{}]",
						customer.getUid(), aptProvider.getBaseUrl(), newRedeemPoints, requestBody, responseBody);
				saveRecord(customer, LoyaltyRecordOperationType.REDEEM_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
						responseBody, provider.getBaseUrl(), abstractOrderModel, null);


			}
			else
			{
				LOG.info("Redeemed loyalty points : customer uid[{}], baseUrl [{}], amount[{}] ,request [{}] , response [{}]",
						customer.getUid(), aptProvider.getBaseUrl(), newRedeemPoints, requestBody,
						"INSUFFECIENT_AMOUNT balance[" + balance.get().getPoints() + "]");

				saveRecord(customer, LoyaltyRecordOperationType.REDEEM_POINTS, LoyaltyRecordOperationStatus.FAILED, requestBody,
						"INSUFFECIENT_AMOUNT balance[" + balance.get().getPoints() + "]", provider.getBaseUrl(), abstractOrderModel,
						null);
				abstractOrderModel.setLoyaltyAmountSelected(0);
				throw new EarthLoyaltyException(EarthLoyaltyExceptionType.INSUFFECIENT_AMOUNT.getMsg(), balance,
						EarthLoyaltyExceptionType.INSUFFECIENT_AMOUNT);
			}
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error(
					"Error occured while attempting to redeem points. Error of type" + e.getType() + " reason is:" + e.getMessage());

			saveRecord(customer, LoyaltyRecordOperationType.REDEEM_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), abstractOrderModel, null);
			abstractOrderModel.setLoyaltyAmountSelected(0);
			throw new EarthLoyaltyException(e.getMessage(), redemptionResponseBean, EarthLoyaltyExceptionType.APT);
		}

		return balance;

	}


	/**
	 *
	 */
	private RedemptionRequestBean getRedemptionRequestBean(final AbstractOrderModel source, final double newRedeemPoints)
	{
		final RedemptionRequestBean target = new RedemptionRequestBean();
		final CustomerModel customer = (CustomerModel) source.getUser();
		final String receiptNo = getReceiptNo(source);

		target.setBarCode(customer.getAptLoyaltyBarCode());
		target.setAmount(newRedeemPoints);
		target.setCountryId(1);
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setTransactionNo(receiptNo);
		target.setReceiptNo(receiptNo);
		target.setInvoiceNo(receiptNo);

		return target;
	}

	@Override
	public Optional<Object> getTransactionHistory(final CustomerModel customerModel, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		LOG.info("Getting customer transaction history.");
		Preconditions.checkArgument(!Objects.isNull(customerModel), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		CustomerTransactionHistoryResponseBean customerTransactionHistoryResponseBean = null;
		try
		{

			LOG.info("Getting transaction history info : customer uid[{}], baseUrl [{}]", customerModel.getUid(),
					aptProvider.getBaseUrl());
			customerTransactionHistoryResponseBean = getAptLoyaltyService().customerTransactionHistory(provider.getBaseUrl(),
					customerModel.getMobileNumber());

			LOG.info("Got transaction history info : customer uid[{}], baseUrl [{}], response[{}]", customerModel.getUid(),
					aptProvider.getBaseUrl(), customerTransactionHistoryResponseBean);

			if (customerTransactionHistoryResponseBean == null)
			{
				return Optional.empty();

			}
			else
			{
				return Optional.ofNullable(customerTransactionHistoryResponseBean);
			}

		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while checking customer itransaction history. Error of type" + e.getType() + " reason is:"
					+ e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), customerTransactionHistoryResponseBean, EarthLoyaltyExceptionType.APT);
		}


	}

	@Override
	public Optional<Object> getLoyaltyCustomerInfo(final CustomerModel customer, final LoyaltyProgramProviderModel provider)
			throws EarthLoyaltyException
	{
		LOG.info("Getting loyalty info for the customer.");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);


		CheckPointsResponseBean checkPointsResponseBean = null;
		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		try
		{
			LOG.info("Getting loyalty customer info : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}]", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl());
			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), TRANSACTION_NO,
					customer.getMobileNumber());
			final String responseBody = gson.toJson(checkPointsResponseBean);
			LOG.info("Got loyalty customer info : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}], response[{}]",
					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			return Optional.ofNullable(checkPointsResponseBean);
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while checking customer loyalty info. Error of type" + e.getType() + " reason is:"
					+ e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}

	}

	@Override
	public void orderCancelRefund(final LoyaltyProgramProviderModel provider, final OrderCancelResponse orderCancelResponse)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(orderCancelResponse), ORDER_CANCEL_RESPONSE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(orderCancelResponse.getOrder()), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(orderCancelResponse.getOrder().getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((orderCancelResponse.getOrder().getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);

		if (CollectionUtils.isEmpty(orderCancelResponse.getEntriesToCancel()))
		{
			LOG.info("no entriesto cancel.");

			return;
		}

		final double loyaltyAmount = orderCancelResponse.getOrder().getLoyaltyAmount();

		if (loyaltyAmount <= 0)
		{
			LOG.info("Loyalty amount is equal to or else than zero.");

			return;
		}
		final double refundedLoyaltyAmount = orderCancelResponse.getOrder().getRefundedLoyaltyAmount();
		final double loyaltyAvalaibleRefundAmount = loyaltyAmount - refundedLoyaltyAmount;

		if (loyaltyAvalaibleRefundAmount <= 0)
		{

			LOG.info("Loyalty refund amount is equal to or else than zero.");

			return;
		}

		final double calcOrderCancelRefundAmount = calcOrderCancelRefund(orderCancelResponse);

		if (calcOrderCancelRefundAmount <= 0)
		{
			LOG.info("Loyalty calcOrderCancelRefundAmount is equal to or less than zero.");

			return;
		}
		final CustomerModel customer = (CustomerModel) orderCancelResponse.getOrder().getUser();

		final double loyaltyRefundAmount = calcOrderCancelRefundAmount <= loyaltyAvalaibleRefundAmount ? calcOrderCancelRefundAmount
				: loyaltyAvalaibleRefundAmount;

		final AddPointsRequestBean addPointsRequestBean = getAddPointsRequestBeanForOrderCancelRefund(loyaltyRefundAmount,
				orderCancelResponse, provider);
		final String requestBody = gson.toJson(addPointsRequestBean);

		LOG.info("Adding points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ", customer.getUid(),
				orderCancelResponse.getOrder().getCode(), provider.getBaseUrl(), requestBody);
		try
		{
			final AddPointsResponseBean addLoyaltyPoints = getAptLoyaltyService().addLoyaltyPoints(provider.getBaseUrl(),
					addPointsRequestBean);
			final String responseBody = gson.toJson(addLoyaltyPoints);

			LOG.info("Added points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ,response [{}] ",
					customer.getUid(), orderCancelResponse.getOrder().getCode(), provider.getBaseUrl(), requestBody, responseBody);

			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
					responseBody, provider.getBaseUrl(), orderCancelResponse.getOrder(), null);
		}
		catch (final APTLoyaltyException e)
		{
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), orderCancelResponse.getOrder(), null);

			LOG.error("Error occured while attempting to add points. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), addPointsRequestBean, EarthLoyaltyExceptionType.APT);
		}

	}

	/**
	 * @param orderCancelResponse
	 *
	 */
	private AddPointsRequestBean getAddPointsRequestBeanForOrderCancelRefund(final double loyaltyRefundAmount,
			final OrderCancelResponse orderCancelResponse, final LoyaltyProgramProviderModel provider)
	{
		final AddPointsRequestBean addPointsRequestBean = getAptOrderCancelResponseAddPointsRequestBeanConverter()
				.convert(orderCancelResponse);

		addPointsRangeAdjust(addPointsRequestBean, provider.getReverseRange());
		return addPointsRequestBean;
	}

	private AddPointsRequestBean getAddPointsRequestBeanForOrderReturnRefund(final double loyaltyRefundAmount,
			final ReturnRequestModel returnRequestModel, final LoyaltyProgramProviderModel provider)
	{
		final AddPointsRequestBean addPointsRequestBean = getAptOrderReturnResponseAddPointsRequestBeanConverter()
				.convert(returnRequestModel);

		addPointsRangeAdjust(addPointsRequestBean, provider.getReverseRange());
		return addPointsRequestBean;
	}

	private AddPointsRequestBean getAddPointsRequestBeanFromOrderReturnRefund(final double loyaltyRefundAmount,
			final ReturnRequestModel returnRequestModel, final LoyaltyProgramProviderModel provider)
	{
		final AddPointsRequestBean addPointsRequestBean = getAptOrderReturnFromResponseAddPointsRequestBeanConverter()
				.convert(returnRequestModel);
		final double negativeRange = -provider.getRange();
		addPointsRangeAdjust(addPointsRequestBean, negativeRange);
		return addPointsRequestBean;
	}


	/**
	 *
	 */
	private double calcOrderCancelRefund(final OrderCancelResponse orderCancelResponse)
	{
		double total = 0.0d;
		for (final OrderCancelEntry orderCancelEntry : orderCancelResponse.getEntriesToCancel())
		{
			if (orderCancelEntry.getCancelQuantity() <= 0 || orderCancelEntry.getOrderEntry().getTotalPrice() <= 0)
			{
				LOG.info(
						"Calculating cancel refund amount, value is not enough : cancel quantity[{}] ,total price[{}] , order quantity [{}]",
						orderCancelEntry.getCancelQuantity(), orderCancelEntry.getOrderEntry().getTotalPrice(),
						orderCancelEntry.getOrderEntry().getQuantity());
				continue;
			}


			final long cancelQuantity = orderCancelEntry.getCancelQuantity();
			final Long quantity = orderCancelEntry.getOrderEntry().getQuantity() + cancelQuantity;
			final Double totalPrice = orderCancelEntry.getOrderEntry().getTotalPrice();

			total += (totalPrice / quantity) * cancelQuantity;

		}
		return total;
	}

	private double calcOrderReturnRefund(final ReturnRequestModel returnRequestModel)
	{
		double total = 0.0d;
		for (final ReturnEntryModel orderReturnEntry : returnRequestModel.getReturnEntries())
		{
			if (orderReturnEntry.getExpectedQuantity() <= 0 || orderReturnEntry.getOrderEntry().getTotalPrice() <= 0)
			{
				LOG.info(
						"Calculating cancel refund amount, value is not enough : cancel quantity[{}] ,total price[{}] , order quantity [{}]",
						orderReturnEntry.getExpectedQuantity(), orderReturnEntry.getOrderEntry().getTotalPrice(),
						orderReturnEntry.getOrderEntry().getQuantity());
				continue;
			}


			final long returnQuantity = orderReturnEntry.getExpectedQuantity();
			final Long quantity = orderReturnEntry.getOrderEntry().getQuantity();
			final Double totalPrice = orderReturnEntry.getOrderEntry().getTotalPrice();

			total += (totalPrice / quantity) * returnQuantity;

		}
		return total;
	}

	@Override
	public void orderReturnRefund(final LoyaltyProgramProviderModel provider, final ReturnRequestModel returnRequestModel)
			throws EarthLoyaltyException
	{
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequestModel), ORDER_CANCEL_RESPONSE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequestModel.getOrder()), ABSTRACT_ORDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequestModel.getOrder().getStore()), BASE_STORE_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(returnRequestModel.getOrder().getUser()), USER_ORDER_CONSIGNMENT_NULL_MSG);
		Preconditions.checkArgument((returnRequestModel.getOrder().getUser() instanceof CustomerModel), USER_IS_NOT_CUSTOMER);

		loyaltyValidationService.validateLoyaltyEnabled((CustomerModel) returnRequestModel.getOrder().getUser(),
				returnRequestModel.getOrder().getStore());


		orderReturnRefundForCustomer(provider, returnRequestModel);
		if (provider.isRefundFromCustomer())
		{
			orderReturnRefundFromCustomer(provider, returnRequestModel);
		}


	}

	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	private void orderReturnRefundFromCustomer(final LoyaltyProgramProviderModel provider,
			final ReturnRequestModel returnRequestModel) throws EarthLoyaltyException
	{

		if (!returnRequestModel.getOrder().isLoyaltyPointsAdded() || returnRequestModel.getOrder().getAddedLoyaltyAmount() <= 0)
		{
			LOG.info("no pointd added to deduct.");

			return;
		}
		if (CollectionUtils.isEmpty(returnRequestModel.getReturnEntries()))
		{
			LOG.info("no entries to return.");

			return;
		}

		final double loyaltyAmount = returnRequestModel.getOrder().getAddedLoyaltyPoints() / provider.getRange();

		final double calcOrderReturnRefundAmount = calcOrderReturnRefund(returnRequestModel);

		if (calcOrderReturnRefundAmount <= 0)
		{
			LOG.info("Loyalty calcOrderReturnRefundAmount is equal to or less than zero.");

			return;
		}
		final CustomerModel customer = (CustomerModel) returnRequestModel.getOrder().getUser();

		final double loyaltyRefundAmount = calcOrderReturnRefundAmount <= loyaltyAmount ? calcOrderReturnRefundAmount
				: loyaltyAmount;

		final AddPointsRequestBean addPointsRequestBean = getAddPointsRequestBeanFromOrderReturnRefund(loyaltyRefundAmount,
				returnRequestModel, provider);
		final String requestBody = gson.toJson(addPointsRequestBean);

		LOG.info("Adding points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ", customer.getUid(),
				returnRequestModel.getOrder().getCode(), provider.getBaseUrl(), requestBody);
		try
		{
			final AddPointsResponseBean addLoyaltyPoints = getAptLoyaltyService().addLoyaltyPoints(provider.getBaseUrl(),
					addPointsRequestBean);
			final String responseBody = gson.toJson(addLoyaltyPoints);

			LOG.info("Added points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ,response [{}] ",
					customer.getUid(), returnRequestModel.getOrder().getCode(), provider.getBaseUrl(), requestBody, responseBody);

			returnRequestModel.setLoyaltyReturnRefundFromCustomer(addLoyaltyPoints.getPointsEared());
			getModelService().save(returnRequestModel);
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
					responseBody, provider.getBaseUrl(), returnRequestModel.getOrder(), null);
		}
		catch (final APTLoyaltyException e)
		{
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), returnRequestModel.getOrder(), null);

			LOG.error("Error occured while attempting to add points. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), addPointsRequestBean, EarthLoyaltyExceptionType.APT);
		}




	}

	/**
	 * @throws EarthLoyaltyException
	 *
	 */
	private void orderReturnRefundForCustomer(final LoyaltyProgramProviderModel provider,
			final ReturnRequestModel returnRequestModel) throws EarthLoyaltyException
	{
		if (CollectionUtils.isEmpty(returnRequestModel.getReturnEntries()))
		{
			LOG.info("no entriesto return.");

			return;
		}

		final double loyaltyAmount = returnRequestModel.getOrder().getLoyaltyAmount();

		if (loyaltyAmount <= 0)
		{
			LOG.info("Loyalty amount is equal to or else than zero.");

			return;
		}
		final double refundedLoyaltyAmount = returnRequestModel.getOrder().getRefundedLoyaltyAmount();
		final double loyaltyAvalaibleRefundAmount = loyaltyAmount - refundedLoyaltyAmount;

		if (loyaltyAvalaibleRefundAmount <= 0)
		{

			LOG.info("Loyalty refund amount is equal to or else than zero.");

			return;
		}

		final double calcOrderReturnRefundAmount = calcOrderReturnRefund(returnRequestModel);

		if (calcOrderReturnRefundAmount <= 0)
		{
			LOG.info("Loyalty calcOrderCancelRefundAmount is equal to or less than zero.");

			return;
		}
		final CustomerModel customer = (CustomerModel) returnRequestModel.getOrder().getUser();

		final double loyaltyRefundAmount = calcOrderReturnRefundAmount <= loyaltyAvalaibleRefundAmount ? calcOrderReturnRefundAmount
				: loyaltyAvalaibleRefundAmount;

		final AddPointsRequestBean addPointsRequestBean = getAddPointsRequestBeanForOrderReturnRefund(loyaltyRefundAmount,
				returnRequestModel, provider);
		final String requestBody = gson.toJson(addPointsRequestBean);

		LOG.info("Adding points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ", customer.getUid(),
				returnRequestModel.getOrder().getCode(), provider.getBaseUrl(), requestBody);
		try
		{
			final AddPointsResponseBean addLoyaltyPoints = getAptLoyaltyService().addLoyaltyPoints(provider.getBaseUrl(),
					addPointsRequestBean);
			final String responseBody = gson.toJson(addLoyaltyPoints);

			LOG.info("Added points Loyalty : customer uid[{}] ,order[{}] , baseUrl [{}] ,request [{}] ,response [{}] ",
					customer.getUid(), returnRequestModel.getOrder().getCode(), provider.getBaseUrl(), requestBody, responseBody);

			returnRequestModel.setLoyaltyReturnRefundForCustomer(addLoyaltyPoints.getPointsEared());
			getModelService().save(returnRequestModel);

			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.SUCCESS, requestBody,
					responseBody, provider.getBaseUrl(), returnRequestModel.getOrder(), null);
		}
		catch (final APTLoyaltyException e)
		{
			saveRecord(customer, LoyaltyRecordOperationType.ADD_POINTS, LoyaltyRecordOperationStatus.FAILED, e.getRequestData(),
					e.getResponseData(), provider.getBaseUrl(), returnRequestModel.getOrder(), null);

			LOG.error("Error occured while attempting to add points. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), addPointsRequestBean, EarthLoyaltyExceptionType.APT);
		}

	}

	@Override
	public Optional<LoyaltyBalance> getLoyaltyBalance(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final BaseStoreModel baseStoreModel) throws EarthLoyaltyException
	{
		LOG.info("Getting current loyalty balance.");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(baseStoreModel), BASESTORE_NULL_MSG);


		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		loyaltyValidationService.validateLoyaltyEnabled(customer, baseStoreModel);

		CheckPointsResponseBean checkPointsResponseBean = null;
		try
		{

			LOG.info("Getting current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] ", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl());

			checkPointsResponseBean = getAptLoyaltyService().checkLoyaltyPoints(provider.getBaseUrl(), customer.getMobileNumber(),
					TRANSACTION_NO);

			final String responseBody = gson.toJson(checkPointsResponseBean);

			LOG.info("Got current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]",
					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			return getLoyaltyBalance(checkPointsResponseBean, provider);
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while getting loyalty balance. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), checkPointsResponseBean, EarthLoyaltyExceptionType.APT);
		}
	}

	@Override
	public boolean editLoyaltyMobileNumber(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final String newNumber) throws EarthLoyaltyException
	{
		LOG.info("Editing loyalty Mobile number.");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		final EditPhoneRequestBean editPhoneRequestBean = getEditPhoneRequestBean(customer.getMobileNumber(), newNumber);
		final String requestBody = gson.toJson(editPhoneRequestBean);

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		EditBean editBean = null;
		try
		{

			LOG.info("Editing customer phone number : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}], ,request [{}] ",
					customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), requestBody);

			editBean = getAptLoyaltyService().editMobileNumber(provider.getBaseUrl(), editPhoneRequestBean);

			final String responseBody = gson.toJson(editBean);

			LOG.info("Edited phone number : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]", customer.getUid(),
					customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
			return editBean.getReturnCode().equals("0");
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while editing phone number. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), editBean, EarthLoyaltyExceptionType.APT);
		}
	}

	/**
	 *
	 */
	private EditPhoneRequestBean getEditPhoneRequestBean(final String oldNum, final String newNumber)
	{
		final EditPhoneRequestBean editPhoneRequestBean = new EditPhoneRequestBean();

		editPhoneRequestBean.setCountryId("1");
		editPhoneRequestBean.setNewMobileNumber(newNumber);
		editPhoneRequestBean.setOldMobileNumber(oldNum);
		return editPhoneRequestBean;
	}

	/**
	 *
	 */
	private EditEmailRequestBean getEditEmailRequestBean(final String oldEmail, final String newEmail)
	{
		final EditEmailRequestBean editEmailRequestBean = new EditEmailRequestBean();

		editEmailRequestBean.setCountryId("1");
		editEmailRequestBean.setNewEmail(newEmail);
		editEmailRequestBean.setOldEmail(oldEmail);
		return editEmailRequestBean;
	}

	@Override
	public boolean editLoyaltyEmail(final CustomerModel customer, final LoyaltyProgramProviderModel provider,
			final String newEmail) throws EarthLoyaltyException
	{
		LOG.info("Editing loyalty Email number.");
		Preconditions.checkArgument(!Objects.isNull(customer), CUSTOMER_NULL_MSG);
		Preconditions.checkArgument(!Objects.isNull(provider), LOYALTY_PROGRAM_PROVIDER_NULL_MSG);

		final EditEmailRequestBean editEmailRequestBean = getEditEmailRequestBean(customer.getContactEmail(), newEmail);
		final String requestBody = gson.toJson(editEmailRequestBean);

		final APTLoyaltyProgramProviderModel aptProvider = getAPTProvider(provider);
		EditBean editBean = null;
		try
		{

			LOG.info("Editing customer Email : customer uid[{}] ,Email[{}] , baseUrl [{}], ,request [{}] ", customer.getUid(),
					customer.getContactEmail(), aptProvider.getBaseUrl(), requestBody);

			editBean = getAptLoyaltyService().editEmail(provider.getBaseUrl(), editEmailRequestBean);

			final String responseBody = gson.toJson(editBean);

			LOG.info("Edited Email : customer uid[{}] ,Email[{}] , baseUrl [{}] , response [{}]", customer.getUid(),
					customer.getContactEmail(), aptProvider.getBaseUrl(), responseBody);
			return editBean.getReturnCode().equals("0");
		}
		catch (final APTLoyaltyException e)
		{
			LOG.error("Error occured while editing email. Error of type" + e.getType() + " reason is:" + e.getMessage());
			throw new EarthLoyaltyException(e.getMessage(), editBean, EarthLoyaltyExceptionType.APT);
		}
	}


	private void setRegisterUserGroup(final CustomerModel newCustomer)
	{

		getModelService().refresh(newCustomer);
		final CMSSiteModel site = cmsSiteService.getCurrentSite();

		if (site == null || CollectionUtils.isEmpty(site.getRegisterCustomerGroups()))
		{
			LOG.info("Site is NUll or the loyalty group hasn't been created yet!");
			return;
		}

		final Set<PrincipalGroupModel> set = new HashSet<>();

		if (!CollectionUtils.isEmpty(newCustomer.getGroups()))
		{
			set.addAll(newCustomer.getGroups());
		}
		set.addAll(site.getRegisterCustomerGroups());

		newCustomer.setGroups(set);

		getModelService().save(newCustomer);
		getModelService().refresh(newCustomer);
	}




}






