package com.earth.earthloyaltyprogramprovider.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyPaymentModeService;
import com.earth.earthloyaltyprogramprovider.strategy.FindLoyaltyAmountStrategy;


/**
 * @author mohammedbaker
 *
 *
 */
public class DefaultFindLoyaltyAmountStrategy extends AbstractBusinessService implements FindLoyaltyAmountStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultFindLoyaltyAmountStrategy.class);

	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;





	public PriceValue getLoyaltyAmount(final AbstractOrderModel order, final double finalTotalAmount)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		if (!loyaltyProgramContext.isLoyaltyEnabled(order) || Objects.isNull(order.getLoyaltyPaymentMode()))
		{
			return new PriceValue(order.getCurrency().getIsocode(), 0, order.getNet().booleanValue());
		}

		double loyaltyAmount = 0.0;


		try
		{
			loyaltyProgramContext.reserve(order, finalTotalAmount);
			getModelService().refresh(order);
			loyaltyAmount = order.getLoyaltyAmount();

		}
		catch (final Exception e)
		{
			LOG.warn("Could not find loyaltyAmount for order [" + order.getCode() + "] due to : " + e + "... skipping!");
		}
		order.setShouldCalculateLoyalty(false);
		getModelService().save(order);
		return new PriceValue(order.getCurrency().getIsocode(), loyaltyAmount, order.getNet().booleanValue());
	}



}
