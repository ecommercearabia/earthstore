/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;
import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramProviderContext;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 *
 */
public class APTOrderReturnFromResponseAddPointsRequestBeanPopulator
		implements Populator<ReturnRequestModel, AddPointsRequestBean>
{

	@Resource(name = "loyaltyProgramProviderContext")
	private LoyaltyProgramProviderContext loyaltyProgramProviderContext;
	private static final Logger LOG = LoggerFactory.getLogger(APTOrderReturnFromResponseAddPointsRequestBeanPopulator.class);

	@Override
	public void populate(final ReturnRequestModel source, final AddPointsRequestBean target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getOrder() == null || source.getOrder().getUser() == null
				|| !(source.getOrder().getUser() instanceof CustomerModel) || CollectionUtils.isEmpty(source.getReturnEntries()))
		{
			return;
		}


		final CustomerModel customer = (CustomerModel) source.getOrder().getUser();

		final String receipt = getReceiptNo(source.getOrder());
		target.setLoyaltyCardNo(customer.getAptLoyaltyBarCode());
		target.setMobileNumber(customer.getMobileNumber());
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setStaffCode("WEB");
		target.setReceiptNo(receipt);
		target.setTransactionNo(receipt);

		final double totalAmountWithTax = getTotalAmountWithTax(source);

		final double totalAmountWithoutTax = totalAmountWithTax;

		target.setTotalBillAmountInclVAT(totalAmountWithTax);

		target.setTotalBillAmount(totalAmountWithoutTax);

		target.setPurchaseDetails(purchaseDetailConverter(totalAmountWithTax, totalAmountWithoutTax, customer));

	}

	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}



	private List<PurchaseDetailBean> purchaseDetailConverter(final double totalAmountWithTax, final double totalAmountWithoutTax,
			final CustomerModel customer)
	{
		final PurchaseDetailBean purchaseDetailBean = new PurchaseDetailBean();
		final List<PurchaseDetailBean> purchaseList = new ArrayList<>();

		purchaseDetailBean.setAmount(totalAmountWithTax);
		purchaseDetailBean.setBarCode(customer.getAptLoyaltyBarCode());
		purchaseDetailBean.setReceiptNo(" ");
		purchaseDetailBean.setProductId("");
		purchaseDetailBean.setInvoiceNo(" ");
		purchaseDetailBean.setStore("ECOM");
		purchaseDetailBean.setTerminal("WEB");
		purchaseDetailBean.setStaffId("WEB");
		purchaseDetailBean.setDescriptionArab(" ");
		purchaseDetailBean.setDescriptionEng("Refund");
		purchaseDetailBean.setQuantity(1);
		purchaseDetailBean.setTotalBillAmount(totalAmountWithTax);
		purchaseDetailBean.setItemAmount(totalAmountWithTax);
		purchaseList.add(purchaseDetailBean);
		return purchaseList;
	}

	private double getTotalAmountWithTax(final ReturnRequestModel source)
	{


		if (source.getOrder() == null || source.getOrder().getAddedLoyaltyPoints() <= 0)
		{
			return 0.0d;
		}

		final Optional<LoyaltyProgramProviderModel> provider = loyaltyProgramProviderContext
				.getProvider(source.getOrder().getStore());


		final double loyaltyAmount = source.getOrder().getAddedLoyaltyPoints() / provider.get().getRange();


		final double calcOrderCancelRefundAmount = calcOrderReturnRefund(source);
		if (calcOrderCancelRefundAmount <= 0)
		{
			return 0.0d;
		}



		return calcOrderCancelRefundAmount <= loyaltyAmount ? calcOrderCancelRefundAmount : loyaltyAmount;

	}


	private double calcOrderReturnRefund(final ReturnRequestModel returnRequestModel)
	{
		double total = 0.0d;
		for (final ReturnEntryModel orderReturnEntry : returnRequestModel.getReturnEntries())
		{
			if (orderReturnEntry.getExpectedQuantity() <= 0 || orderReturnEntry.getOrderEntry().getTotalPrice() <= 0)
			{
				LOG.info(
						"Calculating cancel refund amount, value is not enough : cancel quantity[{}] ,total price[{}] , order quantity [{}]",
						orderReturnEntry.getExpectedQuantity(), orderReturnEntry.getOrderEntry().getTotalPrice(),
						orderReturnEntry.getOrderEntry().getQuantity());
				continue;
			}


			final long returnQuantity = orderReturnEntry.getExpectedQuantity();
			final Long quantity = orderReturnEntry.getOrderEntry().getQuantity();
			final Double totalPrice = orderReturnEntry.getOrderEntry().getTotalPrice();

			total += (totalPrice / quantity) * returnQuantity;

		}
		return total;
	}


}
