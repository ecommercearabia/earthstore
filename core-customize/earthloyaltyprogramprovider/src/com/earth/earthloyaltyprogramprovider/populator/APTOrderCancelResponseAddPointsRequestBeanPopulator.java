/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;


/**
 *
 */
public class APTOrderCancelResponseAddPointsRequestBeanPopulator implements Populator<OrderCancelResponse, AddPointsRequestBean>
{


	@Override
	public void populate(final OrderCancelResponse source, final AddPointsRequestBean target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getOrder() == null || source.getOrder().getUser() == null
				|| !(source.getOrder().getUser() instanceof CustomerModel) || CollectionUtils.isEmpty(source.getEntriesToCancel()))
		{
			return;
		}


		final CustomerModel customer = (CustomerModel) source.getOrder().getUser();

		final String receipt = getReceiptNo(source.getOrder());
		target.setLoyaltyCardNo(customer.getAptLoyaltyBarCode());
		target.setMobileNumber(customer.getMobileNumber());
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setStaffCode("WEB");
		target.setReceiptNo(receipt);
		target.setTransactionNo(receipt);


		final double totalAmountWithTax = getTotalAmountWithTax(source);

		final double totalAmountWithoutTax = totalAmountWithTax;

		target.setTotalBillAmountInclVAT(totalAmountWithTax);

		target.setTotalBillAmount(totalAmountWithoutTax);

		target.setPurchaseDetails(purchaseDetailConverter(totalAmountWithTax, totalAmountWithoutTax, customer));

	}

	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}



	private List<PurchaseDetailBean> purchaseDetailConverter(final double totalAmountWithTax, final double totalAmountWithoutTax,
			final CustomerModel customer)
	{
		final PurchaseDetailBean purchaseDetailBean = new PurchaseDetailBean();
		final List<PurchaseDetailBean> purchaseList = new ArrayList<>();

		purchaseDetailBean.setAmount(totalAmountWithTax);
		purchaseDetailBean.setBarCode(customer.getAptLoyaltyBarCode());
		purchaseDetailBean.setReceiptNo(" ");
		purchaseDetailBean.setProductId("");
		purchaseDetailBean.setInvoiceNo(" ");
		purchaseDetailBean.setStore("ECOM");
		purchaseDetailBean.setTerminal("WEB");
		purchaseDetailBean.setStaffId("WEB");
		purchaseDetailBean.setDescriptionArab(" ");
		purchaseDetailBean.setDescriptionEng("Refund");
		purchaseDetailBean.setQuantity(1);
		purchaseDetailBean.setTotalBillAmount(totalAmountWithTax);
		purchaseDetailBean.setItemAmount(totalAmountWithTax);
		purchaseList.add(purchaseDetailBean);
		return purchaseList;
	}

	private double getTotalAmountWithTax(final OrderCancelResponse source)
	{
		if (source.getOrder() == null || source.getOrder().getLoyaltyAmount() <= 0)
		{
			return 0.0d;
		}
		final double loyaltyAmount = source.getOrder().getLoyaltyAmount();


		final double calcOrderCancelRefundAmount = calcOrderCancelRefund(source);
		if (calcOrderCancelRefundAmount <= 0)
		{
			return 0.0d;
		}

		final double refundedLoyaltyAmount = source.getOrder().getRefundedLoyaltyAmount();
		final double loyaltyAvalaibleRefundAmount = loyaltyAmount - refundedLoyaltyAmount;


		return calcOrderCancelRefundAmount <= loyaltyAvalaibleRefundAmount ? calcOrderCancelRefundAmount
				: loyaltyAvalaibleRefundAmount;

	}


	private double calcOrderCancelRefund(final OrderCancelResponse orderCancelResponse)
	{
		double total = 0.0d;
		for (final OrderCancelEntry orderCancelEntry : orderCancelResponse.getEntriesToCancel())
		{
			if (orderCancelEntry.getCancelQuantity() <= 0 || orderCancelEntry.getOrderEntry().getTotalPrice() <= 0)
			{
				//				LOG.info("Got current loyalty balance : customer uid[{}] ,mobileNumber[{}] , baseUrl [{}] , response [{}]",
				//						customer.getUid(), customer.getMobileNumber(), aptProvider.getBaseUrl(), responseBody);
				continue;
			}


			final long cancelQuantity = orderCancelEntry.getCancelQuantity();
			final Long quantity = orderCancelEntry.getOrderEntry().getQuantity() + cancelQuantity;
			final Double totalPrice = orderCancelEntry.getOrderEntry().getTotalPrice();

			total += (totalPrice / quantity) * cancelQuantity;

		}
		return total;
	}

}
