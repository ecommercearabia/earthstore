/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;


/**
 *
 */
public class APTOrderAddPointsRequestBeanPopulator implements Populator<AbstractOrderModel, AddPointsRequestBean>
{

	/**
	 *
	 */

	@Override
	public void populate(final AbstractOrderModel source, final AddPointsRequestBean target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getUser() == null || !(source.getUser() instanceof CustomerModel))
		{
			return;
		}


		final CustomerModel customer = (CustomerModel) source.getUser();

		//TODO Juman
		final String receiptNo = getReceiptNo(source);


		target.setLoyaltyCardNo(customer.getAptLoyaltyBarCode());
		target.setMobileNumber(customer.getMobileNumber());
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setStaffCode("WEB");
		target.setReceiptNo(receiptNo);
		target.setTransactionNo(receiptNo);


		final double totalTax = source.getTotalTax();
		final double totalAmountWithoutTax = source.getTotalPrice();
		final double totalAmountWithTax = totalTax + totalAmountWithoutTax;
		target.setTotalBillAmountInclVAT(totalAmountWithTax);

		target.setTotalBillAmount(totalAmountWithoutTax);

		target.setPurchaseDetails(getPurchaseDetails(totalAmountWithTax, totalAmountWithoutTax, customer));

	}

	private List<PurchaseDetailBean> getPurchaseDetails(final double totalAmountWithTax, final double totalAmountWithoutTax,
			final CustomerModel customer)
	{
		final PurchaseDetailBean purchaseDetailBean = new PurchaseDetailBean();
		final List<PurchaseDetailBean> purchaseList = new ArrayList<>();

		purchaseDetailBean.setAmount(totalAmountWithTax);
		purchaseDetailBean.setBarCode(customer.getAptLoyaltyBarCode());
		purchaseDetailBean.setReceiptNo(" ");
		purchaseDetailBean.setProductId("");
		purchaseDetailBean.setInvoiceNo(" ");
		purchaseDetailBean.setStore("ECOM");
		purchaseDetailBean.setTerminal("WEB");
		purchaseDetailBean.setStaffId("WEB");
		purchaseDetailBean.setDescriptionArab(" ");
		purchaseDetailBean.setDescriptionEng(" ");
		purchaseDetailBean.setQuantity(1);
		purchaseDetailBean.setTotalBillAmount(totalAmountWithoutTax);
		purchaseDetailBean.setItemAmount(totalAmountWithoutTax);
		return purchaseList;
	}

	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}



}