/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.PurchaseDetailBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.request.AddPointsRequestBean;


/**
 *
 */
public class APTConsignmentAddPointsRequestBeanPopulator implements Populator<ConsignmentModel, AddPointsRequestBean>
{



	@Override
	public void populate(final ConsignmentModel source, final AddPointsRequestBean target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");



		if (source.getOrder() == null || source.getOrder().getUser() == null
				|| !(source.getOrder().getUser() instanceof CustomerModel) || CollectionUtils.isEmpty(source.getConsignmentEntries()))
		{
			return;
		}


		final CustomerModel customer = (CustomerModel) source.getOrder().getUser();


		final String receipt = getReceiptNo(source.getOrder());
		target.setLoyaltyCardNo(customer.getAptLoyaltyBarCode());
		target.setMobileNumber(customer.getMobileNumber());
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setStaffCode("WEB");
		target.setReceiptNo(receipt);
		target.setTransactionNo(receipt);

		double totalAmountWithTax = getTotalAmountWithTax(source);

		totalAmountWithTax = editTotalAmount(totalAmountWithTax, source);

		target.setTotalBillAmountInclVAT(totalAmountWithTax);

		target.setTotalBillAmount(totalAmountWithTax);
		final double loyaltyAmountToBeDeducted = source.getOrder().getLoyaltyAmountToBeDeducted().doubleValue();

		target.setPurchaseDetails(purchaseDetailConverter(totalAmountWithTax, totalAmountWithTax, customer));

	}




	/**
	 * @param source
	 *
	 */
	private double editTotalAmount(final double totalAmountWithTax, final ConsignmentModel source)
	{
		final double loyaltyAmountToBeDeducted = source.getOrder().getLoyaltyAmountToBeDeducted().doubleValue();

		if (loyaltyAmountToBeDeducted <= 0)
		{
			return totalAmountWithTax;
		}
		if (totalAmountWithTax - loyaltyAmountToBeDeducted <= 0)
		{
			return 0.0;
		}
		else
		{
			return totalAmountWithTax - loyaltyAmountToBeDeducted;
		}
	}




	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}



	private List<PurchaseDetailBean> purchaseDetailConverter(final double totalAmountWithTax, final double totalAmountWithoutTax,
			final CustomerModel customer)
	{
		final PurchaseDetailBean purchaseDetailBean = new PurchaseDetailBean();
		final List<PurchaseDetailBean> purchaseList = new ArrayList<>();

		purchaseDetailBean.setAmount(totalAmountWithTax);
		purchaseDetailBean.setBarCode(customer.getAptLoyaltyBarCode());
		purchaseDetailBean.setReceiptNo(" ");
		purchaseDetailBean.setProductId("");
		purchaseDetailBean.setInvoiceNo(" ");
		purchaseDetailBean.setStore("ECOM");
		purchaseDetailBean.setTerminal("WEB");
		purchaseDetailBean.setStaffId("WEB");
		purchaseDetailBean.setDescriptionArab(" ");
		purchaseDetailBean.setDescriptionEng(" ");
		purchaseDetailBean.setQuantity(1);
		purchaseDetailBean.setTotalBillAmount(totalAmountWithoutTax);
		purchaseDetailBean.setItemAmount(totalAmountWithoutTax);
		purchaseList.add(purchaseDetailBean);
		return purchaseList;
	}

	/**
	 *
	 */
	private double getTotalAmountWithTax(final ConsignmentModel source)
	{
		double totalAmountWithTax = 0.0d;

		for (final ConsignmentEntryModel entry : source.getConsignmentEntries())
		{

			if (entry.getQuantity() == null || entry.getQuantity() <= 0 || entry.getOrderEntry() == null)
			{
				continue;
			}
			final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();


			double discount = 0.0;


			if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
			{

				for (final DiscountValue value : orderEntry.getDiscountValues())
				{
					discount += value.getAppliedValue();
				}
				discount = discount / entry.getOrderEntry().getQuantity();

			}

			final double discountVal = discount * entry.getQuantity().doubleValue();
			final double totalPriceVal = (orderEntry.getBasePrice() - discountVal) * entry.getQuantity();

			totalAmountWithTax += totalPriceVal;

		}

		return totalAmountWithTax;
	}

	/**
	 *
	 */
	private double getTotalTax(final ConsignmentModel source)
	{

		double totalTaxPriceVal = 0.0d;

		for (final ConsignmentEntryModel entry : source.getConsignmentEntries())
		{

			if (entry.getQuantity() == null || entry.getQuantity() <= 0 || entry.getOrderEntry() == null)
			{
				continue;
			}
			final AbstractOrderEntryModel orderEntry = entry.getOrderEntry();

			double taxValue = 0.0;
			double taxAppliedValue = 0.0;
			if (!CollectionUtils.isEmpty(orderEntry.getTaxValues()))
			{
				for (final TaxValue value : orderEntry.getTaxValues())
				{
					taxValue += value.getValue();
					taxAppliedValue += value.getAppliedValue();
				}
			}

			totalTaxPriceVal = +(taxAppliedValue / orderEntry.getQuantity()) * entry.getQuantity();


		}

		return totalTaxPriceVal;
	}



}
