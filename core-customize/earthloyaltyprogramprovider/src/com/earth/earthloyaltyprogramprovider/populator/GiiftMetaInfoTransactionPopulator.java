/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Objects;

import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftTransaction;


/**
 *
 */
public class GiiftMetaInfoTransactionPopulator implements Populator<AbstractOrderModel, GiiftTransaction>
{
	@Override
	public void populate(final AbstractOrderModel source, final GiiftTransaction target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		if (!Objects.isNull(source.getCreationtime()))
		{
			//target.setDate(source.getCreationtime().toLocaleString());
		}

		target.setId(source.getCode());
		if (Strings.isNotBlank(source.getCode()))
		{
			target.setPos("POS" + source.getCode());
			target.setSeq("SEQ" + source.getCode());

		}
		else
		{
			target.setPos(Strings.EMPTY);
			target.setSeq(Strings.EMPTY);
		}

		target.setTotal(source.getTotalPrice()
				+ (source.getExpectedLoyaltyRedeemAmount() != null ? source.getExpectedLoyaltyRedeemAmount() : 0d));

	}

}
