/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.apt.beans.request.RegisterNewUserRequestBean;


/**
 *
 */


public class APTCustomerRegisterNewUserRequestBeanPopulator implements Populator<CustomerModel, RegisterNewUserRequestBean>
{
	private static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	private static final String DATE_FORMAT_DD_MM_YYYY = "dd/mm/yyyy";


	@Override
	public void populate(final CustomerModel source, final RegisterNewUserRequestBean target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final String[] fullName = source.getName().split(" ");
		final StringBuilder lName = new StringBuilder();
		target.setFirstName(fullName[0]);
		for (int i = 1; i < fullName.length; i++)
		{
			lName.append(fullName[i]);
		}
		if (fullName.length >= 2)
		{
			target.setLastName(lName.toString());
		}
		else
		{

			target.setLastName("NaN");
		}

		target.setCountryId(1);
		try
		{
			target.setDob(dateFormater(source.getBirthOfDate()));
		}
		catch (final ParseException e)
		{
			throw new ConversionException(e.getMessage());
		}
		target.setEmail(source.getContactEmail());
		target.setPhoneNumber(source.getMobileNumber());
		target.setGender(" ");

		final String password = (source.getCustomerID() + new Random().nextInt(25));
		target.setPassword(password);
		target.setConfirmPassword(password);


	}



	private String dateFormater(final String birthOfDate) throws ParseException
	{
		//		dd/mm/yyyy

		final SimpleDateFormat ogDate = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);
		final SimpleDateFormat resultDate = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
		final Date date1 = ogDate.parse(birthOfDate);

		return resultDate.format(date1);
	}






}
