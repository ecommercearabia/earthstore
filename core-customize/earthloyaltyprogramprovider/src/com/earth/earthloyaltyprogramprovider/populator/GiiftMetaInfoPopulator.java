/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftItem;
import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftMetaInfo;
import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftTransaction;


/**
 *
 */
public class GiiftMetaInfoPopulator implements Populator<AbstractOrderModel, GiiftMetaInfo>
{

	@Resource(name = "giiftMetaInfoItemConverter")
	private Converter<AbstractOrderEntryModel, GiiftItem> giiftMetaInfoItemConverter;

	@Resource(name = "giiftMetaInfoTransactionConverter")
	private Converter<AbstractOrderModel, GiiftTransaction> giiftMetaInfoTransactionConverter;

	@Override
	public void populate(final AbstractOrderModel source, final GiiftMetaInfo target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setTransaction(giiftMetaInfoTransactionConverter.convert(source));
		if (!CollectionUtils.isEmpty(source.getEntries()))
		{
			target.setItems(giiftMetaInfoItemConverter.convertAll(source.getEntries()));
		}


	}

}
