/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Random;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramprovider.apt.beans.request.RedemptionRequestBean;


/**
 *
 */
public class APTAbstractOrderModelRedemptionRequestBeanPopulator implements Populator<AbstractOrderModel, RedemptionRequestBean>
{

	@Override
	public void populate(final AbstractOrderModel source, final RedemptionRequestBean target) throws ConversionException
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getUser() == null || !(source.getUser() instanceof CustomerModel))
		{
			return;
		}


		final CustomerModel customer = (CustomerModel) source.getUser();
		final String receiptNo = getReceiptNo(source);

		target.setBarCode(customer.getAptLoyaltyBarCode());
		target.setAmount(source.getLoyaltyAmountSelected());
		target.setCountryId(1);
		target.setStore("ECOM");
		target.setTerminal("WEB");
		target.setTransactionNo(receiptNo);
		target.setReceiptNo(receiptNo);
		target.setInvoiceNo(receiptNo);
	}


	private String getReceiptNo(final AbstractOrderModel source)
	{
		return source.getCode() + "_" + getRandomString(8);
	}

	private static String getRandomString(final int length)
	{
		final StringBuilder strs = new StringBuilder("1234567890");
		final int range = strs.length();
		final char[] key = new char[length];
		final Random ran = new Random(System.currentTimeMillis());
		for (int i = 0; i < length; i++)
		{
			final int RandKey = ran.nextInt(range);
			key[i] = strs.charAt(RandKey);
		}
		return new String(key);
	}




}
