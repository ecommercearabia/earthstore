/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.service;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 * The Interface LoyaltyProgramProviderService.
 */
public interface LoyaltyProgramProviderService
{
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @param providerClass the provider class
	 * @return the optional
	 */
	public Optional<LoyaltyProgramProviderModel> get(String code, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(String baseStoreUid, final Class<?> providerClass);

	/**
	 * Gets the active provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the active provider
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProvider(BaseStoreModel baseStoreModel, final Class<?> providerClass);

	/**
	 * Gets the active provider by current base store.
	 *
	 * @param providerClass the provider class
	 * @return the active provider by current base store
	 */
	public Optional<LoyaltyProgramProviderModel> getActiveProviderByCurrentBaseStore(final Class<?> providerClass);

}
