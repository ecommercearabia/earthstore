/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.service.impl;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import de.hybris.platform.core.model.order.CartModel;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.earth.earthloyaltyprogramprovider.dao.CartReleaseLoyaltyDao;
import com.earth.earthloyaltyprogramprovider.service.CartReleaseLoyaltyService;


/**
 *
 */
public class DefaultCartReleaseLoyaltyService implements CartReleaseLoyaltyService
{
	@Resource(name = "cartReleaseLoyaltyDao")
	private CartReleaseLoyaltyDao cartReleaseLoyaltyDao;

	@Override
	public List<CartModel> getAllCartsAfterTimeAmount(final int timeAmount) throws ParseException
	{
		final List<CartModel> cartList = cartReleaseLoyaltyDao.getAllCartsByLoyaltyMode();
		return cartList.stream().filter(e -> compareTimeInMinute(timeAmount, e)).collect(Collectors.toList());

	}

	public boolean compareTimeInMinute(final int timeAmount, final CartModel cart)
	{
		final Date previous = cart.getLastLoyaltyPaymentTime();
		final Date now = new Date();

		final long MAX_DURATION = MILLISECONDS.convert(timeAmount, MINUTES);

		final long duration = now.getTime() - previous.getTime();

		return duration >= MAX_DURATION;
	}

}
