/*
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.service;

import de.hybris.platform.core.model.order.CartModel;

import java.text.ParseException;
import java.util.List;


/**
 *
 */


public interface CartReleaseLoyaltyService
{
	public List<CartModel> getAllCartsAfterTimeAmount(int timeAmount) throws ParseException;
}
