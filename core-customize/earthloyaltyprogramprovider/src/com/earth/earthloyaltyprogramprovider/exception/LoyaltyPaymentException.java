/**
 *
 */
package com.earth.earthloyaltyprogramprovider.exception;

import com.earth.earthloyaltyprogramprovider.exception.type.LoyaltyPaymentExceptionType;


/**
 * @author yhammad
 *
 */
public class LoyaltyPaymentException extends RuntimeException
{

	private final LoyaltyPaymentExceptionType loyaltyPaymentExceptionType;


	public LoyaltyPaymentException(final String message, final LoyaltyPaymentExceptionType type)
	{
		super(message);
		this.loyaltyPaymentExceptionType = type;
	}


	/**
	 * @return the loyaltyPaymentExceptionType
	 */
	public LoyaltyPaymentExceptionType getLoyaltyPaymentExceptionType()
	{
		return loyaltyPaymentExceptionType;
	}

}
