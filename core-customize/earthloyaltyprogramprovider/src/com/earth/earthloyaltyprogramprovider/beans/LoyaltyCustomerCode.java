/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.beans;

/**
 *
 */
public class LoyaltyCustomerCode
{
	private String customerID;

	private String qrCode;

	private String customerCode;

	/**
	 * @return the customerID
	 */
	public String getCustomerID()
	{
		return customerID;
	}

	/**
	 * @param customerID
	 *           the customerID to set
	 */
	public void setCustomerID(final String customerID)
	{
		this.customerID = customerID;
	}

	/**
	 * @return the qrCode
	 */
	public String getQrCode()
	{
		return qrCode;
	}

	/**
	 * @param qrCode
	 *           the qrCode to set
	 */
	public void setQrCode(final String qrCode)
	{
		this.qrCode = qrCode;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode()
	{
		return customerCode;
	}

	/**
	 * @param customerCode
	 *           the customerCode to set
	 */
	public void setCustomerCode(final String customerCode)
	{
		this.customerCode = customerCode;
	}

}
