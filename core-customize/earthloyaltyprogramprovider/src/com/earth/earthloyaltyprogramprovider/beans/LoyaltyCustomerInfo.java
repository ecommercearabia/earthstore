/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.beans;

import java.util.List;


/**
 *
 */
public class LoyaltyCustomerInfo
{
	private String customerId;

	private LoyaltyBalance balance;

	private List<CustomerHistory> histories;

	private int totalPage;

	private int totalCount;

	private int pageIndex;

	private int pageSize;


	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	/**
	 * @return the balance
	 */
	public LoyaltyBalance getBalance()
	{
		return balance;
	}

	/**
	 * @param balance
	 *           the balance to set
	 */
	public void setBalance(final LoyaltyBalance balance)
	{
		this.balance = balance;
	}
	/**
	 * @return the totalPage
	 */
	public int getTotalPage()
	{
		return totalPage;
	}

	/**
	 * @param totalPage
	 *           the totalPage to set
	 */
	public void setTotalPage(final int totalPage)
	{
		this.totalPage = totalPage;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount()
	{
		return totalCount;
	}

	/**
	 * @param totalCount
	 *           the totalCount to set
	 */
	public void setTotalCount(final int totalCount)
	{
		this.totalCount = totalCount;
	}

	/**
	 * @return the pageIndex
	 */
	public int getPageIndex()
	{
		return pageIndex;
	}

	/**
	 * @param pageIndex
	 *           the pageIndex to set
	 */
	public void setPageIndex(final int pageIndex)
	{
		this.pageIndex = pageIndex;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize()
	{
		return pageSize;
	}

	/**
	 * @param pageSize
	 *           the pageSize to set
	 */
	public void setPageSize(final int pageSize)
	{
		this.pageSize = pageSize;
	}

	/**
	 * @return the histories
	 */
	public List<CustomerHistory> getHistories()
	{
		return histories;
	}

	/**
	 * @param histories
	 *           the histories to set
	 */
	public void setHistories(final List<CustomerHistory> histories)
	{
		this.histories = histories;
	}

}
