/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.beans;

/**
 *
 */
public class ValidateTransactionResult
{

	private double initialAmount;

	private String customerId;

	private double finalAmount;

	private LoyaltyUsablePoints points;

	private String validateID;



	/**
	 * @return the initialAmount
	 */
	public double getInitialAmount()
	{
		return initialAmount;
	}

	/**
	 * @param initialAmount
	 *           the initialAmount to set
	 */
	public void setInitialAmount(final float initialAmount)
	{
		this.initialAmount = initialAmount;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId()
	{
		return customerId;
	}

	/**
	 * @param customerId
	 *           the customerId to set
	 */
	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}



	/**
	 * @return the finalAmount
	 */
	public double getFinalAmount()
	{
		return finalAmount;
	}

	/**
	 * @param finalAmount
	 *           the finalAmount to set
	 */
	public void setFinalAmount(final double finalAmount)
	{
		this.finalAmount = finalAmount;
	}

	/**
	 * @param initialAmount
	 *           the initialAmount to set
	 */
	public void setInitialAmount(final double initialAmount)
	{
		this.initialAmount = initialAmount;
	}

	/**
	 * @return the points
	 */
	public LoyaltyUsablePoints getPoints()
	{
		return points;
	}

	/**
	 * @param points
	 *           the points to set
	 */
	public void setPoints(final LoyaltyUsablePoints points)
	{
		this.points = points;
	}

	/**
	 * @return the validateID
	 */
	public String getValidateID()
	{
		return validateID;
	}

	/**
	 * @param validateID
	 *           the validateID to set
	 */
	public void setValidateID(final String validateID)
	{
		this.validateID = validateID;
	}

}
