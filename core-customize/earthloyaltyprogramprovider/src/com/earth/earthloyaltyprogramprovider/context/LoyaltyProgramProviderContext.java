/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramprovider.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthloyaltyprogramprovider.model.LoyaltyProgramProviderModel;


/**
 * The Interface LoyaltyProgramProviderContext.
 */
public interface LoyaltyProgramProviderContext
{
	
	/**
	 * Gets the provider.
	 *
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<LoyaltyProgramProviderModel> getProvider(Class<?> providerClass);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreUid the base store uid
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<LoyaltyProgramProviderModel> getProvider(String baseStoreUid, Class<?> providerClass);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @param providerClass the provider class
	 * @return the provider
	 */
	public Optional<LoyaltyProgramProviderModel> getProvider(BaseStoreModel baseStoreModel, Class<?> providerClass);

	/**
	 * Gets the provider.
	 *
	 * @param baseStoreModel the base store model
	 * @return the provider
	 */
	public Optional<LoyaltyProgramProviderModel> getProvider(BaseStoreModel baseStoreModel);
}
