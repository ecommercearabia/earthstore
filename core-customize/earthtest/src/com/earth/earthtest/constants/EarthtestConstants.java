/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthtest.constants;

/**
 * 
 */
public class EarthtestConstants extends GeneratedEarthtestConstants
{

	public static final String EXTENSIONNAME = "earthtest";

}
