/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthsmarteditcustomaddon.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class EarthsmarteditcustomaddonConstants extends GeneratedEarthsmarteditcustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthsmarteditcustomaddon";
	
	private EarthsmarteditcustomaddonConstants()
	{
		//empty
	}
}
