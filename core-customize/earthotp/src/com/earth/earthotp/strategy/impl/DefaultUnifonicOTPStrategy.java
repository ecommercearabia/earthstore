/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.strategy.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.enums.OTPExceptionType;
import com.earth.earthotp.model.OTPProviderModel;
import com.earth.earthotp.model.UnifonicOTPProviderModel;
import com.earth.earthotp.strategy.OTPStrategy;
import com.earth.earthotp.unifonic.service.UnifonicService;


/**
 * @author Husam Dababneh
 */
public class DefaultUnifonicOTPStrategy implements OTPStrategy
{
	/**
	 *
	 */
	private static final String WHATSAPP_MESSAGE_THROUGH_UNIFONIC_IS_NOT_SUPPORTED = "Whatsapp Message through Unifonic is not supported";

	/** The Constant MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGE_CAN_NOT_BE_NULL_OR_EMPTY = "message can not be null or empty";

	/** The Constant MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY. */
	private static final String MESSAGING_SERVICE_SID_CAN_NOT_BE_NULL_OR_EMPTY = "messagingServiceSid can not be null or empty";

	/** The Constant TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY. */
	private static final String TO_NUMBER_MUST_NOT_BE_NULL_OR_EMPTY = "To Number Must not be null or empty";
	/** The Constant APIKEY_MUSTN_T_BE_NULL. */
	private static final String APIKEY_MUSTN_T_BE_NULL = "apiKey mustn't be null or empty";
	/** The Constant ACCOUNT_SID_MUSTN_T_BE_NULL. */
	private static final String ACCOUNT_SID_MUSTN_T_BE_NULL = "accountSid mustn't be null or empty";

	@Resource(name = "unifonicService")
	private UnifonicService unifonicService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public boolean sendOTPCode(final String countryisoCode, final String mobileNumber, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof UnifonicOTPProviderModel,
				"otpProviderModel cannot cast to UnifonicOTPProviderModel");
		final UnifonicOTPProviderModel providerModel = (UnifonicOTPProviderModel) otpProviderModel;

		final CountryModel country = getCommonI18NService().getCountry(countryisoCode);
		if (country == null || StringUtils.isBlank(country.getIsdcode()))
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT,
					"Country with isocode[" + countryisoCode + "] doesn't exist");
		}
		return getUnifonicService().sendVerificationCode(providerModel.getBaseURL(), providerModel.getAppSid(),
				providerModel.getMessage(), country.getIsdcode(), mobileNumber);
	}

	@Override
	public boolean verifyCode(final String countryisoCode, final String mobileNumber, final String code,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(countryisoCode), "countryisoCode cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(mobileNumber), "mobileNumber cannot be null or empty");
		Preconditions.checkArgument(StringUtils.isNotBlank(code), "code cannot be null or empty");
		Preconditions.checkArgument(otpProviderModel != null, "otpProviderModel cannot be null");
		Preconditions.checkArgument(otpProviderModel instanceof UnifonicOTPProviderModel,
				"otpProviderModel cannot cast to UnifonicOTPProviderModel");
		final UnifonicOTPProviderModel providerModel = (UnifonicOTPProviderModel) otpProviderModel;
		return getUnifonicService().verifyCode(providerModel.getBaseURL(), providerModel.getAppSid(), countryisoCode, mobileNumber,
				code);
	}

	@Override
	public boolean sendSMSMessage(final String mobileNumber, final String message, final OTPProviderModel otpProviderModel)
			throws OTPException
	{
		throw new UnsupportedOperationException("The method is not supported");
	}

	@Override
	public String sendSMSMessageWithDescription(final String mobileNumber, final String message,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new UnsupportedOperationException("The method is not supported");
	}

	@Override
	public String sendWhatsappOrderConfirmationMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, WHATSAPP_MESSAGE_THROUGH_UNIFONIC_IS_NOT_SUPPORTED);
	}

	@Override
	public String sendWhatsappOrderDeliveredMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel otpProviderModel) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, WHATSAPP_MESSAGE_THROUGH_UNIFONIC_IS_NOT_SUPPORTED);
	}

	@Override
	public String sendOrderShipmentWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, WHATSAPP_MESSAGE_THROUGH_UNIFONIC_IS_NOT_SUPPORTED);
	}

	@Override
	public String sendOrderCancellationWhatsappMessage(final String orderId, final String mobileNumber,
			final OTPProviderModel provider) throws OTPException
	{
		throw new OTPException(OTPExceptionType.SERVICE_UNAVAILABLE, WHATSAPP_MESSAGE_THROUGH_UNIFONIC_IS_NOT_SUPPORTED);
	}

	/**
	 * @return the unifonicService
	 */
	public UnifonicService getUnifonicService()
	{
		return unifonicService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

}
