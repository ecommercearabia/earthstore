/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthotp.dao;

import java.util.Optional;

import com.earth.earthotp.model.OTPVerificationTokenModel;


/**
 * @author mnasro
 *
 *         The Interface OTPVerificationTokenDao.
 */
public interface OTPVerificationTokenDao
{
	public Optional<OTPVerificationTokenModel> getToken(String token);
}
