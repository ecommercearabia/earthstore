package com.earth.earthotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifiyCodeResponseData extends UnifonicResponse {

	@SerializedName("Recipient")
	@Expose
	private String recipient;

	@SerializedName("VerifyStatus")
	@Expose
	private String verifyStatus;

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(final String recipient) {
		this.recipient = recipient;
	}

	public String getVerifyStatus() {
		return verifyStatus;
	}

	public void setVerifyStatus(final String verifyStatus) {
		this.verifyStatus = verifyStatus;
	}

}
