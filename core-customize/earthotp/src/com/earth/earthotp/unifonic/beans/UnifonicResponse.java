package com.earth.earthotp.unifonic.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UnifonicResponse
{
	@SerializedName("success")
	@Expose
	private String success;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("errorCode")
	@Expose
	private String errorCode;

	public String getSuccess()
	{
		return success;
	}

	public void setSuccess(final String success)
	{
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(final String errorCode)
	{
		this.errorCode = errorCode;
	}

}
