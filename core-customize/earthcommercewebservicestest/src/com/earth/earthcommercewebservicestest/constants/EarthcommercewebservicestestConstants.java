/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservicestest.constants;

/**
 * Global class for all Earthcommercewebservicestest constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthcommercewebservicestestConstants extends GeneratedEarthcommercewebservicestestConstants
{
	public static final String EXTENSIONNAME = "earthcommercewebservicestest";
	// implement here constants used by this extension
	public static final String ADMIN_PASSWORD_PROPERTY = "initialpassword.admin";
	public static final String ADMIN_USERNAME = "admin";

	private EarthcommercewebservicestestConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
