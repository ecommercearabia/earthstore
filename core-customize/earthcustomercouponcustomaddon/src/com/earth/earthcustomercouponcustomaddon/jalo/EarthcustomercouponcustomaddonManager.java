/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponcustomaddon.jalo;

import com.earth.earthcustomercouponcustomaddon.constants.EarthcustomercouponcustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthcustomercouponcustomaddonManager extends GeneratedEarthcustomercouponcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthcustomercouponcustomaddonManager.class.getName() );
	
	public static final EarthcustomercouponcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcustomercouponcustomaddonManager) em.getExtension(EarthcustomercouponcustomaddonConstants.EXTENSIONNAME);
	}
	
}
