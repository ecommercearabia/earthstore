/**
 *
 */
package com.earth.earthcommercewebservices.oauth2;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import com.earth.earthcommercewebservices.oauth2.token.ThirdPartyAuthenticationOCCToken;



/**
 * @author monzer
 *
 */
//@Component("otpTokenGranter")
public class ThirdPartyTokenGranter extends AbstractTokenGranter
{

	private static final String GRANT_TYPE = "third_party";

	private final AuthenticationManager authenticationManager;

	public ThirdPartyTokenGranter(final AuthenticationManager authenticationManager,
			final AuthorizationServerTokenServices tokenServices, final ClientDetailsService clientDetailsService,
			final OAuth2RequestFactory requestFactory)
	{
		this(authenticationManager, tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
	}

	/**
	 * @param tokenServices
	 * @param clientDetailsService
	 * @param requestFactory
	 * @param grantType
	 */
	public ThirdPartyTokenGranter(final AuthenticationManager authenticationManager, final AuthorizationServerTokenServices tokenServices,
			final ClientDetailsService clientDetailsService, final OAuth2RequestFactory requestFactory, final String grantType)
	{
		super(tokenServices, clientDetailsService, requestFactory, grantType);
		this.authenticationManager = authenticationManager;
	}

	@Override
	protected OAuth2Authentication getOAuth2Authentication(final ClientDetails client, final TokenRequest tokenRequest)
	{
		final Map<String, String> parameters = new LinkedHashMap<String, String>(tokenRequest.getRequestParameters());
		final String username = parameters.get("email");
		final String token = parameters.get("thirdpartyToken");
		final String thirdPartyType = parameters.get("thirdPartyType");
		final String callbackURL = parameters.get("callbackURL");

		Authentication authToken = new ThirdPartyAuthenticationOCCToken(username, token, thirdPartyType, callbackURL);
		((ThirdPartyAuthenticationOCCToken) authToken).setDetails(parameters);
		try
		{
			authToken = authenticationManager.authenticate(authToken);
		}
		catch (final AccountStatusException ase)
		{
			//covers expired, locked, disabled cases (mentioned in section 5.2, draft 31)
			throw new InvalidGrantException(ase.getMessage());
		}
		catch (final BadCredentialsException e)
		{
			// If the username/password are wrong the spec says we should send 400/invalid grant
			throw new InvalidGrantException(e.getMessage());
		}
		if (authToken == null || !authToken.isAuthenticated())
		{
			throw new InvalidGrantException("Could not authenticate user: " + username);
		}

		final OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
		return new OAuth2Authentication(storedOAuth2Request, authToken);
	}



}
