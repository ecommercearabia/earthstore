/**
 *
 */
package com.earth.earthcommercewebservices.exceptions;

/**
 * @author monzer
 *
 */
public class CustomerSignatureException extends Exception
{

	private final String message;

	/**
	 * @param message
	 */
	public CustomerSignatureException(final String message)
	{
		super(message);
		this.message = message;
	}



}
