package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.commercefacades.user.data.CityData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthfacades.user.city.facade.CityFacade;


public class CityValidator implements Validator
{
	private static final String INVALID_CITYCODE_ID = " field.invalidCityCode";

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;
	private String cityCode;

	private String countryIsdcode;

	/**
	 * @return the countryIsdcode
	 */
	public String getCountryIsdcode()
	{
		return countryIsdcode;
	}

	/**
	 * @param countryIsdcode
	 *           the countryIsdcode to set
	 */
	public void setCountryIsdcode(final String countryIsdcode)
	{
		this.countryIsdcode = countryIsdcode;
	}

	/**
	 * @return the cityCode
	 */
	public String getCityCode()
	{
		return cityCode;
	}

	/**
	 * @param cityCode
	 *           the cityCode to set
	 */
	public void setCityCode(final String cityCode)
	{
		this.cityCode = cityCode;
	}

	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String city = (String) errors.getFieldValue(this.cityCode);
		final String countryIsd = (String) errors.getFieldValue(this.countryIsdcode);

		if (!findCity(countryIsd, city))
		{
			errors.rejectValue(this.cityCode, INVALID_CITYCODE_ID, new String[]
			{ this.cityCode }, "This field is not a valid city code.");
		}

	}

	/**
	 * @param countryIsdcode2
	 * @param cityCode2
	 * @return
	 */
	private boolean findCity(final String countryIsdcode, final String cityCode)
	{
		if (StringUtils.isBlank(countryIsdcode) || StringUtils.isBlank(cityCode))
		{
			return false;
		}
		final Optional<List<CityData>> byCountryIsocode = cityFacade.getByCountryIsocode(countryIsdcode);
		if (!byCountryIsocode.isPresent() || byCountryIsocode.get().isEmpty())
		{
			return false;
		}
		for (final CityData cityData : byCountryIsocode.get())
		{
			if (cityCode.equals(cityData.getCode()))
			{
				return true;
			}
		}
		return false;

	}


}

