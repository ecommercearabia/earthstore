package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.text.ParseException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.utils.AgeUtil;



public class BirthDateValidator implements Validator
{

	private static final String INVALID_BIRTH_DATE_MESSAGE_ID = "field.register.birthDate.invalid";

	private static final String BIRTH_DATE_KEY = "birthdate";
	private static final String DATE_FORMAT_DD_MM_YYYY = "dd/mm/yyyy";
	private static final Logger LOG = Logger.getLogger(BirthDateValidator.class);

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String birthDate;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String dateOfBirth = (String) errors.getFieldValue(this.birthDate);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		validateBirthDate(errors, currentSite, dateOfBirth);

	}

	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled())
		{

			if (currentSite.isBirthOfDateCustomerRequired() && StringUtils.isEmpty(birthDate))
			{
				errors.rejectValue("birthDate", "register.birthofdate.invalid");
			}
			else
			{
				validDate(birthDate, errors);
			}
		}

	}

	private void validDate(final String birthDate, final Errors errors)
	{

		if (!cmsSiteService.getCurrentSite().isEnableAgeCheck() || cmsSiteService.getCurrentSite().getMinimumAgeInYears() <= 0)
		{
			return;
		}


		int ageYears = 0;
		try
		{
			ageYears = AgeUtil.getAgeYears(birthDate, DATE_FORMAT_DD_MM_YYYY);
			if (AgeUtil.checkUnderAge(birthDate, DATE_FORMAT_DD_MM_YYYY, cmsSiteService.getCurrentSite().getMinimumAgeInYears()))
			{
				return;

			}
		}
		catch (final ParseException e)
		{
			LOG.error("Parse exception occured while trying to validate age, current age is" + ageYears + "while minimum age is"
					+ cmsSiteService.getCurrentSite().getMinimumAgeInYears() + " , the exception is:" + e.getMessage());
		}

		errors.rejectValue("birthDate", "register.birthofdate.years.invalid", new Object[]
		{ String.valueOf(cmsSiteService.getCurrentSite().getMinimumAgeInYears()) }, "register.birthofdate.years.invalid");


	}


	/**
	 * @return the birthDate
	 */
	public String getBirthDate()
	{
		return birthDate;
	}

	/**
	 * @param birthDate
	 *           the birthDate to set
	 */
	public void setBirthDate(final String birthDate)
	{
		this.birthDate = birthDate;
	}



}

