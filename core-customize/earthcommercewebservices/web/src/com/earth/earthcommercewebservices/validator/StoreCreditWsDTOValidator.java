package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.commercewebservicescommons.dto.order.StoreCreditDetailsWsDTO;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthstorecredit.enums.StoreCreditModeType;
import com.earth.earthfacades.facade.CustomAcceleratorCheckoutFacade;



public class StoreCreditWsDTOValidator implements Validator
{
	@Resource(name = "acceleratorCheckoutFacade")
	private CustomAcceleratorCheckoutFacade checkoutFacade;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> arg0)
	{
		// YTODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object storeCreditDetails, final Errors errors)
	{
		final String sctCode = ((StoreCreditDetailsWsDTO) storeCreditDetails).getStoreCreditModeTypeCode();
		final Double scAmount = ((StoreCreditDetailsWsDTO) storeCreditDetails).getAmount();
		if (StringUtils.isEmpty(sctCode))
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}
		StoreCreditModeType storeCreditModeType = null;
		try
		{
			storeCreditModeType = StoreCreditModeType.valueOf(sctCode);
		}
		catch (final Exception e)
		{
			errors.rejectValue("sctCode", "storeCredit.sctCode.invalid");
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(sctCode))
		{
			if (scAmount <= 0)
			{
				errors.rejectValue("scAmount", "storeCredit.scAmount.invalid");
			}


		}
	}
}
