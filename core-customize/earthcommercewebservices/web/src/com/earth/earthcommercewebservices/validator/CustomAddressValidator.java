/**
 *
 */
package com.earth.earthcommercewebservices.validator;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercewebservicescommons.dto.user.AddressWsDTO;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * @author monzer
 *
 *         Validates the configurable attributes by the CMSSite
 */
public class CustomAddressValidator implements Validator
{

	private static final String INVALID_STREET_NAME = "invalid.field.streetName";

	private static final String INVALID_BUILDING_NAME = "invalid.field.buildingName";

	private static final String INVALID_APARTMENT_NUMBER = "invalid.field.apartmentNumber";

	private static final String INVALID_NEAREST_LANDMARK_NUMBER = "invalid.field.nearestLandmark";

	private static final String INVALID_MORE_INSTRUCTIONS_NUMBER = "invalid.field.moreinstructions";

	private static final String INVALID_ADDRESS_INSTRUCTIONS_NUMBER = "invalid.field.addressinstructions";


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	private String floorNumber;
	private String buildingName;
	private String apartmentNumber;
	private String nearestLandmark;
	private String moreInstructions;
	private String addressInstructions;

	@Override
	public boolean supports(final Class<?> arg0)
	{
		return AddressWsDTO.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(final Object obj, final Errors errors)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		if (currentSite == null)
		{
			throw new IllegalArgumentException("current site is null");
		}
		validateFloorNumber(errors, currentSite);
		validateNearestLandmark(errors, currentSite);
		validateBuildingName(errors, currentSite);
		validateApartmentName(errors, currentSite);
		validateMoreInstruction(errors, currentSite);
		validateAddressInstruction(errors, currentSite);

	}

	private void validateFloorNumber(final Errors errors, final CMSSiteModel currentSite)
	{
		if (currentSite.isFloorNumberAddressEnabled() && currentSite.isFloorNumberAddressRequired())
		{
			validateField(errors, this.floorNumber, INVALID_STREET_NAME);
		}
	}

	private void validateNearestLandmark(final Errors errors, final CMSSiteModel currentSite)
	{
		if (currentSite.isNearestLandmarkAddressEnabled() && currentSite.isNearestLandmarkAddressRequired())
		{
			validateField(errors, this.nearestLandmark, INVALID_NEAREST_LANDMARK_NUMBER);
		}
	}

	private void validateBuildingName(final Errors errors, final CMSSiteModel currentSite)
	{
		if (currentSite.isBuildingNameAddressEnabled() && currentSite.isBuildingNameAddressRequired())
		{
			validateField(errors, this.buildingName, INVALID_BUILDING_NAME);
		}
	}

	private void validateApartmentName(final Errors errors, final CMSSiteModel currentSite)
	{
		if (currentSite.isApartmentNumberAddressEnabled() && currentSite.isApartmentNumberAddressRequired())
		{
			validateField(errors, this.apartmentNumber, INVALID_APARTMENT_NUMBER);
		}
	}

	private void validateMoreInstruction(final Errors errors, final CMSSiteModel currentSite)
	{

		if (currentSite.isMoreInstructionsAddressEnabled() && currentSite.isMoreInstructionsAddressRequired())
		{
			validateField(errors, this.moreInstructions, INVALID_MORE_INSTRUCTIONS_NUMBER);
		}
	}

	private void validateAddressInstruction(final Errors errors, final CMSSiteModel currentSite)
	{

		if (currentSite.isAddressInstructionsAddressEnabled() && currentSite.isAddressInstructionsAddressRequired())
		{
			validateField(errors, this.addressInstructions, INVALID_ADDRESS_INSTRUCTIONS_NUMBER);
		}
	}
	private void validateField(final Errors errors, final String field, final String message)
	{
		final String ffield = (String) errors.getFieldValue(field);
		if (StringUtils.isBlank(ffield))
		{
			errors.rejectValue(field, message);
		}
	}



	/**
	 * @return the floorNumber
	 */
	public String getFloorNumber()
	{
		return floorNumber;
	}

	/**
	 * @param floorNumber
	 *           the floorNumber to set
	 */
	public void setFloorNumber(final String floorNumber)
	{
		this.floorNumber = floorNumber;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName()
	{
		return buildingName;
	}

	/**
	 * @param buildingName
	 *           the buildingName to set
	 */
	public void setBuildingName(final String buildingName)
	{
		this.buildingName = buildingName;
	}

	/**
	 * @return the apartmentNumber
	 */
	public String getApartmentNumber()
	{
		return apartmentNumber;
	}

	/**
	 * @param apartmentNumber
	 *           the apartmentNumber to set
	 */
	public void setApartmentNumber(final String apartmentNumber)
	{
		this.apartmentNumber = apartmentNumber;
	}

	/**
	 * @return the nearestLandmark
	 */
	public String getNearestLandmark()
	{
		return nearestLandmark;
	}

	/**
	 * @param nearestLandmark
	 *           the nearestLandmark to set
	 */
	public void setNearestLandmark(final String nearestLandmark)
	{
		this.nearestLandmark = nearestLandmark;
	}

	/**
	 * @return the moreInstructions
	 */
	public String getMoreInstructions()
	{
		return moreInstructions;
	}

	/**
	 * @param moreInstructions
	 *           the moreInstructions to set
	 */
	public void setMoreInstructions(final String moreInstructions)
	{
		this.moreInstructions = moreInstructions;
	}

	/**
	 * @return the addressInstructions
	 */
	public String getAddressInstructions()
	{
		return addressInstructions;
	}

	/**
	 * @param addressInstructions
	 *           the addressInstructions to set
	 */
	public void setAddressInstructions(final String addressInstructions)
	{
		this.addressInstructions = addressInstructions;
	}




}
