/**
 *
 */
package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercewebservicescommons.dto.otp.OTPResponseWsDTO;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;

import java.security.Principal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidRequestException;
import org.springframework.security.oauth2.common.exceptions.UnsupportedGrantTypeException;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.OAuth2RequestValidator;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestValidator;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.earth.earthcommercewebservices.service.OTPLoginUserService;
import com.earth.earthotp.context.OTPContext;
import com.earth.earthotp.exception.OTPException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/otp")
@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 360)
@Api(tags = "OTP Auth")
public class OTPAuthenticationController extends BaseController
{

	private static final Logger LOG = LoggerFactory.getLogger(OTPAuthenticationController.class);

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "otpLoginUserService")
	private OTPLoginUserService otpLoginUserService;

	@Resource(name = "otpTokenGranter")
	private TokenGranter otpTokenGranter;

	@Autowired
	@Qualifier(value = "openIDClientDetails")
	private ClientDetailsService clientDetailsService;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	private OAuth2RequestFactory oAuth2RequestFactory;

	private final OAuth2RequestValidator oAuth2RequestValidator = new DefaultOAuth2RequestValidator();

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/send/login", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "sendOTPCode", value = "send OTP Code", notes = "Send a Otp code to specific mobile number and return true if the message has been sent successfully and return false if the message did not send.")
	@ApiBaseSiteIdParam
	public OTPResponseWsDTO sendOTPCodeForLogin(@ApiParam
	@RequestParam(required = true)
	final String email)
	{
		OTPResponseWsDTO otpResponseWsDTO = null;
		try
		{
			otpResponseWsDTO = new OTPResponseWsDTO();
			otpLoginUserService.sendOtpCodeToCustomer(email);
			otpResponseWsDTO.setStatus(Boolean.TRUE);
			otpResponseWsDTO.setDescription("The message has been sent successfully");
			String successMessage = getMessage("send.otp.code.sent.successfully", null);

			successMessage = org.apache.commons.lang3.StringUtils.isBlank(successMessage)
					? "The number has been verified successfully"
					: successMessage;
			return otpResponseWsDTO;
		}
		catch (final OTPException e)
		{
			otpResponseWsDTO.setDescription(getOTPExceptionErrorMsg(e));
			otpResponseWsDTO.setStatus(Boolean.FALSE);
			return otpResponseWsDTO;
		}
	}

	/**
	 * @param otpException
	 * @return
	 */
	private String getOTPExceptionErrorMsg(final OTPException otpException)
	{
		if (otpException == null)
		{
			return null;
		}
		if (otpException.geType() == null)
		{
			return otpException.getMessage();
		}
		final String errorMessage = getMessage("send.otp.code.error." + otpException.geType(), null);
		return org.apache.commons.lang3.StringUtils.isBlank(errorMessage) ? otpException.getMessage() : errorMessage;
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/verify/login", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "verifyCode", value = "verify Code.", notes = "specifying Otp Code ,return true if the message has been sent successfully and return false if the message did not send.")
	@ApiBaseSiteIdParam
	public ResponseEntity<Map<String, Object>> verifyCodeForLogin(@ApiParam
	@RequestParam(required = true)
	final String email, @ApiParam
	@RequestParam(required = true)
	final String code, final Principal principal) throws OTPException
	{
		OTPResponseWsDTO otpResponseWsDTO = null;
		try
		{
			otpResponseWsDTO = new OTPResponseWsDTO();
			otpLoginUserService.verifyOtpCode(email, code);
			otpResponseWsDTO.setStatus(Boolean.TRUE);

			String successMessage = getMessage("send.otp.code.verified.successfully", null);

			successMessage = org.apache.commons.lang3.StringUtils.isBlank(successMessage)
					? "The number has been verified successfully"
					: successMessage;
			otpResponseWsDTO.setDescription(successMessage);
		}
		catch (final OTPException e)
		{
			otpResponseWsDTO.setDescription(getOTPExceptionErrorMsg(e));
			otpResponseWsDTO.setStatus(Boolean.FALSE);
			throw e;
		}

		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("email", email);
		parameters.put("otpCode", code);
		parameters.put("grant_type", "otp");

		if (!(principal instanceof Authentication))
		{
			throw new InsufficientAuthenticationException(
					"There is no client authentication. Try adding an appropriate authentication filter.");
		}

		final String clientId = getClientId(principal);
		final ClientDetails authenticatedClient = clientDetailsService.loadClientByClientId(clientId);

		final TokenRequest tokenRequest = oAuth2RequestFactory.createTokenRequest(parameters, authenticatedClient);

		if (clientId != null && !clientId.equals(""))
		{
			// Only validate the client details if a client authenticated during this
			// request.
			if (!clientId.equals(tokenRequest.getClientId()))
			{
				// double check to make sure that the client ID in the token request is the same as that in the
				// authenticated client
				throw new InvalidClientException("Given client ID does not match authenticated client");
			}
		}
		if (authenticatedClient != null)
		{
			oAuth2RequestValidator.validateScope(tokenRequest, authenticatedClient);
		}
		if (!StringUtils.hasText(tokenRequest.getGrantType()))
		{
			throw new InvalidRequestException("Missing grant type");
		}
		if (tokenRequest.getGrantType().equals("implicit"))
		{
			throw new InvalidGrantException("Implicit grant type not supported from token endpoint");
		}

		if (isAuthCodeRequest(parameters))
		{
			// The scope was requested or determined during the authorization step
			if (!tokenRequest.getScope().isEmpty())
			{
				LOG.debug("Clearing scope of incoming token request");
				tokenRequest.setScope(Collections.<String> emptySet());
			}
		}

		if (isRefreshTokenRequest(parameters))
		{
			// A refresh token has its own default scopes, so we should ignore any added by the factory here.
			tokenRequest.setScope(OAuth2Utils.parseParameterList(parameters.get(OAuth2Utils.SCOPE)));
		}

		final OAuth2AccessToken token = otpTokenGranter.grant(tokenRequest.getGrantType(), tokenRequest);
		if (token == null)
		{
			throw new UnsupportedGrantTypeException("Unsupported grant type: " + tokenRequest.getGrantType());
		}

		return getResponse(token);

	}

	protected String getClientId(final Principal principal)
	{
		final Authentication client = (Authentication) principal;
		if (!client.isAuthenticated())
		{
			throw new InsufficientAuthenticationException("The client is not authenticated.");
		}
		String clientId = client.getName();
		if (client instanceof OAuth2Authentication)
		{
			// Might be a client and user combined authentication
			clientId = ((OAuth2Authentication) client).getOAuth2Request().getClientId();
		}
		return clientId;
	}

	private ResponseEntity<Map<String, Object>> getResponse(final OAuth2AccessToken accessToken)
	{
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		headers.set("Content-Type", "application/json;charset=UTF-8");
		headers.set("otp-authentication", "The user has been authenticated successfully using otp code");
		final Map<String, Object> tokenBody = new HashMap<String, Object>();
		tokenBody.put("access_token", accessToken.getValue());
		tokenBody.put("token_type", accessToken.getTokenType());
		tokenBody.put("refresh_token", accessToken.getRefreshToken().getValue());
		tokenBody.put("expires_in", accessToken.getExpiresIn());
		tokenBody.put("scope", accessToken.getScope().stream().collect(Collectors.joining(", ")));

		return new ResponseEntity<Map<String, Object>>(tokenBody, headers, HttpStatus.OK);
	}

	private boolean isRefreshTokenRequest(final Map<String, String> parameters)
	{
		return "refresh_token".equals(parameters.get("grant_type")) && parameters.get("refresh_token") != null;
	}

	private boolean isAuthCodeRequest(final Map<String, String> parameters)
	{
		return "authorization_code".equals(parameters.get("grant_type")) && parameters.get("code") != null;
	}
}
