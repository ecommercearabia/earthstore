/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.NationalityData;
import de.hybris.platform.commercefacades.user.data.NationalityListData;
import de.hybris.platform.commercewebservicescommons.dto.order.CardTypeListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.storesession.CurrencyListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.storesession.LanguageListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.CountryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.TitleListWsDTO;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.earth.earthcommercewebservices.configuration.ContactUsConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.AddressConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.RegistrationConfigurationData;
import com.earth.earthcommercewebservices.configuration.site.SiteConfigurationData;
import com.earth.earthcommercewebservices.order.data.CardTypeDataList;
import com.earth.earthcommercewebservices.service.SiteConfigurationsService;
import com.earth.earthcommercewebservices.storesession.data.CurrencyDataList;
import com.earth.earthcommercewebservices.storesession.data.LanguageDataList;
import com.earth.earthcommercewebservices.user.data.CountryDataList;
import com.earth.earthcommercewebservices.user.data.TitleDataList;
import com.earth.earthcommercewebservices.dto.core.configuration.ContactUsConfigurationWsDTO;
import com.earth.earthcommercewebservices.dto.core.configuration.site.AddressConfigurationWsDTO;
import com.earth.earthcommercewebservices.dto.core.configuration.site.RegistrationConfigurationWsDTO;
import com.earth.earthcommercewebservices.dto.core.configuration.site.SiteConfigurationWsDTO;
import com.earth.earthcommercewebservices.dto.user.MaritalStatusListWsDTO;
import com.earth.earthcommercewebservices.dto.user.NationalityListWsDTO;
import com.earth.earthfacades.customer.data.MaritalStatusData;
import com.earth.earthfacades.customer.data.MaritalStatusListData;
import com.earth.earthfacades.user.customer.facade.CustomCustomerFacade;
import com.earth.earthfacades.user.nationality.facade.NationalityFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * Misc Controller
 */
@Controller
//@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 1800)
@Api(tags = "Miscs")
public class MiscsController extends BaseController
{
	@Resource(name = "userFacade")
	private UserFacade userFacade;
	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;
	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "nationalityFacade")
	private NationalityFacade nationalityFacade;

	@Resource(name = "siteConfigurationsService")
	private SiteConfigurationsService siteConfigurationsService;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;

	@RequestMapping(value = "/{baseSiteId}/languages", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getLanguages',#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getLanguages", value = "Get a list of available languages.", notes = "Lists all available languages (all languages used for a particular store). If the list "
			+ "of languages for a base store is empty, a list of all languages available in the system will be returned.")
	@ApiBaseSiteIdParam
	public LanguageListWsDTO getLanguages(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final LanguageDataList dataList = new LanguageDataList();
		dataList.setLanguages(storeSessionFacade.getAllLanguages());
		return getDataMapper().map(dataList, LanguageListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/currencies", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCurrencies',#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getCurrencies", value = "Get a list of available currencies.", notes = "Lists all available currencies (all usable currencies for the current store). If the list "
			+ "of currencies for a base store is empty, a list of all currencies available in the system is returned.")
	@ApiBaseSiteIdParam
	public CurrencyListWsDTO getCurrencies(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CurrencyDataList dataList = new CurrencyDataList();
		dataList.setCurrencies(storeSessionFacade.getAllCurrencies());
		return getDataMapper().map(dataList, CurrencyListWsDTO.class, fields);
	}

	/**
	 * @deprecated since 1808. Please use {@link CountriesController#getCountries(String, String)} instead.
	 */
	@Deprecated(since = "1808", forRemoval = true)
	@RequestMapping(value = "/{baseSiteId}/deliverycountries", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getDeliveryCountries',#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getDeliveryCountries", value = "Get a list of shipping countries.", notes = "Lists all supported delivery countries for the current store. The list is sorted alphabetically.")
	@ApiBaseSiteIdParam
	public CountryListWsDTO getDeliveryCountries(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CountryDataList dataList = new CountryDataList();
		dataList.setCountries(checkoutFacade.getDeliveryCountries());
		return getDataMapper().map(dataList, CountryListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/titles", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getTitles',#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getTitles", value = "Get a list of all localized titles.", notes = "Lists all localized titles.")
	@ApiBaseSiteIdParam
	public TitleListWsDTO getTitles(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final TitleDataList dataList = new TitleDataList();
		dataList.setTitles(userFacade.getTitles());
		return getDataMapper().map(dataList, TitleListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/cardtypes", method = RequestMethod.GET)
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCardTypes',#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getCardTypes", value = "Get a list of supported payment card types.", notes = "Lists supported payment card types.")
	@ApiBaseSiteIdParam
	public CardTypeListWsDTO getCardTypes(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CardTypeDataList dataList = new CardTypeDataList();
		dataList.setCardTypes(checkoutFacade.getSupportedCardTypes());
		return getDataMapper().map(dataList, CardTypeListWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/registration-config", method = RequestMethod.GET)
	@ResponseBody
	//	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getRegistrationConfig',#fields)")
	@ApiOperation(nickname = "getRegistrationSettings", value = "Get the registartion configuration.", notes = "Registration configuration")
	@ApiBaseSiteIdParam
	public RegistrationConfigurationWsDTO getRegistrationConfig(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<RegistrationConfigurationData> currentSiteConfigurationForRegistration = siteConfigurationsService
				.getRegistrationConfigurationForCurrentSite();
		if (currentSiteConfigurationForRegistration.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(currentSiteConfigurationForRegistration.get(), RegistrationConfigurationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/address-config", method = RequestMethod.GET)
	@ResponseBody
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getAddressConfig',#fields)")
	@ApiOperation(nickname = "getAddressConfig", value = "Get the address configuration.", notes = "Address configuration")
	@ApiBaseSiteIdParam
	public AddressConfigurationWsDTO getAddressConfig(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<AddressConfigurationData> addressConfigurationForCurrentSite = siteConfigurationsService
				.getAddressConfigurationForCurrentSite();
		if (addressConfigurationForCurrentSite.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(addressConfigurationForCurrentSite.get(), AddressConfigurationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/config", method = RequestMethod.GET)
	@ResponseBody
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getSiteConfig',#fields)")
	@ApiOperation(nickname = "getSiteConfig", value = "Get the site configuration.", notes = "Site configuration")
	@ApiBaseSiteIdParam
	public SiteConfigurationWsDTO getSiteConfig(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<SiteConfigurationData> siteConfigurationForCurrentSite = siteConfigurationsService
				.getSiteConfigurationForCurrentSite();
		if (siteConfigurationForCurrentSite.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(siteConfigurationForCurrentSite.get(), SiteConfigurationWsDTO.class, fields);
	}

	@RequestMapping(value = "/{baseSiteId}/nationalities", method = RequestMethod.GET)
	@ResponseBody
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getSiteNationalities',#fields)")
	@ApiOperation(nickname = "getSiteNationalities", value = "Get the site nationalities.", notes = "Site nationalities")
	@ApiBaseSiteIdParam
	public NationalityListWsDTO getSiteNationalities(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final List<NationalityData> nationalities = nationalityFacade.getByCurrentSite();
		final NationalityListData listData = new NationalityListData();
		listData.setNationalities(nationalities);
		return getDataMapper().map(listData, NationalityListWsDTO.class);

	}

	@RequestMapping(value = "/{baseSiteId}/marital-statuses", method = RequestMethod.GET)
	@ResponseBody
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getMaritalStatuses',#fields)")
	@ApiOperation(nickname = "getMaritalStatuses", value = "Get Marital Statuses.", notes = "Marital Statuses")
	@ApiBaseSiteIdParam
	public MaritalStatusListWsDTO getMaritalStatuses(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final List<MaritalStatusData> maritalStatuses = customCustomerFacade.getAllMaritailStatuses();
		final MaritalStatusListData listData = new MaritalStatusListData();
		listData.setStatuses(maritalStatuses);
		return getDataMapper().map(listData, MaritalStatusListWsDTO.class);
	}

	@RequestMapping(value = "/{baseSiteId}/contact-us", method = RequestMethod.GET)
	@ResponseBody
	@Cacheable(value = "miscsCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getAddressConfig',#fields)")
	@ApiOperation(nickname = "getAddressConfig", value = "Get the address configuration.", notes = "Address configuration")
	@ApiBaseSiteIdParam
	public ContactUsConfigurationWsDTO getContactUsInfo(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<ContactUsConfigurationData> contactUsConfigurationData = siteConfigurationsService
				.getContactUsConfigurationForCurrentSite();
		if (contactUsConfigurationData.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(contactUsConfigurationData.get(), ContactUsConfigurationWsDTO.class, fields);
	}
}

