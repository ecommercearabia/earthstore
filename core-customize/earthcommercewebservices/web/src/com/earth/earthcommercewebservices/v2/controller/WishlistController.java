package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercewebservicescommons.dto.user.WishlistListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.WishlistWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.earth.earthwishlist.exception.WishlistException;
import com.earth.earthwishlistfacade.data.WishlistData;
import com.earth.earthwishlistfacade.data.WishlistDataList;
import com.earth.earthwishlistfacade.facade.WishlistFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/wishlists")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Wishlist")
public class WishlistController extends BaseCommerceController
{
	private static final Logger LOG = LoggerFactory.getLogger(WishlistController.class);

	private static final String DEFAULT_WISHLIST = "default";

	@Resource(name = "wishlistFacade")
	private WishlistFacade wishlistFacade;

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getWishlists", value = "Get customer's wishlists", notes = "Returns customer's wishlists.")
	@ApiBaseSiteIdAndUserIdParam
	@ApiResponse(code = 200, message = "List of customer's wishlists")
	public WishlistListWsDTO getWishlists(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws WishlistException
	{
		final Optional<List<WishlistData>> lists = wishlistFacade.getWishLists();
		if (lists.isPresent() && !CollectionUtils.isEmpty(lists.get()))
		{
			final WishlistDataList dto = new WishlistDataList();
			dto.setWishlists(lists.get());
			return getDataMapper().map(dto, WishlistListWsDTO.class, fields);
		}
		return new WishlistListWsDTO();
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}/{productCode}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(nickname = "addWishlistEntry", value = "Add wishlist entry", notes = "Updates the wishlist. Adds a new entry.")
	@ApiBaseSiteIdAndUserIdParam
	public void addWishlistEntry(
			@ApiParam(value = "Wishlist PK for a specific wishlist or the literal 'default' for the default wishlist", required = true)
			@PathVariable
			String wishlistPK, @ApiParam(value = "Product code.", required = true)
			@PathVariable
			final String productCode, final HttpServletRequest request) throws WebserviceValidationException //NOSONAR
			, WishlistException
	{
		if (DEFAULT_WISHLIST.equalsIgnoreCase(wishlistPK))
		{
			final Optional<WishlistData> wishList = wishlistFacade.getDefaultWishList();
			wishlistPK = wishList.isPresent() ? wishList.get().getPk() : null;
		}
		wishlistFacade.addWishlistEntry(wishlistPK, productCode, null, null, null);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}/{productCode}", method = RequestMethod.DELETE)
	@ApiOperation(nickname = "removeAddress", value = "Remove wishlist entry.", notes = "Removes wishlist entry.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseStatus(HttpStatus.OK)
	public void removeWishlistEntry(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK, @ApiParam(value = "Product code.", required = true)
	@PathVariable
	final String productCode) throws WishlistException
	{
		wishlistFacade.removeWishlistEntryForProduct(wishlistPK, productCode);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}", method = RequestMethod.DELETE)
	@ApiOperation(nickname = "removeAddress", value = "Removes wishlist.", notes = "Removes wishlist.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseStatus(HttpStatus.OK)
	public void removeWishlist(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK) throws WishlistException
	{
		wishlistFacade.removeWishListByPK(wishlistPK);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}/entries", method = RequestMethod.DELETE)
	@ApiOperation(nickname = "removeAddress", value = "Removes all wishlist entries.", notes = "Removes all wishlist entries.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseStatus(HttpStatus.OK)
	public void removeWishlistEntries(@ApiParam(value = "Wishlist PK.", required = true)
	@PathVariable
	final String wishlistPK) throws WishlistException
	{
		wishlistFacade.removeAllWishlistEntries(wishlistPK);
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{wishlistPK}", method = RequestMethod.GET)
	@ApiOperation(nickname = "getWishlistByPk", value = "get Wishlist by pk", notes = "get Wishlist by pk")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public WishlistWsDTO getWishlistByPk(
			@ApiParam(value = "Wishlist PK for a specific wishlist or the literal 'default' for the default wishlist", required = true)
			@PathVariable(name = "wishlistPK")
			final String wishlistPK, @ApiFieldsParam
			@RequestParam(defaultValue = DEFAULT_FIELD_SET)
			final String fields) throws WishlistException
	{
		String tempPK = wishlistPK;
		if (DEFAULT_WISHLIST.equalsIgnoreCase(wishlistPK))
		{
			final Optional<WishlistData> wishList = wishlistFacade.getDefaultWishList();
			tempPK = wishList.isPresent() ? wishList.get().getPk() : null;
		}
		Optional<WishlistData> wishlistByPK = Optional.empty();
		try
		{
			wishlistByPK = wishlistFacade.getWishlistByPK(tempPK);
		}
		catch (final WishlistException e)
		{
			return new WishlistWsDTO();
		}
		if (wishlistByPK.isPresent())
		{
			final WishlistWsDTO map = getDataMapper().map(wishlistByPK.get(), WishlistWsDTO.class, fields);
			return map;
		}
		return new WishlistWsDTO();
	}

}
