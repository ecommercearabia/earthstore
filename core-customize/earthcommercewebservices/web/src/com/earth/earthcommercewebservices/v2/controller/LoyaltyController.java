/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercewebservicescommons.core.user.data.LoyaltyCustomerHistoriesData;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyBalanceWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerHistoriesWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerInfoWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerQrWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.data.LoyaltyCustomerTransactionHistoryWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.earth.earthfacades.user.customer.facade.CustomCustomerFacade;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerHistoryData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaginationData;
import com.earth.earthloyaltyprogramfacades.facades.LoyaltyPaymentFacade;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/loyalties")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Loyalty")
public class LoyaltyController extends BaseCommerceController
{
	@Resource(name = "loyaltyPaymentFacade")
	private LoyaltyPaymentFacade loyaltyPaymentFacade;

	@Resource(name = "customCustomerFacade")
	private CustomCustomerFacade customCustomerFacade;

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerInfo", value = "Get Loyalty customer info.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyCustomerInfoWsDTO getCustomerInfo(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, @ApiParam(value = "Optional pagination parameter. Default value 0.")
	@RequestParam(defaultValue = DEFAULT_CURRENT_PAGE)
	final int currentPage, @ApiParam(value = "Optional {@link PaginationData} parameter. Default value 20.")
	@RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
	final int pageSize)
	{
		//
		//		final LoyaltyPaginationData loyaltyPaginationData = getDataMapper().map(loyaltyPagintaion, LoyaltyPaginationData.class,
		//				"pageIndex,pageSize");

		final LoyaltyPaginationData loyaltyPaginationData = new LoyaltyPaginationData();
		loyaltyPaginationData.setPageIndex(currentPage);
		loyaltyPaginationData.setPageSize(pageSize);

		final Optional<LoyaltyCustomerInfoData> customerInfoData = loyaltyPaymentFacade
				.getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(loyaltyPaginationData);

		if (customerInfoData.isEmpty())
		{
			return null;
		}
		final List<LoyaltyCustomerHistoryData> histories = customerInfoData.get().getHistories();
		customerInfoData.get().setHistories(null);
		final LoyaltyCustomerInfoWsDTO infoWsDTO = getDataMapper().map(customerInfoData.get(), LoyaltyCustomerInfoWsDTO.class);
		if (!CollectionUtils.isEmpty(histories))
		{
			final LoyaltyCustomerHistoriesData historiesData = new LoyaltyCustomerHistoriesData();
			historiesData.setHistories(histories);

			final LoyaltyCustomerHistoriesWsDTO historiesWsDTO = getDataMapper().map(historiesData,
					LoyaltyCustomerHistoriesWsDTO.class);
			infoWsDTO.setHistories(historiesWsDTO);
		}


		return infoWsDTO;
	}

	@RequestMapping(value = "/qr", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerInfo", value = "Get Loyalty customer info.", notes = "Loyalty customer info")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyCustomerQrWsDTO getCustomerQrCode(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<LoyaltyCustomerQrData> customerQrCode = loyaltyPaymentFacade
				.getLoyaltyCustomerQrCodeByCurrentBaseStoreAndCustomer();
		return customerQrCode.isPresent() ? getDataMapper().map(customerQrCode.get(), LoyaltyCustomerQrWsDTO.class) : null;

	}

	@RequestMapping(value = "/balance", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerBalance", value = "Get Loyalty customer balance.", notes = "Loyalty customer balance")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyBalanceWsDTO getCustomerBalance(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{

		final Optional<LoyaltyBalanceData> loyaltyBalanceData = loyaltyPaymentFacade.getBalanceByCurrentBaseStoreAndCustomer();
		if (loyaltyBalanceData.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(loyaltyBalanceData.get(), LoyaltyBalanceWsDTO.class);

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(nickname = "registerCustomerInLoyalty", value = "regsiter a customer in loyalty", notes = "Loyalty customer registeration")
	@ApiBaseSiteIdAndUserIdParam
	public void registerLoyaltyCustomer(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws EarthLoyaltyException, ParseException
	{
			getCustomCustomerFacade().registerCustomerInLoyaltyByCurrentCustomer();
	}

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "getCustomerHistory", value = "get loyalty hisory", notes = "Loyalty history")
	@ApiBaseSiteIdAndUserIdParam
	public LoyaltyCustomerTransactionHistoryWsDTO loyaltyCustomerHistory(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws EarthLoyaltyException, ParseException
	{
		final Optional<Object> loyaltyHistoryData = loyaltyPaymentFacade.getTransactionHistoryByCurrentBaseStoreAndCustomer();
		if (loyaltyHistoryData.isEmpty())
		{
			return null;
		}

		return getDataMapper().map(loyaltyHistoryData.get(), LoyaltyCustomerTransactionHistoryWsDTO.class);

	}


	/**
	 * @return the customCustomerFacade
	 */
	protected CustomCustomerFacade getCustomCustomerFacade()
	{
		return customCustomerFacade;
	}

}
