/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcommercewebservices.v2.controller;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.commercefacades.user.data.CityData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.enums.CountryType;
import de.hybris.platform.commercewebservicescommons.core.user.data.AreaDataList;
import de.hybris.platform.commercewebservicescommons.core.user.data.CityDataList;
import de.hybris.platform.commercewebservicescommons.dto.user.AreaListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.CityListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.CountryListWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.RegionListWsDTO;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.earth.earthcommercewebservices.service.DeliveryLocationOCCService;
import com.earth.earthcommercewebservices.user.data.CountryDataList;
import com.earth.earthcommercewebservices.user.data.RegionDataList;
import com.earth.earthcommercewebservices.dto.site.address.SiteDefaultAddressWsDTO;
import com.earth.earthfacades.customer.SiteDeliveryCityAreaData;
import com.earth.earthfacades.user.data.CountryListData;
import com.earth.earthfacades.user.area.facade.AreaFacade;
import com.earth.earthfacades.user.city.facade.CityFacade;
import com.earth.earthfacades.user.country.facade.CountryFacade;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping(value = "/{baseSiteId}/countries")
@CacheControl(directive = CacheControlDirective.PRIVATE, maxAge = 120)
@Api(tags = "Countries")
public class CountriesController extends BaseCommerceController
{
	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "deliveryLocationOCCService")
	private DeliveryLocationOCCService deliveryLocationOCCService;

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;

	@Resource(name = "areaFacade")
	private AreaFacade areaFacade;

	@Resource(name = "countryFacade")
	private CountryFacade countryFacade;

	@RequestMapping(method = RequestMethod.GET)
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCountries',#type,#fields)")
	@ResponseBody
	@ApiOperation(nickname = "getCountries", value = "Get a list of countries.", notes = "If the value of type equals to shipping, then return shipping countries. If the value of type equals to billing, then return billing countries."
			+ " If the value of type is not given, return all countries. The list is sorted alphabetically.")
	@ApiBaseSiteIdParam
	public CountryListWsDTO getCountries(@ApiParam(value = "The type of countries.", allowableValues = "SHIPPING,BILLING")
	@RequestParam(required = false)
	final String type, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		if (StringUtils.isNotBlank(type) && !CountryType.SHIPPING.toString().equalsIgnoreCase(type)
				&& !CountryType.BILLING.toString().equalsIgnoreCase(type))
		{
			throw new IllegalStateException(String.format("The value of country type : [%s] is invalid", type));
		}

		final CountryDataList dataList = new CountryDataList();
		dataList.setCountries(checkoutFacade.getCountries(StringUtils.isNotBlank(type) ? CountryType.valueOf(type) : null));
		return getDataMapper().map(dataList, CountryListWsDTO.class, fields);
	}

	@GetMapping("/{countyIsoCode}/regions")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getRegionsForCountry',#countyIsoCode,#fields)")
	@ApiOperation(nickname = "getCountryRegions", value = "Fetch the list of regions for the provided country.", notes = "Lists all regions.")
	@ApiBaseSiteIdParam
	public RegionListWsDTO getCountryRegions(@ApiParam(value = "An ISO code for a country", required = true)
	@PathVariable
	final String countyIsoCode, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final RegionDataList regionDataList = new RegionDataList();
		regionDataList.setRegions(i18NFacade.getRegionsForCountryIso(countyIsoCode.toUpperCase(Locale.ENGLISH)));

		return getDataMapper().map(regionDataList, RegionListWsDTO.class, fields);
	}

	@GetMapping("/cities/areas/default")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getDefaultCountryAndCityAndArea',#fields)")
	@ApiOperation(nickname = "getDefaultCountryAndCityAndArea", value = "Get site default country and city and area codes", notes = "From Site.")
	@ApiBaseSiteIdParam
	public SiteDefaultAddressWsDTO getDefaultCountryAndCityAndArea(@ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<SiteDeliveryCityAreaData> cityAndArea = deliveryLocationOCCService.getDeliveryLocationDataByCurrentSite();

		if (cityAndArea.isEmpty())
		{
			return new SiteDefaultAddressWsDTO();
		}

		return getDataMapper().map(cityAndArea.get(), SiteDefaultAddressWsDTO.class);
	}

	@GetMapping("/{countyIsoCode}/cities")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getCities',#countyIsoCode,#fields)")
	@ApiOperation(nickname = "getCities", value = "Get the list of cities for the provided country.", notes = "Lists all cities.")
	@ApiBaseSiteIdParam
	public CityListWsDTO getCities(@ApiParam(value = "An ISO code for a country", required = true)
	@PathVariable
	final String countyIsoCode, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final CityDataList cityDataList = new CityDataList();
		final Optional<List<CityData>> byCountryIsocode = cityFacade.getByCountryIsocode(countyIsoCode);
		if (byCountryIsocode.isPresent())
		{
			cityDataList.setCities(byCountryIsocode.get());
		}

		return getDataMapper().map(cityDataList, CityListWsDTO.class, fields);
	}

	@GetMapping("/{countyIsoCode}/cities/{cityCode}/areas")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getAreas',#countyIsoCode,#cityCode,#fields)")
	@ApiOperation(nickname = "getAreas", value = "Get the list of areas for the provided city.", notes = "Lists all areas.")
	@ApiBaseSiteIdParam
	public AreaListWsDTO getAreas(@ApiParam(value = "An ISO code for a country", required = true)
	@PathVariable
	final String countyIsoCode, @ApiParam(value = "A code for a city", required = true)
	@PathVariable
	final String cityCode, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final AreaDataList areaDataList = new AreaDataList();
		final Optional<List<AreaData>> areas = areaFacade.getByCityCode(cityCode);
		if (areas.isPresent())
		{
			areaDataList.setAreas(areas.get());
		}
		return getDataMapper().map(areaDataList, AreaListWsDTO.class, fields);
	}

	@GetMapping("/mobile")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	@Cacheable(value = "countriesCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator).generateKey(false,false,'getMobileCountries',#fields)")
	@ApiOperation(nickname = "getMobileCountries", value = "Get the list of mobile countries", notes = "Lists all mobile countries.")
	@ApiBaseSiteIdParam
	public CountryListWsDTO getMobileCountries(@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields)
	{
		final Optional<List<CountryData>> mobileCountries = countryFacade.getMobileCountriesByCuruntSite();
		if (mobileCountries.isEmpty())
		{
			return new CountryListWsDTO();
		}
		final CountryListData listData = new CountryListData();
		listData.setCountries(mobileCountries.get());
		return getDataMapper().map(listData, CountryListWsDTO.class);
	}

}
