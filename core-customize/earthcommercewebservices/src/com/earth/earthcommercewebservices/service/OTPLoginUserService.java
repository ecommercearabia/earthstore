/**
 *
 */
package com.earth.earthcommercewebservices.service;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import com.earth.earthotp.exception.OTPException;


/**
 * @author monzer
 *
 */
public interface OTPLoginUserService
{

	CustomerData getUserByEmail(String email) throws OTPException;

	boolean sendOtpCodeToCustomer(String email) throws OTPException;

	boolean verifyOtpCode(String email, String otpCode) throws OTPException;

}
