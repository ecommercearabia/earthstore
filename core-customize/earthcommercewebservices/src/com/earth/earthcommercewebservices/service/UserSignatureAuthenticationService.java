/**
 *
 */
package com.earth.earthcommercewebservices.service;

/**
 * @author monzer
 *
 */
public interface UserSignatureAuthenticationService
{

	boolean canCustomerLoginWithSignature(String customerUid, String customerSignature);

}
