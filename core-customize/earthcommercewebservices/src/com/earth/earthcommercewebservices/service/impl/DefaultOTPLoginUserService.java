/**
 *
 */
package com.earth.earthcommercewebservices.service.impl;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.earthcommercewebservices.service.OTPLoginUserService;
import com.earth.earthotp.context.OTPContext;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthotp.exception.enums.OTPExceptionType;


/**
 * @author monzer
 *
 */
public class DefaultOTPLoginUserService implements OTPLoginUserService
{

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "customCustomerFacade")
	private CustomerFacade customerFacade;

	@Override
	public CustomerData getUserByEmail(final String email) throws OTPException
	{
		final CustomerData user = customerFacade.getUserForUID(email);
		if (user == null || user.getMobileCountry() == null || StringUtils.isBlank(user.getMobileCountry().getIsocode())
				|| StringUtils.isBlank(user.getMobileNumber()))
		{
			throw new OTPException(OTPExceptionType.MESSAGE_CAN_NOT_BE_SENT,
					"user " + email + " does not have a valid mobile number");
		}
		return user;
	}

	@Override
	public boolean sendOtpCodeToCustomer(final String email) throws OTPException
	{
		final CustomerData customer = getUserByEmail(email);
		otpContext.sendOTPCodeByCurrentSite(customer.getMobileCountry().getIsocode(), customer.getMobileNumber());
		return true;
	}

	@Override
	public boolean verifyOtpCode(final String email, final String otpCode) throws OTPException
	{
		final CustomerData customer = getUserByEmail(email);
		return otpContext.verifyCodeByCurrentSite(customer.getMobileCountry().getIsocode(), customer.getMobileNumber(), otpCode);
	}

}
