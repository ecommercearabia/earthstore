/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthassistedservicecustomerinterestscustomaddon.jalo;

import com.earth.earthassistedservicecustomerinterestscustomaddon.constants.EarthassistedservicecustomerinterestscustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthassistedservicecustomerinterestscustomaddonManager extends GeneratedEarthassistedservicecustomerinterestscustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthassistedservicecustomerinterestscustomaddonManager.class.getName() );
	
	public static final EarthassistedservicecustomerinterestscustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthassistedservicecustomerinterestscustomaddonManager) em.getExtension(EarthassistedservicecustomerinterestscustomaddonConstants.EXTENSIONNAME);
	}
	
}
