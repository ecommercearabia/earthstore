/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.paymentredirect.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.earth.earthpayment.exception.PaymentException;
import com.google.gson.JsonObject;


/**
 *
 */
@Controller
@RequestMapping(value = "/mpgs/3ds")
public class MPGSPayment3DSRedirectController
{

	private static final String AMPERSAND = "&";
	private static final String EQUALS = "=";
	private static final String MPGS_REDIRECT = "mpgsRedirect";
	private static final String UTF_8 = "UTF-8";
	private static final String BODY = "body";

	@RequestMapping(method =
	{ RequestMethod.POST, RequestMethod.GET })
	public String processAndValidRedirect(final HttpServletRequest request, final Model model) throws IOException, PaymentException
	{
		final String result = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
		final String[] parts = result.split(AMPERSAND);

		final JsonObject json = new JsonObject();

		for (final String part : parts)
		{
			final String[] keyVal = part.split(EQUALS); // The equal separates key and values
			json.addProperty(keyVal[0], StringUtils.isBlank(keyVal[1]) ? "" : keyVal[1]);
		}
		model.addAttribute(BODY, URLEncoder.encode(json.toString(), UTF_8));
		return MPGS_REDIRECT;
	}

}
