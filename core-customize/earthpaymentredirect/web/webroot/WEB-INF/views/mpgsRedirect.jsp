<%@page import="org.apache.poi.util.SystemOutLogger"%>
<%@page import="com.mashape.unirest.http.Unirest"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.GsonBuilder"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Earth Payment</title>
<link rel="stylesheet"
	href="<c:url value="/static/earthpaymentredirect-webapp.css"/>"
	type="text/css" media="screen, projection" />
</head>

<div class="container">
	<img src="<c:url value="${logoUrl}" />" alt="Hybris platform logo" />

	<h2>Please wait, you will be redirected soon ..</h2>

	<%
		response.sendRedirect("gatewaysdk://3dsecure?acsResult="+request.getAttribute("body"));	
	%>
</div>
</html>