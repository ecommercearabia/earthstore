/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.paymentredirect.constants;

/**
 * Global class for all Earthpaymentredirect constants. You can add global constants for your extension into this class.
 */
public final class EarthpaymentredirectConstants extends GeneratedEarthpaymentredirectConstants
{
	public static final String EXTENSIONNAME = "earthpaymentredirect";

	private EarthpaymentredirectConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthpaymentredirectPlatformLogo";
}
