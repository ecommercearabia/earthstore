/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.paymentredirect.setup;

import static com.earth.paymentredirect.constants.EarthpaymentredirectConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.earth.paymentredirect.constants.EarthpaymentredirectConstants;
import com.earth.paymentredirect.service.EarthpaymentredirectService;


@SystemSetup(extension = EarthpaymentredirectConstants.EXTENSIONNAME)
public class EarthpaymentredirectSystemSetup
{
	private final EarthpaymentredirectService earthpaymentredirectService;

	public EarthpaymentredirectSystemSetup(final EarthpaymentredirectService earthpaymentredirectService)
	{
		this.earthpaymentredirectService = earthpaymentredirectService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		earthpaymentredirectService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return EarthpaymentredirectSystemSetup.class.getResourceAsStream("/earthpaymentredirect/sap-hybris-platform.png");
	}
}
