/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthnotificationcustomaddon.controllers;

/**
 */
public interface EarthnotificationcustomaddonControllerConstants
{
	// implement here controller constants used by this extension
}
