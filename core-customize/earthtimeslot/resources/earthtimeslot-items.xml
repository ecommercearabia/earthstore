<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
-->
<items 	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
			xsi:noNamespaceSchemaLocation="items.xsd">
	<atomictypes>
   		<atomictype class="java.time.ZonedDateTime" extends="java.lang.Object" autocreate="true" />
	</atomictypes>
	<enumtypes>
		<enumtype code="TimeFormat" generate="true" autocreate="true">
			<description>This ENUM type will be used to identify the display of the time if 12 hours or 24 hours</description>
			<value code="HOUR_12" />
			<value code="HOUR_24" />
		</enumtype>
		<enumtype code="TimeSlotConfigType" generate="true" autocreate="true">
			<description>To determine getting the config by delivery mode or area</description>
			<value code="BY_AREA" />
			<value code="BY_DELIVERYMODE" />
		</enumtype>
	</enumtypes>
	<relations>
		<relation code="ZoneDeliveryMode2TimeSlotRel" localized="false"
			generate="true" autocreate="true">
			<description>This relation connects ZoneDeliveryMode with the TimeSlot</description>
			<sourceElement qualifier="zoneDeliveryModes" type="ZoneDeliveryMode" cardinality="many" collectiontype="set"/>
			<targetElement qualifier="timeSlot" type="TimeSlot" cardinality="one" />
		</relation>
		<relation code="Area2TimeSlotRel" localized="false"
			generate="true" autocreate="true">
			<description>This relation connects Area with the TimeSlot</description>
			<sourceElement qualifier="areas" type="Area" cardinality="many" collectiontype="set"/>
			<targetElement qualifier="timeSlot" type="TimeSlot" cardinality="one" />
		</relation>
		<relation code="TimeSlot2TimeSlotWeekDayRel" generate="true" localized="false" autocreate="true">
			<sourceElement type="TimeSlot" qualifier="timeSlot" cardinality="one">
				<modifiers read="true" write="false" optional="false" search="true" initial="true" unique="true"/>
			</sourceElement>
			<targetElement type="TimeSlotWeekDay" qualifier="timeSlotWeekDays" collectiontype="set" cardinality="many">
				<modifiers read="true" write="true" optional="true" search="true" partof="true"/>
			</targetElement>
		</relation>
		<relation code="TimeSlotWeekDay2PeriodRel" generate="true" localized="false" autocreate="true">
			<sourceElement type="TimeSlotWeekDay" qualifier="timeSlotWeekDay" cardinality="one">
			</sourceElement>
			<targetElement type="Period" qualifier="periods" collectiontype="set" cardinality="many">
				<modifiers read="true" write="true" optional="true" search="true" partof="true"/>
			</targetElement>
		</relation>
	</relations>
	<itemtypes>
	
		<itemtype generate="true" code="TimeSlot" autocreate="true"
			jaloclass="com.earth.earthtimeslot.jalo.TimeSlot">
			<deployment table="TimeSlot" typecode="10912" />
			<attributes>
				<attribute qualifier="code" type="java.lang.String">
					<description>Code Unique String Value</description>
					<modifiers read="true" write="true" optional="false"
						unique="true" initial="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="numberOfDays" type="int">
						<description>It controls the number of days to display in the timeslot checkout step</description>
						<modifiers read="true" write="true" optional="true" />
						<persistence type="property" />
						<defaultvalue>Integer.valueOf(7)</defaultvalue>
				</attribute>
				<attribute qualifier="timezone" type="java.lang.String">
					<description>Timezone of the Timeslot</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<defaultvalue>java.lang.String.valueOf("UTC")</defaultvalue>
				</attribute>
				<attribute qualifier="timeFormat" type="TimeFormat">
					<description>Time Format of the Timeslot</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<defaultvalue>em().getEnumerationValue("TimeFormat", "HOUR_24")</defaultvalue>
				</attribute>
				<attribute qualifier="active" type="boolean">
					<description>Active/Inactive</description>
					<modifiers read="true" write="true" optional="false"/>
					<persistence type="property" />
					<defaultvalue>Boolean.FALSE</defaultvalue>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype generate="true" code="TimeSlotWeekDay" autocreate="true"
			jaloclass="com.earth.earthtimeslot.jalo.TimeSlotWeekDay">
			<deployment table="TimeSlotWeekDay" typecode="10913" />
			<attributes>
				<attribute qualifier="day" type="WeekDay">
					<description>Unique Day ENUM value</description>
					<modifiers read="true" write="true" optional="false"
						unique="true" initial="true"/>
					<persistence type="property" />
				</attribute>
				<attribute qualifier="name" type="localized:java.lang.String">
					<description>Localized name of the Day</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="active" type="boolean">
					<description>Active/Inactive</description>
					<modifiers read="true" write="true" optional="false"/>
					<persistence type="property" />
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype generate="true" code="Period" autocreate="true"
			jaloclass="com.earth.earthtimeslot.jalo.Period">
			<deployment table="Period" typecode="10914" />
			<attributes>
				<attribute qualifier="code" type="java.lang.String">
					<description>Code Unique String Value</description>
					<modifiers read="true" write="true" optional="false"
						unique="true" initial="true"/>
					<persistence type="property" />
				</attribute>
				<attribute qualifier="start" type="java.lang.String">
					<description>Start of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="end" type="java.lang.String">
					<description>End of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="capacity" type="int">
						<description>It controls the capacity of orders allowed</description>
						<modifiers read="true" write="true" optional="true" />
						<persistence type="property" />
				</attribute>
				<attribute qualifier="expiry" type="long">
						<description>Time span to disable this period before the actual period start time</description>
						<modifiers read="true" write="true" optional="true" />
						<persistence type="property" />
						<defaultvalue>Long.valueOf(3600)</defaultvalue>
				</attribute>
				<attribute qualifier="active" type="boolean">
					<description>Active/Inactive</description>
					<modifiers read="true" write="true" optional="false"/>
					<persistence type="property" />
					<defaultvalue>Boolean.TRUE</defaultvalue>
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype generate="true" code="TimeSlotInfo" autocreate="true"
			jaloclass="com.earth.earthtimeslot.jalo.TimeSlotInfo">
			<deployment table="TimeSlotInfo" typecode="10916" />
			<attributes>
				<attribute qualifier="periodCode" type="java.lang.String">
					<description>Code of the chosen time slot period</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="start" type="java.lang.String">
					<description>Start of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="end" type="java.lang.String">
					<description>End of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="date" type="java.lang.String">
					<description>Date of chosen timeslot</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="day" type="java.lang.String">
					<description>The selected time slot day</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="timezone" type="java.lang.String">
					<description>Timezone of the Timeslot</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
					<defaultvalue>java.lang.String.valueOf("Asia/Dubai")</defaultvalue>
				</attribute>
				<attribute qualifier="startDate" type="java.util.Date">
					<description>Start of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="startHour" type="int">
					<description>Hour of the Start of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
				<attribute qualifier="startMinute" type="int">
					<description>Hour of the Start of the slot time interval</description>
					<modifiers read="true" write="true" optional="true" />
					<persistence type="property" />
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="AbstractOrder" autocreate="false" generate="false">
			<attributes>
				<attribute qualifier="timeSlotInfo" type="TimeSlotInfo">
					<description>Info of the chosen time slot</description>
					<modifiers read="true" write="true" optional="true" partof="true"/>
					<persistence type="property" />
				</attribute>
			</attributes>
		</itemtype>
		
		<itemtype code="CMSSite" autocreate="false" generate="false">
			<attributes>
				<attribute qualifier="timeSlotConfigType" type="TimeSlotConfigType" >
					<modifiers read="true" write="true" search="true" optional="true" />
					<persistence type="property" />
					<description>Time Slot Configurations Type</description>
					<defaultvalue>em().getEnumerationValue("TimeSlotConfigType", "BY_DELIVERYMODE")</defaultvalue>
				</attribute>
				
				<attribute qualifier="timeSlotDisabled" type="boolean" >
					<modifiers read="true" write="true" search="true" optional="true" />
					<persistence type="property" />
					<description>Time Slot Disabled</description>
				</attribute>	
			</attributes>
		</itemtype>
	</itemtypes>
</items>
