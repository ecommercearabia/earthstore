/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthtimeslot.constants;

/**
 * Global class for all Earthtimeslot constants. You can add global constants for your extension into this class.
 */
public final class EarthtimeslotConstants extends GeneratedEarthtimeslotConstants
{
	public static final String EXTENSIONNAME = "earthtimeslot";

	private EarthtimeslotConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
