/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthinitialdata.constants;

/**
 * Global class for all Earthinitialdata constants.
 */
public final class EarthinitialdataConstants extends GeneratedEarthinitialdataConstants
{
	public static final String EXTENSIONNAME = "earthinitialdata";

	private EarthinitialdataConstants()
	{
		//empty
	}
}
