/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorecreditfacades.constants;

/**
 * Global class for all Earthstorecreditfacades constants. You can add global constants for your extension into this class.
 */
public final class EarthstorecreditfacadesConstants extends GeneratedEarthstorecreditfacadesConstants
{
	public static final String EXTENSIONNAME = "earthstorecreditfacades";

	private EarthstorecreditfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthstorecreditfacadesPlatformLogo";
}
