/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.sourcingbanconfig.service.impl;

import de.hybris.platform.warehousing.sourcing.ban.service.impl.DefaultSourcingBanService;

import javax.annotation.Resource;

import com.earth.earthcore.model.SourcingBanConfigModel;
import com.earth.earthcore.sourcingbanconfig.dao.SourcingBanConfigurationDao;
import com.earth.earthcore.sourcingbanconfig.service.CustomSourcingBanService;


/**
 * The Class DefaultCustomSourcingBanService.
 *
 * @author monzer
 */
public class DefaultCustomSourcingBanService extends DefaultSourcingBanService implements CustomSourcingBanService
{

	/** The sourcing ban config dao. */
	@Resource(name = "sourcingBanConfigurationDao")
	private SourcingBanConfigurationDao sourcingBanConfigDao;

	/**
	 * Gets the sourcing ban configuration.
	 *
	 * @return the sourcing ban configuration
	 */
	@Override
	public SourcingBanConfigModel getSourcingBanConfiguration()
	{
		return sourcingBanConfigDao.getSourcingBanConfiguration();
	}

}
