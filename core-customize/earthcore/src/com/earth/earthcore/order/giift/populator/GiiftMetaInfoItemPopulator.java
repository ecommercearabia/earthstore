/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.order.giift.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramprovider.giift.beans.metadata.GiiftItem;
import com.earth.earthcore.service.CustomCommercePriceService;


/**
 *
 */
public class GiiftMetaInfoItemPopulator implements Populator<AbstractOrderEntryModel, GiiftItem>
{

	@Resource(name = "customCommercePriceService")
	private CustomCommercePriceService customCommercePriceService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(final AbstractOrderEntryModel source, final GiiftItem target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (!Objects.isNull(source.getProduct()))
		{
			target.setCode(source.getProduct().getCode());
			//target.setUomCode(source.getProduct();
		}

		target.setQuantity(source.getQuantity());
		target.setSign(1);

		double basePrice = 0;
		double taxValue = 0.0;
		double taxAppliedValue = 0.0;
		double netDiscount = 0.0;

		if (source.getBasePrice() != null)
		{
			basePrice = createPrice(source, source.getBasePrice()).getValue().doubleValue();
		}


		if (!CollectionUtils.isEmpty(source.getTaxValues()))
		{
			for (final TaxValue value : source.getTaxValues())
			{
				taxValue += value.getValue();
				taxAppliedValue += value.getAppliedValue();
			}
		}
		if (!CollectionUtils.isEmpty(source.getDiscountValues()))
		{
			for (final DiscountValue value : source.getDiscountValues())
			{
				netDiscount += value.getAppliedValue();
			}
		}
		basePrice -= netDiscount / source.getQuantity();

		final double amount = basePrice * source.getQuantity();
		target.setUnitPrice(formatDouble(basePrice));
		target.setTotalPrice(formatDouble(amount));
		target.setVatAmount(formatDouble(taxAppliedValue));
		target.setVatCode("VAT");
		target.setVatPercent(Math.ceil(taxValue));
	}


	protected PriceData createPrice(final AbstractOrderEntryModel orderEntry, final Double val)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
				orderEntry.getOrder().getCurrency());
	}

	protected Double getBasePrice(final AbstractOrderEntryModel source)
	{
		if (source != null && source.getProduct() != null)
		{
			final PriceInformation info;
			if (CollectionUtils.isEmpty(source.getProduct().getVariants()))
			{
				info = getCustomCommercePriceService().getWebPriceForProductWithTax(source.getProduct(), true);
			}
			else
			{
				info = getCustomCommercePriceService().getFromPriceForProduct(source.getProduct());
			}

			if (info != null && info.getPriceValue() != null)
			{
				return info.getPriceValue().getValue();
			}
		}
		return source == null ? null : source.getBasePrice();
	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.##");
		final String format = df.format(val);
		return Double.valueOf(format);
	}


	/**
	 * @return the customCommercePriceService
	 */
	public CustomCommercePriceService getCustomCommercePriceService()
	{
		return customCommercePriceService;
	}


	/**
	 * @param customCommercePriceService
	 *           the customCommercePriceService to set
	 */
	public void setCustomCommercePriceService(final CustomCommercePriceService customCommercePriceService)
	{
		this.customCommercePriceService = customCommercePriceService;
	}


	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}


	/**
	 * @param priceDataFactory
	 *           the priceDataFactory to set
	 */
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

}
