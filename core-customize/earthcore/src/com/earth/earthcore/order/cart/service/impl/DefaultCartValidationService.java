/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.order.cart.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthcore.model.CartAmountPriceRowModel;
import com.earth.earthcore.order.cart.exception.CartValidationException;
import com.earth.earthcore.order.cart.exception.enums.CartExceptionType;
import com.earth.earthcore.order.cart.service.CartValidationService;
import com.google.common.base.Preconditions;


/**
 * The Class DefaultCartValidationService.
 */
public class DefaultCartValidationService implements CartValidationService
{
	/** The Constant CART_MUSTN_T_BE_NULL. */
	private static final String CART_MUSTN_T_BE_NULL = "cartModel mustn't be null";

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCartValidationService.class);

	private static final int ERROR_ARRAY_SIZE = 5;

	private static final int PARAM_ARRAY_SIZE = 5;

	private static final String MIN_AMOUNT_MSG = "min cart Amount[{}], cart total price [{}], cart code [{}], includeDeliveryCost[{}]";
	private static final String MAX_AMOUNT_MSG = "max cart Amount[{}], cart total price [{}], cart code [{}], includeDeliveryCost[{}], includePaymentCost[{}]";

	/** The cart service. */
	@Resource(name = "cartService")
	private CartService cartService;

	/**
	 * Gets the cart service.
	 *
	 * @return the cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	/**
	 * Validate cart min amount.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	@Override
	public boolean validateCartMinAmount(final CartModel cartModel) throws CartValidationException
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> minCartAmountPrices = baseStoreModel.getMinCartAmountPrices();
		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(minCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return true;
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();

		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}
		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}
		LOG.info(MIN_AMOUNT_MSG, cartAmountPriceRowModel.getAmount(), totalPrice, cartModel.getCode(), includeDeliveryCost);

		if (totalPrice < cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final DecimalFormat format = new DecimalFormat("0.00");
			final String[] parm = new String[ERROR_ARRAY_SIZE];
			parm[0] = format.format(cartAmountPriceRowModel.getAmount().doubleValue());
			parm[1] = cartModel.getCurrency().getSymbol();
			parm[2] = totalPrice + "";
			parm[3] = cartModel.getCode();
			parm[4] = format.format((cartAmountPriceRowModel.getAmount().doubleValue() - totalPrice));
			throw new CartValidationException("You exceed the cart min amount limit ", CartExceptionType.MIN_AMOUNT, parm);
		}
		return true;
	}

	public Optional<String> getCartMinAmount(final CartModel cartModel)
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> minCartAmountPrices = baseStoreModel.getMinCartAmountPrices();
		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(minCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return Optional.empty();
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();

		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}
		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}

		final DecimalFormat format = new DecimalFormat("0.00");

		return Optional.ofNullable(
				format.format(cartAmountPriceRowModel.getAmount().doubleValue()) + " " + cartModel.getCurrency().getSymbol());
	}

	/**
	 * Gets the total price.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return the total price
	 */
	private double getTotalPrice(final CartModel cartModel)
	{
		final double storeCreditAmount = cartModel.getStoreCreditAmount() == null ? 0
				: cartModel.getStoreCreditAmount().doubleValue();
		final double totalPrice = cartModel.getTotalPrice().doubleValue();
		return totalPrice + storeCreditAmount;
	}

	/**
	 * Validate cart max amount.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	@Override
	public boolean validateCartMaxAmount(final CartModel cartModel) throws CartValidationException
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> maxCartAmountPrices = baseStoreModel.getMaxCartAmountPrices();

		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(maxCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return true;
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();
		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}

		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}
		LOG.info(MAX_AMOUNT_MSG, cartAmountPriceRowModel.getAmount(), totalPrice, cartModel.getCode(), includeDeliveryCost,
				includePaymentCost);

		if (totalPrice > cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final DecimalFormat format = new DecimalFormat("0.00");

			final String[] parm = new String[ERROR_ARRAY_SIZE];
			parm[0] = format.format(cartAmountPriceRowModel.getAmount().doubleValue());
			parm[1] = cartModel.getCurrency().getSymbol();
			parm[2] = totalPrice + "";
			parm[3] = cartModel.getCode();
			parm[4] = format.format((cartAmountPriceRowModel.getAmount().doubleValue() - totalPrice));

			throw new CartValidationException("You exceed the cart max amount limit ", CartExceptionType.MAX_AMOUNT, parm);
		}

		return true;
	}

	/**
	 * Gets the cart amount price row for cart currency.
	 *
	 * @param cartAmountPriceRowList
	 *           the cart amount price row list
	 * @param currency
	 *           the currency
	 * @return the cart amount price row for cart currency
	 */
	protected CartAmountPriceRowModel getCartAmountPriceRowForCartCurrency(
			final List<CartAmountPriceRowModel> cartAmountPriceRowList, final CurrencyModel currency)
	{
		if (cartAmountPriceRowList == null || cartAmountPriceRowList.isEmpty() || currency == null)
		{
			return null;
		}

		for (final CartAmountPriceRowModel cartAmountPriceRowModel : cartAmountPriceRowList)
		{
			if (currency.getIsocode().equals(cartAmountPriceRowModel.getCurrency().getIsocode()))
			{
				return cartAmountPriceRowModel;
			}
		}

		return null;
	}

	/**
	 * Validate cart min amount by current cart.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	@Override
	public boolean validateCartMinAmountByCurrentCart() throws CartValidationException
	{
		return getCartService().hasSessionCart() && validateCartMinAmount(getCartService().getSessionCart());
	}

	/**
	 * Validate cart max amount by current cart.
	 *
	 * @return true, if successful
	 * @throws CartValidationException
	 *            the cart validation exception
	 */
	@Override
	public boolean validateCartMaxAmountByCurrentCart() throws CartValidationException
	{
		return getCartService().hasSessionCart() && validateCartMaxAmount(getCartService().getSessionCart());
	}



	@Override
	public Optional<String> getCartMaxAmountLabel(final CartModel cartModel)
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> maxCartAmountPrices = baseStoreModel.getMaxCartAmountForLabelPrices();

		if (CollectionUtils.isEmpty(maxCartAmountPrices))
		{
			return Optional.empty();
		}


		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(maxCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return Optional.empty();
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();
		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}

		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}

		LOG.info(MAX_AMOUNT_MSG, cartAmountPriceRowModel.getAmount(), totalPrice, cartModel.getCode(), includeDeliveryCost,
				includePaymentCost);

		if (totalPrice > cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final DecimalFormat format = new DecimalFormat("0.00");

			final String[] params = new String[PARAM_ARRAY_SIZE];
			params[0] = format.format(cartAmountPriceRowModel.getAmount().doubleValue());
			params[1] = cartModel.getCurrency().getSymbol();
			params[2] = format.format(cartModel.getTotalPrice().doubleValue());
			params[3] = cartModel.getCode();
			if (StringUtils.isBlank(baseStoreModel.getMaxCartAmountLabel()))
			{
				return Optional.empty();
			}

			return Optional.ofNullable(new MessageFormat(baseStoreModel.getMaxCartAmountLabel()).format(params));

		}

		return Optional.empty();
	}


	@Override
	public Optional<String> getCartMinAmountLabel(final CartModel cartModel)
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();

		final List<CartAmountPriceRowModel> minCartAmountPrices = baseStoreModel.getMinCartAmountForLabelPrices();

		if (CollectionUtils.isEmpty(minCartAmountPrices))
		{
			return Optional.empty();
		}

		final CartAmountPriceRowModel cartAmountPriceRowModel = getCartAmountPriceRowForCartCurrency(minCartAmountPrices,
				cartModel.getCurrency());

		if (cartAmountPriceRowModel == null || cartAmountPriceRowModel.getAmount() == null)
		{
			return Optional.empty();
		}

		final boolean includeDeliveryCost = cartAmountPriceRowModel.isIncludeDeliveryCost();

		double totalPrice = getTotalPrice(cartModel);
		if (!includeDeliveryCost && cartModel.getDeliveryCost() != null)
		{
			totalPrice = totalPrice - cartModel.getDeliveryCost().doubleValue();
		}
		final boolean includePaymentCost = cartAmountPriceRowModel.isIncludePaymentCost();
		if (!includePaymentCost && cartModel.getPaymentCost() != null)
		{
			totalPrice = totalPrice - cartModel.getPaymentCost().doubleValue();
		}
		LOG.info(MIN_AMOUNT_MSG, cartAmountPriceRowModel.getAmount(), totalPrice, cartModel.getCode(), includeDeliveryCost);

		if (totalPrice < cartAmountPriceRowModel.getAmount().doubleValue())
		{
			final DecimalFormat format = new DecimalFormat("0.00");
			final String[] params = new String[PARAM_ARRAY_SIZE];
			params[0] = format.format(cartAmountPriceRowModel.getAmount().doubleValue());
			params[1] = cartModel.getCurrency().getSymbol();
			params[2] = totalPrice + "";
			params[3] = cartModel.getCode();
			params[4] = format.format((cartAmountPriceRowModel.getAmount().doubleValue() - totalPrice));
			if (StringUtils.isBlank(baseStoreModel.getMinCartAmountLabel()))
			{
				//				return Optional.ofNullable(String.join(",", params));
				return Optional.empty();
			}

			return Optional.ofNullable(new MessageFormat(baseStoreModel.getMinCartAmountLabel()).format(params));

		}
		return Optional.empty();
	}

	@Override
	public Optional<String> getCartMaxAmountLabelByCurrentCart()
	{
		if (!getCartService().hasSessionCart())
		{
			return Optional.empty();
		}

		return getCartMaxAmountLabel(getCartService().getSessionCart());
	}

	@Override
	public Optional<String> getCartMinAmountLabelByCurrentCart()
	{
		if (!getCartService().hasSessionCart())
		{
			return Optional.empty();
		}

		return getCartMinAmountLabel(getCartService().getSessionCart());
	}

	@Override
	public boolean validateCartDeliveryType(final CartModel cartModel) throws CartValidationException
	{
		Preconditions.checkArgument(cartModel != null, CART_MUSTN_T_BE_NULL);
		final BaseStoreModel baseStoreModel = cartModel.getStore();
		if (CollectionUtils.isEmpty(cartModel.getEntries()))
		{
			return true;
		}
		final AbstractOrderEntryModel abstractOrderEntryModel = cartModel.getEntries().get(0);
		final boolean pickup = abstractOrderEntryModel.getDeliveryPointOfService() != null;
		boolean valid = false;
		if (pickup)
		{
			valid = cartModel.getEntries().stream().allMatch(e -> e.getDeliveryPointOfService() != null);
		}
		else
		{
			valid = cartModel.getEntries().stream().allMatch(e -> e.getDeliveryPointOfService() == null);
		}
		if (!valid)
		{
			throw new CartValidationException("Your cart cannot contain pick up from store and shipping items",
					CartExceptionType.SHIPPINGMETHOD, null);
		}
		return valid;
	}

	@Override
	public boolean validateCartDeliveryTypeByCurrentCart() throws CartValidationException
	{
		return getCartService().hasSessionCart() && validateCartDeliveryType(getCartService().getSessionCart());
	}

	@Override
	public Optional<String> getCartMinAmountByCurrentCart()
	{
		return getCartService().hasSessionCart() ? getCartMinAmount(getCartService().getSessionCart()) : Optional.empty();
	}

}
