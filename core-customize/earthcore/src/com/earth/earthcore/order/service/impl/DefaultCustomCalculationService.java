/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.order.service.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.order.strategies.calculation.ServiceLayerOnlyCalculationVerifier;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.earth.earthloyaltyprogramprovider.strategy.FindLoyaltyAmountStrategy;
import com.earth.earthstorecredit.strategy.FindStoreCreditAmaountStrategy;


/**
 * The Class DefaultCustomCalculationService.
 *
 * @author amjad.shati@erabia.com
 */
public class DefaultCustomCalculationService extends AbstractBusinessService implements CalculationService
{

	/** The Constant _100_0. */
	private static final double _100 = 100.0;

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCustomCalculationService.class);

	/** The find taxes strategies. */
	private List<FindTaxValuesStrategy> findTaxesStrategies;

	/** The find discounts strategies. */
	private List<FindDiscountValuesStrategy> findDiscountsStrategies;

	/** The find price strategy. */
	private FindPriceStrategy findPriceStrategy;

	/** The find delivery cost strategy. */
	private FindDeliveryCostStrategy findDeliveryCostStrategy;

	/** The find payment cost strategy. */
	private FindPaymentCostStrategy findPaymentCostStrategy;

	/** The order requires calculation strategy. */
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

	/** The Constant saveOrderEntryUnneeded. */
	private static final ThreadLocal<Boolean> saveOrderEntryUnneeded = new ThreadLocal<>();

	/** The common I 18 N service. */
	private CommonI18NService commonI18NService;

	/** The find store credit amaount strategy. */
	private FindStoreCreditAmaountStrategy findStoreCreditAmaountStrategy;

	private FindLoyaltyAmountStrategy findLoyaltyAmountStrategy;


	/** The tax free entry support. */
	//see PLA-11851
	private boolean taxFreeEntrySupport = false;

	/**
	 * Calculate.
	 *
	 * @param order
	 *           the order
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void calculate(final AbstractOrderModel order) throws CalculationException
	{
		if (orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			try
			{
				markSaveOrderEntryUnneeded();
				// -----------------------------
				// first calc all entries
				calculateEntries(order, false);
				// -----------------------------
				// reset own values
				final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = resetAllValues(order);
				// -----------------------------
				// now calculate all totals
				calculateTotals(order, false, taxValueMap);
			}
			finally
			{
				unsetSaveOrderEntryUnneeded();
			}
			// notify manual discouns - needed? NO (MONZER)
		}
	}

	/**
	 * Requires calculation.
	 *
	 * @param order
	 *           the order
	 * @return true, if successful
	 */
	@Override
	public boolean requiresCalculation(final AbstractOrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		return orderRequiresCalculationStrategy.requiresCalculation(order);
	}

	/**
	 * Sets the calculated status.
	 *
	 * @param order
	 *           the new calculated status
	 */
	protected void setCalculatedStatus(final AbstractOrderModel order)
	{
		order.setCalculated(Boolean.TRUE);
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				entry.setCalculated(Boolean.TRUE);
			}
		}
	}

	/**
	 * Sets the calculated status.
	 *
	 * @param entry
	 *           the new calculated status
	 */
	protected void setCalculatedStatus(final AbstractOrderEntryModel entry)
	{
		entry.setCalculated(Boolean.TRUE);
	}


	/**
	 * Calculate.
	 *
	 * @param order
	 *           the order
	 * @param date
	 *           the date
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void calculate(final AbstractOrderModel order, final Date date) throws CalculationException
	{
		calculateInternalWithChangedOrderDate(order, date, false);
	}

	/**
	 * Calculate internal with changed order date.
	 *
	 * @param order
	 *           the order
	 * @param date
	 *           the date
	 * @param recalculate
	 *           the recalculate
	 * @throws CalculationException
	 *            the calculation exception
	 */
	private void calculateInternalWithChangedOrderDate(final AbstractOrderModel order, final Date date, final boolean recalculate)
			throws CalculationException
	{
		final Date old = order.getDate();
		try
		{
			order.setDate(date);
			getModelService().save(order);

			if (recalculate)
			{
				recalculate(order);
			}
			else
			{
				calculate(order);
			}
		}
		finally
		{
			order.setDate(old);
			getModelService().save(order);
		}
	}


	/**
	 * Calculate totals.
	 *
	 * @param order
	 *           the order
	 * @param recalculate
	 *           the recalculate
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void calculateTotals(final AbstractOrderModel order, final boolean recalculate) throws CalculationException
	{
		try
		{
			markSaveOrderEntryUnneeded();
			calculateTotals(order, recalculate, calculateSubtotal(order, recalculate));
		}
		finally
		{
			unsetSaveOrderEntryUnneeded();
		}
	}

	/**
	 * Save order.
	 *
	 * @param order
	 *           the order
	 */
	protected void saveOrder(final AbstractOrderModel order)
	{
		// since order *and* entries are changed we need to save them all
		final List<AbstractOrderEntryModel> entries = order.getEntries();

		if (CollectionUtils.isNotEmpty(entries))
		{
			final Collection<ItemModel> all = new ArrayList<>(entries);
			all.add(order);
			getModelService().saveAll(all);
		}
		else
		{
			getModelService().save(order);
		}
	}

	/**
	 * Gets the tax correction factor.
	 *
	 * @param taxValueMap
	 *           the tax value map
	 * @param subtotal
	 *           the subtotal
	 * @param total
	 *           the total
	 * @param order
	 *           the order
	 * @return the tax correction factor
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected double getTaxCorrectionFactor(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap, final double subtotal,
			final double total, final AbstractOrderModel order) throws CalculationException
	{
		// default: adjust taxes relative to total-subtotal ratio
		// Big decimal to avoid floating point compressions
		double factor = BigDecimal.valueOf(subtotal).compareTo(BigDecimal.ZERO) != 0 ? (total / subtotal) : 1.0;

		if (mustHandleTaxFreeEntries(taxValueMap, subtotal, order))
		{
			final double taxFreeSubTotal = getTaxFreeSubTotal(order);

			final double taxedTotal = total - taxFreeSubTotal;
			final double taxedSubTotal = subtotal - taxFreeSubTotal;

			// illegal state: taxed subtotal is <= 0 -> cannot calculate with that
			if (taxedSubTotal <= 0)
			{
				throw new CalculationException("illegal taxed subtotal " + taxedSubTotal + ", must be > 0");
			}
			// illegal state: taxed total is <= 0 -> no sense in having negative taxes (factor would become negative!)
			if (taxedTotal <= 0)
			{
				throw new CalculationException("illegal taxed total " + taxedTotal + ", must be > 0");
			}
			factor = BigDecimal.valueOf(taxedSubTotal).compareTo(BigDecimal.ZERO) != 0 ? (taxedTotal / taxedSubTotal) : 1.0;
		}
		return factor;
	}

	/**
	 * Must handle tax free entries.
	 *
	 * @param taxValueMap
	 *           the tax value map
	 * @param subtotal
	 *           the subtotal
	 * @param order
	 *           the order
	 * @return true, if successful
	 */
	//see PLA-11851: we must take special actions in case some entries DO NOT HAVE TAXES on them
	protected boolean mustHandleTaxFreeEntries(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap, final double subtotal,
			final AbstractOrderModel order)
	{
		return MapUtils.isNotEmpty(taxValueMap) // got taxes at all
				&& taxFreeEntrySupport // mode is enabled
				&& !isAllEntriesTaxed(taxValueMap, subtotal, order); // check sums whether some entries are contributing to tax map
	}

	/**
	 * Calculates the sub total of all order entries with NO tax values.
	 *
	 * @param order
	 *           the order
	 * @return the tax free sub total
	 */
	protected double getTaxFreeSubTotal(final AbstractOrderModel order)
	{
		double sum = 0;
		for (final AbstractOrderEntryModel e : order.getEntries())
		{
			if (CollectionUtils.isEmpty(e.getTaxValues()))
			{
				sum += e.getTotalPrice().doubleValue();
			}
		}
		return sum;
	}

	/**
	 * Checks if is all entries taxed.
	 *
	 * @param taxValueMap
	 *           the tax value map
	 * @param subtotal
	 *           the subtotal
	 * @param order
	 *           the order
	 * @return true, if is all entries taxed
	 */
	protected boolean isAllEntriesTaxed(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap, final double subtotal,
			final AbstractOrderModel order)
	{
		double sum = 0.0;
		final Set<Set<TaxValue>> consumedTaxGroups = new HashSet<>();
		for (final Map.Entry<TaxValue, Map<Set<TaxValue>, Double>> taxEntry : taxValueMap.entrySet())
		{
			for (final Map.Entry<Set<TaxValue>, Double> taxGroupEntry : taxEntry.getValue().entrySet())
			{
				if (consumedTaxGroups.add(taxGroupEntry.getKey())) // avoid duplicate occurrences of the same tax group
				{
					sum += taxGroupEntry.getValue().doubleValue();
				}
			}
		}
		// delta ( 2 digits ) == 10 ^-3 == 0.001
		final double allowedDelta = Math.pow(10d, -1d * (order.getCurrency().getDigits().intValue() + 1));
		return Math.abs(subtotal - sum) <= allowedDelta;
	}

	/**
	 * Recalculate.
	 *
	 * @param order
	 *           the order
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void recalculate(final AbstractOrderModel order) throws CalculationException
	{
		try
		{
			markSaveOrderEntryUnneeded();
			// -----------------------------
			// first force calc entries
			calculateEntries(order, true);
			// -----------------------------
			// reset own values
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = resetAllValues(order);
			// -----------------------------
			// now recalculate totals
			calculateTotals(order, true, taxValueMap);
			// notify discounts -- needed? NO (MONZER)
		}
		finally
		{
			unsetSaveOrderEntryUnneeded();
		}
	}

	/**
	 * Recalculate.
	 *
	 * @param order
	 *           the order
	 * @param date
	 *           the date
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void recalculate(final AbstractOrderModel order, final Date date) throws CalculationException
	{
		calculateInternalWithChangedOrderDate(order, date, true);
	}

	/**
	 * Calculate entries.
	 *
	 * @param order
	 *           the order
	 * @param forceRecalculate
	 *           the force recalculate
	 * @throws CalculationException
	 *            the calculation exception
	 */
	public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
	{
		double subtotal = 0.0;
		for (final AbstractOrderEntryModel e : order.getEntries())
		{
			recalculateOrderEntryIfNeeded(e, forceRecalculate);
			subtotal += e.getTotalPrice().doubleValue();
		}
		order.setTotalPrice(Double.valueOf(subtotal));

	}

	/**
	 * Calculate totals.
	 *
	 * @param entry
	 *           the entry
	 * @param recalculate
	 *           the recalculate
	 */
	@Override
	public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			final AbstractOrderModel order = entry.getOrder();
			final CurrencyModel curr = order.getCurrency();
			getModelService().refresh(curr);
			final int digits = curr.getDigits().intValue();
			final double totalPriceWithoutDiscount = commonI18NService
					.roundCurrency(entry.getBasePrice().doubleValue() * entry.getQuantity().longValue(), digits);
			final double quantity = entry.getQuantity().doubleValue();
			/*
			 * apply discounts (will be rounded each) convert absolute discount values in case their currency doesn't match
			 * the order currency
			 */
			final List<DiscountValue> appliedDiscounts = DiscountValue.apply(quantity, totalPriceWithoutDiscount, digits,
					convertDiscountValues(order, entry.getDiscountValues()), curr.getIsocode());
			entry.setDiscountValues(appliedDiscounts);
			double totalPrice = totalPriceWithoutDiscount;
			for (final Iterator<DiscountValue> it = appliedDiscounts.iterator(); it.hasNext();)
			{
				totalPrice -= it.next().getAppliedValue();
			}
			// set total price
			entry.setTotalPrice(Double.valueOf(totalPrice));
			// apply tax values too
			calculateTotalTaxValues(entry);
			setCalculatedStatus(entry);

			if (!isSaveOrderEntryUnneeded() || hasJaloStrategies())
			{
				getModelService().save(entry);
			}
		}
	}

	/**
	 * Checks for jalo strategies.
	 *
	 * @return true, if successful
	 */
	protected boolean hasJaloStrategies()
	{
		return getJaloStrategies().count() > 0;
	}

	/**
	 * Gets the jalo strategies.
	 *
	 * @return the jalo strategies
	 */
	private Stream<ServiceLayerOnlyCalculationVerifier> getJaloStrategies()
	{
		final List<ServiceLayerOnlyCalculationVerifier> strategies = new ArrayList<>();
		strategies.add(findDeliveryCostStrategy);
		strategies.add(findPaymentCostStrategy);
		strategies.add(findPriceStrategy);
		strategies.addAll(findDiscountsStrategies);
		strategies.addAll(findTaxesStrategies);

		return strategies.stream().filter(p -> (!p.isSLOnly()));
	}

	/**
	 * Calculate total tax values.
	 *
	 * @param entry
	 *           the entry
	 */
	protected void calculateTotalTaxValues(final AbstractOrderEntryModel entry)
	{
		final AbstractOrderModel order = entry.getOrder();
		final double quantity = entry.getQuantity().doubleValue();
		final double totalPrice = entry.getTotalPrice().doubleValue();
		final CurrencyModel curr = order.getCurrency();
		final int digits = curr.getDigits().intValue();

		entry.setTaxValues(
				TaxValue.apply(quantity, totalPrice, digits, entry.getTaxValues(), order.getNet().booleanValue(), curr.getIsocode()));
	}

	/**
	 * Recalculate order entry if needed.
	 *
	 * @param entry
	 *           the entry
	 * @param forceRecalculation
	 *           the force recalculation
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected void recalculateOrderEntryIfNeeded(final AbstractOrderEntryModel entry, final boolean forceRecalculation)
			throws CalculationException
	{
		if (forceRecalculation || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			resetAllValues(entry);
			calculateTotals(entry, true);
		}
	}

	/**
	 * Recalculate.
	 *
	 * @param entry
	 *           the entry
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public void recalculate(final AbstractOrderEntryModel entry) throws CalculationException
	{
		recalculateOrderEntryIfNeeded(entry, true);
	}

	/**
	 * Refresh order.
	 *
	 * @param order
	 *           the order
	 */
	protected void refreshOrder(final AbstractOrderModel order)
	{
		getModelService().refresh(order);
		for (final AbstractOrderEntryModel entry : order.getEntries())
		{
			getModelService().refresh(entry);
		}
	}

	/**
	 * Reset all values.
	 *
	 * @param entry
	 *           the entry
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		// taxes
		final Collection<TaxValue> entryTaxes = findTaxValues(entry);
		entry.setTaxValues(entryTaxes);
		final PriceValue pv = findBasePrice(entry);
		final AbstractOrderModel order = entry.getOrder();
		final PriceValue basePrice = convertPriceIfNecessary(pv, order.getNet().booleanValue(), order.getCurrency(), entryTaxes);
		entry.setBasePrice(Double.valueOf(basePrice.getValue()));
		final List<DiscountValue> entryDiscounts = findDiscountValues(entry);
		entry.setDiscountValues(entryDiscounts);
	}

	/**
	 * Reset all values.
	 *
	 * @param order
	 *           the order
	 * @return the map
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected Map<TaxValue, Map<Set<TaxValue>, Double>> resetAllValues(final AbstractOrderModel order) throws CalculationException
	{
		// -----------------------------
		// set subtotal and get tax value map
		final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = calculateSubtotal(order, false);
		/*
		 * filter just relative tax values - payment and delivery prices might require conversion using taxes -> absolute
		 * taxes do not apply here ask someone for absolute taxes and how they apply to delivery cost etc. - this
		 * implementation might be wrong now
		 */
		final Collection<TaxValue> relativeTaxValues = new LinkedList<>();
		for (final Map.Entry<TaxValue, ?> e : taxValueMap.entrySet())
		{
			final TaxValue taxValue = e.getKey();
			if (!taxValue.isAbsolute())
			{
				relativeTaxValues.add(taxValue);
			}
		}

		//PLA-10914
		final boolean setAdditionalCostsBeforeDiscounts = Config
				.getBoolean("ordercalculation.reset.additionalcosts.before.discounts", true);
		if (setAdditionalCostsBeforeDiscounts)
		{
			resetAdditionalCosts(order, relativeTaxValues);
		}
		// -----------------------------
		// set discount values ( not applied yet ) - dont needed in model domain (?) not needed (MONZER)
		order.setGlobalDiscountValues(findGlobalDiscounts(order));
		// -----------------------------
		// set delivery costs - convert if net or currency is different

		if (!setAdditionalCostsBeforeDiscounts)
		{
			resetAdditionalCosts(order, relativeTaxValues);
		}

		return taxValueMap;

	}

	/**
	 * converts a PriceValue object into a double matching the target currency and net/gross - state if necessary. this
	 * is the case when the given price value has a different net/gross flag or different currency.
	 *
	 * @param pv
	 *           the base price to convert
	 * @param toNet
	 *           the target net/gross state
	 * @param toCurrency
	 *           the target currency
	 * @param taxValues
	 *           the collection of tax values which apply to this price
	 * @return a new PriceValue containing the converted price or pv in case no conversion was necessary
	 */
	public PriceValue convertPriceIfNecessary(final PriceValue pv, final boolean toNet, final CurrencyModel toCurrency,
			final Collection<TaxValue> taxValues)
	{
		// net - gross - conversion
		double convertedPrice = pv.getValue();
		if (pv.isNet() != toNet)
		{
			convertedPrice = pv.getOtherPrice(taxValues).getValue();
			convertedPrice = commonI18NService.roundCurrency(convertedPrice, toCurrency.getDigits().intValue());
		}
		// currency conversion
		final String iso = pv.getCurrencyIso();
		if (iso != null && !iso.equals(toCurrency.getIsocode()))
		{
			try
			{
				final CurrencyModel basePriceCurrency = commonI18NService.getCurrency(iso);
				convertedPrice = commonI18NService.convertAndRoundCurrency(basePriceCurrency.getConversion().doubleValue(),
						toCurrency.getConversion().doubleValue(), toCurrency.getDigits().intValue(), convertedPrice);
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.warn("Cannot convert from currency '" + iso + "' to currency '" + toCurrency.getIsocode() + "' since '" + iso
						+ "' doesn't exist any more - ignored");
			}
		}
		return new PriceValue(toCurrency.getIsocode(), convertedPrice, toNet);
	}

	/**
	 * Convert discount values.
	 *
	 * @param order
	 *           the order
	 * @param dvs
	 *           the dvs
	 * @return the list
	 */
	protected List<DiscountValue> convertDiscountValues(final AbstractOrderModel order, final List<DiscountValue> dvs)
	{
		if (dvs == null)
		{
			return Collections.emptyList();
		}
		if (dvs.isEmpty())
		{
			return dvs;
		}
		//

		final CurrencyModel curr = order.getCurrency();
		final String iso = curr.getIsocode();
		final List<DiscountValue> tmp = new ArrayList<>(dvs);
		/*
		 * convert absolute discount values to order currency is needed
		 */
		final Map<String, CurrencyModel> currencyMap = new HashMap<>(); // just don't search twice for an isocode
		for (int i = 0; i < tmp.size(); i++)
		{
			final DiscountValue discountValue = tmp.get(i);
			if (discountValue.isAbsolute() && !iso.equals(discountValue.getCurrencyIsoCode()))
			{
				// get currency
				CurrencyModel dCurr = currencyMap.get(discountValue.getCurrencyIsoCode());
				if (dCurr == null)
				{
					dCurr = commonI18NService.getCurrency(discountValue.getCurrencyIsoCode());
					currencyMap.put(discountValue.getCurrencyIsoCode(), dCurr);
				}
				// replace old value in temp list
				tmp.set(i,
						new DiscountValue(discountValue.getCode(),
								commonI18NService.convertAndRoundCurrency(dCurr.getConversion().doubleValue(),
										curr.getConversion().doubleValue(), curr.getDigits().intValue(), discountValue.getValue()),
								true, iso));
			}
		}
		return tmp;
	}

	/**
	 * Calculate subtotal.
	 *
	 * @param order
	 *           the order
	 * @param recalculate
	 *           the recalculate
	 * @return the map
	 */
	protected Map<TaxValue, Map<Set<TaxValue>, Double>> calculateSubtotal(final AbstractOrderModel order,
			final boolean recalculate)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			double subtotal = 0.0;
			// entry grouping via map  tax code to Double
			final List<AbstractOrderEntryModel> entries = order.getEntries();
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new LinkedHashMap<>(entries.size() * 2);

			for (final AbstractOrderEntryModel entry : entries)
			{
				calculateTotals(entry, recalculate);
				final double entryTotal = entry.getTotalPrice().doubleValue();
				subtotal += entryTotal;
				// use un-applied version of tax values!!!
				final Collection<TaxValue> allTaxValues = entry.getTaxValues();
				final Set<TaxValue> relativeTaxGroupKey = getUnappliedRelativeTaxValues(allTaxValues);
				for (final TaxValue taxValue : allTaxValues)
				{
					addEntryTaxValue(taxValueMap, entry, entryTotal, relativeTaxGroupKey, taxValue);
				}
			}
			// store subtotal
			subtotal = commonI18NService.roundCurrency(subtotal, order.getCurrency().getDigits().intValue());
			order.setSubtotal(Double.valueOf(subtotal));
			return taxValueMap;
		}
		return Collections.emptyMap();
	}


	/**
	 * Adds the entry tax value.
	 *
	 * @param taxValueMap
	 *           the tax value map
	 * @param entry
	 *           the entry
	 * @param entryTotal
	 *           the entry total
	 * @param relativeTaxGroupKey
	 *           the relative tax group key
	 * @param taxValue
	 *           the tax value
	 */
	private void addEntryTaxValue(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap, final AbstractOrderEntryModel entry,
			final double entryTotal, final Set<TaxValue> relativeTaxGroupKey, final TaxValue taxValue)
	{
		if (taxValue.isAbsolute())
		{
			addAbsoluteEntryTaxValue(entry.getQuantity().longValue(), taxValue.unapply(), taxValueMap);
		}
		else
		{
			addRelativeEntryTaxValue(entryTotal, taxValue.unapply(), relativeTaxGroupKey, taxValueMap);
		}
	}

	/**
	 * Returns the total discount for this abstract order.
	 *
	 * @param order
	 *           the order
	 * @param recalculate
	 *           <code>true</code> forces a recalculation
	 * @return totalDiscounts
	 */
	protected double calculateDiscountValues(final AbstractOrderModel order, final boolean recalculate)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			final List<DiscountValue> discountValues = order.getGlobalDiscountValues();
			if (discountValues != null && !discountValues.isEmpty())
			{
				// clean discount value list -- do we still need it?
				final CurrencyModel curr = order.getCurrency();
				final String iso = curr.getIsocode();

				final int digits = curr.getDigits().intValue();
				final double discountablePrice = order.getSubtotal().doubleValue()
						+ (order.isDiscountsIncludeDeliveryCost() ? order.getDeliveryCost().doubleValue() : 0.0)
						+ (order.isDiscountsIncludePaymentCost() ? order.getPaymentCost().doubleValue() : 0.0);
				/*
				 * apply discounts to this order's total
				 */
				final List<DiscountValue> appliedDiscounts = DiscountValue.apply(1.0, discountablePrice, digits,
						convertDiscountValues(order, discountValues), iso);
				// store discount values
				order.setGlobalDiscountValues(appliedDiscounts);
				return DiscountValue.sumAppliedValues(appliedDiscounts);
			}
			return 0.0;
		}
		else
		{
			return DiscountValue.sumAppliedValues(order.getGlobalDiscountValues());
		}
	}

	/**
	 * Calculate total tax values.
	 *
	 * @param order
	 *           the order
	 * @param recalculate
	 *           the recalculate
	 * @param digits
	 *           the digits
	 * @param taxAdjustmentFactor
	 *           the tax adjustment factor
	 * @param taxValueMap
	 *           the tax value map
	 * @return total taxes
	 */
	protected double calculateTotalTaxValues(final AbstractOrderModel order, final boolean recalculate, final int digits,
			final double taxAdjustmentFactor, final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			final CurrencyModel curr = order.getCurrency();
			final String iso = curr.getIsocode();
			//Do we still need it?
			final boolean net = order.getNet().booleanValue();
			double totalTaxes = 0.0;
			// compute absolute taxes if necessary
			if (MapUtils.isNotEmpty(taxValueMap))
			{
				// adjust total taxes if additional costs or discounts are present
				//	create tax values which contains applied values too
				final List<TaxValue> orderTaxValues = new ArrayList<>(taxValueMap.size());
				for (final Map.Entry<TaxValue, Map<Set<TaxValue>, Double>> taxValueEntry : taxValueMap.entrySet())
				{
					// e.g. VAT_FULL 19%
					final TaxValue unappliedTaxValue = taxValueEntry.getKey();
					// e.g. if VAT_FULL 19%, CUSTOM 2% then 10EUR, (VAT_FULL) then 20EUR
					// or, in case of absolute taxes one single entry
					//  ABS_1 4,50EUR then 2 (pieces)
					final Map<Set<TaxValue>, Double> taxGroups = taxValueEntry.getValue();

					final TaxValue appliedTaxValue;

					appliedTaxValue = applyTaxValue(digits, taxAdjustmentFactor, curr, iso, net, unappliedTaxValue, taxGroups);
					totalTaxes += appliedTaxValue.getAppliedValue();
					orderTaxValues.add(appliedTaxValue);
				}
				order.setTotalTaxValues(orderTaxValues);
			}
			return totalTaxes;
		}
		else
		{
			return order.getTotalTax().doubleValue();
		}
	}


	/**
	 * Apply tax value.
	 *
	 * @param digits
	 *           the digits
	 * @param taxAdjustmentFactor
	 *           the tax adjustment factor
	 * @param curr
	 *           the curr
	 * @param iso
	 *           the iso
	 * @param net
	 *           the net
	 * @param unappliedTaxValue
	 *           the unapplied tax value
	 * @param taxGroups
	 *           the tax groups
	 * @return the tax value
	 */
	private TaxValue applyTaxValue(final int digits, final double taxAdjustmentFactor, final CurrencyModel curr, final String iso,
			final boolean net, final TaxValue unappliedTaxValue, final Map<Set<TaxValue>, Double> taxGroups)
	{
		final TaxValue appliedTaxValue;
		if (unappliedTaxValue.isAbsolute())
		{
			// absolute tax entries ALWAYS map to a single-entry map -> we'll use a shortcut here:
			final double quantitySum = taxGroups.entrySet().iterator().next().getValue().doubleValue();
			appliedTaxValue = calculateAbsoluteTotalTaxValue(curr, iso, digits, net, unappliedTaxValue, quantitySum);
		}
		else if (net)
		{
			appliedTaxValue = applyNetMixedRate(unappliedTaxValue, taxGroups, digits, taxAdjustmentFactor);
		}
		else
		{
			appliedTaxValue = applyGrossMixedRate(unappliedTaxValue, taxGroups, digits, taxAdjustmentFactor);
		}
		return appliedTaxValue;
	}

	/**
	 * Adds the relative entry tax value.
	 *
	 * @param entryTotal
	 *           the entry total
	 * @param taxValue
	 *           the tax value
	 * @param relativeEntryTaxValues
	 *           the relative entry tax values
	 * @param taxValueMap
	 *           the tax value map
	 */
	protected void addRelativeEntryTaxValue(final double entryTotal, final TaxValue taxValue,
			final Set<TaxValue> relativeEntryTaxValues, final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		Double relativeTaxTotalSum = null;
		// A. is tax value already registered ?
		Map<Set<TaxValue>, Double> taxTotalsMap = taxValueMap.get(taxValue);
		if (taxTotalsMap != null) // tax value exists
		{
			// A.1 is set of tax un-applied values already registered by set of all relative tax values ?
			relativeTaxTotalSum = taxTotalsMap.get(relativeEntryTaxValues);
		}
		// B tax value did not exist before
		else
		{
			taxTotalsMap = new LinkedHashMap<>();
			taxValueMap.put(taxValue, taxTotalsMap);
		}
		taxTotalsMap.put(relativeEntryTaxValues,
				Double.valueOf((relativeTaxTotalSum != null ? relativeTaxTotalSum.doubleValue() : 0d) + entryTotal));

	}

	/**
	 * Adds the absolute entry tax value.
	 *
	 * @param entryQuantity
	 *           the entry quantity
	 * @param taxValue
	 *           the tax value
	 * @param taxValueMap
	 *           the tax value map
	 */
	protected void addAbsoluteEntryTaxValue(final long entryQuantity, final TaxValue taxValue,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		Map<Set<TaxValue>, Double> taxGroupMap = taxValueMap.get(taxValue);
		Double quantitySum = null;
		final Set<TaxValue> absoluteTaxGroupKey = Collections.singleton(taxValue);
		if (taxGroupMap == null)
		{
			taxGroupMap = new LinkedHashMap<>();
			taxValueMap.put(taxValue, taxGroupMap);
		}
		else
		{
			quantitySum = taxGroupMap.get(absoluteTaxGroupKey);
		}
		taxGroupMap.put(absoluteTaxGroupKey, Double.valueOf((quantitySum != null ? quantitySum.doubleValue() : 0) + entryQuantity));

	}

	/**
	 * Gets the unapplied relative tax values.
	 *
	 * @param allTaxValues
	 *           the all tax values
	 * @return the unapplied relative tax values
	 */
	protected Set<TaxValue> getUnappliedRelativeTaxValues(final Collection<TaxValue> allTaxValues)
	{
		if (CollectionUtils.isNotEmpty(allTaxValues))
		{
			final Set<TaxValue> ret = new LinkedHashSet<>(allTaxValues.size());
			for (final TaxValue appliedTv : allTaxValues)
			{
				if (!appliedTv.isAbsolute())
				{
					ret.add(appliedTv.unapply());
				}
			}
			return ret;
		}
		else
		{
			return Collections.emptySet();
		}
	}

	/**
	 * Calculate absolute total tax value.
	 *
	 * @param curr
	 *           the curr
	 * @param currencyIso
	 *           the currency iso
	 * @param digits
	 *           the digits
	 * @param net
	 *           the net
	 * @param taxValue
	 *           the tax value
	 * @param cumulatedEntryQuantities
	 *           the cumulated entry quantities
	 * @return the tax value
	 */
	protected TaxValue calculateAbsoluteTotalTaxValue(final CurrencyModel curr, final String currencyIso, final int digits,
			final boolean net, TaxValue taxValue, final double cumulatedEntryQuantities)
	{
		final String taxValueIsoCode = taxValue.getCurrencyIsoCode();
		// convert absolute tax values into order currency if necessary

		if (taxValueIsoCode != null && !currencyIso.equalsIgnoreCase(taxValueIsoCode))
		{
			final CurrencyModel taxCurrency = commonI18NService.getCurrency(taxValueIsoCode);
			final double taxConvertedValue = commonI18NService.convertAndRoundCurrency(taxCurrency.getConversion().doubleValue(),
					curr.getConversion().doubleValue(), digits, taxValue.getValue());
			taxValue = new TaxValue(taxValue.getCode(), taxConvertedValue, true, 0, currencyIso);
		}
		return taxValue.apply(cumulatedEntryQuantities, 0.0, digits, net, currencyIso);
	}

	/**
	 * Apply gross mixed rate.
	 *
	 * @param unappliedTaxValue
	 *           the unapplied tax value
	 * @param taxGroups
	 *           the tax groups
	 * @param digits
	 *           the digits
	 * @param taxAdjustmentFactor
	 *           the tax adjustment factor
	 * @return the tax value
	 */
	protected TaxValue applyGrossMixedRate(final TaxValue unappliedTaxValue, final Map<Set<TaxValue>, Double> taxGroups,
			final int digits, final double taxAdjustmentFactor)
	{
		if (unappliedTaxValue.isAbsolute())
		{
			throw new IllegalStateException("AbstractOrder.applyGrossMixedRate(..) cannot be called for absolute tax value!");
		}
		final double singleTaxRate = unappliedTaxValue.getValue();
		double appliedTaxValueNotRounded = 0.0;
		for (final Map.Entry<Set<TaxValue>, Double> taxGroupEntry : taxGroups.entrySet())
		{
			final double groupTaxesRate = TaxValue.sumRelativeTaxValues(taxGroupEntry.getKey());
			final double taxGroupPrice = taxGroupEntry.getValue().doubleValue();

			appliedTaxValueNotRounded += (taxGroupPrice * singleTaxRate) / (_100 + groupTaxesRate);
		}

		//adjust taxes according to global discounts / costs
		appliedTaxValueNotRounded = appliedTaxValueNotRounded * taxAdjustmentFactor;

		return new TaxValue(//
				unappliedTaxValue.getCode(), //
				unappliedTaxValue.getValue(), //
				false, //
				// always round (even if digits are 0) since relative taxes result in unwanted precision !!!
				commonI18NService.roundCurrency(appliedTaxValueNotRounded, Math.max(digits, 0)), //
				null //
		);
	}

	/**
	 * Apply net mixed rate.
	 *
	 * @param unappliedTaxValue
	 *           the unapplied tax value
	 * @param taxGroups
	 *           the tax groups
	 * @param digits
	 *           the digits
	 * @param taxAdjustmentFactor
	 *           the tax adjustment factor
	 * @return the tax value
	 */
	protected TaxValue applyNetMixedRate(final TaxValue unappliedTaxValue, final Map<Set<TaxValue>, Double> taxGroups,
			final int digits, final double taxAdjustmentFactor)
	{
		if (unappliedTaxValue.isAbsolute())
		{
			throw new IllegalStateException("cannot applyGrossMixedRate(..) cannot be called on absolute tax value!");
		}

		// In NET mode we don't care for tax groups since they're only needed to calculated *incldued* taxes!
		// Here we just sum up all group totals...
		double entriesTotalPrice = 0.0;
		for (final Map.Entry<Set<TaxValue>, Double> taxGroupEntry : taxGroups.entrySet())
		{
			entriesTotalPrice += taxGroupEntry.getValue().doubleValue();
		}
		// and apply them in one go:
		return unappliedTaxValue.apply(1.0, entriesTotalPrice * taxAdjustmentFactor, digits, true, null);
	}


	/**
	 * Find tax values.
	 *
	 * @param entry
	 *           the entry
	 * @return the collection
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected Collection<TaxValue> findTaxValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (findTaxesStrategies.isEmpty())
		{
			LOG.warn("No strategies for finding tax values could be found!");
			return Collections.<TaxValue> emptyList();
		}
		else
		{
			final List<TaxValue> result = new ArrayList<>();
			for (final FindTaxValuesStrategy findStrategy : findTaxesStrategies)
			{
				result.addAll(findStrategy.findTaxValues(entry));
			}
			return result;
		}
	}

	/**
	 * Find discount values.
	 *
	 * @param entry
	 *           the entry
	 * @return the list
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected List<DiscountValue> findDiscountValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (findDiscountsStrategies.isEmpty())
		{
			LOG.warn("No strategies for finding discount values could be found!");
			return Collections.<DiscountValue> emptyList();
		}
		else
		{
			final List<DiscountValue> result = new ArrayList<>();
			for (final FindDiscountValuesStrategy findStrategy : findDiscountsStrategies)
			{
				result.addAll(findStrategy.findDiscountValues(entry));
			}
			return result;
		}
	}

	/**
	 * Find global discounts.
	 *
	 * @param order
	 *           the order
	 * @return the list
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected List<DiscountValue> findGlobalDiscounts(final AbstractOrderModel order) throws CalculationException
	{
		if (findDiscountsStrategies.isEmpty())
		{
			LOG.warn("No strategies for finding discount values could be found!");
			return Collections.<DiscountValue> emptyList();
		}
		else
		{
			final List<DiscountValue> result = new ArrayList<>();
			for (final FindDiscountValuesStrategy findStrategy : findDiscountsStrategies)
			{
				result.addAll(findStrategy.findDiscountValues(order));
			}
			return result;
		}
	}


	/**
	 * Find base price.
	 *
	 * @param entry
	 *           the entry
	 * @return the price value
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return findPriceStrategy.findBasePrice(entry);
	}

	/**
	 * Checks if is save order entry unneeded.
	 *
	 * @return true, if is save order entry unneeded
	 */
	private static boolean isSaveOrderEntryUnneeded()
	{
		return Boolean.TRUE.equals(saveOrderEntryUnneeded.get());
	}

	/**
	 * Mark save order entry unneeded.
	 */
	private static void markSaveOrderEntryUnneeded()
	{
		saveOrderEntryUnneeded.set(Boolean.TRUE);
	}

	/**
	 * Unset save order entry unneeded.
	 */
	private static void unsetSaveOrderEntryUnneeded()
	{
		saveOrderEntryUnneeded.remove();
	}

	/**
	 * Sets the store credit amount on order.
	 *
	 * @param order
	 *           the order
	 * @param relativeTaxValues
	 *           the tax value map
	 * @param roundedTotalDiscounts
	 *           the rounded total discounts
	 */
	protected void setStoreCreditAmountOnOrder(final AbstractOrderModel order, final Collection<TaxValue> relativeTaxValues,
			final double finalTotalAmount)
	{

		// set Store Credit Amount - convert if net or currency is different
		final PriceValue storeCreditAmount = findStoreCreditAmaountStrategy.getStoreCreditAmaount(order, finalTotalAmount);

		double storeCreditAmountValue = 0.0;
		if (storeCreditAmount != null)
		{
			storeCreditAmountValue = convertPriceIfNecessary(storeCreditAmount, order.getNet().booleanValue(), order.getCurrency(),
					relativeTaxValues).getValue();
		}

		order.setStoreCreditAmount(storeCreditAmountValue);
		order.setStoreCreditAmountSelected(storeCreditAmountValue);

		getModelService().save(order);
	}

	protected Collection<TaxValue> getRelativeTaxValues(final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		final Collection<TaxValue> relativeTaxValues = new LinkedList<>();
		for (final Map.Entry<TaxValue, ?> e : taxValueMap.entrySet())
		{
			final TaxValue taxValue = e.getKey();
			if (!taxValue.isAbsolute())
			{
				relativeTaxValues.add(taxValue);
			}
		}
		return relativeTaxValues;
	}

	protected void setLoyaltyAmountOnOrder(final AbstractOrderModel order, final Collection<TaxValue> relativeTaxValues,
			final double finalTotalAmount)
	{
		// set Loyalty Amount - convert if net or currency is different

		final PriceValue loyaltyAmount = findLoyaltyAmountStrategy.getLoyaltyAmount(order, finalTotalAmount);
		getModelService().refresh(order);
		double loyaltyAmountValue = 0.0;
		if (loyaltyAmount != null)
		{
			loyaltyAmountValue = convertPriceIfNecessary(loyaltyAmount, order.getNet().booleanValue(), order.getCurrency(),
					relativeTaxValues).getValue();
		}
		order.setLoyaltyAmount(loyaltyAmountValue);

		getModelService().save(order);
	}

	/**
	 * calculates all totals. this does not trigger price, tax and discount calculation but takes all currently set
	 * price, tax and discount values as base. this method requires the correct subtotal to be set before and the correct
	 * tax value map.
	 *
	 * @param order
	 *           the order
	 * @param recalculate
	 *           if false calculation is done only if the calculated flag is not set
	 * @param taxValueMap
	 *           the map { tax value -> Double( sum of all entry totals for this tax ) } obtainable via
	 *           {@link #calculateSubtotal(AbstractOrderModel, boolean)}
	 * @throws CalculationException
	 *            the calculation exception
	 */
	protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			final CurrencyModel curr = order.getCurrency();
			final int digits = curr.getDigits().intValue();
			// subtotal
			final double subtotal = order.getSubtotal().doubleValue();
			// discounts

			final double totalDiscounts = calculateDiscountValues(order, recalculate);
			final double roundedTotalDiscounts = commonI18NService.roundCurrency(totalDiscounts, digits);
			order.setTotalDiscounts(Double.valueOf(roundedTotalDiscounts));
			// set store credit amount
			// set total
			double total = subtotal + order.getPaymentCost().doubleValue() + order.getDeliveryCost().doubleValue()
					- roundedTotalDiscounts;

			final Collection<TaxValue> relativeTaxValues = getRelativeTaxValues(taxValueMap);

			setStoreCreditAmountOnOrder(order, relativeTaxValues, total - roundedTotalDiscounts);

			total = total - order.getStoreCreditAmount();
			if (order.isShouldCalculateLoyalty())
			{
				setLoyaltyAmountOnOrder(order, relativeTaxValues, total);
			}
			total = total - order.getLoyaltyAmount();

			final double totalRounded = commonI18NService.roundCurrency(total, digits);
			order.setTotalPrice(Double.valueOf(totalRounded));
			// taxes
			final double totalTaxes = calculateTotalTaxValues(//
					order, recalculate, //
					digits, //
					getTaxCorrectionFactor(taxValueMap, subtotal, total, order), //
					taxValueMap);//
			final double totalRoundedTaxes = commonI18NService.roundCurrency(totalTaxes, digits);
			order.setTotalTax(Double.valueOf(totalRoundedTaxes));
			setCalculatedStatus(order);
			saveOrder(order);
		}
	}

	/**
	 * Reset additional costs.
	 *
	 * @param order
	 *           the order
	 * @param relativeTaxValues
	 *           the relative tax values
	 */
	protected void resetAdditionalCosts(final AbstractOrderModel order, final Collection<TaxValue> relativeTaxValues)
	{
		final PriceValue deliCost = findDeliveryCostStrategy.getDeliveryCost(order);
		double deliveryCostValue = 0.0;
		if (deliCost != null)
		{
			deliveryCostValue = convertPriceIfNecessary(deliCost, order.getNet().booleanValue(), order.getCurrency(),
					relativeTaxValues).getValue();
		}
		order.setDeliveryCost(Double.valueOf(deliveryCostValue));
		// -----------------------------
		// set payment cost - convert if net or currency is different
		final PriceValue payCost = findPaymentCostStrategy.getPaymentCost(order);
		double paymentCostValue = 0.0;
		if (payCost != null)
		{
			paymentCostValue = convertPriceIfNecessary(payCost, order.getNet().booleanValue(), order.getCurrency(),
					relativeTaxValues).getValue();
		}
		order.setPaymentCost(Double.valueOf(paymentCostValue));
		order.setStoreCreditAmount(null);
		//		order.setLoyaltyAmount(0d);

		getModelService().save(order);
	}



	/**
	 * Sets the find store credit amaount strategy.
	 *
	 * @param findStoreCreditAmaountStrategy
	 *           the new find store credit amaount strategy
	 */
	@Autowired
	public void setFindStoreCreditAmaountStrategy(final FindStoreCreditAmaountStrategy findStoreCreditAmaountStrategy)
	{
		this.findStoreCreditAmaountStrategy = findStoreCreditAmaountStrategy;
	}


	/**
	 * Sets the find delivery cost strategy.
	 *
	 * @param findDeliveryCostStrategy
	 *           the new find delivery cost strategy
	 */
	@Autowired
	public void setFindDeliveryCostStrategy(final FindDeliveryCostStrategy findDeliveryCostStrategy)
	{
		this.findDeliveryCostStrategy = findDeliveryCostStrategy;
	}


	/**
	 * Sets the find payment cost strategy.
	 *
	 * @param findPaymentCostStrategy
	 *           the new find payment cost strategy
	 */
	@Autowired
	public void setFindPaymentCostStrategy(final FindPaymentCostStrategy findPaymentCostStrategy)
	{
		this.findPaymentCostStrategy = findPaymentCostStrategy;
	}


	/**
	 * Sets the order requires calculation strategy.
	 *
	 * @param orderRequiresCalculationStrategy
	 *           the new order requires calculation strategy
	 */
	@Autowired
	public void setOrderRequiresCalculationStrategy(final OrderRequiresCalculationStrategy orderRequiresCalculationStrategy)
	{
		this.orderRequiresCalculationStrategy = orderRequiresCalculationStrategy;
	}


	/**
	 * Sets the common I 18 N service.
	 *
	 * @param commonI18NService
	 *           the new common I 18 N service
	 */
	@Autowired
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * Sets the find taxes strategies.
	 *
	 * @param findTaxesStrategies
	 *           the new find taxes strategies
	 */
	@Autowired
	public void setFindTaxesStrategies(final List<FindTaxValuesStrategy> findTaxesStrategies)
	{
		this.findTaxesStrategies = findTaxesStrategies;
	}

	/**
	 * Sets the find discounts strategies.
	 *
	 * @param findDiscountsStrategies
	 *           the new find discounts strategies
	 */
	@Autowired
	public void setFindDiscountsStrategies(final List<FindDiscountValuesStrategy> findDiscountsStrategies)
	{
		this.findDiscountsStrategies = findDiscountsStrategies;
	}

	/**
	 * Sets the find price strategy.
	 *
	 * @param findPriceStrategy
	 *           the new find price strategy
	 */
	@Autowired
	public void setFindPriceStrategy(final FindPriceStrategy findPriceStrategy)
	{
		this.findPriceStrategy = findPriceStrategy;
	}

	/**
	 * Sets the tax free entry support.
	 *
	 * @param taxFreeEntrySupport
	 *           the taxFreeEntrySupport to set
	 */
	public void setTaxFreeEntrySupport(final boolean taxFreeEntrySupport)
	{
		this.taxFreeEntrySupport = taxFreeEntrySupport;
	}

	/**
	 * @param findLoyaltyAmountStrategy
	 *           the findLoyaltyAmountStrategy to set
	 */
	@Autowired
	public void setFindLoyaltyAmountStrategy(final FindLoyaltyAmountStrategy findLoyaltyAmountStrategy)
	{
		this.findLoyaltyAmountStrategy = findLoyaltyAmountStrategy;
	}




}
