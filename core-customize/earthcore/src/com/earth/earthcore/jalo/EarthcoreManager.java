/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthcore.constants.EarthcoreConstants;
import com.earth.earthcore.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class EarthcoreManager extends GeneratedEarthcoreManager
{
	public static final EarthcoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthcoreManager) em.getExtension(EarthcoreConstants.EXTENSIONNAME);
	}
}
