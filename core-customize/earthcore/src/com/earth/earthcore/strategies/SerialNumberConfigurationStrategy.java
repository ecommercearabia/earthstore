/**
 *
 */
package com.earth.earthcore.strategies;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;


/**
 * @author monzer
 *
 */
public interface SerialNumberConfigurationStrategy
{

	/**
	 * Generate invoice number by current store.
	 *
	 * @return the optional
	 */
	Optional<String> generateSerialNumberByCurrentStore();

	/**
	 * Generate invoice number for base store.
	 *
	 * @param baseStore
	 *           the base store
	 * @return the optional
	 */
	Optional<String> generateSerialNumberForBaseStore(BaseStoreModel baseStore);

}
