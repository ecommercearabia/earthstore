/**
 *
 */
package com.earth.earthcore.strategies.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthcore.service.AbstractSerialNumberConfigurationService;
import com.earth.earthcore.strategies.SerialNumberConfigurationStrategy;


/**
 * @author monzer
 *
 */
public class OrderSerialNumberConfigurationStrategy implements SerialNumberConfigurationStrategy
{

	@Resource(name = "orderSerialNumberService")
	private AbstractSerialNumberConfigurationService orderSerialNumberService;

	@Override
	public Optional<String> generateSerialNumberByCurrentStore()
	{
		return orderSerialNumberService.generateSerialNumberByCurrentStore();
	}

	@Override
	public Optional<String> generateSerialNumberForBaseStore(final BaseStoreModel baseStore)
	{
		return orderSerialNumberService.generateSerialNumberForBaseStore(baseStore);
	}

}
