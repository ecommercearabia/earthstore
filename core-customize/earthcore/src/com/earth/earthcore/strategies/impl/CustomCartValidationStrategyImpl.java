package com.earth.earthcore.strategies.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.OrderManager;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCartValidationStrategyImpl extends DefaultCartValidationStrategy
{
	private CommerceCartModification removeEntryCartModification(final CartModel cartModel, final CartEntryModel cartEntryModel,
			final long oldQuantity)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		modification.setQuantityAdded(0);
		modification.setQuantity(oldQuantity);
		final CartEntryModel entry = new CartEntryModel()
		{
			@Override
			public Double getBasePrice()
			{
				return null;
			}

			@Override
			public Double getTotalPrice()
			{
				return null;
			}
		};
		entry.setProduct(cartEntryModel.getProduct());
		modification.setEntry(entry);
		getModelService().remove(cartEntryModel);
		getModelService().refresh(cartModel);
		return modification;
	}

	@Override
	protected CommerceCartModification validateCartEntry(final CartModel cartModel, final CartEntryModel cartEntryModel)
	{
		// First verify that the product exists
		ProductModel productForCode = null;
		try
		{
			productForCode = getProductService().getProductForCode(cartEntryModel.getProduct().getCode());
		}
		catch (final UnknownIdentifierException e)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (productForCode == null || productForCode.getApprovalStatus() == null
				|| !ArticleApprovalStatus.APPROVED.equals(productForCode.getApprovalStatus()))
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		final AbstractOrderEntry object = (AbstractOrderEntry) getModelService().getSource(cartEntryModel);
		try
		{
			getCurrentPriceFactory().getBasePrice(object);
		}
		catch (final JaloPriceFactoryException e)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (productForCode.getEurope1Prices() == null || productForCode.getEurope1Prices().isEmpty())
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (cartEntryModel.getBasePrice() == null || cartEntryModel.getBasePrice().doubleValue() <= 0)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		// Overall availability of this product
		final Long stockLevel = getStockLevel(cartEntryModel);
		// Overall stock quantity in the cart
		final long cartLevel = getCartLevel(cartEntryModel, cartModel);
		// Stock quantity for this cartEntry
		final long cartEntryLevel = cartEntryModel.getQuantity().longValue();
		// New stock quantity for this cartEntry
		final long newOrderEntryLevel;
		Long stockLevelForProductInBaseStore = null;
		if (stockLevel != null)
		{
			// this product is not available at the given point of service.
			if (isProductNotAvailableInPOS(cartEntryModel, stockLevel))
			{
				stockLevelForProductInBaseStore = getCommerceStockService()
						.getStockLevelForProductAndBaseStore(cartEntryModel.getProduct(), getBaseStoreService().getCurrentBaseStore());
				final long compareTo = stockLevelForProductInBaseStore != null ? stockLevelForProductInBaseStore.longValue()
						: cartLevel;
				newOrderEntryLevel = Math.min(cartEntryLevel, compareTo);
			}
			else
			{
				// if stock is available.. get either requested quantity if its lower than available stock or maximum stock.
				newOrderEntryLevel = Math.min(cartEntryLevel, stockLevel.longValue());
			}
		}
		else
		{
			// if stock is not available.. play save.. only allow quantity that was already in cart.
			newOrderEntryLevel = Math.min(cartEntryLevel, cartLevel);
		}
		// this product is not available at the given point of service.
		if (stockLevelForProductInBaseStore != null && stockLevelForProductInBaseStore.longValue() != 0)
		{
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(CommerceCartModificationStatus.MOVED_FROM_POS_TO_STORE);
			final CartEntryModel existingEntryForProduct = getExistingShipCartEntryForProduct(cartModel,
					cartEntryModel.getProduct());
			if (existingEntryForProduct != null)
			{
				getModelService().remove(cartEntryModel);
				final long quantityAdded = stockLevelForProductInBaseStore.longValue() >= cartLevel ? newOrderEntryLevel
						: (cartLevel - stockLevelForProductInBaseStore.longValue());
				modification.setQuantityAdded(quantityAdded);
				final long updatedQuantity = (stockLevelForProductInBaseStore.longValue() <= cartLevel
						? stockLevelForProductInBaseStore.longValue()
						: cartLevel);
				modification.setQuantity(updatedQuantity);
				existingEntryForProduct.setQuantity(Long.valueOf(updatedQuantity));
				getModelService().save(existingEntryForProduct);
				modification.setEntry(existingEntryForProduct);
			}
			else
			{
				modification.setQuantityAdded(newOrderEntryLevel);
				modification.setQuantity(cartEntryLevel);
				cartEntryModel.setDeliveryPointOfService(null);
				modification.setEntry(cartEntryModel);
				getModelService().save(cartEntryModel);
			}
			getModelService().refresh(cartModel);
			return modification;
		}
		if ((stockLevel != null && stockLevel.longValue() <= 0) || newOrderEntryLevel < 0)
		{
			// If no stock is available or the cart is full for this product, remove the entry from the cart
			return removeEntryCartModification(cartModel, cartEntryModel, cartEntryLevel);
		}
		if (cartEntryLevel != newOrderEntryLevel)
		{
			// If the orderLevel has changed for this cartEntry, then recalculate the quantity
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(CommerceCartModificationStatus.LOW_STOCK);
			modification.setQuantityAdded(newOrderEntryLevel);
			modification.setQuantity(cartEntryLevel);
			modification.setEntry(cartEntryModel);
			cartEntryModel.setQuantity(Long.valueOf(newOrderEntryLevel));
			getModelService().save(cartEntryModel);
			getModelService().refresh(cartModel);
			return modification;
		}
		if (hasConfigurationErrors(cartEntryModel))
		{
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(CommerceCartModificationStatus.CONFIGURATION_ERROR);
			modification.setQuantityAdded(cartEntryLevel);
			modification.setQuantity(cartEntryLevel);
			modification.setEntry(cartEntryModel);
			return modification;
		}
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		modification.setQuantityAdded(cartEntryLevel);
		modification.setQuantity(cartEntryLevel);
		modification.setEntry(cartEntryModel);
		return modification;
	}

	public PriceFactory getCurrentPriceFactory()
	{
		// Actually OrderManager.getPriceFactory() implements default / session specific price
		// factory fetching. So no need to do it twice.
		return OrderManager.getInstance().getPriceFactory();
	}
}
