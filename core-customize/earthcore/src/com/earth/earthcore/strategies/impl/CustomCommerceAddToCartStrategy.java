/**
 *
 */
package com.earth.earthcore.strategies.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import javax.annotation.Resource;

import org.fest.util.Collections;

import com.earth.earthfacades.units.UnitFactorRangeData;
import com.earth.earthcore.service.UnitFactorRangeDataFactory;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy
{
	@Resource(name = "unitFactorRangeDataFactory")
	private UnitFactorRangeDataFactory unitFactorRangeDataFactory;

	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{

		final List<UnitFactorRangeData> unitFactorRangeData = getUnitFactorRangeDataFactory().create(productModel);
		if (Collections.isEmpty(unitFactorRangeData))
		{
			return super.getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd, pointOfServiceModel);
		}

		final long cartLevel = checkCartLevel(productModel, cartModel, pointOfServiceModel);
		final long stockLevel = getAvailableStockLevel(productModel, pointOfServiceModel);


		// How many will we have in our cart if we add quantity
		final long newTotalQuantity = cartLevel + quantityToAdd;

		// Now limit that to the total available in stock
		long newTotalQuantityAfterStockLimit = Math.min(newTotalQuantity, stockLevel);
		newTotalQuantityAfterStockLimit = getSutableStockQuantity(unitFactorRangeData, newTotalQuantityAfterStockLimit);
		// So now work out what the maximum allowed to be added is (note that
		// this may be negative!)
		final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();

		if (isMaxOrderQuantitySet(maxOrderQuantity))
		{
			final long newTotalQuantityAfterProductMaxOrder = Math.min(newTotalQuantityAfterStockLimit,
					maxOrderQuantity.longValue());
			return newTotalQuantityAfterProductMaxOrder - cartLevel;
		}
		return newTotalQuantityAfterStockLimit - cartLevel;
	}

	/**
	 * @param unitFactorRangeData
	 * @param newTotalQuantity
	 * @return
	 */
	private long getSutableStockQuantity(final List<UnitFactorRangeData> unitFactorRangeData, final long newTotalQuantity)
	{

		int a = 0;
		for (; a < unitFactorRangeData.size(); a++)
		{
			if (newTotalQuantity < unitFactorRangeData.get(a).getQuantity())
			{
				if (a == 0)
				{
					return 0;
				}
				break;
			}
		}
		return unitFactorRangeData.get(a - 1).getQuantity();
	}

	/**
	 * @return the unitFactorRangeDataFactory
	 */
	public UnitFactorRangeDataFactory getUnitFactorRangeDataFactory()
	{
		return unitFactorRangeDataFactory;
	}



}
