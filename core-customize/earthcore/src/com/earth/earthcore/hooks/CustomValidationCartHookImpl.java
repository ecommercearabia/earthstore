/**
 *
 */
package com.earth.earthcore.hooks;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.hooks.CartValidationHook;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.OrderManager;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class CustomValidationCartHookImpl implements CartValidationHook
{
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "productService")
	private ProductService productService;

	private CommerceCartModification validateCartEntryPrice(final CartEntryModel cartEntryModel, final CartModel cartModel)
	{
		// First verify that the product exists
		ProductModel productForCode = null;
		try
		{
			productForCode = getProductService().getProductForCode(cartEntryModel.getProduct().getCode());
		}
		catch (final UnknownIdentifierException e)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (productForCode == null || productForCode.getApprovalStatus() == null
				|| !ArticleApprovalStatus.APPROVED.equals(productForCode.getApprovalStatus()))
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		final AbstractOrderEntry object = (AbstractOrderEntry) getModelService().getSource(cartEntryModel);
		try
		{
			getCurrentPriceFactory().getBasePrice(object);
		}
		catch (final JaloPriceFactoryException e)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (productForCode.getEurope1Prices() == null || productForCode.getEurope1Prices().isEmpty())
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		if (cartEntryModel.getBasePrice() == null || cartEntryModel.getBasePrice().doubleValue() <= 0)
		{
			return removeEntryCartModification(cartModel, cartEntryModel, 0);
		}
		return null;
	}

	@Override
	public void beforeValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications)
	{
		final CartModel cart = parameter.getCart();
		final List<CommerceCartModification> collect = cart.getEntries().stream()
				.map(e -> validateCartEntryPrice((CartEntryModel) e, cart)).filter(Objects::nonNull).collect(Collectors.toList());
		modifications.addAll(collect);
	}

	@Override
	public void afterValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications)
	{
		// NOP
	}

	private CommerceCartModification removeEntryCartModification(final CartModel cartModel, final CartEntryModel cartEntryModel,
			final long oldQuantity)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		modification.setQuantityAdded(0);
		modification.setQuantity(oldQuantity);
		final CartEntryModel entry = new CartEntryModel()
		{
			@Override
			public Double getBasePrice()
			{
				return null;
			}

			@Override
			public Double getTotalPrice()
			{
				return null;
			}
		};
		entry.setProduct(cartEntryModel.getProduct());
		modification.setEntry(entry);
		getModelService().remove(cartEntryModel);
		getModelService().refresh(cartModel);
		return modification;
	}

	private ModelService getModelService()
	{
		return modelService;
	}

	private ProductService getProductService()
	{
		return productService;
	}

	public PriceFactory getCurrentPriceFactory()
	{
		// Actually OrderManager.getPriceFactory() implements default / session specific price
		// factory fetching. So no need to do it twice.
		return OrderManager.getInstance().getPriceFactory();
	}
}
