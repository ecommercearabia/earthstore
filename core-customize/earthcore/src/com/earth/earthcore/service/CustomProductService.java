/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;


/**
 * @author mnasro
 *
 */
public interface CustomProductService extends ProductService
{
	/**
	 * Returns the Product with the specified barcodeIdentifier. As default the search uses the current session user, the
	 * current session language and the current active catalog versions (which are stored at the session in the attribute
	 * {@link de.hybris.platform.catalog.constants.CatalogConstants#SESSION_CATALOG_VERSIONS SESSION_CATALOG_VERSIONS}).
	 * For modifying the search session context see {@link de.hybris.platform.servicelayer.search.FlexibleSearchQuery
	 * FlexibleSearchQuery}.
	 *
	 * @param barcodeIdentifier
	 *           the barcodeIdentifier of the Product
	 * @return the Product with the specified barcodeIdentifier.
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 *            if no Product with the specified barcodeIdentifier is found
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 *            if more than one Product with the specified barcodeIdentifier is found
	 * @throws IllegalArgumentException
	 *            if parameter barcodeIdentifier is <barcodeIdentifier>null</barcodeIdentifier>
	 */
	public ProductModel getProductForBarcodeIdentifier(String barcodeIdentifier);
}
