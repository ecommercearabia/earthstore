/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.commerceservices.customer.CustomerService;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;
import java.util.Optional;


/**
 * @author core
 *
 */
public interface CustomCustomerService extends CustomerService
{
	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber);

	public Optional<CustomerModel> getCurrentCustomer();

}
