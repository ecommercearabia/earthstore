/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.earth.earthfacades.units.UnitFactorRangeData;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public interface UnitFactorRangeDataFactory
{


	List<UnitFactorRangeData> create(final ProductModel model);
}
