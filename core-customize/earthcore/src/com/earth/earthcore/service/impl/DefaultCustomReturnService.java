/**
 *
 */
package com.earth.earthcore.service.impl;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.returns.impl.DefaultReturnService;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthcore.dao.CustomReturnDAO;
import com.earth.earthcore.service.CustomReturnService;


/**
 * @author Tuqa
 *
 */
public class DefaultCustomReturnService extends DefaultReturnService implements CustomReturnService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomReturnService.class);

	@Resource(name = "customReturnDAO")
	private CustomReturnDAO customReturnDAO;


	/**
	 * @return the customReturnDAO
	 */
	protected CustomReturnDAO getCustomReturnDAO()
	{
		return customReturnDAO;
	}


	@Override
	public Set<ReturnRequestModel> findByStoreAndByStatusAndBySentStatus(final BaseStoreModel store, final ReturnStatus status,
			final boolean isSent)
	{
		if (store == null || status == null)
		{
			LOG.error("Store and status can not be null");
			return null;
		}

		return getCustomReturnDAO().findByStoreAndByStatusAndBySentStatus(store, status, isSent);

	}


	@Override
	public Set<ReturnRequestModel> findByStoreAndBySetOfStatusAndBySentStatus(final BaseStoreModel store,
			final Set<ReturnStatus> statuses, final boolean isSent)
	{
		if (store == null || CollectionUtils.isEmpty(statuses))
		{
			LOG.error("Store and status can not be null");
			return null;
		}

		return getCustomReturnDAO().findByStoreAndBySetOfStatusAndBySentStatus(store, statuses, isSent);
	}

}
