/**
 *
 */
package com.earth.earthcore.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.List;

import javax.annotation.Resource;

import com.earth.earthcore.dao.CustomProductDao;
import com.earth.earthcore.service.CustomProductService;


/**
 * @author user2
 *
 */
public class CustomProductServiceImpl extends DefaultProductService implements CustomProductService
{
	@Resource(name = "productDao")
	private CustomProductDao customProductDao;


	/**
	 * @return the customProductDao
	 */
	protected CustomProductDao getCustomProductDao()
	{
		return customProductDao;
	}


	@Override
	public ProductModel getProductForBarcodeIdentifier(final String barcodeIdentifier)
	{
		validateParameterNotNull(barcodeIdentifier, "Parameter barcodeIdentifier must not be null");
		final List<ProductModel> products = getCustomProductDao().findProductsByBarcodeIdentifier(barcodeIdentifier);

		validateIfSingleResult(products, format("Product with code '%s' not found!", barcodeIdentifier),
				format("Product code '%s' is not unique, %d products found!", barcodeIdentifier, Integer.valueOf(products.size())));

		return products.get(0);
	}

}
