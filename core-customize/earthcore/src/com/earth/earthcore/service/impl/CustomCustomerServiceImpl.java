/**
 *
 */
package com.earth.earthcore.service.impl;

import de.hybris.platform.commerceservices.customer.dao.CustomerDao;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthcore.dao.CustomCustomerDao;
import com.earth.earthcore.service.CustomCustomerService;
import com.google.common.base.Preconditions;


/**
 * @author core
 *
 */
public class CustomCustomerServiceImpl extends DefaultCustomerService implements CustomCustomerService
{

	@Resource(name = "customCustomerDao")
	private CustomCustomerDao customCustomerDao;


	@Resource(name = "userService")
	private UserService userService;

	/**
	 * @return the userService
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * @return the customCustomerDao
	 */
	protected CustomCustomerDao getCustomCustomerDao()
	{
		return customCustomerDao;
	}


	/**
	 * @param customerDao
	 * @param regexp
	 */
	public CustomCustomerServiceImpl(final CustomerDao customerDao, final String regexp)
	{
		super(customerDao, regexp);
	}

	@Override
	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber)
	{
		Preconditions.checkArgument(mobileNumber != null, "mobileNumber can not be null");

		return getCustomCustomerDao().getCustomersByMobileNumber(mobileNumber);
	}

	@Override
	public Optional<CustomerModel> getCurrentCustomer()
	{
		final UserModel currentUser = getUserService().getCurrentUser();

		return (currentUser instanceof CustomerModel) ? Optional.ofNullable((CustomerModel) currentUser) : Optional.empty();
	}



}
