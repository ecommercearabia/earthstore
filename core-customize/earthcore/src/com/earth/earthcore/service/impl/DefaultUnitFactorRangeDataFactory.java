/**
 *
 */
package com.earth.earthcore.service.impl;


import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.earth.earthcore.model.UnitFactorRangeModel;
import com.earth.earthcore.service.UnitFactorRangeDataFactory;
import com.earth.earthfacades.units.UnitData;
import com.earth.earthfacades.units.UnitFactorRangeData;
import com.google.common.base.Preconditions;


/**
 * @author husam.dababneh@erabia.com
 *
 */
public class DefaultUnitFactorRangeDataFactory implements UnitFactorRangeDataFactory
{
	private static final Logger LOG = Logger.getLogger(DefaultUnitFactorRangeDataFactory.class);

	@Resource(name = "commercePriceService")
	private CommercePriceService commercePriceService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "unitConverter")
	private Converter<UnitModel, UnitData> unitConverter;

	@Resource(name = "productPricePopulator")
	private Populator<ProductModel, ProductData> productPricePopulator;


	@Resource(name = "commerceStockService")
	private CommerceStockService commerceStockService;

	@Override
	public List<UnitFactorRangeData> create(final ProductModel model)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (currentBaseStore == null || !currentBaseStore.isEnableUnitFactorRange() || !model.isEnableUnitFactorRange())
		{
			return Collections.emptyList();
		}

		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
		final Optional<PriceRowModel> findPriceRowByCurrency = findPriceRowByCurrency(model.getEurope1Prices(),
				currentCurrency.getIsocode());

		final PriceRowModel priceRow = findPriceRowByCurrency.isPresent() ? findPriceRowByCurrency.get() : null;
		if (priceRow == null)
		{
			LOG.warn(String.format("PriceRow on product[%s] is null", model.getCode()));
			return Collections.emptyList();
		}

		final UnitFactorRangeModel unitFactorRange = getUnitFactorRange(model, priceRow);
		if (unitFactorRange == null)
		{
			LOG.warn(String.format("Cannot Find UnitFactorRange on product[%s] of unit", model.getCode()));
			return Collections.emptyList();
		}

		final List<UnitFactorRangeData> result = new ArrayList<>();

		if (unitFactorRange.getStartUnitValue() < 1)
		{
			LOG.warn("UnitFactorRange.Start must be at least 1");
			return Collections.emptyList();
		}

		final Long calculateAvailabilityLong = commerceStockService.getStockLevelForProductAndBaseStore(model, currentBaseStore);
		long calculateAvailability = calculateAvailabilityLong == null ? 0 : calculateAvailabilityLong.longValue();

		final StockLevelStatus stockLevelStatusForProductAndBaseStore = commerceStockService
				.getStockLevelStatusForProductAndBaseStore(model, currentBaseStore);
		if (StockLevelStatus.INSTOCK.equals(stockLevelStatusForProductAndBaseStore))
		{
			calculateAvailability = Long.MAX_VALUE;
		}


		long maxValue = calculateAvailability;
		if (currentBaseStore.isEnableUnitFactorRangeStockFilter())
		{
			maxValue = unitFactorRange.getEndUnitValue();
		}


		for (int value = unitFactorRange.getStartUnitValue(); value <= maxValue; value += unitFactorRange.getUnitValueStep())
		{
			final double unitFactor = priceRow.getUnitFactor();
			final boolean inStock = value <= calculateAvailability;
			result.add(createUnitFactorRangeData(value / unitFactor, model.getUnit(), value, inStock));
		}

		return result;
	}


	/**
	 * @param value
	 *           value in units
	 * @param unit
	 *           unit of measure
	 * @return
	 */
	private UnitFactorRangeData createUnitFactorRangeData(final double value, final UnitModel unit, final int qty,
			final boolean inStock)
	{
		final UnitFactorRangeData data = creatUnitFactorRangeData();
		data.setFormatedValue(getFormattedValue(value, unit, qty));
		data.setValue(value);
		data.setQuantity(qty);
		data.setUnit(unit.getName());
		data.setInStock(inStock);
		data.setUnitData(unitConverter.convert(unit));
		data.setPromotionPrice(null);
		return data;
	}

	private String getFormattedValue(final double value, final UnitModel unit, final int qty)
	{
		if (!"kg".equalsIgnoreCase(unit.getName()))
		{
			return String.format("%.2f %s", value, unit.getName());
		}

		if (qty < 1000.f)
		{
			return String.format("%d Gram", qty);
		}

		return String.format("%.2f %s", value, unit.getName());
	}

	/**
	 * @param model
	 * @param priceRowModel
	 */
	private UnitFactorRangeModel getUnitFactorRange(final ProductModel model, final PriceRowModel priceRowModel)
	{
		UnitFactorRangeModel range = model.getUnitFactorRange();

		if (range == null && priceRowModel.getUnit() != null)
		{
			range = priceRowModel.getUnit().getUnitFactorRange();
		}

		return range;
	}


	/**
	 *
	 */
	private Optional<PriceRowModel> findPriceRowByCurrency(final Collection<PriceRowModel> priceRowModels,
			final String isocodeCurrency)
	{
		Preconditions.checkArgument(StringUtils.isNoneEmpty(isocodeCurrency), "isocodeCurrency mustn't be null or empty");
		if (CollectionUtils.isEmpty(priceRowModels))
		{
			return Optional.empty();
		}
		for (final PriceRowModel priceRowModel : priceRowModels)
		{
			if (priceRowModel.getCurrency() != null && isocodeCurrency.equals(priceRowModel.getCurrency().getIsocode()))
			{
				return Optional.of(priceRowModel);
			}
		}
		return Optional.empty();
	}




	private UnitFactorRangeData creatUnitFactorRangeData()
	{
		return new UnitFactorRangeData();
	}


	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}


	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}


	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}


	/**
	 * @return the unitConverter
	 */
	public Converter<UnitModel, UnitData> getUnitConverter()
	{
		return unitConverter;
	}



	/**
	 * @return the productPricePopulator
	 */
	public Populator<ProductModel, ProductData> getProductPricePopulator()
	{
		return productPricePopulator;
	}
}
