/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.service;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;


/**
 *
 */
public interface ActionHistoryService
{

	void updateCartHistory(AbstractOrderModel abstractOrder, SalesApplication sourceApplication, String operationName,
			String operationMethod);

	void updateUserHistory(UserModel user, SalesApplication sourceApplication, String operationName, String operationMethod);

	void updateAddressHistory(AddressModel address, SalesApplication sourceApplication, String operationName,
			String operationMethod);

}