/**
 *
 */
package com.earth.earthcore.service;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;


/**
 * @author Tuqa
 *
 */
public interface CustomReturnService extends ReturnService
{
	Set<ReturnRequestModel> findByStoreAndByStatusAndBySentStatus(BaseStoreModel store, ReturnStatus status, boolean isSent);

	Set<ReturnRequestModel> findByStoreAndBySetOfStatusAndBySentStatus(BaseStoreModel store, Set<ReturnStatus> status,
			boolean isSent);
}
