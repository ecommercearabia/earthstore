/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcore.user.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.earth.earthcore.service.CustomCustomerService;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


/**
 * The Class DefaultMobilePhoneService.
 *
 * @author mnasro
 */
public class DefaultMobilePhoneService implements MobilePhoneService
{
	/** The common I 18 N service. */
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "customerService")
	private CustomCustomerService customerService;

	/**
	 * @return the customerService
	 */
	protected CustomCustomerService getCustomerService()
	{
		return customerService;
	}

	/**
	 * Validate and normalize phone number by iso code.
	 *
	 * @param countryIsoCode
	 *           the country iso code
	 * @param number
	 *           the number
	 * @return the Optional<String>
	 */
	@Override
	public Optional<String> validateAndNormalizePhoneNumberByIsoCode(final String countryIsoCode, final String number)
	{
		validateParameterNotNull(countryIsoCode, "countryIsoCode cannot be null");
		validateParameterNotNull(countryIsoCode, "countryIsoCode cannot be null");

		final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber = null;
		try
		{
			phoneNumber = phoneNumberUtil.parse(number, countryIsoCode);
		}
		catch (final NumberParseException e)
		{
			return Optional.empty();
		}

		final boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);

		if (!isValid)
		{
			return Optional.empty();
		}
		final String e164FormatNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
		if (e164FormatNumber == null)
		{
			return Optional.empty();
		}
		return Optional.ofNullable(e164FormatNumber.replaceAll("\\+", ""));
	}

	@Override
	public boolean isValidMobileUniqueness(final String mobileNumebr)
	{
		validateParameterNotNull(mobileNumebr, "mobileNumebr cannot be null");

		final List<CustomerModel> customersByMobileNumber = getCustomerService().getCustomersByMobileNumber(mobileNumebr);

		return CollectionUtils.isEmpty(customersByMobileNumber);
	}
	@Override
	public Optional<String> getNationalPhoneNumberFormat(final String countryIsoCode, final String number)
	{
		try
		{
			return Optional
					.ofNullable(getNationalMobileNumber(countryIsoCode, number));
		}
		catch (final Exception e)
		{
			return Optional.ofNullable(number);
		}
	}

	private String getNationalMobileNumber(final String countryIsoCode, final String number)
	{
		if (countryIsoCode == null)
		{
			throw new IllegalArgumentException("countryIsoCode cannot be null");
		}
		if (number == null)
		{
			throw new IllegalArgumentException("number cannot be null");
		}

		final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber = null;
		try
		{
			phoneNumber = phoneNumberUtil.parse(number, countryIsoCode);
		}
		catch (final NumberParseException e)
		{
			return number;
		}

		final boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);

		if (!isValid)
		{
			return number;
		}
		final String nationalFormatNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);
		if (nationalFormatNumber == null)
		{
			return number;
		}
		return nationalFormatNumber.replaceAll("\\ ", "");
	}

}
