/**
 *
 */
package com.earth.earthcore.user.nationality.service.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.dao.NationalityDao;
import com.earth.earthcore.user.nationality.service.NationalityService;


/**
 * The Class DefaultNationalityService.
 *
 * @author mnasro
 */
public class DefaultNationalityService implements NationalityService
{
	/** The area dao. */
	@Resource(name = "nationalityDao")
	private NationalityDao nationalityDao;

	/** The cms site service. */
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	/**
	 * Gets the cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * Gets the nationality dao.
	 *
	 * @return the nationality dao
	 */
	public NationalityDao getNationalityDao()
	{
		return nationalityDao;
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<NationalityModel> getAll()
	{
		return getNationalityDao().findAll();
	}

	/**
	 * Gets the.
	 *
	 * @param code
	 *           the code
	 * @return the optional
	 */
	@Override
	public Optional<NationalityModel> get(final String code)
	{
		return getNationalityDao().find(code);
	}

	/**
	 * Gets the by site.
	 *
	 * @param cmsSiteModel
	 *           the cms site model
	 * @return the by site
	 */
	@Override
	public List<NationalityModel> getBySite(final CMSSiteModel cmsSiteModel)
	{
		return cmsSiteModel == null ? Collections.emptyList() : new ArrayList<>(cmsSiteModel.getNationalities());
	}

	/**
	 * Gets the by current site.
	 *
	 * @return the by current site
	 */
	@Override
	public List<NationalityModel> getByCurrentSite()
	{

		return getCmsSiteService().getCurrentSite() == null ? Collections.emptyList()
				: getBySite(getCmsSiteService().getCurrentSite());
	}

}
