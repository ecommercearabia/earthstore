/**
 *
 */
package com.earth.earthcore.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;


public class AgeUtil
{
	private static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";


	private AgeUtil()
	{
	}

	public static Date getDate(final String birthOfDate, final String dateForma) throws ParseException
	{

		if (isBlank(birthOfDate) || isBlank(dateForma))
		{
			throw new IllegalArgumentException();
		}

		final SimpleDateFormat ogDate = new SimpleDateFormat(dateForma);
		return ogDate.parse(birthOfDate);

	}

	public static int getAgeYears(final String birthOfDate, final String dateFormat) throws ParseException
	{
		if (isBlank(birthOfDate) || isBlank(dateFormat))
		{
			throw new IllegalArgumentException();
		}

		final String date = dateFormater(birthOfDate, dateFormat);

		final LocalDate dob = LocalDate.parse(date);
		final LocalDate curDate = LocalDate.now();

		// calculates the amount of time between two dates and returns the years

		return Period.between(dob, curDate).getYears();

	}

	public static boolean checkUnderAge(final String birthOfDate, final String dateFormat, final int age) throws ParseException
	{

		// calculates the amount of time between two dates and returns the years

		final int ageYears = getAgeYears(birthOfDate, dateFormat);

		return ageYears >= age;
	}

	private static String dateFormater(final String birthOfDate, final String dateFormat) throws ParseException
	{

		final SimpleDateFormat ogDate = new SimpleDateFormat(dateFormat);
		final SimpleDateFormat resultDate = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
		final Date date1 = ogDate.parse(birthOfDate);

		return resultDate.format(date1);
	}

	private static boolean isBlank(final String str)
	{

		return str == null || str.isBlank();
	}
}
