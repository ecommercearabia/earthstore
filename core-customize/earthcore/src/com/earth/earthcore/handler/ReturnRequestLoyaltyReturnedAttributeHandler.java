package com.earth.earthcore.handler;

import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


/**
 * @author mnasro
 *
 */
public class ReturnRequestLoyaltyReturnedAttributeHandler implements DynamicAttributeHandler<Double, ReturnRequestModel>
{

	@Override
	public Double get(final ReturnRequestModel returnRequestModel)
	{
		if (returnRequestModel == null)
		{
			return null;
		}

		return returnRequestModel.getLoyaltyReturnRefundForCustomer() + returnRequestModel.getLoyaltyReturnRefundFromCustomer();
	}

	@Override
	public void set(final ReturnRequestModel returnRequestModel, final Double loyaltyReturned)
	{
		throw new UnsupportedOperationException();
	}


}
