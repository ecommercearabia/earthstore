/**
 *
 */
package com.earth.earthcore.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;


/**
 * @author user2
 *
 */
public interface CustomProductDao extends ProductDao
{
	public List<ProductModel> findProductsByBarcodeIdentifier(final String barcodeIdentifier);

}
