/**
 *
 */
package com.earth.earthcore.dao;

import de.hybris.platform.commerceservices.customer.dao.CustomerDao;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;


/**
 * @author core
 *
 */
public interface CustomCustomerDao extends CustomerDao
{

	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber);


}
