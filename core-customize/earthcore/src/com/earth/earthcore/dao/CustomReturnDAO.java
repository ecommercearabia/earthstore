/**
 *
 */
package com.earth.earthcore.dao;


import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.returns.dao.ReturnRequestDao;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Set;


/**
 * @author Tuqa
 *
 */
public interface CustomReturnDAO extends ReturnRequestDao
{

	Set<ReturnRequestModel> findByStoreAndByStatusAndBySentStatus(BaseStoreModel store, ReturnStatus status, boolean isSent);

	Set<ReturnRequestModel> findByStoreAndBySetOfStatusAndBySentStatus(BaseStoreModel store, Set<ReturnStatus> statuses,
			boolean isSent);
}
