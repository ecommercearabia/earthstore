/**
 *
 */
package com.earth.earthcore.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerDao;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import com.earth.earthcore.dao.CustomCustomerDao;


/**
 * @author core
 *
 */
public class CustomCustomerDaoImpl extends DefaultCustomerDao implements CustomCustomerDao
{

	@Override
	public List<CustomerModel> getCustomersByMobileNumber(final String mobileNumber)
	{
		validateParameterNotNullStandardMessage("mobileNumber", mobileNumber);
		final StringBuilder queryString = new StringBuilder("SELECT {c.pk} FROM {Customer  AS c} WHERE {c.mobileNumber} ='")
				.append(mobileNumber).append("'");
		final SearchResult<CustomerModel> result = getFlexibleSearchService().search(queryString.toString());
		return result == null ? Collections.emptyList() : result.getResult();
	}

}
