/**
 *
 */
package com.earth.earthcore.dao.impl;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.returns.dao.impl.DefaultReturnRequestDao;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthcore.dao.CustomReturnDAO;


/**
 * @author Tuqa
 *
 */
public class DefaultCustomReturnDAO extends DefaultReturnRequestDao implements CustomReturnDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomReturnDAO.class);

	private static final String FIND_BY_STORE_AND_SENT_STATUS = "select {rr.pk}  from {  ReturnRequest as rr  join Order  as o on {o.pk}={rr.order}   join BaseStore as bs on {o.store} = {bs.pk} join ReturnStatus  as status on {rr.status} = {status.pk}}  where {bs.pk}=?storepk  and {status.code}=?status and ({rr.sentToERP}=?sent)";

	private static final String FIND_BY_STORE_AND_SENT_STATUSES = "select {rr.pk}  from {  ReturnRequest as rr  join Order  as o on {o.pk}={rr.order}   join BaseStore as bs on {o.store} = {bs.pk} join ReturnStatus  as status on {rr.status} = {status.pk}}  where {bs.pk}=?storepk  and {status.code} in (?statuses) and ({rr.sentToERP}=?sent)";

	@Override
	public Set<ReturnRequestModel> findByStoreAndByStatusAndBySentStatus(final BaseStoreModel store, final ReturnStatus status,
			final boolean isSent)
	{
		if (store == null || status == null)
		{
			LOG.error("Store and status can not be null");
			return null;
		}

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_BY_STORE_AND_SENT_STATUS);
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("sent", isSent);
		params.put("status", status.getCode());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ReturnRequestModel.class));

		final SearchResult<ReturnRequestModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return new HashSet<>(searchResult.getResult());
	}

	@Override
	public Set<ReturnRequestModel> findByStoreAndBySetOfStatusAndBySentStatus(final BaseStoreModel store,
			final Set<ReturnStatus> statuses, final boolean isSent)
	{
		if (store == null || CollectionUtils.isEmpty(statuses))
		{
			LOG.error("Store and status can not be null");
			return null;
		}

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(FIND_BY_STORE_AND_SENT_STATUSES);
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("sent", isSent);
		params.put("statuses", statuses.stream().map(ReturnStatus::getCode).collect(Collectors.toSet()));
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ReturnRequestModel.class));

		final SearchResult<ReturnRequestModel> searchResult = getFlexibleSearchService().search(searchQuery);
		return new HashSet<>(searchResult.getResult());
	}

}
