/**
 *
 */
package com.earth.earthcore.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.earth.earthcore.dao.CustomProductDao;
import com.earth.earthcore.model.BarcodeIdentifierModel;


/**
 * @author user2
 *
 */
public class CustomProductDaoImpl extends DefaultProductDao implements CustomProductDao
{

	/**
	 * @param typecode
	 */
	public CustomProductDaoImpl(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<ProductModel> findProductsByBarcodeIdentifier(final String barcodeIdentifier)
	{
		validateParameterNotNull(barcodeIdentifier, "Product barcodeIdentifier must not be null!");

		final StringBuilder query = new StringBuilder();
		query.append("SELECT {p.").append(ProductModel.PK).append("} ");
		query.append("FROM {").append(ProductModel._TYPECODE).append(" AS p ");
		query.append("JOIN ").append(BarcodeIdentifierModel._TYPECODE).append(" AS bi ");
		query.append("ON {bi.").append(ProductModel._TYPECODE).append("} = {p.").append(ProductModel.PK).append("} }");
		query.append("WHERE {bi.").append(BarcodeIdentifierModel.CODE).append(" } = '").append(barcodeIdentifier)
		.append("'");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	//	public List<ProductModel> findProductsByBarcodeIdentifier(final String barcodeIdentifier)
	//	{
	//		validateParameterNotNull(barcodeIdentifier, "Product barcodeIdentifier must not be null!");
	//
	//		final StringBuilder query = new StringBuilder();
	//		query.append("SELECT {p:").append(ProductModel.PK).append("} ");
	//		query.append("FROM {").append(ProductModel._TYPECODE).append(" AS p }");
	//		query.append("WHERE {p: ").append(ProductModel.BARCODESIDENTIFIER).append(" } LIKE  '%").append(barcodeIdentifier)
	//				.append("%'");
	//
	//		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
	//		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
	//		return searchResult.getResult();
	//	}

}
