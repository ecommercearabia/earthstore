/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthnotificationcustomocc.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthnotificationcustomocc.constants.EarthnotificationcustomoccConstants;
import org.apache.log4j.Logger;

public class EarthnotificationcustomoccManager extends GeneratedEarthnotificationcustomoccManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthnotificationcustomoccManager.class.getName() );
	
	public static final EarthnotificationcustomoccManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthnotificationcustomoccManager) em.getExtension(EarthnotificationcustomoccConstants.EXTENSIONNAME);
	}
	
}
