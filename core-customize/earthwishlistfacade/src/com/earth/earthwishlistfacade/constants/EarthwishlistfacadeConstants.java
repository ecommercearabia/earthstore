/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthwishlistfacade.constants;

/**
 * Global class for all Earthwishlistfacade constants. You can add global constants for your extension into this class.
 */
public final class EarthwishlistfacadeConstants extends GeneratedEarthwishlistfacadeConstants
{
	public static final String EXTENSIONNAME = "earthwishlistfacade";

	private EarthwishlistfacadeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthwishlistfacadePlatformLogo";
}
