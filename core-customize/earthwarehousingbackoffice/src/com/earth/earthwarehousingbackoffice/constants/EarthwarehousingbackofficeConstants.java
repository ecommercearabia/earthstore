/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.earth.earthwarehousingbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class EarthwarehousingbackofficeConstants extends GeneratedEarthwarehousingbackofficeConstants
{
	public static final String EXTENSIONNAME = "earthwarehousingbackoffice";

	private EarthwarehousingbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
