package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.warehousingbackoffice.actions.util.AbstractConsignmentWorkflow;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.earth.earthcore.service.WarehousingDeliveryService;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;


/**
 * @author Juman
 *
 */
public class ConfirmConsignmentDeliveryManualAction extends AbstractConsignmentWorkflow
		implements CockpitAction<ConsignmentModel, ConsignmentModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ConfirmConsignmentDeliveryManualAction.class);

	public static final String SUCCESS_MESSAGE = "confirm.consignment.delivery.manual.action.message.success";

	public static final String FAILED_MESSAGE = "confirm.consignment.delivery.manual.action.message.failed";


	protected static final String DELIVERING_TEMPLATE_CODE = "NPR_Delivering";

	@Resource(name = "warehousingDeliveryService")
	private WarehousingDeliveryService warehousingDeliveryService;

	@Resource(name = "warehousingConsignmentWorkflowService")
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;


	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		if (ctx == null || ctx.getData() == null)
		{
			Messagebox.show("No consignment found for this action");
			return new ActionResult(ActionResult.ERROR);
		}
		final ConsignmentModel consignment = ctx.getData();


		if (!isPreformable(consignment))
		{
			Messagebox.show("Actioncannot be preformed.");
			return new ActionResult(ActionResult.ERROR);
		}

		final WorkflowActionModel packWorkflowAction = getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(DELIVERING_TEMPLATE_CODE, consignment);
		if (consignment.isShipped() && packWorkflowAction != null
				&& !WorkflowActionStatus.COMPLETED.equals(packWorkflowAction.getStatus()))
		{
			LOG.info("Consignment with code {} is being completed", consignment.getCode());
			getWarehousingDeliveryService().confirmConsignmentDelivery(consignment);
			return getConsignmentActionResult(ctx, SUCCESS_MESSAGE, FAILED_MESSAGE, ConsignmentStatus.DELIVERY_COMPLETED);
		}
		else
		{
			Messagebox.show("Order is not shipped or has been completed.");
			return new ActionResult(ActionResult.ERROR);
		}

	}


	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		return isPreformable(consignment);
	}


	private boolean isPreformable(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			return false;
		}

		return isEnabeldConfirmConsignmentDeliveryAction(consignment) && consignment.getStatus() != null
				&& !(ConsignmentStatus.READY.getCode().equals(consignment.getStatus().getCode())
						|| ConsignmentStatus.CANCELLED.getCode().equals(consignment.getStatus().getCode())
						|| ConsignmentStatus.DELIVERY_COMPLETED.getCode().equals(consignment.getStatus().getCode())
						|| ConsignmentStatus.READY_FOR_SHIPPING.getCode().equals(consignment.getStatus().getCode()));


	}

	private boolean isEnabeldConfirmConsignmentDeliveryAction(final ConsignmentModel consignment)
	{
		return consignment != null && consignment.getOrder() != null && consignment.getOrder().getStore() != null
				&& consignment.getOrder().getStore().isEnabeldConfirmConsignmentDeliveryAction();
	}

	/**
	 * @return the warehousingConsignmentWorkflowService
	 */
	protected WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}


	/**
	 * @return the warehousingDeliveryService
	 */
	protected WarehousingDeliveryService getWarehousingDeliveryService()
	{
		return warehousingDeliveryService;
	}
}
