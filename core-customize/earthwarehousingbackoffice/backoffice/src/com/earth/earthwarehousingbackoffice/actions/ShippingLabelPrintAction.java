/**
 *
 */
package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.earth.earthfulfillment.context.FulfillmentContext;
import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class ShippingLabelPrintAction implements CockpitAction<ConsignmentModel, ConsignmentModel>
{
	private static final String PICKUP_IN_STORE_MODE = "pickup";
	private static final Logger LOG = LoggerFactory.getLogger(ShippingLabelPrintAction.class);
	private static final String AIRWAY_BILL_NOT_FOUND = "Air Waybill not found";


	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentContext
	 */
	protected FulfillmentContext getFulfillmentContext()
	{
		return fulfillmentContext;
	}

	protected void checkData(final byte[] obj)
	{
		if (obj == null || obj.length == 0)
		{
			throw new IllegalArgumentException(AIRWAY_BILL_NOT_FOUND);
		}
	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> ctx)
	{
		final ActionResult<ConsignmentModel> actionResult = new ActionResult(ActionResult.SUCCESS);
		final ConsignmentModel consignment = ctx.getData();
		try
		{

			final Optional<byte[]> printAWB = getFulfillmentContext().printAWBByCurrentStore(consignment);
			if (printAWB.isPresent())
			{
				Filedownload.save(printAWB.get(), MediaType.APPLICATION_PDF_VALUE, consignment.getTrackingID());
			}
			else
			{
				Messagebox.show(AIRWAY_BILL_NOT_FOUND);
			}
		}
		catch (final FulfillmentException ex)
		{
			LOG.error(ex.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			Messagebox.show(ex.getMessage());
		}

		return actionResult;
	}

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> ctx)
	{
		final ConsignmentModel consignment = ctx.getData();
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}

		final AbstractOrderModel order = consignment.getOrder();

		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(consignment.getOrder().getStore());

		return consignment != null && consignment.getCarrierDetails() != null && provider.isPresent()
				&& Boolean.TRUE.equals(provider.get().getActive());
	}

	@Override
	public boolean needsConfirmation(final ActionContext<ConsignmentModel> ctx)
	{
		return true;
	}

	@Override
	public String getConfirmationMessage(final ActionContext<ConsignmentModel> consignmentModelActionContext)
	{
		return "Do you want to print AWB?";
	}
}
