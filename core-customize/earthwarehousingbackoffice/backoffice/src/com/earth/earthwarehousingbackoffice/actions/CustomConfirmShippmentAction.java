/**
 *
 */
package com.earth.earthwarehousingbackoffice.actions;

import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.warehousingbackoffice.actions.ship.ConfirmShippedConsignmentAction;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Messagebox;

import com.earth.eartherpintegration.exception.EarthERPException;
import com.earth.eartherpintegration.service.ERPIntegrationService;
import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramContext;
import com.earth.earthloyaltyprogramprovider.context.LoyaltyProgramProviderContext;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;
import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;


/**
 * @author monzer
 *
 */
public class CustomConfirmShippmentAction extends ConfirmShippedConsignmentAction
{

	@Resource(name = "erpIntegrationService")
	private ERPIntegrationService erpIntegrationService;


	@Resource(name = "loyaltyProgramContext")
	private LoyaltyProgramContext loyaltyProgramContext;

	@Resource(name = "loyaltyProgramProviderContext")
	private LoyaltyProgramProviderContext loyaltyProgramProviderContext;

	private static final Logger LOG = LoggerFactory.getLogger(CustomConfirmShippmentAction.class);

	@Override
	public boolean canPerform(final ActionContext<ConsignmentModel> actionContext)
	{
		final ConsignmentModel consignment = actionContext.getData();
		if (consignment == null || consignment.getOrder() == null)
		{
			return false;
		}
		final boolean canPerform = super.canPerform(actionContext);
		return canPerform && checkShipment(consignment);
	}

	/**
	 * @param data
	 * @return
	 */
	private boolean checkShipment(final ConsignmentModel consignment)
	{
		if (consignment == null || consignment.getOrder() == null || consignment.getOrder().getStore() == null)
		{
			return false;
		}
		if (consignment.getOrder().getStore().isEnabeldCheckShipmentTrackingFromConfirmshipAction())
		{
			return consignment.getCarrierDetails() != null && StringUtils.isNotBlank(consignment.getTrackingID());
		}

		return true;

	}

	@Override
	public ActionResult<ConsignmentModel> perform(final ActionContext<ConsignmentModel> actionContext)
	{

		final ActionResult<ConsignmentModel> actionResult = new ActionResult<>(ActionResult.SUCCESS);

		if (actionContext == null || actionContext.getData() == null)
		{
			Messagebox.show("No consignment found for this action");
			actionResult.setResultCode(ActionResult.ERROR);
			return actionResult;
		}
		try
		{
			takeLoyaltyAction(actionContext.getData());
		}
		catch (final EarthLoyaltyException e)
		{

			LOG.error(e.getMessage());
			Messagebox.show(e.getMessage());
			actionResult.setResultCode(ActionResult.ERROR);
			return actionResult;
		}

		return super.perform(actionContext);
	}

	/**
	 * @param consignment
	 */
	protected boolean sendSalesOrder(final Object data)
	{

		if (!(data instanceof ConsignmentModel))
		{
			return false;
		}

		final ConsignmentModel consignment = (ConsignmentModel) data;
		if (Objects.isNull(consignment.getOrder()))
		{
			return false;
		}

		try
		{
			return getErpIntegrationService().sendSalesOrder(consignment.getOrder().getStore(), consignment);
		}
		catch (final EarthERPException e)
		{
			return false;
		}


	}

	/**
	 * @return the erpIntegrationService
	 */
	protected ERPIntegrationService getErpIntegrationService()
	{
		return erpIntegrationService;
	}


	private void takeLoyaltyAction(final ConsignmentModel consignment) throws EarthLoyaltyException
	{

		if (!loyaltyProgramContext.isLoyaltyEnabled(consignment))
		{
			LOG.info("Loyalty not enabled for customer: " + consignment.getOrder().getUser().getName());
			return;
		}

		LOG.info("Starting adding loyalty points after confirm shipment.");
		loyaltyProgramContext.addPoints(consignment);
		LOG.info("Loyalty points added after confirming shipment.");
	}


}
