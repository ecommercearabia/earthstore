/**
 *
 */
package com.earth.earthstorecredit.dao;


import com.earth.earthstorecredit.enums.StoreCreditModeType;
import com.earth.earthstorecredit.model.StoreCreditModeModel;

/**
 * @author mnasro
 *
 */
public interface StoreCreditModeDao
{
	public StoreCreditModeModel getStoreCreditMode(StoreCreditModeType storeCreditModeType);
}
