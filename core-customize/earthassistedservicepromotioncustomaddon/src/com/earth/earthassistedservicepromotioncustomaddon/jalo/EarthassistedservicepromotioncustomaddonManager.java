/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthassistedservicepromotioncustomaddon.jalo;

import com.earth.earthassistedservicepromotioncustomaddon.constants.EarthassistedservicepromotioncustomaddonConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

public class EarthassistedservicepromotioncustomaddonManager extends GeneratedEarthassistedservicepromotioncustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthassistedservicepromotioncustomaddonManager.class.getName() );
	
	public static final EarthassistedservicepromotioncustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthassistedservicepromotioncustomaddonManager) em.getExtension(EarthassistedservicepromotioncustomaddonConstants.EXTENSIONNAME);
	}
	
}
