/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.earth.earthwarehousingcustomwebservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthwarehousingcustomwebservices.constants.EarthwarehousingcustomwebservicesConstants;

@SuppressWarnings("PMD")
public class EarthwarehousingcustomwebservicesManager extends GeneratedEarthwarehousingcustomwebservicesManager
{
	public static final EarthwarehousingcustomwebservicesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthwarehousingcustomwebservicesManager) em.getExtension(EarthwarehousingcustomwebservicesConstants.EXTENSIONNAME);
	}
	
}
