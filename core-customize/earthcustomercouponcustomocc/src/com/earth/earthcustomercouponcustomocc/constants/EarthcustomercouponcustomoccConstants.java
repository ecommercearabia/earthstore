/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomercouponcustomocc.constants;

/**
 * Earthcustomercouponcustomocc constants
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthcustomercouponcustomoccConstants extends GeneratedEarthcustomercouponcustomoccConstants
{
	public static final String EXTENSIONNAME = "earthcustomercouponcustomocc"; //NOSONAR

	private EarthcustomercouponcustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
