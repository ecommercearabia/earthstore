package com.earth.earthfulfillment.exception;

import com.earth.earthfulfillment.exception.enums.FulfillmentExceptionType;


public class FulfillmentException extends Exception
{

	private static final long serialVersionUID = 1L;
	private final FulfillmentExceptionType type;

	public FulfillmentException(final FulfillmentExceptionType type)
	{
		super(type != null ? type.getValue() : null);
		this.type = type;

	}

	public FulfillmentException(final FulfillmentExceptionType type, final String message)
	{
		super(message);
		this.type = type;
	}

	public FulfillmentException(final FulfillmentExceptionType type, final Throwable throwable)
	{
		super(type != null ? type.getValue() : null, throwable);
		this.type = type;

	}

	public FulfillmentExceptionType getType()
	{
		return type;
	}



}