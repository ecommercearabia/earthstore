/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.dao;

import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;


/**
 *
 * @author abu-muhasien
 *
 */
public interface CarrierDao
{
	CarrierModel get(final String code);
}
