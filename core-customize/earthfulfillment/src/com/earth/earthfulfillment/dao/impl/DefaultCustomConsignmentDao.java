/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.dao.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.earth.earthfulfillment.dao.CustomConsignmentDao;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class DefaultCustomConsignmentDao extends DefaultGenericDao<ConsignmentModel> implements CustomConsignmentDao
{
	private static final String FQL = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} != ?status" + " AND {c:" + ConsignmentModel.CARRIERDETAILS
			+ "} = ?carrier" + " AND {c:" + ConsignmentModel.TRACKINGID + "} IS NOT NULL";

	private static final String FQL2 = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} != ?status" + " AND {c:" + ConsignmentModel.TRACKINGID
			+ "} IS NOT NULL";

	private static final String QUERY = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.STATUS + "} != ?status";

	private static final String TRACKING_ID_QUERY = "SELECT {c:" + ConsignmentModel.PK + "} FROM {" + ConsignmentModel._TYPECODE
			+ " AS c} WHERE {c:" + ConsignmentModel.TRACKINGID + "} = ?trackingId";

	private static final String CARRIER_MUST_NOT_BE_NULL = "CarrierModel must not be null";

	private static final String STATUS_MUST_NOT_BE_NULL = "ConsignmentStatus must not be null";

	private static final String STATUS = "status";

	private static final String CARRIER = "carrier";

	private static final String TRACKING_ID = "trackingId";

	/**
	 *
	 */
	public DefaultCustomConsignmentDao()
	{
		super(ConsignmentModel._TYPECODE);
	}

	@Override
	public List<ConsignmentModel> findByCarrierAndNotStatus(final CarrierModel carrier, final ConsignmentStatus status)
	{
		Preconditions.checkArgument(carrier != null, CARRIER_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FQL);
		query.addQueryParameter(STATUS, status);
		query.addQueryParameter(CARRIER, carrier);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<ConsignmentModel> findByNotStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FQL2);
		query.addQueryParameter(STATUS, status);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<ConsignmentModel> findByStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY);
		query.addQueryParameter(STATUS, status);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			Collections.emptyList();
		}
		return result;
	}

	@Override
	public Optional<ConsignmentModel> findByTackingId(final String trackingId, final String baseStoreId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(trackingId), "Tracking id must not be null");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(TRACKING_ID_QUERY);
		query.addQueryParameter(TRACKING_ID, trackingId);

		final List<ConsignmentModel> result = getFlexibleSearchService().<ConsignmentModel> search(query).getResult();

		if (CollectionUtils.isEmpty(result))
		{
			return Optional.empty();
		}
		return result.stream().filter(consignment -> consignment.getOrder() != null && consignment.getOrder().getStore() != null
				&& baseStoreId.equals(consignment.getOrder().getStore().getUid())).findFirst();
	}

	@Override
	public List<ConsignmentModel> findByStore(final BaseStoreModel store)
	{
		final StringBuilder query = new StringBuilder("select {c.pk} \n" + "from {" + " Consignment as c "
				+ "        join Order       as o on {o.pk}={c.order}" + "        join BaseStore as s on {o.store} = {s.pk}"
				+ "        " + "     } " + "" + "        " + "     where {s.pk}=?pk ");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final Map<String, Object> params = new HashMap<>();
		params.put("pk", store.getPk());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ConsignmentModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ConsignmentModel> findByStoreAndByDate(final BaseStoreModel store, final String startingDate,
			final String endingDate)
	{
		final StringBuilder query = new StringBuilder(
				"select {c.pk} \n" + "from {" + " Consignment as c " + "        join Order       as o on {o.pk}={c.order}"
						+ "        join BaseStore as s on {o.store} = {s.pk}" + "        " + "     } " + "" + "        "
						+ "     where {s.pk}=?storepk and {c.creationtime}>=?startingDate and {c.creationtime}<=?endingDate ");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("startingDate", startingDate);
		params.put("endingDate", endingDate);
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ConsignmentModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ConsignmentModel> findByStoreAndByDateAndByStatus(final BaseStoreModel store, final String startingDate,
			final String endingDate, final ConsignmentStatus status)
	{
		final StringBuilder query = new StringBuilder("select {c.pk} from {  Consignment as c "
				+ " join Order  as o on {o.pk}={c.order}   join BaseStore as s on {o.store} = {s.pk}"
				+ " join ConsignmentStatus as stat on {c.status} = {stat.pk}"
				+ "}   where {s.pk}=?storepk and {c.creationtime}>=?startingDate and {c.creationtime}<=?endingDate and {stat.code}=?status ");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("startingDate", startingDate);
		params.put("endingDate", endingDate);
		params.put(STATUS, status.getCode());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ConsignmentModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ConsignmentModel> findByStoreAndByShipmentStatusAndBySentStatus(final BaseStoreModel store,
			final ConsignmentStatus status, final boolean isSent)
	{
		final StringBuilder query = new StringBuilder("select {c.pk} from {  Consignment as c "
				+ " join Order  as o on {o.pk}={c.order}   join BaseStore as s on {o.store} = {s.pk}"
				+ " join ConsignmentStatus as stat on {c.status} = {stat.pk}"
				+ "}   where {s.pk}=?storepk  and {stat.code}=?status and ({c.sent}=?sent or {c.sent} is null)");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("sent", isSent);
		params.put(STATUS, status.getCode());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ConsignmentModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();
	}

	@Override
	public List<ConsignmentModel> findByStoreAndByShipmentStatusesAndBySentStatus(final BaseStoreModel store,
			final List<ConsignmentStatus> statuses, final boolean isSent)
	{
		final StringBuilder query = new StringBuilder("select {c.pk} from {  Consignment as c "
				+ " join Order  as o on {o.pk}={c.order}   join BaseStore as s on {o.store} = {s.pk}"
				+ " join ConsignmentStatus as stat on {c.status} = {stat.pk}"
				+ "}   where {s.pk}=?storepk   and {c.sent}=?sent");
		final Map<String, Object> params = new HashMap<>();
		params.put("storepk", store.getPk());
		params.put("sent", isSent);

		for (int i = 0; i < statuses.size(); i++)
		{
			if (i == 0)
			{
				query.append(" and ({stat.code}=?status" + i);
				params.put("status" + i, statuses.get(i).getCode());
				continue;
			}

			query.append(" or {stat.code}=?status" + i);
			params.put("status" + i, statuses.get(i).getCode());
		}
		query.append(")");

		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		searchQuery.addQueryParameters(params);
		searchQuery.setResultClassList(Collections.singletonList(ConsignmentModel.class));

		final SearchResult searchResult = getFlexibleSearchService().search(searchQuery);
		return searchResult.getResult();

	}
}