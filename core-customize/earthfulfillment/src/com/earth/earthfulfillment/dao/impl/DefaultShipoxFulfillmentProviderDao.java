/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.dao.impl;

import com.earth.earthfulfillment.dao.FulfillmentProviderDao;
import com.earth.earthfulfillment.model.ShipoxFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipoxFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipoxFulfillmentProviderDao()
	{
		super(ShipoxFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipoxFulfillmentProviderModel._TYPECODE;
	}

}
