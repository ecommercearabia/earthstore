/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.dao.impl;

import com.earth.earthfulfillment.dao.FulfillmentProviderDao;
import com.earth.earthfulfillment.model.ShipaFulfillmentProviderModel;

/**
 *
 */
public class DefaultShipaFulfillmentProviderDao extends DefaultFulfillmentProviderDao
		implements FulfillmentProviderDao
{

	/**
	 *
	 */
	public DefaultShipaFulfillmentProviderDao()
	{
		super(ShipaFulfillmentProviderModel._TYPECODE);
	}

	@Override
	protected String getModelName()
	{
		return ShipaFulfillmentProviderModel._TYPECODE;
	}

}
