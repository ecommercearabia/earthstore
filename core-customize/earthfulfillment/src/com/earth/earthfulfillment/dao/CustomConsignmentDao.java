/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.dao;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface CustomConsignmentDao
{
	public List<ConsignmentModel> findByCarrierAndNotStatus(CarrierModel carrier, ConsignmentStatus status);

	public List<ConsignmentModel> findByNotStatus(ConsignmentStatus status);

	public List<ConsignmentModel> findByStatus(ConsignmentStatus status);

	public Optional<ConsignmentModel> findByTackingId(String trackingId, String baseStoreId);

	List<ConsignmentModel> findByStore(BaseStoreModel store);

	List<ConsignmentModel> findByStoreAndByDate(BaseStoreModel store, String startingDate, String endingDate);

	List<ConsignmentModel> findByStoreAndByDateAndByStatus(BaseStoreModel store, String startingDate, String endingDate,
			ConsignmentStatus status);

	List<ConsignmentModel> findByStoreAndByShipmentStatusAndBySentStatus(BaseStoreModel store, ConsignmentStatus status,
			boolean isSent);

	List<ConsignmentModel> findByStoreAndByShipmentStatusesAndBySentStatus(BaseStoreModel store, List<ConsignmentStatus> statuses,
			boolean isSent);


}