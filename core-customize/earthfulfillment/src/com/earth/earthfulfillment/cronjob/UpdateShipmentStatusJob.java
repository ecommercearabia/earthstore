/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.cronjob;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.warehousing.taskassignment.services.WarehousingConsignmentWorkflowService;
import de.hybris.platform.workflow.enums.WorkflowActionStatus;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.earth.earthcore.service.WarehousingDeliveryService;
import com.earth.earthfulfillment.context.FulfillmentContext;
import com.earth.earthfulfillment.model.UpdateDeliveryStatusCronJobModel;
import com.earth.earthfulfillment.service.CustomConsignmentService;


/**
 *
 */
public class UpdateShipmentStatusJob extends AbstractJobPerformable<UpdateDeliveryStatusCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(UpdateShipmentStatusJob.class);

	private static final String CRONJOB_FINISHED = "UpdateShipmentStatusJob is Finished ...";
	protected static final String CONFIRM_CONSIGNMENT_DELIVERY_CHOICE = "confirmConsignmentDelivery";
	protected static final String DELIVERING_TEMPLATE_CODE = "NPR_Delivering";

	@Resource(name = "customConsignmentService")
	private CustomConsignmentService customConsignmentService;

	@Resource(name = "fulfillmentContext")
	private FulfillmentContext fulfillmentContext;

	@Resource(name = "warehousingDeliveryService")
	private WarehousingDeliveryService warehousingDeliveryService;

	@Resource
	private WarehousingConsignmentWorkflowService warehousingConsignmentWorkflowService;

	@Override
	public PerformResult perform(final UpdateDeliveryStatusCronJobModel cronjob)
	{
		LOG.info("UpdateFulfillmentStatusJob is Starting ...");

		final List<ConsignmentModel> consignments = customConsignmentService
				.getConsignmentsByNotStatus(ConsignmentStatus.DELIVERY_COMPLETED);


		if (CollectionUtils.isEmpty(consignments))
		{
			LOG.info("No consignments found.");
			LOG.info(CRONJOB_FINISHED);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		consignments.stream()
				.filter(consignment -> Objects.nonNull(consignment) && StringUtils.isNotBlank(consignment.getTrackingID()))
				.forEach(c -> checkAndChangeConsignmentStatus(c));
		LOG.info(CRONJOB_FINISHED);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void checkAndChangeConsignmentStatus(final ConsignmentModel consignment)
	{
		Optional<ConsignmentStatus> optionalStatus = Optional.empty();
		try
		{
			optionalStatus = fulfillmentContext.updateStatusByCurrentStore(consignment);
		}
		catch (final Exception e)
		{
			LOG.error("Error getting status for consignment: " + consignment.getCode(), e);
			String msg = e.getMessage();
			if (e.getCause() != null)
			{
				msg = e.getCause().getMessage();
				if (e.getCause().getCause() != null)
				{
					msg = e.getCause().getCause().getMessage();
				}
			}
			return;
		}
		if (optionalStatus.isEmpty())
		{
			LOG.error("Empty status for consignment: " + consignment.getCode());
			return;
		}
		final WorkflowActionModel packWorkflowAction = this.getWarehousingConsignmentWorkflowService()
				.getWorkflowActionForTemplateCode(DELIVERING_TEMPLATE_CODE, consignment);
		if (consignment.isShipped() && packWorkflowAction != null
				&& !WorkflowActionStatus.COMPLETED.equals(packWorkflowAction.getStatus())
				&& ConsignmentStatus.DELIVERY_COMPLETED.equals(optionalStatus.get()))
		{
			LOG.info("Consignment with code {} is being completed", consignment.getCode());
			getWarehousingDeliveryService().confirmConsignmentDelivery(consignment);
		}
		LOG.info("Consignment {} has been updated successfully", consignment.getCode());
	}

	/**
	 * @return the warehousingDeliveryService
	 */
	public WarehousingDeliveryService getWarehousingDeliveryService()
	{
		return warehousingDeliveryService;
	}

	/**
	 * @return the warehousingConsignmentWorkflowService
	 */
	public WarehousingConsignmentWorkflowService getWarehousingConsignmentWorkflowService()
	{
		return warehousingConsignmentWorkflowService;
	}

}
