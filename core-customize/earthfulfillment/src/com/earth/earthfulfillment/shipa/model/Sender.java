package com.earth.earthfulfillment.shipa.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *
 * @author mohammad-abu-muhasien
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sender
{
	private String address;
	private String email;
	private String name;
	private String phone;

	public Sender(final String address, final String email, final String name, final String phone)
	{
		super();
		this.address = address;
		this.email = email;
		this.name = name;
		this.phone = phone;
	}

	public Sender()
	{

	}

	public Sender(final String email, final String phone)
	{
		super();
		this.email = email;
		this.phone = phone;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(final String address)
	{
		this.address = address;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	@Override
	public String toString()
	{
		return "Sender [address=" + address + ", email=" + email + ", name=" + name + ", phone=" + phone + "]";
	}



}
