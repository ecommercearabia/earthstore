package com.earth.earthfulfillment.shipa.exception;

import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.exception.enums.FulfillmentExceptionType;


public class ShipaException extends FulfillmentException
{


	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type)
	{
		super(type);
	}

	/**
	 *
	 */
	public ShipaException(final FulfillmentExceptionType type, final Throwable throwable)
	{
		super(type, throwable);
	}

	private static final long serialVersionUID = 1L;



}