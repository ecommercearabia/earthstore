package com.earth.earthfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class ShipoxUserBean implements Serializable{

	private String address_type;
	private String name;
	private String email;
	private String apartment;
	private String building;
	private String street;
	private String landmark;
	private String phone;
	private ShipoxCountry country;
	private ShipoxCity city;
	private ShipoxNeighborhood neighborhood;
	private Double lat;
	private Double lon;

	public String getAddress_type() {
		return address_type;
	}
	public void setAddress_type(final String address_type) {
		this.address_type = address_type;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(final String email) {
		this.email = email;
	}
	public String getApartment() {
		return apartment;
	}
	public void setApartment(final String apartment) {
		this.apartment = apartment;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(final String building) {
		this.building = building;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(final String street) {
		this.street = street;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(final String landmark) {
		this.landmark = landmark;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(final String phone) {
		this.phone = phone;
	}
	public ShipoxCountry getCountry() {
		return country;
	}
	public void setCountry(final ShipoxCountry country) {
		this.country = country;
	}
	public ShipoxCity getCity() {
		return city;
	}
	public void setCity(final ShipoxCity city) {
		this.city = city;
	}
	public ShipoxNeighborhood getNeighborhood() {
		return neighborhood;
	}
	public void setNeighborhood(final ShipoxNeighborhood neighborhood) {
		this.neighborhood = neighborhood;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(final Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(final Double lon) {
		this.lon = lon;
	}
	@Override
	public String toString() {
		return "UserData [address_type=" + address_type + ", name=" + name + ", email=" + email + ", apartment="
				+ apartment + ", building=" + building + ", street=" + street + ", landmark=" + landmark + ", phone="
				+ phone + ", country=" + country + ", city=" + city + ", neighborhood=" + neighborhood + ", lat=" + lat
				+ ", lon=" + lon + "]";
	}

}
