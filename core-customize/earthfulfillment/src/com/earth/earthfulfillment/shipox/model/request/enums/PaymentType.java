package com.earth.earthfulfillment.shipox.model.request.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PaymentType {

	@JsonProperty("credit_balance")CREDIT_BALANCE, @JsonProperty("cash")CASH;
}
