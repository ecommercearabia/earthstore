package com.earth.earthfulfillment.shipox.model.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class Dimension implements Serializable{

	private Double weight;
	private Double width;
	private Double height;
	private Double length;
	private String unit;
	private Boolean domestic;
	public Double getWeight() {
		return weight;
	}
	public void setWeight(final Double weight) {
		this.weight = weight;
	}
	public Double getWidth() {
		return width;
	}
	public void setWidth(final Double width) {
		this.width = width;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(final Double height) {
		this.height = height;
	}
	public Double getLength() {
		return length;
	}
	public void setLength(final Double length) {
		this.length = length;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(final String unit) {
		this.unit = unit;
	}
	public Boolean isDomestic() {
		return domestic;
	}
	public void setDomestic(final Boolean domestic) {
		this.domestic = domestic;
	}
	@Override
	public String toString() {
		return "Dimension [weight=" + weight + ", width=" + width + ", height=" + height + ", length=" + length
				+ ", unit=" + unit + ", domestic=" + domestic + "]";
	}

}
