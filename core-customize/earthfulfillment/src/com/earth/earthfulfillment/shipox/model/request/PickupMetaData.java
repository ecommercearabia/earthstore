package com.earth.earthfulfillment.shipox.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class PickupMetaData {

	private String instruction;
	private String name;
	private double price_per_unit;
	private int quantity;

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(final String instruction) {
		this.instruction = instruction;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public double getPrice_per_unit() {
		return price_per_unit;
	}

	public void setPrice_per_unit(final double price_per_unit) {
		this.price_per_unit = price_per_unit;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(final int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "PickupMetaData [instruction=" + instruction + ", name=" + name + ", price_per_unit=" + price_per_unit
				+ ", quantity=" + quantity + "]";
	}

}
