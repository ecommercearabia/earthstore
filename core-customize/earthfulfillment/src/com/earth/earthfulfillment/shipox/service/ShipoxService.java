package com.earth.earthfulfillment.shipox.service;

import com.earth.earthfulfillment.shipox.exceptions.ShipoxException;
import com.earth.earthfulfillment.shipox.model.request.ShipoxOrderRequest;
import com.earth.earthfulfillment.shipox.model.request.UserCredentials;
import com.earth.earthfulfillment.shipox.model.response.FulfillmentResponse;
import com.earth.earthfulfillment.shipox.model.response.TrackOrderResponse;


public interface ShipoxService
{



	public FulfillmentResponse getOrderDetails(long orderId, String baseURL, String username, String password)
			throws ShipoxException;

	public FulfillmentResponse cancelOrder(long orderId, String baseURL, String username, String password) throws ShipoxException;

	public TrackOrderResponse trackOrder(String orderNumber, String baseURL, String username, String password)
			throws ShipoxException;

	public FulfillmentResponse printAWBLabel(String orderId, String baseUrl, String username, String password)
			throws ShipoxException;

	public String authorize(UserCredentials userCredentials, String authURL) throws ShipoxException;

	public FulfillmentResponse createOrder(ShipoxOrderRequest orderData, String baseURL, String username, String password)
			throws ShipoxException;


}
