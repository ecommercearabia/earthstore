/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderStrategy
{
	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProvider(String baseStoreUid);

	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProvider(BaseStoreModel baseStoreModel);

	/**
	 *
	 */
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore();

}
