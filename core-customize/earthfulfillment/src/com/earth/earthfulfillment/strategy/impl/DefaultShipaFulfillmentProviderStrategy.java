package com.earth.earthfulfillment.strategy.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.model.ShipaFulfillmentProviderModel;
import com.earth.earthfulfillment.service.FulfillmentProviderService;
import com.earth.earthfulfillment.strategy.FulfillmentProviderStrategy;

/**
 *
 */
public class DefaultShipaFulfillmentProviderStrategy implements FulfillmentProviderStrategy
{
	@Resource(name = "fulfillmentProviderService")
	private FulfillmentProviderService fulfillmentProviderService;

	protected FulfillmentProviderService getFulfillmentProviderService()
	{
		return fulfillmentProviderService;
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final String baseStoreUid)
	{
		return getFulfillmentProviderService().getActive(baseStoreUid, ShipaFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProvider(final BaseStoreModel baseStoreModel)
	{
		return getFulfillmentProviderService().getActive(baseStoreModel, ShipaFulfillmentProviderModel.class);
	}

	@Override
	public Optional<FulfillmentProviderModel> getActiveProviderByCurrentBaseStore()
	{
		return getFulfillmentProviderService().getActiveProviderByCurrentBaseStore(ShipaFulfillmentProviderModel.class);
	}
}

