/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.strategy.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.service.FulfillmentService;
import com.earth.earthfulfillment.strategy.FulfillmentStrategy;


/**
 *
 */
public class DefaultShipoxFulfillmentStrategy implements FulfillmentStrategy
{
	@Resource(name = "shipoxFulfillmentService")
	private FulfillmentService shipoxFulfillmentService;

	/**
	 * @return the shipaFulfillmentService
	 */
	public FulfillmentService getShipoxFulfillmentService()
	{
		return shipoxFulfillmentService;
	}

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getShipoxFulfillmentService().createShipment(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getShipoxFulfillmentService().printAWB(consignmentModel, fulfillmentProviderModel);
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getShipoxFulfillmentService().getStatus(consignmentModel, fulfillmentProviderModel);
	}


	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		return getShipoxFulfillmentService().updateStatus(consignmentModel, fulfillmentProviderModel);
	}

}
