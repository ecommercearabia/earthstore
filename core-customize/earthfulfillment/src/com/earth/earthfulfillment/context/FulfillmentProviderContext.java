package com.earth.earthfulfillment.context;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthfulfillment.enums.FulfillmentProviderType;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 *
 */
public interface FulfillmentProviderContext
{
	public Optional<FulfillmentProviderModel> getProvider(Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(String baseStoreUid, Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, Class<?> providerClass);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel);

	public Optional<FulfillmentProviderModel> getProvider(BaseStoreModel baseStoreModel, final FulfillmentProviderType type);


}