/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.carrier;

import de.hybris.platform.consignmenttrackingservices.adaptors.CarrierAdaptor;
import de.hybris.platform.consignmenttrackingservices.delivery.data.ConsignmentEventData;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.model.ShipoxFulfillmentProviderModel;


/**
 *
 */
public class ShipoxCarrierAdapter implements CarrierAdaptor
{
	private static final Logger LOG = LoggerFactory.getLogger(ShipoxCarrierAdapter.class);

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Override
	public List<ConsignmentEventData> getConsignmentEvents(final String trackingId)
	{
		return null;
	}

	@Override
	public URL getTrackingUrl(final String trackingID)
	{
		final Optional<FulfillmentProviderModel> provider = getFulfillmentProviderContext()
				.getProvider(ShipoxFulfillmentProviderModel.class);
		if (provider.isEmpty())
		{
			return null;
		}
		final String trackingUrl = provider.get().getTrackingUrl();
		if (StringUtils.isNotBlank(trackingUrl))
		{
			try
			{
				return new URL(trackingUrl + trackingID);
			}
			catch (final MalformedURLException e)
			{
				LOG.error("Invalid Tracking URL");
			}
		}
		return null;
	}

	@Override
	public int getDeliveryLeadTime(final ConsignmentModel consignment)
	{
		return 0;
	}

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}


}
