package com.earth.earthfulfillment.saee.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import com.earth.earthfulfillment.exception.enums.FulfillmentExceptionType;
import com.earth.earthfulfillment.saee.exceptions.SaeeException;
import com.earth.earthfulfillment.saee.model.SaeeOrderData;
import com.earth.earthfulfillment.saee.model.responses.CreateOrderResponse;
import com.earth.earthfulfillment.saee.model.responses.Details;
import com.earth.earthfulfillment.saee.model.responses.ShipmentTrakingResponse;
import com.earth.earthfulfillment.saee.service.SaeeService;
import com.earth.earthwebserviceapi.util.WebServiceApiUtil;


@Component
public class DefaultSaeeService implements SaeeService
{

	private final Logger LOG = LoggerFactory.getLogger(DefaultSaeeService.class);


	@Override
	public Optional<String> createShipment(final SaeeOrderData param, final String baseURL) throws SaeeException
	{
		final String url = baseURL + "/deliveryrequest/new";

		ResponseEntity<?> response = null;
		try
		{
			response = WebServiceApiUtil.httPOST(url, param, null, CreateOrderResponse.class);
		}
		catch (final HttpClientErrorException ex)
		{
			throw new SaeeException(FulfillmentExceptionType.CLIENT_ERROR, "Client Error", ex.getMessage());
		}

		if (response.getStatusCode() != HttpStatus.OK)
		{
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Server Error", "");
		}

		final CreateOrderResponse body = (CreateOrderResponse) response.getBody();
		if (body == null || !body.isSuccess())
		{
			throw new SaeeException(FulfillmentExceptionType.FAILED_REQUEST, "", "");
		}

		return Optional.ofNullable((body.getWaybill()));
	}

	@Override
	public Optional<Details> getStatus(final String waybill, final String baseURL) throws SaeeException
	{
		if (waybill == null || waybill.isEmpty() || waybill.isBlank())
		{
			throw new SaeeException(FulfillmentExceptionType.INVALID_ATTRIBUTE, "Waybill is not present", "");
		}

		final String url = baseURL + "/tracking?trackingnum=" + waybill;
		ResponseEntity<?> response = null;
		try
		{
			LOG.info("call api");
			response = WebServiceApiUtil.httpGet(url, null, ShipmentTrakingResponse.class);
		}
		catch (final HttpClientErrorException ex)
		{
			LOG.error(String.format("error in server ex=%s", ex.getMessage()));
			throw new SaeeException(FulfillmentExceptionType.CLIENT_ERROR, "Client Error", ex.getMessage());
		}

		if (response.getStatusCode() != HttpStatus.OK)
		{
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Server Error", "");
		}

		final ShipmentTrakingResponse body = (ShipmentTrakingResponse) response.getBody();
		if (body == null || !body.isSuccess())
		{
			throw new SaeeException(FulfillmentExceptionType.FAILED_REQUEST, "", "");

		}

		/***
		 * @Note(Husam Dababneh): details will return an array which in theory the first element of it should be the last
		 *             update status that's why we took the first element aka (0th).
		 */
		return Optional.ofNullable(body.getDetails().get(0));

	}


	@Override
	public Optional<byte[]> getPDF(final String waybill, final String baseURL) throws SaeeException
	{
		final String url = baseURL + "/deliveryrequest/printsticker/pdf/" + waybill;

		ResponseEntity<?> response = null;
		try
		{
			LOG.info("call api");
			response = WebServiceApiUtil.httpGetPdf(url, null);
		}
		catch (final HttpClientErrorException ex)
		{
			LOG.error(String.format("error in server ex=%s", ex.getMessage()));
		}

		if (response == null || response.getStatusCode() != HttpStatus.OK)
		{
			throw new SaeeException(FulfillmentExceptionType.BAD_REQUEST, "Http", null);
		}
		final byte[] body = (byte[]) response.getBody();
		return Optional.ofNullable(body);
	}

}
