/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;


/**
 * The Interface CustomConsignmentService.
 *
 * @author amjad.shati@erabia.com
 * @author mohammad-abumuhasien
 * @author monzer
 */
public interface CustomConsignmentService
{

	/**
	 * Gets the consignments by store and not status.
	 *
	 * @param store
	 *           the store
	 * @param status
	 *           the status
	 * @return the consignments by store and not status
	 */
	public List<ConsignmentModel> getConsignmentsByStoreAndNotStatus(BaseStoreModel store, ConsignmentStatus status);

	/**
	 * Gets the consignments by current store and not status.
	 *
	 * @param status
	 *           the status
	 * @return the consignments by current store and not status
	 */
	public List<ConsignmentModel> getConsignmentsByCurrentStoreAndNotStatus(ConsignmentStatus status);

	/**
	 * Gets the consignments by not status.
	 *
	 * @param status
	 *           the status
	 * @return the consignments by not status
	 */
	public List<ConsignmentModel> getConsignmentsByNotStatus(ConsignmentStatus status);

	/**
	 * Gets the consignments by status.
	 *
	 * @param status
	 *           the status
	 * @return the consignments by status
	 */
	public List<ConsignmentModel> getConsignmentsByStatus(ConsignmentStatus status);


	/**
	 * Find by tacking id.
	 *
	 * @param trackingId
	 *           the tracking id
	 * @return the optional
	 */
	public Optional<ConsignmentModel> findByTackingId(String trackingId, String baseStoreId);

	public List<ConsignmentModel> getByStore(BaseStoreModel store);

	List<ConsignmentModel> getByStoreAndByDate(BaseStoreModel store, String startingDate, String endingDate);

	List<ConsignmentModel> getByStoreAndByDateAndByStatus(BaseStoreModel store, String startingDate, String endingDate,
			ConsignmentStatus status);

	List<ConsignmentModel> getByStoreAndByShipmentStatusAndBySentStatus(BaseStoreModel store, ConsignmentStatus status,
			boolean isSent);

	List<ConsignmentModel> getByStoreAndByShipmentStatusesAndBySentStatus(BaseStoreModel store, List<ConsignmentStatus> statuses,
			boolean isSent);
}