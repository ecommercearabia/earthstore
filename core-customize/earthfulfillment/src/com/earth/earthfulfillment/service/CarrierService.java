/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service;

import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;

import com.earth.earthfulfillment.enums.FulfillmentProviderType;


/**
 *
 * @author abu-muhasien
 *
 */
public interface CarrierService
{
	CarrierModel get(final String code);

	CarrierModel create(final String code, final String name, final FulfillmentProviderType type);

}