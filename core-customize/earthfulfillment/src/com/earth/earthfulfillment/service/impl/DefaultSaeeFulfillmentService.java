/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.earth.earthfulfillment.enums.FulfillmentActionHistoryType;
import com.earth.earthfulfillment.enums.FulfillmentProviderType;
import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.exception.enums.FulfillmentExceptionType;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.model.SaeeFulfillmentProviderModel;
import com.earth.earthfulfillment.saee.exceptions.SaeeException;
import com.earth.earthfulfillment.saee.model.SaeeOrderData;
import com.earth.earthfulfillment.saee.model.responses.Details;
import com.earth.earthfulfillment.saee.service.SaeeService;
import com.earth.earthfulfillment.service.CarrierService;
import com.earth.earthfulfillment.service.FulfillmentService;
import com.earth.earthfulfillment.service.impl.ConsignmentShipmentResponseComponent.Operation;
import com.google.common.base.Preconditions;


/**
 * @author Husam Dababneh
 */
public class DefaultSaeeFulfillmentService implements FulfillmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultSaeeFulfillmentService.class);

	private static final String ORDER_MUST_NOT_BE_NULL = "Order Must not be null";
	private static final String DELIVERY_ADDRESS_MUST_NOT_BE_NULL = "Deliviry Address must not be null";
	private static final String CITY_MUST_NOT_BE_NULL = "City must not be null";

	@Resource(name = "saeeService")
	private SaeeService saeeService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "saeeFulfillmentStatusMap")
	private Map<Integer, ConsignmentStatus> saeeFulfillmentStatusMap;

	@Resource(name = "consignmentShipmentResponseComponent")
	private ConsignmentShipmentResponseComponent saveResponseAndRequest;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("creating SaeeShipment");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;
		final SaeeOrderData data = prepareSAEEShipmentData(consignmentModel, saeefulfillmentProviderModel);

		saveResponseAndRequest.saveShipmentRequest(consignmentModel, data.toString(), Operation.CREATE);

		Optional<String> trackingID = Optional.empty();
		try
		{
			trackingID = saeeService.createShipment(data, saeefulfillmentProviderModel.getBaseUrl());
		}
		catch (final SaeeException ex)
		{
			saveResponseAndRequest.saveActionInHistory(consignmentModel, data.toString(), "Fail",
					FulfillmentActionHistoryType.CREATESHIPMENT);
			switch (ex.getType()) // NOSONAR
			{
				case MISSING_ATTRIBUTE:
					throw new FulfillmentException(FulfillmentExceptionType.MISSING_ARGUMENT);
				case SERVER_DOWN:
					throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
			}
		}
		if (!trackingID.isPresent())
		{
			throw new FulfillmentException(FulfillmentExceptionType.BAD_REQUEST);
		}
		modelService.refresh(consignmentModel);
		saveResponseAndRequest.saveTrackingIdAndCarrier(saeefulfillmentProviderModel, consignmentModel, trackingID.get(), "",
				FulfillmentProviderType.SAEE);
		saveResponseAndRequest.saveShipmentResponse(consignmentModel, trackingID.get(), Operation.CREATE);

		saveResponseAndRequest.saveActionInHistory(consignmentModel, data.toString(), trackingID.get(),
				FulfillmentActionHistoryType.CREATESHIPMENT);

		return trackingID;
	}

	private SaeeOrderData prepareSAEEShipmentData(final ConsignmentModel consignmentModel,
			final SaeeFulfillmentProviderModel fulfillmentProviderModel) throws SaeeException
	{

		LOG.info("Preparing Saee ShipmentData");

		final SaeeOrderData data = new SaeeOrderData();
		data.setSecret(fulfillmentProviderModel.getSecret());
		data.setSendername(fulfillmentProviderModel.getSendername());
		data.setSenderaddress(fulfillmentProviderModel.getSenderaddress());
		data.setSendercity(fulfillmentProviderModel.getSendercity());

		data.setSendercountry(fulfillmentProviderModel.getSendercountry());
		data.setSendermail(fulfillmentProviderModel.getSendermail());
		data.setSenderphone(fulfillmentProviderModel.getSenderphone());
		data.setQuantity(getQuantity(fulfillmentProviderModel));

		Preconditions.checkArgument(consignmentModel.getOrder() != null, ORDER_MUST_NOT_BE_NULL);
		final OrderModel orderModel = (OrderModel) consignmentModel.getOrder();
		final AddressModel deliveryAddress = consignmentModel.getOrder().getDeliveryAddress();
		Preconditions.checkArgument(deliveryAddress != null, DELIVERY_ADDRESS_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(deliveryAddress.getCity() != null, CITY_MUST_NOT_BE_NULL);

		data.setOrdernumber(orderModel.getCode());
		data.setName(deliveryAddress.getFirstname() + " " + deliveryAddress.getLastname());
		data.setMobile(deliveryAddress.getMobile());
		data.setStreetaddress(deliveryAddress.getAddressName());
		data.setDistrict(StringUtils.isEmpty(deliveryAddress.getDistrict()) ? "" : deliveryAddress.getDistrict());

		if (deliveryAddress.getCity().getSaeeCity() == null || StringUtils.isEmpty(deliveryAddress.getCity().getSaeeCity()))
		{
			throw new SaeeException(FulfillmentExceptionType.MISSING_ATTRIBUTE, "Saee Cities Should not be Empty or null", "");
		}

		data.setCity(deliveryAddress.getCity().getSaeeCity());
		data.setEmail(deliveryAddress.getEmail());
		data.setCashondelivery(getCodAmount(consignmentModel));
		data.setWeight(Float.valueOf(getWeightOfItems(consignmentModel)));
		final Optional<String> descs = getProductsSKUs(consignmentModel);
		data.setDescription(descs.isPresent() ? descs.get() : "");


		LOG.info("Finished preparing Saee ShipmentData");
		return data;
	}


	/**
	 *
	 */
	private Optional<String> getProductsSKUs(final ConsignmentModel consignmentModel)
	{
		final StringBuilder builder = new StringBuilder();

		for (final Iterator<ConsignmentEntryModel> it = consignmentModel.getConsignmentEntries().iterator(); it.hasNext();)
		{
			final ConsignmentEntryModel consignmentEntryModel = it.next();
			if (consignmentEntryModel != null)
			{
				builder.append(consignmentEntryModel.getOrderEntry().getProduct().getCode() + (it.hasNext() ? ", " : ""));
			}
		}
		return Optional.ofNullable(builder.toString());
	}

	/**
	 *
	 */
	private int getQuantity(final SaeeFulfillmentProviderModel fulfillmentProviderModel)
	{
		if (fulfillmentProviderModel.getQuantity() != null)
		{
			return fulfillmentProviderModel.getQuantity();
		}
		return 1;
	}

	/**
	 *
	 */
	private float getCodAmount(final ConsignmentModel consignmentModel)
	{
		if ("card".equalsIgnoreCase(consignmentModel.getOrder().getPaymentMode().getCode()))
		{
			return 0.0f;
		}
		else if ("cod".equalsIgnoreCase(consignmentModel.getOrder().getPaymentMode().getCode()))
		{
			return consignmentModel.getOrder().getTotalPrice().floatValue();
		}
		return 0.0f;
	}

	/**
	 *
	 */
	private String getWeightOfItems(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getPackagingInfo() == null ? "" : consignmentModel.getPackagingInfo().getGrossWeight();
	}


	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting PDF for Saee Shipment order");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;

		try
		{
			final Optional<byte[]> pdf = saeeService.getPDF(consignmentModel.getTrackingID(),
					saeefulfillmentProviderModel.getBaseUrl());

			saveResponseAndRequest.saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Success",
					FulfillmentActionHistoryType.PRINTAWB);
			return pdf;
		}
		catch (final Exception e)
		{
			saveResponseAndRequest.saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail",
					FulfillmentActionHistoryType.CREATESHIPMENT);
			throw new FulfillmentException(FulfillmentExceptionType.CLIENT_ERROR);
		}
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		LOG.info("getting status for Saee Shipment order");
		final SaeeFulfillmentProviderModel saeefulfillmentProviderModel = (SaeeFulfillmentProviderModel) fulfillmentProviderModel;
		final Optional<Details> status = saeeService.getStatus(consignmentModel.getTrackingID(),
				saeefulfillmentProviderModel.getBaseUrl());

		if (status.isEmpty())
		{
			throw new FulfillmentException(FulfillmentExceptionType.COULDNT_FETCH_STATUS, "Status is null");
		}

		final ConsignmentStatus consignmentStatus = saeeFulfillmentStatusMap.get(status.get().getStatus());
		consignmentModel.setFulfillmentStatus(consignmentStatus);
		consignmentModel.setFulfillmentStatusText(status.get().getNotes());

		modelService.save(consignmentModel);
		modelService.refresh(consignmentModel);

		saveResponseAndRequest.saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(),
				consignmentStatus == null ? "No mapping found for status: " + status.get().getStatus() : consignmentStatus.toString(),
				FulfillmentActionHistoryType.GETSTATUS);

		return Optional.ofNullable(consignmentStatus == null ? null : consignmentStatus.toString());
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		final Optional<String> currentStatus = getStatus(consignmentModel, fulfillmentProviderModel);
		if (currentStatus.isEmpty())
		{
			LOG.error("Couldn't fetch consignment status");
			saveResponseAndRequest.saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), "Fail",
					FulfillmentActionHistoryType.UPDATESTATUS);
			throw new FulfillmentException(FulfillmentExceptionType.COULDNT_FETCH_STATUS);
		}
		if (ConsignmentStatus.DELIVERY_COMPLETED.equals(ConsignmentStatus.valueOf(currentStatus.get())))
		{
			saveResponseAndRequest.updateConsignmentStatus(consignmentModel, ConsignmentStatus.valueOf(currentStatus.get()),
					currentStatus.get());
		}

		saveResponseAndRequest.saveActionInHistory(consignmentModel, consignmentModel.getTrackingID(), currentStatus.get(),
				FulfillmentActionHistoryType.UPDATESTATUS);

		modelService.refresh(consignmentModel);
		return Optional.ofNullable(consignmentModel.getStatus());

	}

}
