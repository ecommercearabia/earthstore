/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.consignmenttrackingservices.model.CarrierModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.dao.CustomConsignmentDao;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.service.CarrierService;
import com.earth.earthfulfillment.service.CustomConsignmentService;
import com.google.common.base.Preconditions;


/**
 * @author amjad.shati@erabia.com
 * @author mohammad-abumuhasien
 *
 */
public class DefaultCustomConsignmentService implements CustomConsignmentService
{
	protected static final Logger LOG = Logger.getLogger(DefaultCustomConsignmentService.class);

	private static final String STORE_MUST_NOT_BE_NULL = "BaseStoreModel must not be null";

	private static final String STATUS_MUST_NOT_BE_NULL = "ConsignmentStatus must not be null";

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "customConsignmentDao")
	private CustomConsignmentDao customConsignmentDao;

	@Override
	public List<ConsignmentModel> getConsignmentsByStoreAndNotStatus(final BaseStoreModel store, final ConsignmentStatus status)
	{
		Preconditions.checkArgument(store != null, STORE_MUST_NOT_BE_NULL);
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);

		final Optional<FulfillmentProviderModel> optional = fulfillmentProviderContext.getProvider(store);
		if (!optional.isPresent())
		{
			LOG.error("No FulfillmentProviderModel found for BaseStoreModel: " + store.getUid());
			return Collections.emptyList();
		}
		final CarrierModel carrierModel = carrierService.get(optional.get().getCode());
		if (carrierModel != null)
		{
			return customConsignmentDao.findByCarrierAndNotStatus(carrierModel, status);
		}
		LOG.warn("No CarrierModel found with code: " + optional.get().getCode());
		return Collections.emptyList();
	}

	@Override
	public List<ConsignmentModel> getConsignmentsByCurrentStoreAndNotStatus(final ConsignmentStatus status)
	{
		return getConsignmentsByStoreAndNotStatus(baseStoreService.getCurrentBaseStore(), status);
	}

	@Override
	public List<ConsignmentModel> getConsignmentsByNotStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);
		final List<ConsignmentModel> consignments = customConsignmentDao.findByNotStatus(status);
		if (consignments.isEmpty())
		{
			LOG.warn("No CarrierModel found");
			return Collections.emptyList();
		}
		LOG.info(consignments.size() + " consignments found to be updated");
		return consignments;
	}

	@Override
	public List<ConsignmentModel> getConsignmentsByStatus(final ConsignmentStatus status)
	{
		Preconditions.checkArgument(status != null, STATUS_MUST_NOT_BE_NULL);
		final List<ConsignmentModel> consignments = customConsignmentDao.findByStatus(status);
		if (consignments.isEmpty())
		{
			LOG.warn("No CarrierModel found");
			return Collections.emptyList();
		}
		return consignments;
	}

	@Override
	public Optional<ConsignmentModel> findByTackingId(final String trackingId, final String baseStoreId)
	{
		Preconditions.checkArgument(StringUtils.isNotBlank(trackingId), "Tracking Id is null");
		Preconditions.checkArgument(StringUtils.isNotBlank(baseStoreId), "base Store Id is null");

		return customConsignmentDao.findByTackingId(trackingId, baseStoreId);
	}

	@Override
	public List<ConsignmentModel> getByStore(final BaseStoreModel store)
	{
		Preconditions.checkArgument(store != null, "Store is null");
		return customConsignmentDao.findByStore(store);
	}

	@Override
	public List<ConsignmentModel> getByStoreAndByDate(final BaseStoreModel store, final String startingDate,
			final String endingDate)
	{
		Preconditions.checkArgument(store != null, "Store is null");
		Preconditions.checkArgument(startingDate != null, "Starting date is null");
		Preconditions.checkArgument(endingDate != null, "Ending date is null");


		return customConsignmentDao.findByStoreAndByDate(store, startingDate, endingDate);
	}

	@Override
	public List<ConsignmentModel> getByStoreAndByDateAndByStatus(final BaseStoreModel store, final String startingDate,
			final String endingDate, final ConsignmentStatus status)
	{
		Preconditions.checkArgument(store != null, "Store is null");
		Preconditions.checkArgument(startingDate != null, "Starting date is null");
		Preconditions.checkArgument(endingDate != null, "Ending date is null");
		Preconditions.checkArgument(status != null, "Consginment status is null");

		return customConsignmentDao.findByStoreAndByDateAndByStatus(store, startingDate, endingDate, status);
	}

	@Override
	public List<ConsignmentModel> getByStoreAndByShipmentStatusAndBySentStatus(final BaseStoreModel store,
			final ConsignmentStatus status, final boolean isSent)
	{
		Preconditions.checkArgument(store != null, "Store is null");
		Preconditions.checkArgument(status != null, "Consginment status is null");

		return customConsignmentDao.findByStoreAndByShipmentStatusAndBySentStatus(store, status, isSent);
	}

	@Override
	public List<ConsignmentModel> getByStoreAndByShipmentStatusesAndBySentStatus(final BaseStoreModel store,
			final List<ConsignmentStatus> statuses, final boolean isSent)
	{
		Preconditions.checkArgument(store != null, "Store is null");
		Preconditions.checkArgument(!CollectionUtils.isEmpty(statuses), "Consginment statuss is empty");

		return customConsignmentDao.findByStoreAndByShipmentStatusesAndBySentStatus(store, statuses, isSent);
	}


}