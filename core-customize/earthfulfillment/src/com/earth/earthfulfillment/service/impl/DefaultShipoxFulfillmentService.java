/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfulfillment.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.earth.earthfulfillment.enums.FulfillmentActionHistoryType;
import com.earth.earthfulfillment.enums.FulfillmentProviderType;
import com.earth.earthfulfillment.exception.FulfillmentException;
import com.earth.earthfulfillment.exception.enums.FulfillmentExceptionType;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;
import com.earth.earthfulfillment.model.ShipoxFulfillmentProviderModel;
import com.earth.earthfulfillment.service.CarrierService;
import com.earth.earthfulfillment.service.FulfillmentService;
import com.earth.earthfulfillment.service.impl.ConsignmentShipmentResponseComponent.Operation;
import com.earth.earthfulfillment.shipox.exceptions.ShipoxException;
import com.earth.earthfulfillment.shipox.model.ShipoxOrderContainer;
import com.earth.earthfulfillment.shipox.model.request.ShipoxOrderRequest;
import com.earth.earthfulfillment.shipox.model.response.FulfillmentResponse;
import com.earth.earthfulfillment.shipox.model.response.TrackOrderResponse;
import com.earth.earthfulfillment.shipox.service.ShipoxService;
import com.google.common.base.Preconditions;


/**
 *
 */
public class DefaultShipoxFulfillmentService implements FulfillmentService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultShipoxFulfillmentService.class);
	private static final String PICKUP_IN_STORE_MODE = "pickup";

	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "carrierService")
	private CarrierService carrierService;

	@Resource(name = "shipoxService")
	private ShipoxService shipoxService;

	@Resource(name = "consignmentShipmentResponseComponent")
	private ConsignmentShipmentResponseComponent saveResponseAndRequest;

	@Resource(name = "shipoxOrderRequestConverter")
	private Converter<ShipoxOrderContainer, ShipoxOrderRequest> shipoxOrderRequestPopulator;

	@Override
	public Optional<String> createShipment(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		// validate consignment and provider
		validateRequestData(consignmentModel, fulfillmentProviderModel, Operation.CREATE);

		final ShipoxFulfillmentProviderModel provider = (ShipoxFulfillmentProviderModel) fulfillmentProviderModel;

		final ShipoxOrderContainer container = new ShipoxOrderContainer();
		container.setConsignment(consignmentModel);
		container.setProvider(provider);

		final ShipoxOrderRequest shipoxOrderRequest = shipoxOrderRequestPopulator.convert(container);
		LOG.info("Create Shipment request is: {}", shipoxOrderRequest);
		saveResponseAndRequest.saveShipmentRequest(consignmentModel, shipoxOrderRequest.toString(), Operation.CREATE);

		FulfillmentResponse createOrder;
		try
		{
			createOrder = shipoxService.createOrder(shipoxOrderRequest, provider.getBaseUrl(), provider.getUsername(),
					provider.getPassword());
		}
		catch (final ShipoxException e)
		{
			LOG.error("Error in creating order - Shipox: {}", e.getMessage());
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, e.getMessage(), Operation.CREATE);
			saveResponseAndRequest.saveActionInHistory(consignmentModel, shipoxOrderRequest.toString(), e.getMessage(),
					FulfillmentActionHistoryType.CREATESHIPMENT);
			throw new FulfillmentException(FulfillmentExceptionType.FAILED_REQUEST, e.getMessage());
		}

		if (createOrder == null)
		{
			LOG.error("Empty response!");
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, "Empty response!", Operation.CREATE);
			saveResponseAndRequest.saveActionInHistory(consignmentModel, shipoxOrderRequest.toString(), "Empty response!",
					FulfillmentActionHistoryType.CREATESHIPMENT);
			return Optional.empty();
		}

		if (!createOrder.getStatus().equalsIgnoreCase("success") || StringUtils.isBlank(createOrder.getTrackingId()))
		{
			LOG.error("Failed request due to failed status in response or empty shipment tracking ID");
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, createOrder.toString(), Operation.CREATE);
			saveResponseAndRequest.saveActionInHistory(consignmentModel, shipoxOrderRequest.toString(), createOrder.toString(),
					FulfillmentActionHistoryType.CREATESHIPMENT);
			return Optional.empty();
		}

		LOG.info("Create Shipment response is: {}", createOrder);
		saveResponseAndRequest.saveShipmentResponse(consignmentModel, createOrder.toString(), Operation.CREATE);
		saveResponseAndRequest.saveTrackingIdAndCarrier(fulfillmentProviderModel, consignmentModel, createOrder.getTrackingId(),
				createOrder.getId(), FulfillmentProviderType.SHIPOX);
		saveResponseAndRequest.saveActionInHistory(consignmentModel, shipoxOrderRequest.toString(), createOrder.toString(),
				FulfillmentActionHistoryType.CREATESHIPMENT);
		return StringUtils.isBlank(createOrder.getTrackingId()) ? Optional.empty() : Optional.of(createOrder.getTrackingId());
	}

	@Override
	public Optional<byte[]> printAWB(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		validateRequestData(consignmentModel, fulfillmentProviderModel, Operation.AWB);

		final ShipoxFulfillmentProviderModel provider = (ShipoxFulfillmentProviderModel) fulfillmentProviderModel;

		FulfillmentResponse awbLabel = null;
		try
		{
			awbLabel = shipoxService.printAWBLabel(consignmentModel.getShipmentOrderId(), provider.getBaseUrl(),
					provider.getUsername(), provider.getPassword());
		}
		catch (final ShipoxException e)
		{
			LOG.error("Error in creating order - Shipox: {}", e.getMessage());
			saveResponseAndRequest.saveActionInHistory(consignmentModel, "Print AWB for " + consignmentModel.getShipmentOrderId(),
					e.getMessage(), FulfillmentActionHistoryType.PRINTAWB);
			throw new FulfillmentException(FulfillmentExceptionType.FAILED_REQUEST, e.getMessage());
		}

		if (awbLabel == null)
		{
			LOG.error("Empty response!");
			saveResponseAndRequest.saveActionInHistory(consignmentModel, "Print AWB for " + consignmentModel.getShipmentOrderId(),
					"Empty response!", FulfillmentActionHistoryType.PRINTAWB);
			return Optional.empty();
		}

		LOG.info("Create Shipment response is: {}", awbLabel);
		saveResponseAndRequest.saveActionInHistory(consignmentModel, "Print AWB for " + consignmentModel.getShipmentOrderId(),
				awbLabel.toString(), FulfillmentActionHistoryType.PRINTAWB);
		return awbLabel.getAwbFile() == null || awbLabel.getAwbFile().length == 0 ? Optional.empty()
				: Optional.of(awbLabel.getAwbFile());
	}

	@Override
	public Optional<String> getStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		validateRequestData(consignmentModel, fulfillmentProviderModel, Operation.STATUS);

		final ShipoxFulfillmentProviderModel provider = (ShipoxFulfillmentProviderModel) fulfillmentProviderModel;

		TrackOrderResponse order = null;
		try
		{
			order = shipoxService.trackOrder(consignmentModel.getTrackingID(), provider.getBaseUrl(), provider.getUsername(),
					provider.getPassword());
		}
		catch (final ShipoxException e)
		{
			LOG.error("Error in tracking order - Shipox: {}", e.getMessage());
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, e.getMessage(), Operation.STATUS);
			saveResponseAndRequest.saveActionInHistory(consignmentModel, "Tracking shipment " + consignmentModel.getTrackingID(),
					e.getMessage(), FulfillmentActionHistoryType.GETSTATUS);
			throw new FulfillmentException(FulfillmentExceptionType.FAILED_REQUEST, e.getMessage());
		}

		if (order == null)
		{
			LOG.error("Empty response!");
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, "Empty response!", Operation.STATUS);
			saveResponseAndRequest.saveActionInHistory(consignmentModel, "Tracking shipment " + consignmentModel.getTrackingID(),
					"Empty response!", FulfillmentActionHistoryType.GETSTATUS);
			return Optional.empty();
		}

		LOG.info("Create Shipment response is: {}", order);
		saveResponseAndRequest.saveShipmentResponse(consignmentModel, order.toString(), Operation.STATUS);
		saveResponseAndRequest.saveActionInHistory(consignmentModel, "Tracking shipment " + consignmentModel.getTrackingID(),
				order.toString(), FulfillmentActionHistoryType.GETSTATUS);

		return Optional.ofNullable(order.getOrdersResponse().get(0).getOrderStatus());
	}

	@Override
	public Optional<ConsignmentStatus> updateStatus(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel) throws FulfillmentException
	{
		final Optional<String> status = getStatus(consignmentModel, fulfillmentProviderModel);
		if (status.isEmpty())
		{
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, "No status retrieved!", Operation.STATUS);
			saveResponseAndRequest.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), "No status retrieved!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}
		LOG.info("retreived status for {} is {}", consignmentModel.getCode(), status.get());
		final ShipoxFulfillmentProviderModel provider = (ShipoxFulfillmentProviderModel) fulfillmentProviderModel;
		final Optional<ConsignmentStatus> mappedStatus = provider.getShipmentStatusMappings().keySet().stream()
				.filter(shipmentStatus -> shipmentStatus.equalsIgnoreCase(status.get()))
				.map(key -> provider.getShipmentStatusMappings().get(key)).findAny();
		if (mappedStatus.isEmpty())
		{
			LOG.info("Could not map the {} shipment status", status.get());
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, status.get() + " not found in the status mapping!",
					Operation.STATUS);
			saveResponseAndRequest.saveActionInHistory(consignmentModel,
					"Updating the status of " + consignmentModel.getTrackingID(), status.get() + " not found in the status mapping!",
					FulfillmentActionHistoryType.UPDATESTATUS);
			return Optional.empty();
		}

		LOG.info("Shipment status {} is mapped with {}", status.get(), mappedStatus.get());
		saveResponseAndRequest.saveShipmentResponse(consignmentModel, "Mapped status : " + mappedStatus.get(), Operation.UPDATE);
		saveResponseAndRequest.updateConsignmentStatus(consignmentModel, mappedStatus.get(), status.get());
		saveResponseAndRequest.saveActionInHistory(consignmentModel, "Updating the status of " + consignmentModel.getTrackingID(),
				mappedStatus.get().toString(), FulfillmentActionHistoryType.UPDATESTATUS);
		return Optional.of(mappedStatus.get());
	}

	protected void validateRequestData(final ConsignmentModel consignmentModel,
			final FulfillmentProviderModel fulfillmentProviderModel, final Operation operation) throws FulfillmentException
	{
		Preconditions.checkArgument(consignmentModel != null, "Consignment model is empty");
		Preconditions.checkArgument(consignmentModel.getOrder() != null, "Order model is empty");
		Preconditions.checkArgument(fulfillmentProviderModel != null, "Shipox provider is not available");
		Preconditions.checkArgument(fulfillmentProviderModel instanceof ShipoxFulfillmentProviderModel,
				fulfillmentProviderModel.getClass() + " Provider not supported");
		Preconditions.checkArgument(validateConsignmentWarehousePointOfService(consignmentModel),
				"Invalid consignment, check the warehouse or point of service configurations");
		Preconditions.checkArgument(validateOrderDeliveryMode(consignmentModel.getOrder()),
				"Cannot create shipment to an order with PICKUP IN STORE delivery mode");

		if (!validateShipoxProvider((ShipoxFulfillmentProviderModel) fulfillmentProviderModel))
		{
			LOG.error("Not valid provider, check Shipox provider for missing required data");
			saveResponseAndRequest.saveShipmentResponse(consignmentModel, FulfillmentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(),
					operation);
			FulfillmentActionHistoryType type = null;
			switch (operation)
			{
				case CREATE:
					type = FulfillmentActionHistoryType.CREATESHIPMENT;
					break;
				case STATUS:
					type = FulfillmentActionHistoryType.GETSTATUS;
					break;
				case UPDATE:
					type = FulfillmentActionHistoryType.UPDATESTATUS;
					break;
				default:
					type = FulfillmentActionHistoryType.PRINTAWB;
			}

			saveResponseAndRequest.saveActionInHistory(consignmentModel, fulfillmentProviderModel.getCode() + " for " + operation,
					FulfillmentExceptionType.PROVIDER_NOT_SUPPORTED.getValue(), type);
			throw new FulfillmentException(FulfillmentExceptionType.PROVIDER_NOT_SUPPORTED);
		}
	}

	/**
	 *
	 */
	private boolean validateOrderDeliveryMode(final AbstractOrderModel order)
	{
		if (order.getDeliveryMode() == null || PICKUP_IN_STORE_MODE.equalsIgnoreCase(order.getDeliveryMode().getCode()))
		{
			return false;
		}
		return true;
	}

	/**
	 *
	 */
	protected boolean validateShipoxProvider(final ShipoxFulfillmentProviderModel provider)
	{
		if (provider == null)
		{
			return false;
		}
		if (!provider.getActive())
		{
			LOG.error("Shipox provider is not active");
			return false;
		}
		if (StringUtils.isBlank(provider.getBaseUrl()))
		{
			LOG.error("Empty base url on Shipox provider");
			return false;
		}
		if (StringUtils.isBlank(provider.getUsername()))
		{
			LOG.error("Empty username on Shipox provider");
			return false;
		}
		if (StringUtils.isBlank(provider.getPassword()))
		{
			LOG.error("Empty password on Shipox provider");
			return false;
		}
		//		if (CollectionUtils.isEmpty(provider.getShipmentStatusMappings()))
		//		{
		//			return false;
		//		}
		return true;
	}

	protected boolean validateConsignmentWarehousePointOfService(final ConsignmentModel consignment)
	{
		if (consignment == null)
		{
			LOG.error("Empty Consignment!");
			return false;
		}
		if (consignment.getWarehouse() == null)
		{
			LOG.error("Empty Warehouse!");
			return false;
		}
		final WarehouseModel warehouse = consignment.getWarehouse();
		if (consignment.getDeliveryPointOfService() == null && (warehouse.getDeliveryCountry() == null
				|| warehouse.getDeliveryCity() == null || warehouse.getDeliveryArea() == null))
		{
			LOG.error("Missing delivery attributes on the {} warehouse", warehouse.getCode());
			return false;
		}
		if (consignment.getDeliveryPointOfService() != null && (consignment.getDeliveryPointOfService().getAddress() == null
				|| consignment.getDeliveryPointOfService().getAddress().getCountry() == null
				|| consignment.getDeliveryPointOfService().getAddress().getCity() == null
						&& consignment.getDeliveryPointOfService().getAddress().getArea() == null))
		{
			LOG.error("Missing delivery attributes on the {} point of service", consignment.getDeliveryPointOfService().getName());
			return false;
		}
		return true;
	}

}
