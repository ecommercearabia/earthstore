/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramfacades.constants;

/**
 * Global class for all Earthloyaltyprogramfacades constants. You can add global constants for your extension into this class.
 */
public final class EarthloyaltyprogramfacadesConstants extends GeneratedEarthloyaltyprogramfacadesConstants
{
	public static final String EXTENSIONNAME = "earthloyaltyprogramfacades";

	private EarthloyaltyprogramfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "earthloyaltyprogramfacadesPlatformLogo";
}
