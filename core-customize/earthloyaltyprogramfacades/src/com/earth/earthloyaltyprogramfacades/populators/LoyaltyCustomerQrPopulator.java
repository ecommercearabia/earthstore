/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerCode;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyCustomerQrPopulator implements Populator<LoyaltyCustomerCode, LoyaltyCustomerQrData>
{

	@Override
	public void populate(final LoyaltyCustomerCode source, final LoyaltyCustomerQrData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCustomerCode(source.getCustomerCode());
		target.setQrCode(source.getQrCode());

	}


}
