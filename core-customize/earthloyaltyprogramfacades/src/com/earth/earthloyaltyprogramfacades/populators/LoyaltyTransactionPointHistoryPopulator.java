/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyTransactionPointHistoryData;
import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.TransactionPointHistory;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyTransactionPointHistoryPopulator
		implements Populator<TransactionPointHistory, LoyaltyTransactionPointHistoryData>
{

	@Override
	public void populate(final TransactionPointHistory source, final LoyaltyTransactionPointHistoryData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setAmount(source.getAmount());
		target.setCreatedDate(source.getCreatedDate());

	}



}
