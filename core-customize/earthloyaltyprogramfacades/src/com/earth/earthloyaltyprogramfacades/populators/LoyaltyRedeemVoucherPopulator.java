/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyRedeemVoucherData;
import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.RedeemVoucherBean;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyRedeemVoucherPopulator implements Populator<RedeemVoucherBean, LoyaltyRedeemVoucherData>
{

	@Override
	public void populate(final RedeemVoucherBean source, final LoyaltyRedeemVoucherData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setPurchaseDate(source.getCreatedDate());
		target.setRedeemedPoints(source.getRedeemedPoints());
	}

}
