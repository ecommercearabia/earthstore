/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyBalancePopulator implements Populator<LoyaltyBalance, LoyaltyBalanceData>
{
	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;


	@Override
	public void populate(final LoyaltyBalance source, final LoyaltyBalanceData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setBalancePoints(source.getPoints());
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (!Objects.isNull(currentBaseStore))
		{
			target.setBalanceAmount(
					getPriceData(BigDecimal.valueOf(source.getValue()), PriceDataType.BUY, currentBaseStore.getDefaultCurrency()));

		}
	}

	private PriceData getPriceData(final BigDecimal value, final PriceDataType priceType, final CurrencyModel currency)
	{
		if (currency == null)
		{
			return null;
		}
		return priceDataFactory.create(priceType, value, currency);
	}
}
