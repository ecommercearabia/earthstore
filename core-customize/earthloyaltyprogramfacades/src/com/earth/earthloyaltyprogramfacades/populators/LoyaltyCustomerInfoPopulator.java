/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerHistoryData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.earth.earthloyaltyprogramprovider.beans.CustomerHistory;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyBalance;
import com.earth.earthloyaltyprogramprovider.beans.LoyaltyCustomerInfo;



/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyCustomerInfoPopulator implements Populator<LoyaltyCustomerInfo, LoyaltyCustomerInfoData>
{

	@Resource(name = "loyaltyBalanceConverter")
	private Converter<LoyaltyBalance, LoyaltyBalanceData> loyaltyBalanceConverter;

	@Resource(name = "loyaltyCustomerHistoryConverter")
	private Converter<CustomerHistory, LoyaltyCustomerHistoryData> loyaltyCustomerHistoryConverter;

	@Override
	public void populate(final LoyaltyCustomerInfo source, final LoyaltyCustomerInfoData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		if (!Objects.isNull(source.getBalance()))
		{
			target.setBalance(loyaltyBalanceConverter.convert(source.getBalance()));
		}

		target.setCustomerId(source.getCustomerId());
		if (!CollectionUtils.isEmpty(source.getHistories()))
		{
			target.setHistories(loyaltyCustomerHistoryConverter.convertAll(source.getHistories()));
		}
		target.setPageIndex(source.getPageIndex());
		target.setPageSize(source.getPageSize());
		target.setTotalCount(source.getTotalCount());
		target.setTotalPage(source.getTotalPage());
	}
}
