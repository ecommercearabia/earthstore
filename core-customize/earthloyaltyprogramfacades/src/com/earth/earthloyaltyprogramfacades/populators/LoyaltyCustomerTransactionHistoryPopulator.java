/**
 *
 */
package com.earth.earthloyaltyprogramfacades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerTransactionHistoryData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyRedeemVoucherData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyTransactionPointHistoryData;
import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.RedeemVoucherBean;
import com.earth.earthloyaltyprogramprovider.apt.beans.innerbeans.TransactionPointHistory;
import com.earth.earthloyaltyprogramprovider.apt.beans.response.CustomerTransactionHistoryResponseBean;



/**
 * @author mnasro
 *
 */
public class LoyaltyCustomerTransactionHistoryPopulator
		implements Populator<CustomerTransactionHistoryResponseBean, LoyaltyCustomerTransactionHistoryData>
{

	@Resource(name = "loyaltyTransactionPointHistoryConverter")
	private Converter<TransactionPointHistory, LoyaltyTransactionPointHistoryData> loyaltyTransactionPointHistoryConverter;

	@Resource(name = "loyaltyRedeemVoucherConverter")
	private Converter<RedeemVoucherBean, LoyaltyRedeemVoucherData> loyaltyRedeemVoucherConverter;


	/**
	 * @return the loyaltyRedeemVoucherConverter
	 */
	protected Converter<RedeemVoucherBean, LoyaltyRedeemVoucherData> getLoyaltyRedeemVoucherConverter()
	{
		return loyaltyRedeemVoucherConverter;
	}

	/**
	 * @return the loyaltyTransactionPointHistoryConverter
	 */
	protected Converter<TransactionPointHistory, LoyaltyTransactionPointHistoryData> getLoyaltyTransactionPointHistoryConverter()
	{
		return loyaltyTransactionPointHistoryConverter;
	}


	@Override
	public void populate(final CustomerTransactionHistoryResponseBean source, final LoyaltyCustomerTransactionHistoryData target)
			throws ConversionException
	{
		if (source == null)
		{
			return;
		}
		Assert.notNull(target, "Parameter target cannot be null.");

		if (!CollectionUtils.isEmpty(source.getTransactionPointHistory()))
		{
			target.setLoyaltyTransactionPointHistories(
					getLoyaltyTransactionPointHistoryConverter().convertAll(source.getTransactionPointHistory()));
		}

		if (!CollectionUtils.isEmpty(source.getRedeemedVouchers()))
		{
			target.setLoyaltyRedeemVouchers(getLoyaltyRedeemVoucherConverter().convertAll(source.getRedeemedVouchers()));
		}
	}

}
