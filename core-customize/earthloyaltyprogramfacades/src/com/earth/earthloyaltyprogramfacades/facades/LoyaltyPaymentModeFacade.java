package com.earth.earthloyaltyprogramfacades.facades;

import java.util.List;
import java.util.Optional;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaymentModeData;


/**
 * @author amjad.shati@erabia.com
 *
 */
public interface LoyaltyPaymentModeFacade
{
	public Optional<LoyaltyPaymentModeData> getLoyaltyPaymentMode(String loyaltyPointModeTypeCode);

	public List<LoyaltyPaymentModeData> getSupportedLoyaltyPaymentModesCurrentBaseStore();

	public boolean isLoyaltyModeSupportedByCurrentBaseStore(final String paymentModeTypeCode);

}
