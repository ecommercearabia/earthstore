/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramfacades.facades.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.earth.earthloyaltyprogramfacades.facades.LoyaltyPaymentModeFacade;
import com.earth.earthloyaltyprogramprovider.model.LoyaltyPaymentModeModel;
import com.earth.earthloyaltyprogramprovider.service.LoyaltyPaymentModeService;


/**
 *
 */
public class DefaultLoyaltyPaymentModeFacade implements LoyaltyPaymentModeFacade
{
	@Resource(name = "loyaltyPaymentModeService")
	private LoyaltyPaymentModeService loyaltyPaymentModeService;

	@Resource(name = "loyaltyPaymentModeConverter")
	private Converter<LoyaltyPaymentModeModel, LoyaltyPaymentModeData> loyaltyPaymentModeConverter;


	@Override
	public Optional<LoyaltyPaymentModeData> getLoyaltyPaymentMode(final String loyaltyPointModeTypeCode)
	{
		if (Strings.isBlank(loyaltyPointModeTypeCode))
		{
			throw new IllegalArgumentException("loyaltyPointModeTypeCode is null or empty");
		}
		final Optional<LoyaltyPaymentModeModel> loyaltyPaymentMode = loyaltyPaymentModeService
				.getLoyaltyPaymentMode(loyaltyPointModeTypeCode);

		if (loyaltyPaymentMode.isEmpty())
		{
			return Optional.empty();
		}

		return Optional.ofNullable(loyaltyPaymentModeConverter.convert(loyaltyPaymentMode.get()));
	}

	@Override
	public List<LoyaltyPaymentModeData> getSupportedLoyaltyPaymentModesCurrentBaseStore()
	{
		final List<LoyaltyPaymentModeModel> supportedLoyaltyPaymentModesCurrentBaseStore = loyaltyPaymentModeService
				.getSupportedLoyaltyPaymentModesCurrentBaseStore();

		if (CollectionUtils.isEmpty(supportedLoyaltyPaymentModesCurrentBaseStore))
		{
			return Collections.emptyList();
		}
		return loyaltyPaymentModeConverter.convertAll(supportedLoyaltyPaymentModesCurrentBaseStore);
	}

	@Override
	public boolean isLoyaltyModeSupportedByCurrentBaseStore(final String paymentModeTypeCode)
	{
		final List<LoyaltyPaymentModeData> loyaltyPaymentModes = getSupportedLoyaltyPaymentModesCurrentBaseStore();

		if (CollectionUtils.isEmpty(loyaltyPaymentModes))
		{
			return false;
		}
		final List<String> loyaltyPaymentModesCodes = new ArrayList();
		for (final LoyaltyPaymentModeData loyaltyPaymentMode : loyaltyPaymentModes)
		{
			loyaltyPaymentModesCodes.add(loyaltyPaymentMode.getLoyaltyPaymentModeType().getCode());
		}
		return loyaltyPaymentModesCodes.contains(paymentModeTypeCode);
	}

}
