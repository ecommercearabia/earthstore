/*
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthloyaltyprogramfacades.facades;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerInfoData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyCustomerQrData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaginationData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyUsablePointData;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;


/**
 *
 */
public interface LoyaltyPaymentFacade
{
	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePoints(CartModel cart) throws EarthLoyaltyException;

	public Optional<LoyaltyUsablePointData> getLoyaltyUsablePointsByCurrentCart() throws EarthLoyaltyException;

	public Optional<LoyaltyBalanceData> getBalance(final AbstractOrderModel abstractOrderModel);

	public Optional<LoyaltyBalanceData> getBalance(final CustomerModel customerModel, final BaseStoreModel baseStoreModel);

	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStore(final CustomerModel customerModel);

	public Optional<LoyaltyBalanceData> getBalanceByCurrentBaseStoreAndCustomer();

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomer(CustomerModel customer, BaseStoreModel baseStoreModel,
			LoyaltyPaginationData paginationData);

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStore(CustomerModel customer,
			LoyaltyPaginationData paginationData);

	public Optional<LoyaltyCustomerInfoData> getLoyaltyCustomerByCurrentBaseStoreAndCurrentCustomer(
			LoyaltyPaginationData paginationData);

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCode(CustomerModel customer, BaseStoreModel baseStoreModel);

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStore(CustomerModel customer);

	public Optional<LoyaltyCustomerQrData> getLoyaltyCustomerQrCodeByCurrentBaseStoreAndCustomer();

	public boolean isEnabledForCustomer(CustomerModel customer, BaseStoreModel baseStoreModel);

	public boolean isEnabledForCustomerByCurrentBaseStore(CustomerModel customer);

	public boolean isEnabledForCustomerByCurrentBaseStoreAndCurrentCustomer();

	public boolean isEnabledOnStore(BaseStoreModel baseStoreModel);

	public boolean isEnabledOnStoreByCurrentBaseStore();

	public Optional<Object> getTransactionHistory(CustomerModel customerModel, BaseStoreModel baseStoreModel);

	public Optional<Object> getTransactionHistoryByCurrentBaseStore(CustomerModel customerModel);

	public Optional<Object> getTransactionHistoryByCurrentBaseStoreAndCustomer();



}
