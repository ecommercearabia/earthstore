/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @author Juman Resolves page title according to page, search text, current category or product
 */
public class CustomPageTitleResolver extends PageTitleResolver
{
	@Override
	public String resolveProductPageTitle(final ProductModel product)
	{
		// Lookup categories
		final List<CategoryModel> path = getCategoryPath(getProductAndCategoryHelper().getBaseProduct(product));
		// Lookup site (or store)
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		final String prefix = StringUtils.isBlank(currentSite.getPrefixTitleForProductPage()) ? StringUtils.EMPTY
				: currentSite.getPrefixTitleForProductPage();

		final String suffix = StringUtils.isBlank(currentSite.getSuffixTitleForProductPage()) ? StringUtils.EMPTY
				: currentSite.getSuffixTitleForProductPage();
		final String suffixProductTitle = StringUtils.isBlank(currentSite.getSuffixProductTitleForProductPage()) ? StringUtils.EMPTY
				: currentSite.getSuffixProductTitleForProductPage();

		// Construct page title
		final String identifier = product.getName();
		final String articleNumber = product.getCode();
		final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
		final StringBuilder builder = new StringBuilder(prefix);
		if (!StringUtils.isBlank(builder.toString()))
		{
			builder.append(" ");
		}

		builder.append(productName);

		if (!StringUtils.isBlank(suffixProductTitle))
		{
			builder.append(" ");

			builder.append(suffixProductTitle);
			builder.append(" ");

		}

		final int size = getNeededIndexSize(path);
		for (int i = 0; i < size; i++)
		{
			builder.append(TITLE_WORD_SEPARATOR).append(path.get(i).getName());

		}

		if (!StringUtils.isBlank(suffix))
		{
			builder.append(TITLE_WORD_SEPARATOR).append(suffix);
		}

		if (currentSite != null)
		{
			builder.append(TITLE_WORD_SEPARATOR).append(currentSite.getName());
		}

		return StringEscapeUtils.escapeHtml(builder.toString().trim());
	}

	/**
	 * @param path
	 * @return
	 */
	private int getNeededIndexSize(final List<CategoryModel> path)
	{
		if (CollectionUtils.isEmpty(path))
		{
			return 0;
		}

		final String code = path.get(path.size() - 1).getCode();

		final int indexSize = code.equalsIgnoreCase("1") ? path.size() - 1 : path.size();
		if (indexSize == 0)
		{
			return 0;
		}
		if (indexSize == 1)
		{
			return 1;
		}

		return indexSize - 1;
	}


}
