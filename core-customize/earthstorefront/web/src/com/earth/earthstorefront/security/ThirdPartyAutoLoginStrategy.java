/**
 *
 */
package com.earth.earthstorefront.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author monzer
 *
 */
public interface ThirdPartyAutoLoginStrategy
{
	boolean login(String username, Object token, String provider, HttpServletRequest request, final HttpServletResponse response);
}
