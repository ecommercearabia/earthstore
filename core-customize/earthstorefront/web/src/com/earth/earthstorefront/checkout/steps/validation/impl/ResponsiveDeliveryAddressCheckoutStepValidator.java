/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.checkout.steps.validation.impl;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.earth.earthfacades.facade.CustomCheckoutFlowFacade;
import com.earth.earthstorefront.checkout.steps.validation.AbstractCheckoutStepValidator;
import com.earth.earthstorefront.checkout.steps.validation.ValidationResults;


public class ResponsiveDeliveryAddressCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	@Resource(name = "customCheckoutFlowFacade")
	private CustomCheckoutFlowFacade customCheckoutFlowFacade;

	public CustomCheckoutFlowFacade getCustomCheckoutFlowFacade()
	{
		return customCheckoutFlowFacade;
	}

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCustomCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (!getCheckoutFacade().hasShippingItems())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		return ValidationResults.SUCCESS;
	}
}
