/**
 *
 */
package com.earth.earthstorefront.form.validation;



import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.enums.MaritalStatus;
import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.user.nationality.service.NationalityService;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthstorefront.form.ThirdPartyProfileForm;



/**
 * @author monzer
 *
 */
@Component("thirdPartyProfileValidator")
public class ThirdPartyProfileValidator implements Validator
{

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return ThirdPartyProfileForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		if (!(object instanceof ThirdPartyProfileForm))
		{
			errors.rejectValue("profile", "thirdParty.profile.invalid");
			return;
		}
		final ThirdPartyProfileForm profile = (ThirdPartyProfileForm) object;
		final String titleCode = profile.getTitleCode();
		final String firstName = profile.getFirstName();
		final String lastName = profile.getLastName();
		final String nationality = profile.getNationality();
		final String nationalityId = profile.getNationalityId();
		final String birthDate = profile.getBirthDate();

		validateUserData(titleCode, firstName, lastName, errors);
		validateMobileData(profile, errors);

		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		validateNationality(errors, currentSite, nationality);
		if (profile.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityId);
		}
		validateBirthDate(errors, currentSite, birthDate);
		if (currentSite != null && currentSite.isCustomerMaritalStatusEnabled() && currentSite.isCustomerMaritalStatusRequired())
		{
			validateMaritalStatus(profile.getMaritalStatusCode(), errors);
		}
	}

	/**
	 * @param profile
	 * @param errors
	 */
	private void validateMobileData(final ThirdPartyProfileForm profile, final Errors errors)
	{
		if (StringUtils.isEmpty(profile.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "thirdParty.profile.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(profile.getMobileNumber()) || !validateMobileNumberPattern(profile.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "thirdParty.profile.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(profile.getMobileCountry()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(profile.getMobileCountry(), profile.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				profile.setMobileNumber(normalizedPhoneNumber.get());
			}
			else
			{
				errors.rejectValue("mobileNumber", "thirdParty.profile.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "profile.mobileCountry.invalid");
			}
		}
	}

	/**
	 * @param titleCode
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param errors
	 */
	private void validateUserData(final String titleCode, final String firstName, final String lastName,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(titleCode) && StringUtils.length(titleCode) > 255)
		{
			errors.rejectValue("titleCode", "thirdParty.profile.titleCode.invalid");
		}

		if (StringUtils.isBlank(firstName) || StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "thirdParty.profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName) || StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "thirdParty.profile.lastName.invalid");
		}
	}

	/**
	 * @param maritalStatusCode
	 * @param errors
	 * @param currentSite
	 */
	private void validateMaritalStatus(final String maritalStatusCode, final Errors errors)
	{
		if (StringUtils.isBlank(maritalStatusCode))
		{
			errors.rejectValue("maritalStatusCode", "thirdParty.profile.maritalStatus.empty");
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatusCode);
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("maritalStatusCode", "thirdParty.profile.maritalStatus.invalid");
		}
	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue("birthDate", "thirdParty.profile.birthofdate.invalid");
		}

	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue("nationalityID", "thirdParty.profile.nationalityid.invalid");
		}

	}

	/**
	 * @param errors
	 * @param cmsSiteModel
	 * @param nationality
	 */
	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "thirdParty.profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "thirdParty.profile.nationality.invalid");
			}
		}


	}

	private boolean validateMobileNumberPattern(final String mobileNumber)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(mobileNumber);
		return matcher.matches();
	}

}
