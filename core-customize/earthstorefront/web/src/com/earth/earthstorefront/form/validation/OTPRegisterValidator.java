package com.earth.earthstorefront.form.validation;

import org.springframework.stereotype.Component;

import com.earth.earthstorefront.form.OTPForm;


/**
 * The Class OTPValidator.
 *
 * @author mnasro
 */
@Component("otpRegisterValidator")
public class OTPRegisterValidator extends OTPValidator
{

	@Override
	public boolean supports(final Class<?> form)
	{
		return form.getClass().equals(OTPForm.class);
	}

}
