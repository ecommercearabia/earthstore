/**
 *
 */
package com.earth.earthstorefront.form;

import java.io.Serializable;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class LoyaltyPointsForm implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String loyaltyPaymentCode;
	private String loyaltyPaymentAmount;
	
	/**
	 * @return the loyaltyPaymentCode
	 */
	public String getLoyaltyPaymentCode()
	{
		return loyaltyPaymentCode;
	}
	
	/**
	 * @param loyaltyPaymentCode
	 *           the loyaltyPaymentCode to set
	 */
	public void setLoyaltyPaymentCode(final String loyaltyPaymentCode)
	{
		this.loyaltyPaymentCode = loyaltyPaymentCode;
	}
	
	/**
	 * @return the loyaltyPaymentAmount
	 */
	public String getLoyaltyPaymentAmount()
	{
		return loyaltyPaymentAmount;
	}
	
	/**
	 * @param loyaltyPaymentAmount
	 *           the loyaltyPaymentAmount to set
	 */
	public void setLoyaltyPaymentAmount(final String loyaltyPaymentAmount)
	{
		this.loyaltyPaymentAmount = loyaltyPaymentAmount;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}


}
