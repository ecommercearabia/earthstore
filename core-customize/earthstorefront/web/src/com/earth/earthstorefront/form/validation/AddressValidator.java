/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.form.validation;



import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.AddressService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.model.AreaModel;
import com.earth.earthcore.model.CityModel;
import com.earth.earthcore.user.area.service.AreaService;
import com.earth.earthcore.user.city.service.CityService;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthstorefront.form.AddressForm;


/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("customAddressValidator")
public class AddressValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;
	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;

	@Resource(name = "addressService")
	private AddressService addressService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "cityService")
	private CityService cityService;

	@Resource(name = "areaService")
	private AreaService areaService;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		validateStandardFields(addressForm, errors);
		validateCountrySpecificFields(addressForm, errors);
	}

	protected void validateStandardFields(final AddressForm addressForm, final Errors errors)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getAddressName(), AddressField.ADDRESS_NAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);

		if (currentSite != null && currentSite.isNearestLandmarkAddressEnabled() && currentSite.isNearestLandmarkAddressRequired())
		{
			validateStringField(addressForm.getNearestLandmark(), AddressField.NEAREST_LANDMARK, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isBuildingNameAddressEnabled() && currentSite.isBuildingNameAddressRequired())
		{
			validateStringField(addressForm.getBuildingName(), AddressField.BUILDING_NAME, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isFloorNumberAddressEnabled() && currentSite.isFloorNumberAddressRequired())
		{
			validateStringField(addressForm.getFloorNumber(), AddressField.FLOOR_NUMBER, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isApartmentNumberAddressEnabled() && currentSite.isApartmentNumberAddressRequired())
		{
			validateStringField(addressForm.getApartmentNumber(), AddressField.APARTMENT_NUMBER, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isMoreInstructionsAddressEnabled()
				&& currentSite.isMoreInstructionsAddressRequired())
		{
			validateStringField(addressForm.getMoreInstructions(), AddressField.MORE_INSTRUCTIONS, MAX_FIELD_LENGTH, errors);
		}
		if (currentSite != null && currentSite.isAddressInstructionsAddressEnabled()
				&& currentSite.isAddressInstructionsAddressRequired())
		{
			validateStringField(addressForm.getAddressInstructions(), AddressField.ADDRESS_INSTRUCTIONS, MAX_FIELD_LENGTH, errors);
		}

		validateStringField(addressForm.getMobile(), AddressField.MOBILE, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getMobileCountry(), AddressField.MOBILECOUNTRY, MAX_FIELD_LENGTH, errors);
		vaildateMobile(addressForm, errors);
	}

	protected void vaildateMobile(final AddressForm addressForm, final Errors errors)
	{
		if (addressForm.getMobileCountry() != null && !StringUtils.isEmpty(addressForm.getMobile()))
		{
			addressForm.setMobile(addressForm.getMobile());
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(addressForm.getMobileCountry(), addressForm.getMobile());

			if (normalizedPhoneNumber.isEmpty())
			{
				errors.rejectValue("mobile", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.countryMobileNumber.invalid");
			}
			else
			{
				addressForm.setMobile(normalizedPhoneNumber.get());
			}

		}
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null)
		{
			switch (CountryCode.lookup(isoCode))
			{
				case CHINA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case CANADA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case USA:

					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case JAPAN:
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
					break;
				default:
					if (isCityRequired())
					{
						validateCityField(addressForm.getCityCode(), AddressField.CITY, MAX_FIELD_LENGTH, errors, isoCode);
					}
					if (isAreaRequired())
					{
						validateAreaField(addressForm.getAreaCode(), AddressField.AREA, MAX_FIELD_LENGTH, errors,
								addressForm.getCityCode());
					}
					break;
			}
		}
	}

	/**
	 * @param areaCode
	 * @param area
	 * @param maxFieldLength
	 * @param errors
	 */
	private void validateAreaField(final String areaCode, final AddressField addressField, final int maxFieldLength,
			final Errors errors, final String cityCode)
	{
		if (validateStringField(areaCode, addressField, maxFieldLength, errors))
		{
			return;
		}
		final Optional<AreaModel> areaModel = areaService.get(areaCode);
		if (!areaModel.isPresent() || !areaModel.get().getCity().getCode().equals(cityCode))
		{
			errors.rejectValue(addressField.getFieldKey(), addressField.getErrorKey());
		}
	}

	/**
	 * @param cityCode
	 * @param addressField
	 * @param maxFieldLength
	 * @param errors
	 */
	private void validateCityField(final String cityCode, final AddressField addressField, final int maxFieldLength,
			final Errors errors, final String countryCode)
	{
		if (validateStringField(cityCode, addressField, maxFieldLength, errors))
		{
			return;
		}
		final Optional<CityModel> cityModel = cityService.get(cityCode);
		if (cityModel.isEmpty() || !cityModel.get().getCountry().getIsocode().equals(countryCode))
		{
			errors.rejectValue(addressField.getFieldKey(), addressField.getErrorKey());
		}
	}

	/**
	 * @return
	 */
	private boolean isAreaRequired()
	{
		final CMSSiteModel site = cmsSiteService.getCurrentSite();
		final String siteID = site == null ? "" : cmsSiteService.getCurrentSite().getUid();
		return site != null && site.isAreaAddressEnabled() && site.isAreaAddressRequired();
	}

	private boolean isCityRequired()
	{
		final CMSSiteModel site = cmsSiteService.getCurrentSite();
		final String siteID = site == null ? "" : cmsSiteService.getCurrentSite().getUid();
		return site != null && site.isCityAddressEnabled() && site.isCityAddressRequired();
	}

	protected static boolean validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
			return true;
		}
		return false;
	}

	protected static void validateStringFieldLength(final String field, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(field) && StringUtils.length(field) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN(
						"townCity", "address.townCity.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
								"address.regionIso.invalid"), COUNTRY("countryIso", "address.country.invalid"), CITY("cityCode",
										"address.city.invalid"), AREA("areaCode", "address.area.invalid"), MOBILECOUNTRY("mobileCountry",
												"address.mobile.country.filed.error"), MOBILE("mobile",
														"address.mobile.filed.error"), ADDRESS_NAME("addressName",
																"address.addressName.invalid"), BUILDING_NAME("buildingName",
																		"address.buildingName.invalid"), STREET_NAME("streetName",
																				"address.streetName.invalid"), APARTMENT_NUMBER("apartmentNumber",
																						"address.apartmentNumber.invalid"), NEAREST_LANDMARK(
																								"nearestLandmark",
																								"address.nearestLandmark.invalid"), FLOOR_NUMBER(
																										"floorNumber",
																										"address,floorNumber.invalid"), MORE_INSTRUCTIONS(
																												"moreInstructions",
																												"address.moreInstructions.invalid"), ADDRESS_INSTRUCTIONS(
																														"addressInstructions",
																														"address.addressInstructions.invalid");


		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
