/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.form.validation;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthcore.enums.MaritalStatus;
import com.earth.earthcore.model.NationalityModel;
import com.earth.earthcore.service.CustomCustomerService;
import com.earth.earthcore.user.nationality.service.NationalityService;
import com.earth.earthcore.user.service.MobilePhoneService;
import com.earth.earthstorefront.form.UpdateProfileForm;


/**
 * Validator for profile forms.
 */
@Component("customProfileValidator")
public class ProfileValidator implements Validator
{
	public static final Pattern MOBILE_REGEX = Pattern.compile("[^a-zA-Z.]+$");

	@Resource(name = "nationalityService")
	private NationalityService nationalityService;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "mobilePhoneService")
	private MobilePhoneService mobilePhoneService;



	@Resource(name = "customerService")
	private CustomCustomerService customerService;

	/**
	 * @return the customerService
	 */
	protected CustomCustomerService getCustomerService()
	{
		return customerService;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdateProfileForm profileForm = (UpdateProfileForm) object;
		final String title = profileForm.getTitleCode();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();
		final String nationality = profileForm.getNationality();
		final String nationalityID = profileForm.getNationalityId();
		final String birthDate = profileForm.getBirthDate();

		validateUserData(title, firstName, lastName, errors);
		validateMobileAttributes(profileForm, errors);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		validateNationality(errors, currentSite, nationality);
		if (profileForm.isHasNationalId())
		{
			validateNationalityID(errors, currentSite, nationalityID);
		}
		validateBirthDate(errors, currentSite, birthDate);

		if (currentSite != null && currentSite.isCustomerMaritalStatusEnabled() && currentSite.isCustomerMaritalStatusRequired())
		{
			validateMaritalStatus(profileForm.getMaritalStatusCode(), errors);
		}
	}

	/**
	 * @param mobileCountry
	 * @param mobileNumber
	 * @param errors
	 */
	private void validateMobileAttributes(final UpdateProfileForm registerForm, final Errors errors)
	{
		if (StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
		}
		if (StringUtils.isEmpty(registerForm.getMobileNumber()) || !validateMobileNumber(registerForm.getMobileNumber()))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.invalid");
		}
		else if (!StringUtils.isEmpty(registerForm.getMobileCountry()))
		{
			final Optional<String> normalizedPhoneNumber = mobilePhoneService
					.validateAndNormalizePhoneNumberByIsoCode(registerForm.getMobileCountry(), registerForm.getMobileNumber());

			if (normalizedPhoneNumber.isPresent())
			{
				registerForm.setMobileNumber(normalizedPhoneNumber.get());

				if (cmsSiteService.getCurrentSite().isEnableMobileUnique())
				{
					validateMobileUniqueness(normalizedPhoneNumber.get(), errors);
				}

			}
			else
			{
				errors.rejectValue("mobileNumber", "register.mobileNumber.format.invalid");
				errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			}
		}

	}

	/**
	 * @param phoneNumber
	 * @param errors
	 */
	private void validateMobileUniqueness(final String phoneNumber, final Errors errors)
	{


		if (StringUtils.isEmpty(phoneNumber))
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			return;
		}
		final Optional<CustomerModel> currentCustomer = getCustomerService().getCurrentCustomer();

		if (currentCustomer.isEmpty())
		{
			errors.rejectValue("mobileCountry", "register.mobileCountry.invalid");
			return;
		}
		if (phoneNumber.equalsIgnoreCase(currentCustomer.get().getMobileNumber()))
		{
			return;
		}

		if (!mobilePhoneService.isValidMobileUniqueness(phoneNumber))
		{
			errors.rejectValue("mobileNumber", "register.mobileNumber.unique");
		}

	}

	/**
	 *
	 * @param number
	 * @return true or false
	 */
	public boolean validateMobileNumber(final String number)
	{
		final Matcher matcher = MOBILE_REGEX.matcher(number);
		return matcher.matches();
	}

	/**
	 * @param title
	 * @param firstName
	 * @param lastName
	 * @param errors
	 */
	private void validateUserData(final String title, final String firstName, final String lastName, final Errors errors)
	{
		if (StringUtils.isEmpty(title) || StringUtils.length(title) > 255)
		{
			errors.rejectValue("titleCode", "profile.title.invalid");
		}

		if (StringUtils.isBlank(firstName) || StringUtils.length(firstName) > 255)
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName) || StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}
	}

	/**
	 * @param maritalStatusCode
	 * @param errors
	 * @param currentSite
	 */
	private void validateMaritalStatus(final String maritalStatusCode, final Errors errors)
	{
		if (StringUtils.isBlank(maritalStatusCode))
		{
			errors.rejectValue("maritalStatusCode", "profile.maritalStatus.empty");
			return;
		}
		try
		{
			MaritalStatus.valueOf(maritalStatusCode);
		}
		catch (final IllegalArgumentException e)
		{
			errors.rejectValue("maritalStatusCode", "profile.maritalStatus.invalid");
		}
	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateBirthDate(final Errors errors, final CMSSiteModel currentSite, final String birthDate)
	{
		if (currentSite != null && currentSite.isBirthOfDateCustomerEnabled() && currentSite.isBirthOfDateCustomerRequired()
				&& StringUtils.isEmpty(birthDate))
		{
			errors.rejectValue("birthDate", "profile.birthofdate.invalid");
		}

	}

	/**
	 * @param errors
	 * @param currentSite
	 * @param nationality
	 */
	private void validateNationalityID(final Errors errors, final CMSSiteModel currentSite, final String nationalityId)
	{
		if (currentSite != null && currentSite.isNationalityIdCustomerEnabled() && currentSite.isNationalityIdCustomerRequired()
				&& StringUtils.isEmpty(nationalityId))
		{
			errors.rejectValue("nationalityID", "profile.nationalityid.invalid");
		}

	}

	/**
	 * @param errors
	 * @param cmsSiteModel
	 * @param nationality
	 */
	private void validateNationality(final Errors errors, final CMSSiteModel cmsSiteModel, final String nationality)
	{
		if (cmsSiteModel != null && cmsSiteModel.isNationalityCustomerEnabled() && cmsSiteModel.isNationalityCustomerRequired()
				&& StringUtils.isEmpty(nationality))
		{
			errors.rejectValue("nationality", "profile.nationality.invalid");
		}
		if (StringUtils.isNotEmpty(nationality))
		{
			final Optional<NationalityModel> national = nationalityService.get(nationality);
			if (!national.isPresent())
			{
				errors.rejectValue("nationality", "profile.nationality.invalid");
			}
		}


	}
}
