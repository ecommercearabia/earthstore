/**
 *
 */
package com.earth.earthstorefront.form.validation;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.earth.earthstorefront.form.TimeSlotForm;


/**
 * @author amjad.shati@erabia.com
 *
 */
@Component("timeSlotValidator")
public class TimeSlotValidator implements Validator
{
	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return TimeSlotForm.class.equals(clazz);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final CMSSiteModel site = getCurrentSite();

		final TimeSlotForm form = (TimeSlotForm) object;
		final String periodCode = form.getPeriodCode();
		final String start = form.getStart();
		final String end = form.getEnd();
		final String day = form.getDay();
		final String date = form.getDate();
		final String addressInstructions = form.getAddressInstructions();

		if (StringUtils.isEmpty(periodCode))
		{
			errors.rejectValue("periodCode", "timeslot.periodCode.invalid");
		}
		if (StringUtils.isEmpty(start))
		{
			errors.rejectValue("start", "timeslot.start.invalid");
		}
		if (StringUtils.isEmpty(end))
		{
			errors.rejectValue("end", "timeslot.end.invalid");
		}
		if (StringUtils.isEmpty(day))
		{
			errors.rejectValue("day", "timeslot.day.invalid");
		}
		if (StringUtils.isEmpty(date))
		{
			errors.rejectValue("date", "timeslot.date.invalid");
		}
		if (StringUtils.length(form.getNote()) > 500)
		{
			errors.rejectValue("note", "timeslot.note.invalid");
		}
		if (site.isAddressInstructionsAddressEnabled() && site.isAddressInstructionsAddressRequired()
				&& StringUtils.isEmpty(addressInstructions))
		{
			errors.rejectValue("addressInstructions", "timeslot.addressInstructions.invalid");
		}


	}

	public CMSSiteModel getCurrentSite()
	{
		return getCMSSiteService().getCurrentSite();
	}

	protected CMSSiteService getCMSSiteService()
	{
		return cmsSiteService;
	}
}
