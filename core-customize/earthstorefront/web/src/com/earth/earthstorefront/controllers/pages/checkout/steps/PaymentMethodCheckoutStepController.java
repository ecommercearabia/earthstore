/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstorefront.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.NoCardPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.NoCardTypeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PaymentModeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.earth.earthfacades.user.facade.CustomUserFacade;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyBalanceData;
import com.earth.earthloyaltyprogramfacades.data.LoyaltyPaymentModeData;
import com.earth.earthloyaltyprogramprovider.enums.LoyaltyPaymentModeType;
import com.earth.earthloyaltyprogramprovider.exception.LoyaltyPaymentException;
import com.earth.earthstorecredit.enums.StoreCreditModeType;
import com.earth.earthstorecredit.exception.StoreCreditException;
import com.earth.earthstorecreditfacades.data.StoreCreditModeData;
import com.earth.earthstorefront.checkout.steps.CheckoutStep;
import com.earth.earthstorefront.controllers.ControllerConstants;
import com.earth.earthstorefront.form.LoyaltyPointsForm;
import com.earth.earthstorefront.form.PaymentDetailsForm;
import com.earth.earthstorefront.form.StoreCreditForm;
import com.earth.earthstorefront.util.AddressDataUtil;




@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final Map<String, String> CYBERSOURCE_SOP_CARD_TYPES = new HashMap<>();
	private static final String PAYMENT_METHOD = "payment-method";
	private static final String CART_DATA_ATTR = "cartData";
	private static final String SUPPORTED_PAYMENT_MODES_ATTR = "supportedPaymentModes";

	private static final Logger LOGGER = Logger.getLogger(PaymentMethodCheckoutStepController.class);

	@Resource(name = "customAddressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "storeCreditValidator")
	private Validator storeCreditValidator;

	@Resource(name = "loyaltyPointsValidator")
	private Validator loyaltyPointsValidator;

	@Resource(name = "userFacade")
	private CustomUserFacade customUserFacade;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	protected CustomUserFacade getCustomUserFacade()
	{
		return customUserFacade;
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();

		model.addAttribute("availableBalanceStoreCredit",
				availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);

		final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();

		model.addAttribute("storeCreditAmountFullRedeem",
				storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);


		//		final Optional<LoyaltyUsablePointData> loyaltyUsablePoint = getCheckoutFacade().getLoyaltyUsablePoints();
		//		model.addAttribute("loyaltyUsablePoint", loyaltyUsablePoint.isPresent() ? loyaltyUsablePoint.get() : null);

		final Optional<LoyaltyBalanceData> loyaltyBalance = getCheckoutFacade().getLoyaltyBalance();
		model.addAttribute("loyaltyBalance", loyaltyBalance.isPresent() ? loyaltyBalance.get() : null);


		model.addAttribute("loyaltyEnabled", getCheckoutFacade().isLoyaltyEnabled());

		getCheckoutFacade().setDeliveryModeIfAvailable();
		setupAddPaymentPage(model);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		setupSilentOrderPostPage(model);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);


		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}


	@RequestMapping(value = "/store-credit/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectStoreCredit(final StoreCreditForm storeCreditForm, final Model model,
			final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		storeCreditValidator.validate(storeCreditForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			final Optional<PriceData> availableBalanceStoreCredit = getCheckoutFacade().getAvailableBalanceStoreCreditAmount();

			model.addAttribute("availableBalanceStoreCredit",
					availableBalanceStoreCredit.isPresent() ? availableBalanceStoreCredit.get() : null);

			final Optional<PriceData> storeCreditAmountFullRedeem = getCheckoutFacade().getStoreCreditAmountFullRedeem();

			model.addAttribute("storeCreditAmountFullRedeem",
					storeCreditAmountFullRedeem.isPresent() ? storeCreditAmountFullRedeem.get() : null);
			GlobalMessages.addErrorMessage(model, "checkout.error.storecredit.entry.invalid");
			setupAddPaymentPage(model);
			setupSilentOrderPostPage(model);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		if (StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(storeCreditForm.getSctCode()))
		{
			final double scAmountValue = Double.parseDouble(storeCreditForm.getScAmount());

			getCheckoutFacade().setStoreCreditMode(StoreCreditModeType.REDEEM_SPECIFIC_AMOUNT.getCode(),
					Double.valueOf(scAmountValue));
		}
		else
		{
			getCheckoutFacade().setStoreCreditMode(storeCreditForm.getSctCode(), null);
		}

		setupAddPaymentPage(model);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().currentStep();
	}

	@RequestMapping(value = "/loyalty-points/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectLoyaltyPoints(final LoyaltyPointsForm loyaltyPointsForm, final Model model,
			final RedirectAttributes redirectAttributes, final BindingResult bindingResult)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		loyaltyPointsValidator.validate(loyaltyPointsForm, bindingResult);

		if (bindingResult.hasErrors())
		{

			GlobalMessages.addErrorMessage(model, "checkout.error.loyaltypoints.entry.invalid");
			setupAddPaymentPage(model);
			setupSilentOrderPostPage(model);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		if (LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.getCode().equals(loyaltyPointsForm.getLoyaltyPaymentCode()))

		{
			final double scAmountValue = Double.parseDouble(loyaltyPointsForm.getLoyaltyPaymentAmount());

			getCheckoutFacade().setLoyaltyPaymentMode(LoyaltyPaymentModeType.REDEEM_SPECIFIC_AMOUNT.getCode(), scAmountValue);
		}
		else
		{
			getCheckoutFacade().setLoyaltyPaymentMode(loyaltyPointsForm.getLoyaltyPaymentCode(), 0d);
		}

		setupAddPaymentPage(model);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().currentStep();
	}

	@ModelAttribute("supportedStoreCreditModes")
	public Collection<StoreCreditModeData> getSupportedStoreCreditModes()
	{
		try
		{
			final Optional<List<StoreCreditModeData>> supportedStoreCreditModes = getCheckoutFacade().getSupportedStoreCreditModes();
			return supportedStoreCreditModes.isPresent() ? supportedStoreCreditModes.get() : Collections.emptyList();
		}
		catch (final StoreCreditException e)
		{
			return Collections.emptyList();
		}
	}

	@ModelAttribute("supportedLoyaltyModes")
	public Collection<LoyaltyPaymentModeData> getSupportedLoyaltyModes()
	{

		try
		{
			return getCheckoutFacade().getLoyaltyPaymentModes();
		}
		catch (final LoyaltyPaymentException e)
		{
			LOGGER.error(e.getMessage());
			return Collections.emptyList();
		}

	}

	protected void setupSilentOrderPostPage(final Model model)
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		final Optional<List<PaymentModeData>> supportedPaymentModes = getCheckoutFacade().getSupportedPaymentModes();

		if (supportedPaymentModes.isPresent())
		{
			model.addAttribute(SUPPORTED_PAYMENT_MODES_ATTR, supportedPaymentModes.get());
		}

		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();


		if (cartData.getPaymentMode() == null)
		{
			final Optional<PaymentModeData> defaultPaymentMode = getCheckoutFacade()
					.getDefaultPaymentModesForStore(baseStoreService.getCurrentBaseStore());

			if (defaultPaymentMode.isPresent() && supportedPaymentModes.isPresent()
					&& !CollectionUtils.isEmpty(supportedPaymentModes.get())
					&& supportedPaymentModes.get().stream().anyMatch(e -> e.getCode().equals(defaultPaymentMode.get().getCode())))
			{
				paymentDetailsForm.setPaymentModeCode(defaultPaymentMode.get().getCode());
			}
		}
		else
		{
			paymentDetailsForm.setPaymentModeCode(cartData.getPaymentMode().getCode());
		}
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);
	}

	protected boolean checkPaymentSubscription(final Model model, final PaymentDetailsForm paymentDetailsForm,
			final NoCardPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getId()))
		{
			final Optional<List<NoCardPaymentInfoData>> noCardPaymentInfos = getCustomUserFacade().getNoCardPaymentInfos(true);
			if (paymentDetailsForm.getSaveInAccount() && noCardPaymentInfos.isPresent() && noCardPaymentInfos.get().size() <= 1)
			{
				getCustomUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setGeneralPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return false;
		}
		return true;
	}

	@RequestMapping(value =
	{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid
	final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		if (bindingResult.hasErrors())
		{
			setupSilentOrderPostPage(model);
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		AddressData addressData = cartData.getDeliveryAddress();

		if (addressData != null)
		{
			addressData.setShippingAddress(Boolean.TRUE);
			addressData.setBillingAddress(Boolean.TRUE);
		}

		getAddressVerificationFacade().verifyAddressData(addressData);

		switch (paymentDetailsForm.getPaymentModeCode().toLowerCase())
		{
			case "pis": //NOSONAR
				addressData = getBillingAddressForPickup(cartData);
				if (addressData != null)
				{
					addressData.setTitleCode("mr");
				}
			case "cod":
			case "ccod":
			case "continue":
				final NoCardPaymentInfoData noCardPaymentInfoData = new NoCardPaymentInfoData();
				final NoCardTypeData noCardTypeData = new NoCardTypeData();
				noCardTypeData.setCode(paymentDetailsForm.getPaymentModeCode().toUpperCase());
				noCardPaymentInfoData.setNoCardTypeData(noCardTypeData);
				noCardPaymentInfoData.setDefaultPaymentInfo(true);
				noCardPaymentInfoData.setBillingAddress(addressData);
				noCardPaymentInfoData.setSaved(true);

				final Optional<NoCardPaymentInfoData> createPaymentSubscription = getCheckoutFacade()
						.createPaymentSubscription(noCardPaymentInfoData);

				if (!checkPaymentSubscription(model, paymentDetailsForm, createPaymentSubscription.orElse(null)))
				{
					setupSilentOrderPostPage(model);
					return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
				}

				if (addressData != null)
				{
					getCheckoutFacade().saveBillingAddress(addressData);
				}
				model.addAttribute("paymentId", noCardPaymentInfoData.getId());

				break;

			case "card":

				getCheckoutFacade().saveBillingAddress(addressData);

				//				final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
				//				fillInPaymentData(paymentDetailsForm, ccPaymentInfoData);
				//				ccPaymentInfoData.setBillingAddress(addressData);
				//
				//				final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(ccPaymentInfoData);
				//				if (!checkPaymentSubscription(model, paymentDetailsForm, newPaymentSubscription))
				//				{
				//					return ControllerConstants..MultiStepCheckout.AddPaymentMethodPage;
				//				}
				//				model.addAttribute("paymentId", ccPaymentInfoData.getId());

				//				cartService.getSessionCart();


				break;

			default:
				break;
		}
		getCheckoutFacade().setPaymentMode(paymentDetailsForm.getPaymentModeCode());
		return getCheckoutStep().nextStep();
	}

	/**
	 * @param cartData
	 * @return
	 */
	private AddressData getBillingAddressForPickup(final CartData cartData)
	{
		if (!CollectionUtils.isEmpty(cartData.getEntries()) && isSinglePOS(cartData.getEntries()))
		{
			final Optional<OrderEntryData> findFirst = cartData.getEntries().stream()
					.filter(e -> e.getDeliveryPointOfService() != null).filter(e -> e.getDeliveryPointOfService().getAddress() != null)
					.findFirst();
			return findFirst.isPresent() ? findFirst.get().getDeliveryPointOfService().getAddress() : getDefaultDeliveryOfOrigin();
		}
		return getDefaultDeliveryOfOrigin();
	}

	private AddressData getDefaultDeliveryOfOrigin()
	{
		final PointOfServiceModel defaultDeliveryOrigin = baseStoreService.getCurrentBaseStore().getDefaultDeliveryOrigin();
		return defaultDeliveryOrigin != null && defaultDeliveryOrigin.getAddress() != null
				? addressConverter.convert(defaultDeliveryOrigin.getAddress())
				: null;
	}

	private boolean isSinglePOS(final List<OrderEntryData> entries)
	{
		final List<String> pointsOfServiceNames = entries.stream().filter(e -> e.getDeliveryPointOfService() != null)
				.map(e -> e.getDeliveryPointOfService().getName()).collect(Collectors.toList());
		return verifyAllEqualUsingStream(pointsOfServiceNames);
	}

	private boolean verifyAllEqualUsingStream(final List<String> values)
	{
		return values.stream().distinct().count() <= 1;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}



	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}


}
