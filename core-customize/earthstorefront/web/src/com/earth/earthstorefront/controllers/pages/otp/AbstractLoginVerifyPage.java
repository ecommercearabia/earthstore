/**
 *
 */
package com.earth.earthstorefront.controllers.pages.otp;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.user.data.OTPData;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

import com.earth.earthotp.model.OTPVerificationTokenModel;
import com.earth.earthcore.enums.LoginIdentifier;
import com.earth.earthstorefront.form.OTPForm;
import com.earth.earthstorefront.form.validation.OTPLoginValidator;
import com.earth.earthstorefront.form.validation.OTPValidator;


/**
 * @author mnasro
 *
 */
public abstract class AbstractLoginVerifyPage extends AbstractOTPVerifyPage
{
	@Resource(name = "otpLoginValidator")
	private OTPLoginValidator otpLoginValidator;

	@Override
	public boolean isEmailCheck()
	{
		final CMSSiteModel currentBaseSite = getCmsSiteService().getCurrentSite();

		return currentBaseSite != null && LoginIdentifier.EMAIL.equals(currentBaseSite.getOtpLoginIdentifier());
	}

	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpLoginValidator;
	}

	public String generatOTPLoginToken(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		if (!validateOTPEnabilityAndType() || getCmsSiteService().getCurrentSite() == null
				|| !getCmsSiteService().getCurrentSite().isActiveLoginWithOTP()
				|| getCmsSiteService().getCurrentSite().getDefaultMobileCountry() == null)
		{
			return getNotFoundPage(model, response);
		}

		final OTPData otpData = new OTPData();
		otpData.setMobileCountry(getCmsSiteService().getCurrentSite().getDefaultMobileCountry().getIsocode());
		otpData.setMobileNumber("");
		otpData.setUid("");

		final Optional<OTPVerificationTokenModel> otpVerificationTokenModel = getOtpContext()
				.generateTokenCurrentCustomer(getOTPVerificationTokenType(), otpData, otpData.getMobileCountry());

		if (otpVerificationTokenModel.isEmpty())
		{
			return getNotFoundPage(model, response);
		}

		final String token = getEncodeToken(otpVerificationTokenModel.get().getToken());

		return REDIRECT_PREFIX + getChangeNumberActionURL() + "?token=" + token;

	}

	@Override
	protected Object updateTokenData(final Object data, final OTPForm otpForm)
	{
		if (data == null || !(data instanceof OTPData) || otpForm == null)
		{
			return data;
		}
		final OTPData otpData = (OTPData) data;
		otpData.setMobileCountry(otpForm.getMobileCountry());
		otpData.setMobileNumber(otpForm.getMobileNumber());
		otpData.setUid(otpForm.getEmail());
		return data;
	}

	@Override
	protected OTPForm updateOTPForm(final OTPForm otpForm, final OTPVerificationTokenModel otpVerificationToken)
	{
		if (otpForm == null || otpVerificationToken == null || otpVerificationToken.getData() == null
				|| !(otpVerificationToken.getData() instanceof OTPData))
		{
			return otpForm;
		}

		final OTPData otpData = (OTPData) otpVerificationToken.getData();
		otpForm.setEmail(otpData.getUid());

		return otpForm;
	}
}
