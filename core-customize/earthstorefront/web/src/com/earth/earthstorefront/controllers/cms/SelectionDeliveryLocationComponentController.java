package com.earth.earthstorefront.controllers.cms;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.earth.earthcomponents.model.SelectionDeliveryLocationComponentModel;
import com.earth.earthcore.model.AreaModel;
import com.earth.earthcore.model.CityModel;
import com.earth.earthfacades.customer.DeliveryCityAreaData;
import com.earth.earthfacades.customer.TimeDeliveryLocationData;
import com.earth.earthfacades.user.city.facade.CityFacade;
import com.earth.earthfacades.user.delivery.DeliveryLocationService;
import com.earth.earthstorefront.controllers.ControllerConstants;
import com.earth.earthtimeslotfacades.exception.TimeSlotException;


/**
 * The Class ResponsiveRotatingImagesComponentController.
 *
 * @author mohammad-abumuhasien
 */
@Controller("SelectionDeliveryLocationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.SelectionDeliveryLocationComponent)
public class SelectionDeliveryLocationComponentController
		extends AbstractAcceleratorCMSComponentController<SelectionDeliveryLocationComponentModel>
{

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "deliveryLocationService")
	private DeliveryLocationService deliveryLocationService;

	@Resource(name = "cityFacade")
	private CityFacade cityFacade;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final SelectionDeliveryLocationComponentModel component)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();

		final AreaModel defaultSelectedDeliveryLocationArea = currentSite.getDefaultDeliveryLocationArea();
		final CityModel defaultSelectedDeliveryLocationCity = currentSite.getDefaultDeliveryLocationCity();
		final CountryModel defaultCountry = currentSite.getDefaultCountryAddress();


		final UserModel user = userService.getCurrentUser();

		final Optional<DeliveryCityAreaData> selectedDeliveryCityAndArea = deliveryLocationService.getSelectedDeliveryCityAndArea();


		if (selectedDeliveryCityAndArea.isPresent() && selectedDeliveryCityAndArea.get().getSelectedCityCode() != null
				&& selectedDeliveryCityAndArea.get().getSelectedAreaCode() != null)
		{

			final Optional<TimeDeliveryLocationData> timeDeliveryLocationData = getSelectedDeliveryLocation(
					selectedDeliveryCityAndArea.get().getSelectedAreaCode(), selectedDeliveryCityAndArea.get().getSelectedCityCode());
			model.addAttribute("selectedCityCode", selectedDeliveryCityAndArea.get().getSelectedCityCode());
			model.addAttribute("selectedAreaCode", selectedDeliveryCityAndArea.get().getSelectedAreaCode());

			model.addAttribute("timeDeliveryLocationData",
					timeDeliveryLocationData.isPresent() ? timeDeliveryLocationData.get() : null);
		}


		model.addAttribute("content", component.getContent());
		model.addAttribute("defaultArea", defaultSelectedDeliveryLocationArea);
		model.addAttribute("defaultCity", defaultSelectedDeliveryLocationCity);
		model.addAttribute("defaultCountry", defaultCountry);
		model.addAttribute("googlemapreference", defaultSelectedDeliveryLocationArea.getGoogleMapReference());

	}

	private Optional<TimeDeliveryLocationData> getSelectedDeliveryLocation(final String areaCode, final String cityCode)
	{
		final Optional<TimeDeliveryLocationData> customerDeliveryLocationData;
		try
		{
			return deliveryLocationService.getTimeDeliveryLocationData(areaCode, cityCode);
		}
		catch (final TimeSlotException e)
		{
			return Optional.empty();

		}
	}

	/**
	 * @return the cmsSiteService
	 */
	public CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}



}

