/**
 *
 */
package com.earth.earthstorefront.controllers.pages.otp;

import de.hybris.platform.commercefacades.user.data.RegisterData;

import javax.annotation.Resource;

import com.earth.earthotp.model.OTPVerificationTokenModel;
import com.earth.earthstorefront.form.OTPForm;
import com.earth.earthstorefront.form.validation.OTPRegisterValidator;
import com.earth.earthstorefront.form.validation.OTPValidator;


/**
 * @author mnasro
 *
 */
public abstract class AbstractRegisterVerifyPage extends AbstractOTPVerifyPage
{


	/** The otp validator. */
	@Resource(name = "otpRegisterValidator")
	private OTPRegisterValidator otpRegisterValidator;


	/**
	 * @return the otpValidator
	 */
	@Override
	public OTPValidator getOTPValidator()
	{
		return otpRegisterValidator;
	}


	@Override
	protected String getPageLabelOrId()
	{
		return "/verify";
	}

	@Override
	public boolean isEmailCheck()
	{
		return false;
	}

	@Override
	protected Object updateTokenData(final Object data, final OTPForm otpForm)
	{
		return data;
	}

	@Override
	protected OTPForm updateOTPForm(final OTPForm otpForm, final OTPVerificationTokenModel otpVerificationToken)
	{
		if (otpForm == null || otpVerificationToken == null || otpVerificationToken.getData() == null
				|| !(otpVerificationToken.getData() instanceof RegisterData))
		{
			return otpForm;
		}

		final RegisterData otpData = (RegisterData) otpVerificationToken.getData();
		otpForm.setEmail(otpData.getLogin());
		otpForm.setMobileCountry(otpData.getMobileCountry());
		otpForm.setMobileNumber(otpData.getMobileNumber());
		return otpForm;
	}

}
