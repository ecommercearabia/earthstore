/**
 *
 */
package com.earth.earthstorefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.earth.earthotp.context.OTPContext;
import com.earth.earthotp.exception.OTPException;
import com.earth.earthstorefront.controllers.ControllerConstants;
import com.earth.earthstorefront.form.OTPLoginForm;
import com.earth.earthstorefront.form.validation.OTPLoginValidator;
import com.earth.earthstorefront.security.OTPAutoLoginStrategy;


/**
 * @author monzer
 *
 */
@Controller
@RequestMapping("/otp-login")
public class OTPLoginController extends AbstractLoginPageController
{
	private HttpSessionRequestCache httpSessionRequestCache;

	private static final Logger LOG = Logger.getLogger(OTPLoginController.class);

	private static final String PHONENUMBER_VERIFICATION_PAGE_LABEL = "/otp-login";

	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	private static final String OTP_CONFIG_ERROR_MESSAGE = "otp.config.error.";

	private static final String OTP_PINCODE_ERROR_MESSAGE = "otp.pinCode.format.invalid";

	private static final String TOO_MANY_REQUESTS_MESSAGE = "otp.too.many.requests.message";

	private static final String INVALID_CONFIG_MESSAGE = "otp.invalid.config.message";

	private static final String MESSAGE_NOT_SENT_MESSAGE = "otp.message.not.sent.message";

	private static final String RESEND_PINCODE_MESSAGE = "otp.resend.message";


	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "otpContext")
	private OTPContext otpContext;

	@Resource(name = "customCustomerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "otpLoginValidator")
	private OTPLoginValidator otpLoginValidator;

	@Resource(name = "otpAutoLoginStrategy")
	private OTPAutoLoginStrategy autoLoginStrategy;

	@RequestMapping(method = RequestMethod.POST, value = "/change-number")
	public String changeNumber(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		sessionService.removeAttribute("isSend");
		sessionService.removeAttribute("otpForm");
		return REDIRECT_PREFIX + PHONENUMBER_VERIFICATION_PAGE_LABEL;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/resend")
	public String resend(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final Object otpSessionData = sessionService.getCurrentSession().getAttribute("otpForm");

		if (!otpContext.isEnabledByCurrentSite() || otpSessionData == null || !(otpSessionData instanceof OTPLoginForm))
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}
		final OTPLoginForm data = (OTPLoginForm) otpSessionData;

		try
		{
			otpContext.sendOTPCodeByCurrentSite(data.getMobileCountry(), data.getMobileNumber());
			model.addAttribute("mobileNumber", data.getMobileNumber());
			model.addAttribute("otpForm", new OTPLoginForm());
			sessionService.getCurrentSession().setAttribute("isSend", Boolean.TRUE);
			model.addAttribute("isSend", Boolean.TRUE);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, RESEND_PINCODE_MESSAGE);
			return getView(model, PHONENUMBER_VERIFICATION_PAGE_LABEL);
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, INVALID_CONFIG_MESSAGE);
					break;
			}
			return REDIRECT_PREFIX + "/login";
		}

	}



	@RequestMapping(method = RequestMethod.POST, value = "/send")
	public String send(final OTPLoginForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{

		if (!otpContext.isEnabledByCurrentSite())
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}

		//		otpLoginValidator.validateEmail(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("noEmail", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "otp.login.email.invalid");
			return handleError(model);
		}


		CustomerData customer = null;
		try
		{
			customer = customerFacade.getUserForUID(otpForm.getEmail());
		}
		catch (final Exception e)
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("noEmail", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "otp.no.customer.found");
			return handleError(model);
		}
		if (customer == null)
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("noEmail", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "otp.no.customer.found");
			return handleError(model);
		}
		if (customer.getMobileCountry() == null || StringUtils.isBlank(customer.getMobileCountry().getIsocode())
				|| StringUtils.isBlank(customer.getMobileNumber()))
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("isSend", Boolean.FALSE);
			model.addAttribute("noEmail", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "otp.login.customer.noMobileNumber");
			return getView(model, PHONENUMBER_VERIFICATION_PAGE_LABEL);
		}
		otpForm.setMobileNumber(customer.getMobileNumber());
		otpForm.setMobileCountry(customer.getMobileCountry().getIsocode());
		//		otpLoginValidator.validatePhoneNumber(otpForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("isSend", Boolean.FALSE);
			GlobalMessages.addErrorMessage(model, "otp.login.customer.noMobileNumber.invalid");
			return handleError(model);
		}

		try
		{
			otpContext.sendOTPCodeByCurrentSite(otpForm.getMobileCountry(), otpForm.getMobileNumber());
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("mobileNumber", otpForm.getMobileNumber());
			sessionService.getCurrentSession().setAttribute("isSend", Boolean.TRUE);
			sessionService.getCurrentSession().setAttribute("otpForm", otpForm);
			model.addAttribute("isSend", Boolean.TRUE);
		}
		catch (final OTPException ex)
		{
			switch (ex.geType())
			{
				case DISABLED:
				case SERVICE_UNAVAILABLE:
				case CMS_SITE_NOT_FOUND:
				case OTP_CONFIG_UNAVAILABLE:
				case OTP_TYPE_NOTE_FOUND:
					GlobalMessages.addErrorMessage(model, INVALID_CONFIG_MESSAGE);
					break;
			}
			model.addAttribute("otpForm", otpForm);
			return handleError(model);
		}
		return getView(model, PHONENUMBER_VERIFICATION_PAGE_LABEL);

	}

	@RequestMapping(method = RequestMethod.POST)
	public String verify(final OTPLoginForm otpForm, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final Object otpSessionData = sessionService.getCurrentSession().getAttribute("otpForm");
		if (otpSessionData != null)
		{
			otpForm.setEmail(((OTPLoginForm) otpSessionData).getEmail());
			otpForm.setMobileCountry(((OTPLoginForm) otpSessionData).getMobileCountry());
			otpForm.setMobileNumber(((OTPLoginForm) otpSessionData).getMobileNumber());
		}

		otpLoginValidator.validate(otpForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("isSend", sessionService.getAttribute("isSend"));
			GlobalMessages.addErrorMessage(model, "otp.otpCode.format.invalid");
			return handleError(model);
		}

		boolean verified = false;
		try
		{
			verified = otpContext.verifyCodeByCurrentSite(otpForm.getMobileCountry(), otpForm.getMobileNumber(),
					otpForm.getOtpCode());
		}
		catch (final OTPException e)
		{
			model.addAttribute("otpForm", otpForm);
			model.addAttribute("mobileNumber", otpForm.getMobileNumber());
			model.addAttribute("isSend", Boolean.TRUE);
			GlobalMessages.addErrorMessage(model, "otp.otpCode.format.invalid");
			return handleError(model);
		}
		if (verified)
		{
			autoLoginStrategy.login(otpForm, request, response);
			sessionService.removeAttribute("isSend");
			sessionService.removeAttribute("otpForm");
		}
		else
		{
			model.addAttribute("otpForm", otpForm);
			GlobalMessages.addErrorMessage(model, "otp.otpCode.format.invalid");
			return handleError(model);
		}
		return REDIRECT_PREFIX + "/";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showVerificationPage(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{

		if (!otpContext.isEnabledByCurrentSite())
		{
			prepareNotFoundPage(model, response);
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}



		final OTPLoginForm otpForm = new OTPLoginForm();
		final Object otpSessionData = sessionService.getCurrentSession().getAttribute("otpForm");
		if (otpSessionData != null)
		{
			otpForm.setEmail(((OTPLoginForm) otpSessionData).getEmail());
			otpForm.setMobileCountry(((OTPLoginForm) otpSessionData).getMobileCountry());
			otpForm.setMobileNumber(((OTPLoginForm) otpSessionData).getMobileNumber());
			model.addAttribute("mobileNumber", ((OTPLoginForm) otpSessionData).getMobileNumber());
		}
		model.addAttribute("otpForm", otpForm);
		model.addAttribute("isSend", sessionService.getAttribute("isSend"));
		model.addAttribute("noEmail", Boolean.TRUE);



		return getView(model, PHONENUMBER_VERIFICATION_PAGE_LABEL);
	}

	/**
	 * @param model
	 * @param pageControllerConstants
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	private String getView(final Model model, final String pageControllerConstants) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(pageControllerConstants));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(pageControllerConstants));
		return getViewForPage(model);
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(PHONENUMBER_VERIFICATION_PAGE_LABEL);
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.LoginVerifyPhoneNumber;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		sessionService.setAttribute("showThankYouPage", Boolean.TRUE);
		return PHONENUMBER_VERIFICATION_PAGE_LABEL + "/thank-you";
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}


	protected String handleError(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(PHONENUMBER_VERIFICATION_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PHONENUMBER_VERIFICATION_PAGE_LABEL));

		return getViewForPage(model);
	}
}
