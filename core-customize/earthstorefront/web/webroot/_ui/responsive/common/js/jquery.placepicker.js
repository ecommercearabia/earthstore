(function ($, window) {
    var init_update_map = true;
    var pluginName = "placepicker";
    var defaults = {
        map: "",
        mapIconClass: "far fa-location",
        mapOptions: {
            zoom: 17,
        },
        places: {
            icons: false
        },
        autoCompleteOptions: {},
        // callbacks
        placeChanged: null,
        location: null,
        preventSubmit: true
    };

    function PlacePicker(element, options) {

        var instance = this;

        var geocoder = null;

        var mapElement, map, marker;

        var service = null;

        var autocomplete;

        // stores the current place
        var _place = null;
        var _latLng = null;

        /**
         * Generates a piece of HTML that is used to replace the input element
         * with one that also contains a globe button.
         */
        function template() {
            var templateString =
                "<div class=\"input-group hidden\">" +
                "<span class=\"input-group-btn\">" +
                "<button type=\"button\" data-toggle=\"collapse\" href=\"#" + options.mapContainerId + "\" class=\"btn btn-default\">" +
                "<span class=\"" + options.mapIconClass + "\"></span>" +
                "</button>" +
                "</span>" +
                "</div>";

            return templateString;
        }

        /**
         * Modifies the DOM to add a globe button that hides and shows a map
         * element.
         */
        function initDomElements() {
            if (!options.mapContainerId) {
                return;
            }

            // Find the index of our element under its parent
            var $element = $(element);
            var $parent = $element.parent();
            var index = $parent.children().index(element);

            // Replace the element with our template code
            $element.replaceWith(template());

            // Then inject back in the existing element. This retains all
            // attributes on the element.
            $parent.children().eq(index).after(element);
        }

        function codePlace(query) {

            if (!query) {
                return;
            }

            var request = {
                query: query
            };

            if (service) {
                service.textSearch(request, function (results, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        for (var i = 0; i < results.length; i++) {
                            setPlace(results[i]);
                            return;
                        }
                    }
                });
            }
        }

        function codeLatLng(latlng) {
            geocoder.geocode({"latLng": latlng}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    console.log(results)
                    if (results) {
                        var place = results[0];
                        setPlace(place, false);

                       var latval = $("#lat").val()
                       var longval =  $("#long").val()
                     
                        $.ajax({
                            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latval+','+longval+ "&language=en-GB&sensor=true&key=AIzaSyBR8aS0pCX8pjNvIlCsBxmwn03Mx4yu7Nc",
                        
                            success: function (data) {
                           var res =  data.results;
                               
                                    var ShouldBeComponent = {
                                      home: ["street_number"],
                                      postal_code: ["postal_code"],
                                      street: ["street_address", "route"],
                                      area: [
                        				"neighborhood",
                                        "administrative_area_level_2",
                                        "administrative_area_level_3",
                                        "administrative_area_level_4",
                                        "administrative_area_level_5",
                                        "locality",
                                        "political",
                                        "sublocality",
                                        "sublocality_level_1",
                                        "sublocality_level_2",
                                        "sublocality_level_3",
                                        "sublocality_level_4"
                                      ],
                                      city: [
                                        "locality",
                                        "political",
                                        "sublocality",
                                        "sublocality_level_1",
                                        "sublocality_level_2",
                                        "sublocality_level_3",
                                        "sublocality_level_4"
                                      ],
                                      country: ["country"]
                                    };
                              
                                    var address = {
                                      home: [],
                                      postal_code: [],
                                      street: [],
                                      area: [],
                                      city: [],
                                      country: []
                                    };
                         
                                    for (var i = 0; i < res.length; i++) {
                                        res[i].address_components.forEach(component => {
                                            for (var shouldBe in ShouldBeComponent) {
                                              if (ShouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1) {
                                                if (shouldBe === "country") {
                                                  address[shouldBe].push(component.short_name.replace(/\s+/g, '_').toUpperCase());
                                                } else {
                                                  address[shouldBe].push(component.long_name.replace(/\s+/g, '_').toUpperCase());
                                                }
                                              }
                                            }
                                          });
                                    }
                                   console.log(address)
                                
                                    if($.inArray('ABU_DHABI', address.area) > -1) {
//	alert (1)
                                        address.area.push("ABU_DHABI") ;
	
                                       
                                      } 


                                     else  if($.inArray('ABUDHABI', address.area) > -1) {
                                        address.area.push("ABU_DHABI") ;  
                                       
                                      } 
  								 	 else  if($.inArray('AL_AIN', address.area) > -1) {
											//	alert(2)
                                        address.area.push("Al Ain") ;  
                                       address.area.push("Al_Ain") ;  
										address.area.push("AL_AIN") ;  
                                      } 



    								 else  if($.inArray('ALAIN', address.area) > -1) {
									//	alert(5)
                                         address.area.push("Al Ain") ;  
                                       address.area.push("Al_Ain") ;  
address.area.push("AL_AIN") ;  

                                       
                                      } 
	 									

                                    let myArr = address.area;
                                    $("#areaId").val('');
                                    $("#cityId").val('');
                                    $("#cityId option:selected").removeAttr("selected");
                                   
                                    $("#areaId option:selected").removeAttr("selected");
                                    $('select').selectpicker('refresh');
                                 //   alert('1')
                                    $(function() {
                                        $("#cityId option").each(function(i){
                                            
                                            let addressdata = $(this).val().toUpperCase();
										   //let addressMap= $(this).data("map").toUpperCase();
                                            if($.inArray(addressdata, myArr) > -1) {
                                                $(this).prop('selected', true);
console.log('truee')
                                            $('#cityId').selectpicker('refresh').change();
 var cityCode = $("#cityId").val();
						
						$.ajax({
							type : "GET",
							url : ACC.config.encodedContextPath + "/misc/city/" + cityCode + "/areas",
							dataType : "json",
							success : function(response) {
							
			                    var emptyListLBL = $('#areaId option:first').html();
			                    var options = '';
			                    $('#areaId').html('');
		                        var emptyOption = '<option disabled="disabled" selected="selected" class="__web-inspector-hide-shortcut__">' + emptyListLBL + '</option>';
	                       $('#areaId').append(emptyOption);
		                        for (i = 0; i < response.data.length; i++) {
		                            var code = response.data[i].code;
		                            var name = response.data[i].name;
									var mapname = response.data[i].googleMapReference;
									var  mmapcodde = response.data[i].googleMapReference;
									if($.inArray(mmapcodde, myArr) > -1) {
                                                 options += '<option value="' + code + '" data-map="'+ mapname +'"  selected>' + name + '</option>';
                                                   
                                                    $("#areaId").val(code)

                                                $(".notaccpted").addClass('hidden');
                                                  } else {
	options += '<option value="' + code + '" data-map="'+ mapname +'">' + name + '</option>';
                                                    
                                                  }
									
		                            
		                        }
		                        $('#areaId').append(options);
		                        $('#areaId').selectpicker('refresh');


                                            if ($("#areaId").val() == ''){
                                                $(".placepicker").val('');
                                                $(".notaccpted").removeClass('hidden');
                                            }




							}
							
					});
                             
                                            if ($("#areaId").val() == ''){
                                                $(".placepicker").val('');
                                                $(".notaccpted").removeClass('hidden');
                                            }               
                                           
                                        
                                           
                                              } else {
	
                                            if ($("#areaId").val() == ''){
                                                $(".placepicker").val('');
                                                $(".notaccpted").removeClass('hidden');
                                            }
                                                   
                                              }
            
                                        });
                                        
                                    });

          
                                    
            
                              
                                 
               
                  
                  
                            }
                          });



                    } else {
                        // alert("No results found");
                    }
                } else {
                    // alert("Geocoder failed due to: " + status);
                }
            });
        }

        /**
         * Find and store the DOM element that holds the Google Map.
         * @return a boolean indicating whether an element was successfully
         * set.
         */
        function setMapElement() {

            mapElement = $(options.map).get(0);

            if (!mapElement) {
                if (options.mapContainerId) {

                    mapElement = $("#" + options.mapContainerId + " .placepicker-map").get(0);
                    console.log(mapElement)
                }
            }

            return mapElement ? true : false;
        }

        function initMap() {

            if (!setMapElement()) {
                return;
            }

            map = new google.maps.Map(mapElement, options.mapOptions);

            autocomplete.bindTo("bounds", map);
            marker = new google.maps.Marker({
                map: map, draggable: true
            });
            google.maps.event.addListener(map, "dragend", function (e) {

                // var pos = e.latLng;
                // marker.setPosition(pos);
                // map.panTo(pos);
                // element.blur();
                // codeLatLng(pos);
            });


            $('.selectorlocation').click(function () {
                instance.geoLocation();

            });
            marker.addListener('dragend', function (e) {
                // Do something with the positions here
                // once the user has finished dragging the marker
                var pos = e.latLng;
   
                $("#lat").val(pos.lat())
                $("#long").val(pos.lng())
                 codeLatLng(pos);

            });
            service = new google.maps.places.PlacesService(map);

            // When the map is made visible, if we have no location set then
            // attempt geolocation. The css() calls ensure that the map is
            // refreshed.

            var lat = $('#lat').val();
            var long = $('#long').val();
            var val_text_input = $("#collapseOne").val();
            if (lat) {
                var pos = new google.maps.LatLng(lat, long);
                map.setZoom(17)
            } else {


                var pos = new google.maps.LatLng(24.466667, 54.366669);
                map.setZoom(5)
            }

            updatePosition(pos);
            codeLatLng(pos);
            $("#collapseOne").val(val_text_input);
            setTimeout(function () {
                init_update_map = false
            }, 1000);


        }

        function initAutoComplete() {

            autocomplete = new google.maps.places.Autocomplete(element, {

                types: []
            });
            google.maps.event.addListener(autocomplete, "place_changed", function () {
                var place = autocomplete.getPlace();
                if (place.geometry) {
                    ///console.log(place.geometry.location.lat(), place.geometry.location.lng())
                    $("#lat").val(place.geometry.location.lat())
                    $("#long").val(place.geometry.location.lng())
                    setPlace(place);
                    map.setZoom(17)
					
					var pos = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
   
                 codeLatLng(pos);
                }
            });
        }

        function resizeHandler() {
            instance.resize.call(instance);
        }

        /**
         * We want to be able to select the first suggestion from Google when
         * the user presses return. This function applies a solution from Stack
         * Overflow, shimming the addEventListener function on the input element
         * so that when the Google code adds event listeners, they are actually
         * proxied through a custom handler. If a return keypress is detected
         * (keyCode 13), then a down arrow is inserted first, which has the
         * effect of selecting the first item in the suggestions, if any.
         */
        function overrideAddEventListener() {
            // http://stackoverflow.com/a/11703018/1220963
            var _addEventListener = (element.addEventListener) ? element.addEventListener : element.attachEvent;

            function addEventListenerWrapper(type, listener) {
                // Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
                // and then trigger the original listener.
                if (type === "keydown") {
                    var orig_listener = listener;
                    listener = function (event) {
                        var suggestion_selected = $(".pac-item-selected").length > 0;
                        if ((event.which === 13 || event.keyCode === 13) && !suggestion_selected) {
                            var simulated_downarrow = $.Event("keydown", {
                                keyCode: 40,
                                which: 40
                            });
                            orig_listener.apply(element, [simulated_downarrow]);
                        }

                        orig_listener.apply(element, [event]);
                    };
                }

                _addEventListener.apply(element, [type, listener]);
            }

            element.addEventListener = addEventListenerWrapper;
            element.attachEvent = addEventListenerWrapper;
        }

        function init() {

            geocoder = new google.maps.Geocoder();

            overrideAddEventListener();

            initDomElements();
            initAutoComplete();
            initMap();

            if (!element.value) {
                var lat = options.latitude || $(options.latitudeInput).prop("value");
                var lng = options.longitude || $(options.longitudeInput).prop("value");
                if (lat && lng) {
                    instance.setLocation(lat, lng);
                }
            } else {
                codePlace(element.value);
            }

            $(window).on("resize", resizeHandler);
            $(element).on("keypress", function (e) {
                if (options.preventSubmit && e.keyCode === 13) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            });

        }

        function setPlace(place, updateMap) {

            updateMap = typeof updateMap === "undefined" ? true : false;
            _place = place;

            instance.resize();

            var pos = place.geometry.location;

            if (updateMap) {
                updatePosition(pos);
            }

            $(options.latitudeInput).prop("value", pos.lat());
            $(options.longitudeInput).prop("value", pos.lng());

            // update inputs
            if (!updateMap && !init_update_map) {
                console.log(init_update_map)
                element.value = place.formatted_address;
            }

            if (typeof options.placeChanged === "function") {
                options.placeChanged.call(instance, place);
            }
            

               

        }

        function updatePosition(pos) {

            if (!map) {
                return;
            }

            map.setCenter(pos);

            var icon = options.icon || options.placesIcon && place.icon ? place.icon : null;

            if (icon) {
                var iconOptions = {
                    url: icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                };
                marker.setIcon(iconOptions);
            }

            marker.setPosition(pos);
            marker.setVisible(true);


        }

        this.setValue = function (value) {
            element.value = value;
            codePlace(value);
        };

        this.getValue = function () {
            return element.value;
        };

        this.setLocation = function (latitude, longitude) {
            var latLng = new google.maps.LatLng(latitude, longitude);
            this.setLatLng(latLng);
            map.setZoom(17)
        };

        this.getLocation = function () {
            var latLng = this.getLatLng();
            return {
                latitude: latLng && latLng.lat() || options.latitude,
                longitude: latLng && latLng.lng() || options.longitude
            };
        };

        this.setLatLng = function (latLng) {
            _latLng = latLng;
            codeLatLng(_latLng);
        };

        this.getLatLng = function () {
            if (_place && _place.geometry) {
                return _place.geometry.location;
            }
            return _latLng;
        };

        this.getMap = function () {
            return map;
        };

        this.reload = function () {
            if (map) {
                codePlace(element.value);
            }
        };

        this.resize = function () {
            if (map) {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            }
        };

        this.geoLocation = function (callback) {

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    updatePosition(pos);
                    codeLatLng(pos);

                    $("#lat").val(pos.lat())
                    $("#long").val(pos.lng())
                    codeLatLng(pos);
                    map.setZoom(17)

                    if (callback) {
                        callback(pos);
                    }

                }, function () {
                    // error
                    if (callback) {
                        callback(null);
                    }
                });
            } else {
                // Browser doesn't support Geolocation
                if (callback) {
                    callback(null);
                }
            }
       
        };


        init.call(this);
    }

    var pluginClass = PlacePicker;

    // register plugin
    $.fn[pluginName] = function (options) {

        return this.each(function () {
            if (!$(this).data(pluginName)) {
                $(this).data(pluginName, new pluginClass(this, $.extend({}, defaults, options, $(this).data())));
            }
            return $(this);
        });
    };


})(jQuery, window);


