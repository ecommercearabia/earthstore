<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty pageData.breadcrumbs}">

	<div class="facet js-facet AppliedFilter">

	<div class="facet__name js-facet-name">
		<spring:theme code="search.nav.applied.facets"/>
		<span class="clickarrowFilter">
			
			<i class="far fa-plus "></i>
			</span>
	</div>
		<div class="facet__values js-facet-values">
			<ul class="facet__list">
				<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
					<li class="fecet-applied">
						<c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
						${fn:escapeXml(breadcrumb.facetValueName)}&nbsp;<a href="${fn:escapeXml(removeQueryUrl)}" ><span class="fas fa-times"></span></a>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>

</c:if>