<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
   <div class="row margin_top no-margin w1300">
   <div class="col-md-2  col-xs-12 logo">
    <cms:pageSlot position="FooterLogo" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    <div class="col-md-7 col-xs-12">
    <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
    <div class="col-md-3 col-xs-12 FooterAppStoreLinks mobile-action footer__nav--container">
    <cms:pageSlot position="FooterAppStoreLinks" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>
 <%--   <div class="col-md-2 col-xs-12 SubscribeContainer mobile-action footer__nav--container">
    <cms:pageSlot position="FooterNewsLetter" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
    </div>--%>
    </div>
    <div class="FooterBottom">
    <div class="row w1300 algin-footer">
    
    <div class="col-md-3  col-xs-12 FooterFollowUsLinks ">
	    <cms:pageSlot position="FooterFollowUsLinks" var="feature">
	        <cms:component component="${feature}"/>
	    </cms:pageSlot>
	    </div>
	      <div class="col-md-6  col-xs-12 CopyrightFooter ">
	     <cms:pageSlot position="CopyrightFooter" var="feature">
	        <cms:component component="${feature}"/>
	    </cms:pageSlot>
	    </div>
	      <div class="col-md-3  col-xs-12 FooterPaymentLinks ">
	    <cms:pageSlot position="FooterPaymentLinks" var="feature">
	        <cms:component component="${feature}"/>
	    </cms:pageSlot>
	    </div>
    </div>
    
    </div>
    
   
</footer>

