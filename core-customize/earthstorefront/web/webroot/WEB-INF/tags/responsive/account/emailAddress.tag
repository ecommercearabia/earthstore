<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="email" required="true" type="java.lang.String" %>
<spring:url var="updateEmailURL" value="{contextPath}/my-account/update-email" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>

<div class="cont_box">
<div class="left-side">
	<div class="fal fa-envelope"></div></div>
	<div class="right-side">
	<div class="headline">


	<div class="head_myaccount">
		<spring:theme code="email.address"/>
	</div>
	
	
	<div class="order_dis">
	<div class="font-title">
	${email}</div>
	</div></div>
	<div class="body">
		
	</div>

</div>
<a href="${updateEmailURL}" class="link_box_cont"></a>
</div>
