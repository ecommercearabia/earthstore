<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="wishlistEntries" required="true" type="java.util.Collection" %>

<spring:url var="myWishlistURL" value="{contextPath}/my-account/my-wishlist" htmlEscape="false" >
	<spring:param name="contextPath" value="${request.contextPath}" />
</spring:url>
<div class="cont_box_wish">


	<div class="headline">
	<div class="head_wish"><div class="fas fa-heart"></div><div class="mywishlist_name"><spring:theme code="myaccount.mywishlist"/></div></div>

	</div>
	<div class="body">
	<div class="row">
		<c:if test="${not empty wishlistEntries}">
		<c:forEach items="${wishlistEntries}" var="entry">
		<div class="col-md-4 col-sm-6 col-xs-12  mr_b">
			<div class="item__image">
				<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
			</div>
			<div class="item__info">
				<span class="item__name">${fn:escapeXml(entry.product.name)}</span>
			</div>
			</div>
		</c:forEach>
		</c:if>
		<c:if test="${empty wishlistEntries}">
			<div class="search-empty text-center">
		<div class="headline">
			<img src="${fn:escapeXml(themeResourcePath)}/images/wishlist-orange.svg" class="noSearch" style="max-width:250px"><br/>
			<h3><spring:theme text="myaccount.mywishlist.empty.wishlist"/></h3>
		</div>
	</div>
		</c:if>
	</div>
	</div>
	<a href="${myWishlistURL}" class="link_box_cont"></a>
</div>