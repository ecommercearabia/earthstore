<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="thirdpartyAuthenticationProvider" tagdir="/WEB-INF/tags/responsive/thirdpartyAuthenticationProvider"%>
<%@ attribute name="isCheckout" required="false" type="java.lang.Boolean"%>


<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="hideDescription" value="checkout.login.loginAndCheckout" />

<div class="login-page__headline">
	<spring:theme code="login.title" />
</div>

<c:if test="${actionNameKey ne hideDescription}">
	<p class="loginDescription">
		<spring:theme code="login.description" />
	</p>
</c:if>

<div class="login_cont">
<form:form action="${action}" method="post" modelAttribute="loginForm">
	<c:if test="${not empty message}">
		<span class="has-error"> <spring:theme code="${message}" />
		</span>
	</c:if>	
	
		<formElement:formLoginInputBox idKey="j_username" labelKey="login.email"
			path="j_username" mandatory="true" placeholder="login.email" customer="${user}" />
						
		<formElement:formPasswordBox idKey="j_password"
			labelKey="login.password" path="j_password" inputCSS="form-control"
			mandatory="true" />
			<c:if test="${attempts >= 5 }">
				<input type="hidden" id="recaptchaChallangeAnswered" value="${fn:escapeXml(requestScope.recaptchaChallangeAnswered)}"/>
				<div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>
			</c:if>
			<div class="forgotten-password">
				<ycommerce:testId code="login_forgotPassword_link">
					<a href="#" data-link="<c:url value='/login/pw/request'/>" class="js-password-forgotten" data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
						<spring:theme code="login.link.forgottenPwd" />
					</a>
				</ycommerce:testId>
			</div>
			<div class="text-center">
		<ycommerce:testId code="loginAndCheckoutButton">
			<button type="submit" class="btn btn-primary btnLogin">
				<spring:theme code="${actionNameKey}" />
			</button>
		</ycommerce:testId>
	</div>
<%-- 				<c:if test="${cmsSite.activeLoginWithOTP}">
		<div class="text-center">
		<div class="my-3 OrLoginotp"></div>
		<c:url value="/otp-login" var="otpLoginURL" />
<a href="${otpLoginURL}" class="btn btn-primary  btnLogin">
	<spring:theme code="login.with.otp" />
</a></div>
		</c:if> --%>
		
</form:form>






	<c:choose>
	<c:when test="${isCheckout}">
		<c:url value="/register/checkout" var="registerPageUrl" />
		<c:url value="/login/otp/checkout" var="otpLoginURL" />
	
	
	</c:when>
	<c:otherwise>
		<c:url value="/register" var="registerPageUrl" />
		<c:url value="/login/otp" var="otpLoginURL" />
	</c:otherwise>
	
	</c:choose>
	
<c:if test="${cmsSite.activeLoginWithOTP}">
	<form:form action="${otpLoginURL}" method="post">
		<button type="submit" class="btn btn-primary  btnLogin">
			<spring:theme code="login.with.otp" />
		</button>
	</form:form>
</c:if>


<div class="loginSocialMediaContianer">
	<span class="TitleloginSocialMedia"><spring:theme code="register.quickly.title" /></span> <span class="loginSocialMedia">	<thirdpartyAuthenticationProvider:providers providers="${supportedThirdPartyAuthenticationProvider}"/>
		</span></div>
		
<c:url value="/register" var="registerPageUrl" />

		<div class="text-center">	<span class="or-style"><span><spring:theme code="login.singup.or" /></span></span>
			<a href="${registerPageUrl}"> <spring:theme
					code="login.singup.popup" text="Sign Up" />
			</a></div>
	<c:if test="${expressCheckoutAllowed}">
		<button type="submit" class="btn btn-default btn-block expressCheckoutButton"><spring:theme code="text.expresscheckout.header" /></button>
		<input id="expressCheckoutCheckbox" name="expressCheckoutEnabled" type="checkbox" class="form left doExpressCheckout display-none" />
	</c:if>


</div>

