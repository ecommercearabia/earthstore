<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="areas" required="false" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:choose>
	<c:when test="${country == 'US'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.state" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CA'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.state" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postalcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'CN'}">
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postalcode" path="postcode" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.street" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.building" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'JP'}">
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.furtherSubarea" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.subarea" path="line2" inputCSS="form-control" mandatory="true"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townJP" path="townCity" inputCSS="form-control" mandatory="true" />
		<formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="regionIso" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}" selectedValue="${addressForm.regionIso}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.postalcode" labelKey="address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:when>
	<c:when test="${country == 'AE'}">

		<div class="row">

			<div class="col-md-2 col-sm-12">

				<formElement:formSelectBoxDefaultEnabled idKey="address.title"
					labelKey="address.title" path="titleCode" mandatory="true"
					skipBlank="false" skipBlankMessageKey="address.title.none"
					items="${titles}" selectedValue="${addressForm.titleCode}"
					selectCSSClass="form-control" />
			</div>
			<div class="col-md-5 col-sm-12">
				<formElement:formInputBox placeholder="address.firstName"
					idKey="address.firstName" labelKey="address.firstName"
					path="firstName" inputCSS="form-control" mandatory="true" />
			</div>
			<div class="col-md-5 col-sm-12">
				<formElement:formInputBox placeholder="address.surname"
					idKey="address.surname" labelKey="address.surname" path="lastName"
					inputCSS="form-control" mandatory="false" />
			</div>
			<div class="col-md-12 col-sm-12">
				<formElement:formInputBox placeholder="address.addressName"
					idKey="address.addressName" labelKey="address.addressName"
					path="addressName" inputCSS="form-control" mandatory="true" />
			</div>
			<c:if test="${cmsSite.cityAddressEnabled }">
				<div class="col-md-12 col-sm-12 ">
					<div
						class="<c:if test="${cmsSite.cityAddressHidden}">hidden</c:if>">
						<formElement:formSelectBoxDefaultEnabled idKey="cityId"
							labelKey="address.townCity" selectCSSClass="form-control f16 "
							path="cityCode" mandatory="true" skipBlank="false"
							skipBlankMessageKey="form.select.empty" items="${cities}" selectedValue="${addressForm.cityCode }" />
					</div>
				</div>
			</c:if>

			<c:if test="${cmsSite.areaAddressEnabled }">
				<div class="col-md-12 col-sm-12 ">
					<div
						class="<c:if test="${cmsSite.areaAddressHidden}">hidden</c:if>">
						<formElement:formSelectBoxDefaultEnabled idKey="areaId"
							labelKey="address.area" selectCSSClass="form-control f16 "
							path="areaCode" mandatory="true" skipBlank="false"
							skipBlankMessageKey="form.select.empty" items="${areas}" />

					</div>
				</div>
			</c:if>
			<div class="col-md-12 col-sm-12 find-me-btn">
				
				    <formElement:formInputBoxmap idKey="address.line1_" labelKey="address.line1" path="line1"
                                             inputCSS="placepicker form-control" mandatory="true"/>
                <a href="javascript:;" class="selectorlocation btn btn-primary pull-right btn-block"><i class="fas fa-map-marker-alt"></i> <spring:theme code="address.useThisAddress"/></a>
                <div id="collapseOne" class="collapse in btn-block pull-right">
                    <div class="placepicker-map thumbnail"></div>
                </div>
                <span class="notaccpted hidden"><spring:theme code="address.useThisAddress.error"/></span>
               
			</div>

						<c:if test="${cmsSite.nearestLandmarkAddressEnabled }">
				<div class="col-md-12 col-sm-12">
					<div
						class="<c:if test="${cmsSite.nearestLandmarkAddressHidden}">hidden</c:if>">
						<formElement:formInputBox placeholder="address.nearestLandmark"
							idKey="address.nearestLandmark"
							labelKey="address.nearestLandmark" path="nearestLandmark"
							inputCSS="form-control"
							mandatory="${cmsSite.nearestLandmarkAddressRequired}" />
					</div>
				</div>
			</c:if>

			<c:if test="${cmsSite.buildingNameAddressEnabled }">
				<div class="col-md-4 col-sm-12">
					<div
						class="<c:if test="${cmsSite.buildingNameAddressHidden}">hidden</c:if>">
						<formElement:formInputBox placeholder="address.buildingName"
							idKey="address.buildingName" labelKey="address.buildingName"
							path="buildingName" inputCSS="form-control"
							mandatory="${cmsSite.buildingNameAddressRequired}" />
					</div>
				</div>
			</c:if>

			<c:if test="${cmsSite.floorNumberAddressEnabled }">
				<div class="col-md-4 col-sm-12">
					<div
						class="<c:if test="${cmsSite.floorNumberAddressHidden}">hidden</c:if>">
						<formElement:formInputBox placeholder="address.floorNumber"
							idKey="address.floorNumber" labelKey="address.floorNumber"
							path="floorNumber" inputCSS="form-control"
							mandatory="${cmsSite.floorNumberAddressRequired}" />
					</div>
				</div>
			</c:if>

			<c:if test="${cmsSite.apartmentNumberAddressEnabled }">
				<div class="col-md-4 col-sm-12">
					<div
						class="<c:if test="${cmsSite.apartmentNumberAddressHidden}">hidden</c:if>">
						<formElement:formInputBox placeholder="address.apartmentNumber"
							idKey="address.apartmentNumber"
							labelKey="address.apartmentNumber" path="apartmentNumber"
							inputCSS="form-control"
							mandatory="${cmsSite.apartmentNumberAddressRequired}" />
					</div>
				</div>
			</c:if>
			<div class="col-md-12 col-sm-12">
				<formElement:formInputBox placeholder="address.line2"
					idKey="address.line2" labelKey="address.line2" path="line2"
					inputCSS="form-control" mandatory="false" />
			</div>
			<div class="col-md-4 col-sm-12">
				<formElement:formCountrySelectBoxDefaultEnabled
					idKey="mobileCountry" labelKey="address.mobileCountry"
					selectCSSClass="form-control f16 " path="mobileCountry"
					mandatory="true" skipBlank="false"
					skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
			</div>
			<div class="col-md-8 col-sm-12">
				<formElement:formInputBox idKey="address.mobile"
					labelKey="address.mobile" path="mobile" inputCSS="form-control"
					mandatory="true" placeholder="address.mobile" />
			</div>
			
			
			<c:if test="${cmsSite.moreInstructionsAddressEnabled }">
				<div class="col-md-12 col-sm-12">
					<div
						class="<c:if test="${cmsSite.moreInstructionsAddressHidden}">hidden</c:if>">
						<formElement:formTextArea placeholder="address.moreInstructions"
							idKey="address.moreInstructions"
							labelKey="address.moreInstructions" path="moreInstructions"
							areaCSS="form-control"
							mandatory="${cmsSite.moreInstructionsAddressRequired}" />
					</div>
				</div>
			</c:if>




			<div class="col-md-6 col-sm-12 hidden ">
				<formElement:formInputBox idKey="long" labelKey="longitude"
					path="longitude" inputCSS="form-control" mandatory="true"
					placeholder="longitude" />
			</div>
			<div class="col-md-6 col-sm-12 hidden">
				<formElement:formInputBox idKey="lat" labelKey="address"
					path="latitude" inputCSS="form-control" mandatory="true"
					placeholder="latitude" />
			</div>

			<%-- <formElement:formInputBox  placeholder="address."  idKey="address.postcode" labelKey="address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox  placeholder="address."  idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" /> --%>

		</div>
	</c:when>
	<c:otherwise>
		<formElement:formSelectBoxDefaultEnabled idKey="address.title" labelKey="address.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="address.title.none" items="${titles}" selectedValue="${addressForm.titleCode}" selectCSSClass="form-control"/>
		<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="cityCode" inputCSS="form-control" mandatory="true" />
		<formElement:formInputBox idKey="address.area" labelKey="address.area" path="areaCode" inputCSS="form-control" mandatory="true" />
		<formElement:formCountrySelectBoxDefaultEnabled idKey="address.mobileCountry" labelKey="address.mobileCountry" selectCSSClass="form-control f16 " path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
		<formElement:formInputBox idKey="address.mobile" labelKey="address.mobile" path="mobile" inputCSS="form-control" mandatory="true" placeholder="address.mobile" />
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="postcode" inputCSS="form-control" mandatory="true" />
        <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control" mandatory="false" />
	</c:otherwise>
</c:choose>

