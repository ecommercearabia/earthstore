<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">

    <div class="col-xs-12 col-md-5 col-lg-5 ">
        <product:productImagePanel galleryImages="${galleryImages}" />
    </div>
    <div class="clearfix hidden-sm hidden-md hidden-lg"></div>
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 ${product.code}">
        <div class="product-main-info">

            <div class="product-details page-title">
                <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                    <div class="name">${fn:escapeXml(product.name)}</div>
                    <span class="sku">SKU:</span><span class="code">${fn:escapeXml(product.code)}</span>

                </ycommerce:testId>
                <product:productReviewSummary product="${product}" showLinks="true" />

                <div class="countryoforigin">

                    <c:if test="${not empty product.countryOfOrigin}">

                        <c:set value="${fn:replace(product.countryOfOrigin, ' ', '-')}" var="countryOfOrigin"></c:set>
                        <i class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin}
                    </c:if>

                </div>
                <c:if test="${ product.expressDelivery}">
											<span class="expressdelivery"><img
                                                    src="${fn:escapeXml(themeResourcePath)}/images/Express.svg" />
											</span>
                </c:if>


            </div>
            

            <div class="product-details">


                <product:productPromotionSection product="${product}" />





                <div class="brand-name"> ${product.brand.name}</div>




                <%--  <span>${not empty product.unitFactorRangeData}<br/></span>

                  <span>${product.discountUnitPrice.price.value} discount -> price (priceData)<br/></span>
                  <span>${product.discountUnitPrice.discountPrice.value} discount -> discountPrice (priceData)<br/></span>
                  <span>${product.discountUnitPrice.saving.value} discount -> saving (priceData)<br/></span>
                  <span>${product.discountUnitPrice.percentage} discount -> percentage<br/></span>
                  --%>
                <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">

                    <c:choose>
                        <c:when test="${not empty product.unitFactorRangeData}">


                            <c:choose>
                                <c:when test="${not empty product.unitFactorRangeData[0].discountUnitPrice}">


                                    <span class="price">

                                           <format:fromPrice
                                                   priceData="${product.unitFactorRangeData[0].discountUnitPrice.discountPrice}"/>


                                   </span>
                                    <span class="scratched">
                                        <span class="Pricescratched">  <format:fromPrice priceData="${product.unitFactorRangeData[0].discountUnitPrice.price}"/></span>
                                    <span
                                            class="line_dis ${display_inline} "></span></span>


                                </c:when>
                                <c:otherwise>



                                     <span class="price">
                                    <format:fromPrice priceData="${product.unitFactorRangeData[0].price}"/>
                                            <span class="scratched hidden">
                                                <span class="Pricescratched"> </span>
                                    <span
                                            class="line_dis hidden "></span></span>

                                    </span>

                                </c:otherwise>

                            </c:choose>


                        </c:when>
                        <c:otherwise>


                            <c:choose>

                                <c:when test="${not empty product.discount}">
                                    <c:set value="display_in" var="display_inline"></c:set>
                                    <span class="price">




                                                <format:fromPrice
                                                        priceData="${product.discount.discountPrice}"/>

									</span>


                                    <span class="scratched"><format:fromPrice
                                            priceData="${product.discount.price}"/><span
                                            class="line_dis ${display_inline} "></span></span>
                                    <%-- 										<span class="discount_style">(${product.discount.percentage})</span> --%>

                                </c:when>
                                <c:otherwise>

                                    <span class="scratched"></span>
                                    <product:productPricePanel product="${product}"/>

                                </c:otherwise>

                            </c:choose>


                        </c:otherwise>
                    </c:choose>


                </ycommerce:testId>
   <c:if test="${not empty product.unitFactorRangeData}">
            <div>        <span class="price-per-unit">${product.price.currencyIso}&nbsp;${product.unitPrice.value} <span class="unit"> / ${product.unit.unit}</span></span></div>
                </c:if>

                <!-- <div class="product-classifications"> -->
                <%-- 	<c:if test="${not empty product.classifications}"> --%>
                <%-- 		<c:forEach items="${product.classifications}" var="classification"> --%>
                <%-- 			<div class="calssifiationHeadline">${fn:escapeXml(classification.name)}</div> --%>

                <%-- 						<c:forEach items="${classification.features}" var="feature"> --%>

                <%-- 								<span class="calssifiationkey">${fn:escapeXml(feature.name)} :</span> --%>
                <!-- 								<span class="calssifiationValue"> -->
                <%-- 									<c:forEach items="${feature.featureValues}" var="value" varStatus="status"> --%>
                <%-- 										${fn:escapeXml(value.value)} --%>
                <%-- 										<c:choose> --%>
                <%-- 											<c:when test="${feature.range}"> --%>
                <%-- 												${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)} --%>
                <%-- 											</c:when> --%>
                <%-- 											<c:otherwise> --%>
                <%-- 												${fn:escapeXml(feature.featureUnit.symbol)} --%>
                <%-- 												${not status.last ? '<br/>' : ''} --%>
                <%-- 											</c:otherwise> --%>
                <%-- 										</c:choose> --%>
                <%-- 									</c:forEach> --%>
                <!-- 								</span> -->
                <!-- 							<br/> -->
                <%-- 						</c:forEach> --%>

                <%-- 		</c:forEach> --%>
                <%-- 	</c:if> --%>
                <!-- </div> -->
            </div>


            <div class="">
                <%-- 				<c:if test="${not empty product.unitFactorRangeData}"> --%>
                <!-- 		<select id="SelectproductQtyItem">   -->
                <%-- 			<c:forEach items="${product.unitFactorRangeData}" var="item"> --%>
                <%-- 				 <option class="itemSelectQty" data-value="${item.formatedValue}" data-Qty="${item.quantity}" data-stock="${item.inStock}" data-price='<format:fromPrice	priceData="${item.price}"/>'>${item.formatedValue} --%>
                <!-- 				</option> -->
                <%-- 			</c:forEach> --%>
                <!-- 		</select> -->
                <%-- 	</c:if> --%>

                <cms:pageSlot position="VariantSelector" var="component" element="div"
                              class="page-details-variants-select">
                    <cms:component component="${component}" element="div"
                                   class="yComponentWrapper page-details-variants-select-component" />
                </cms:pageSlot>
                <cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
                    <cms:component component="${component}" element="div"
                                   class="yComponentWrapper page-details-add-to-cart-component" />
                </cms:pageSlot>
            </div>
            <div class="block_padd">
							<span class="shareTitle">
								<spring:theme code="Share" />
							</span>
                <!-- AddToAny BEGIN -->
                <script type='text/javascript'
                        src='https://platform-api.sharethis.com/js/sharethis.js#property=5e8200d89e501a001a5215df&product=inline-share-buttons&cms=sop'
                        async='async'></script>
                <div class="shareIcon">
                    <div data-network="facebook" class="st-custom-button facebook"><i class="fab fa-facebook-f fb"></i>
                    </div>


                    <div class="st-custom-button twitter" data-network="twitter">
                        <i class="fab fa-twitter"></i>
                    </div>
                    <div class="st-custom-button pinterest" data-color="#CB2027" data-network="pinterest">
                        <i class="fab fa-pinterest-p"></i>
                    </div>
                    <div class="st-custom-button whatsapp" data-color="#25d366" data-network="whatsapp">
                        <i class="fab fa-whatsapp"></i>
                    </div>
                </div>
            </div>
            <div class="description-pdp">
                 <c:if test="${not empty ycommerce:sanitizeHTML(product.summary) }">
                <h3 class="title-description"><spring:theme code="description.pdp"  /></h3>
             
             ${ ycommerce:sanitizeHTML(product.summary) }
                </c:if>
            </div>


        </div>

    </div>
</div>