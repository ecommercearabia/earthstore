<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="productData" required="true" type="java.util.List" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="carousel__component carouselProduct">
    <div class="titleSection"><c:if test="${not empty title}">
        <div class="carousel__component--headline">${fn:escapeXml(title)}</div>
    </c:if>

        <span class="linkView">	<cms:component component="${component.link}" /></span>

    </div>

    <div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-default owl-theme">
        <c:forEach items="${productData}" var="product">


            <c:url value="${product.url}" var="productUrl" />


            <div class="carousel__item  ${product.code}">


                <div class="carousel__item--thumb">
                    <c:if test="${not empty product.discount.percentage}">
                        <div class="carousel__item--label"><fmt:formatNumber type="NUMBER"
                                                                             value="${product.discount.percentage}"
                                                                             maxFractionDigits="0" />% <spring:theme
                                        code='off' />
                        </div>
                    </c:if>
                    <div class="div-label-cla">
                        <c:if test="${ product.ecoFriendly}">
                            <div class="label-ecoFriendly"></div>
                        </c:if>
                        <c:if test="${ product.glutenFree}">
                            <div class="label-glutenFree"></div>
                        </c:if>
                        <c:if test="${ product.vegan}">
                            <div class="label-vegan"></div>
                        </c:if>
                        <c:if test="${ product.organic}">
                            <div class="label-organic"></div>
                        </c:if>
                        <c:if test="${product.sugarFree}">
                            <div class="label-sugarFree"></div>
                        </c:if>
                    </div>

                    <c:url value="javascript:;" var="link"></c:url>
                    <c:set value="" var="PopupWishlist"></c:set>
                    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                        <c:set value="js-popup-wishlist" var="PopupWishlist"></c:set>
                        <c:url value="javascript:;" var="link"></c:url>
                    </sec:authorize>
                    <c:set value="" var="isout"></c:set>
                    <c:set value="hidden" var="isin"></c:set>
                    <c:if test="${product.inWishlist}">
                        <c:set value="" var="isin"></c:set>
                        <c:set value="hidden" var="isout"></c:set>
                    </c:if>
                    <c:if test="${!product.inWishlist}">
                        <c:set value="hidden" var="isin"></c:set>
                        <c:set value="" var="isout"></c:set>
                    </c:if>


                    <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry ${PopupWishlist} wishlistbtn  ${isin} "
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry ${PopupWishlist} wishlistbtn  ${isout}"
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
				
				</span>
                    <a href="${productUrl}">
                        <c:if test="${not empty product.productLabel}">
                            <div class="carousel__item--label">${product.productLabel} <spring:theme
                                        code='off' /></div>
                        </c:if>
                        <product:productPrimaryImage product="${product}" format="product" />


                    </a>


                    <c:choose>
                        <c:when test="${empty product.unitFactorRangeData}">
                            <c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}">
                                <a href="${fn:escapeXml(productUrl)}" class="stockLevel">
                                                <span class="outstock"><i class="fas fa-times-circle"></i><spring:theme
                                                        code='product.variants.out.of.stock' /></span>
                                </a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${product.unitFactorRangeData[0].inStock}">
                                    <a href="${fn:escapeXml(productUrl)}" class="stockLevel hidden">
                                                    <span class="OutOfStock "><i
                                                            class="fas fa-times-circle"></i><spring:theme
                                                            code="product.variants.out.of.stock" /></span>
                                    </a>
                                </c:when>

                                <c:otherwise>
                                    <a href="${fn:escapeXml(productUrl)}" class="stockLevel">
                                                    <span class="OutOfStock "><i
                                                            class="fas fa-times-circle"></i><spring:theme
                                                            code="product.variants.out.of.stock" /></span>
                                    </a>
                                </c:otherwise>
                            </c:choose>


                        </c:otherwise>
                    </c:choose>


                </div>

                <div class="cont_detail_carousel">
                    <a href="${productUrl}">
                        <div class="cont_href_div">
                            <div class="carousel__item--name">
                                <a href="${productUrl}">
                                    <c:choose>
                                        <c:when test="${fn:length(product.name) > 44}">
                                            <c:out value="${fn:substring(product.name, 0, 44)}..." />
                                        </c:when>
                                        <c:otherwise>
                                            ${fn:escapeXml(product.name)}
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </div>


                            <div class="carousel__item--countryoforigin">
                                <c:if test="${not empty product.countryOfOrigin}">

                                    <c:set value="${fn:replace(product.countryOfOrigin, ' ', '-')}"
                                           var="countryOfOrigin"></c:set>
                                    <div class="carousel__item--countryoforiginisocode"><i
                                            class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin}
                                    </div>
                                </c:if></div>
                        </div>
                    </a>
                    <div class="price_counter">

                        <div class="carousel__item--price price">

                            <c:if test="${ product.expressDelivery}">
											<span class="expressdelivery"><img
                                                    src="${fn:escapeXml(themeResourcePath)}/images/Express.svg" />
											</span>
                            </c:if>

                            <c:choose>
                                <c:when test="${not empty product.unitFactorRangeData}">


                                    <c:choose>
                                        <c:when test="${not empty product.unitFactorRangeData[0].discountUnitPrice}">


                                                   <span class="disCountPrice">  <span class="scratched">
                                        <span class="Pricescratched">  <format:fromPrice
                                                priceData="${product.unitFactorRangeData[0].discountUnitPrice.price}" /></span>
                                    <span class="line_dis ${display_inline} "></span></span>
                                        </span>


                                            <span class="price">

                                           <format:fromPrice
                                                   priceData="${product.unitFactorRangeData[0].discountUnitPrice.discountPrice}" />


                                   </span>


                                        </c:when>
                                        <c:otherwise>



                                     <span class="price">

                                          <span class="disCountPrice">  <span class="scratched hidden">
                                                <span class="Pricescratched"> </span>
                                    <span class="line_dis hidden "></span></span>
                                        </span>
<format:fromPrice priceData="${product.unitFactorRangeData[0].price}" />
                                    </span>

                                        </c:otherwise>

                                    </c:choose>


                                </c:when>
                                <c:otherwise>


                                    <c:choose>

                                        <c:when test="${not empty product.discount}">
                                            <c:set value="display_in" var="display_inline"></c:set>


                                            <span class="scratched"><format:fromPrice
                                                    priceData="${product.discount.price}" /><span
                                                    class="line_dis ${display_inline} "></span></span>
                                            <%--                                        <span class="discount_style">(${product.discount.percentage})</span> --%>
                                            <span class="price">




                                                <format:fromPrice priceData="${product.discount.discountPrice}" />

                                    </span>

                                        </c:when>
                                        <c:otherwise>

                                            <span class="scratched"></span>
                                            <product:productPricePanel product="${product}" />

                                        </c:otherwise>

                                    </c:choose>


                                </c:otherwise>
                            </c:choose>
                        </div>

                        <product:addtocartcarousel showQuantityBox="true" product="${product}" />
                    </div>
                </div>
                </a>


            </div>


        </c:forEach>
    </div>
</div>