<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showQuantityBox" required="false" type="java.lang.Boolean" %>
<spring:htmlEscape defaultHtmlEscape="true" />
<c:set value="" var="unitFactorRangeData"></c:set><c:set value="hidden" var="QTY"></c:set>
<c:if test="${not empty product.unitFactorRangeData}">
    <c:set value="" var="QTY"></c:set>
    <c:set value="hidden" var="unitFactorRangeData"></c:set>
</c:if>


<c:set var="isForceInStock" value="${product.stock.stockLevelStatus.code eq 'inStock' and empty product.stock.stockLevel}"/>
<c:choose>
    <c:when test="${isForceInStock}">
        <c:set var="maxQty" value="FORCE_IN_STOCK"/>
    </c:when>
    <c:otherwise>
        <c:set var="maxQty" value="${product.stock.stockLevel}"/>
    </c:otherwise>
</c:choose>
<c:set var="qtyMinus" value="${not empty product.unitFactorRangeData ? product.unitFactorRangeData[0].quantity : '1' }" />


<div class="addtocart-component">
    <c:if test="${product.stock.stockLevel gt 0}">
        <c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
            <spring:theme code="product.variants.in.stock"/>
        </c:set>
    </c:if>
    <c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
        <c:set var="productStockLevel">
            <spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
        </c:set>
    </c:if>
    <c:if test="${isForceInStock}">
        <c:set var="productStockLevel">
            <spring:theme code="product.variants.available"/>
        </c:set>
    </c:if>
    <!--    <div class="stock-wrapper clearfix"> -->
    <%--      ${productStockLevel} --%>
    <!--    </div> -->
    <div class="actions">
        <c:if test="${multiDimensionalProduct}" >
            <c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
            <a href="${productOrderFormUrl}" class="btn btn-default btn-block btn-icon js-add-to-cart fas fa-list-alt">
                <spring:theme code="order.form" />
            </a>
        </c:if>


        <c:if test="${not product.multidimensional }">
            <c:url value="/cart/add" var="addToCartUrl"/>
            <spring:url value="${product.url}/configuratorPage/{/configuratorType}" var="configureProductUrl" htmlEscape="false">
                <spring:param name="configuratorType" value="${configuratorType}" />
            </spring:url>

            <form:form id="" action="${addToCartUrl}" method="post" class="add_to_cart_form pdpAddtocart">

                <ycommerce:testId code="addToCartButton">
                    <input type="hidden" name="productCodePost" value="${fn:escapeXml(product.code)}"/>
                    <input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
                    <input type="hidden" name="productPostPrice" value="${product.price.value}"/>
                    <%--             <c:if test="${showQuantityBox}"> --%>

                    <span class="contianerQty">
            <span class="titleQty hidden-md hidden-lg"> Quantity</span>

    <span class="${QTY} selectUnit wishlist-select">
		<c:if test="${not empty product.unitFactorRangeData}">
            <span class="SelectitemUnit"> <span class="valueUnit">${product.unitFactorRangeData[0].formatedValue}</span> <i class="far fa-chevron-down arrowUnitVal"></i></span>
            <div class="SelectproductQtyItem hidden">

		<c:set var="firstActive" value="" />
			<c:forEach items="${product.unitFactorRangeData}" var="item" varStatus="loop">
                <c:if test="${loop.index eq 0 }">
                    <c:set var="firstActive" value="active" />
                </c:if>
                <div class="itemSelectQty ${firstActive }" data-value="${item.formatedValue}" data-Qty="${item.quantity}" data-stock="${item.inStock}" data-price='${item.price.formattedValue}'>${item.formatedValue}
                </div>
                <c:set var="firstActive" value="" />
            </c:forEach>
		</div>
        </c:if>
		</span>

            <div class="qty-selector input-group js-qty-selector ${unitFactorRangeData}">
          <span class="contBtnqy" >
            <button class="js-qty-selector-minus btnqty" type="button"  ><span class="far fa-minus" aria-hidden="true"></span></button>
          </span>
          
          
          <input type="text" maxlength="3" autocomplete="off" class="js-qty-selector-input" size="1" value="${qtyMinus}" data-max="${maxQty}" data-min="1" name="qty" />
          <span class="contBtnqy">
            <button class="js-qty-selector-plus btnqty" type="button"><span class="far fa-plus" aria-hidden="true"></span></button>
          </span>
        </div>
</span>
                    <%-- </c:if> --%>


                    <c:choose>
                        <c:when test="${not empty product.unitFactorRangeData}">

                            <c:choose>
                                <c:when test="${product.unitFactorRangeData[0].inStock}">
                                    <button type="submit" class="btn btn-primary js-enable-btn btnpdpAddtocart" title="<spring:theme code='basket.add.to.basket'/>"
                                            disabled="disabled">
                                        <span class="iconBtnCart"><img  src="${fn:escapeXml(themeResourcePath)}/images/CartWhite.svg"><i class="fas fa-plus-circle"></i></span>
                                        <span class="btnCartText"><spring:theme code="add.btn.cart"/></span>

                                    </button>
                                </c:when>
                                <c:otherwise>

                                    <button type="submit" class="btn btn-primary btnpdpAddtocart"
                                            aria-disabled="true" disabled="disabled" title="<spring:theme code='product.variants.out.of.stock'/>">
                                        <span class="iconBtnCart"><img  src="${fn:escapeXml(themeResourcePath)}/images/CartWhite.svg"><i class="fas fa-plus-circle"></i></span>
                                        <span class="btnCartText"><spring:theme code="add.btn.cart"/></span>
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
                                    <button type="submit" class="btn btn-primary btnpdpAddtocart"
                                            aria-disabled="true" disabled="disabled" title="<spring:theme code='product.variants.out.of.stock'/>">
                                        <span class="iconBtnCart"><img  src="${fn:escapeXml(themeResourcePath)}/images/CartWhite.svg"><i class="fas fa-plus-circle"></i></span>
                                        <span class="btnCartText"><spring:theme code="add.btn.cart"/></span>
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button type="submit" class="btn btn-primary js-enable-btn btnpdpAddtocart" title="<spring:theme code='basket.add.to.basket'/>"
                                            disabled="disabled">
                                        <span class="iconBtnCart"><img  src="${fn:escapeXml(themeResourcePath)}/images/CartWhite.svg"><i class="fas fa-plus-circle"></i></span>
                                        <span class="btnCartText"><spring:theme code="add.btn.cart"/></span>
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </ycommerce:testId>
            </form:form>

            <form:form id="" action="${configureProductUrl}" method="get" class="configure_form">
                <c:if test="${product.configurable}">
                    <c:choose>
                        <c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
                            <button id="configureProduct" type="button" class="btn btn-primary btn-block"
                                    disabled="disabled">
                                <spring:theme code="basket.configure.product"/>
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled"
                                    onclick="location.href='${configureProductUrl}'">
                                <spring:theme code="basket.configure.product"/>
                            </button>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </form:form>
        </c:if>

        <div class="addedtocart hidden">
            <spring:theme code="basket.added.to.basket"/>
        </div>
        <div class="addedtocarterror hidden">

        </div>
        <div class="addedtocartinfo hidden">

        </div>
    </div>

</div>