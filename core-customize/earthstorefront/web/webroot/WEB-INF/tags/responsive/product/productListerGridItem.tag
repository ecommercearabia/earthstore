<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<spring:theme code="text.addToCart" var="addToCartText" />
<c:url value="${product.url}" var="productUrl" />
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion" />

<c:set value="product-item" var="productTagClasses" />
<c:forEach var="tag" items="${product.tags}">
    <c:set value="${productTagClasses} tag-${tag}" var="productTagClasses" />
</c:forEach>

<div class=" ${product.code} detail ${fn:escapeXml(productTagClasses)}">
    <div class="product-item-content">


        <ycommerce:testId code="product_wholeProduct">
            <div class="stockAthumb">
                <a class="thumb" href="${fn:escapeXml(productUrl)}" title="${fn:escapeXml(product.name)}">


                    <c:if test="${not empty product.discount.percentage}">
                        <div class="promo_label "><fmt:formatNumber type="NUMBER" value="${product.discount.percentage}"
                                                                    maxFractionDigits="0" />% <spring:theme
                                        code='off' />
                        </div>
                    </c:if>

                    <product:productPrimaryImage product="${product}" format="product" />
                    <div class="div-label-cla">
                        <c:if test="${product.ecoFriendly}">
                            <div class="label-ecoFriendly"></div>
                        </c:if>
                        <c:if test="${product.glutenFree}">
                            <div class="label-glutenFree"></div>
                        </c:if>
                        <c:if test="${product.vegan}">
                            <div class="label-vegan"></div>
                        </c:if>
                        <c:if test="${product.organic}">
                            <div class="label-organic"></div>
                        </c:if>
                        <c:if test="${product.sugarFree}">
                            <div class="label-sugarFree"></div>
                        </c:if>
                    </div>

                </a>
                <c:if test="${not empty product.inWishlist}">
                    <c:url value="javascript:;" var="link"></c:url>
                    <c:set value="" var="PopupWishlist"></c:set>
                    <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                        <c:set value="js-popup-wishlist" var="PopupWishlist"></c:set>
                        <c:url value="javascript:;" var="link"></c:url>
                    </sec:authorize>
                    <c:set value="" var="isout"></c:set>
                    <c:set value="hidden" var="isin"></c:set>
                    <c:if test="${product.inWishlist}">
                        <c:set value="" var="isin"></c:set>
                        <c:set value="hidden" var="isout"></c:set>
                    </c:if>
                    <c:if test="${!product.inWishlist}">
                        <c:set value="hidden" var="isin"></c:set>
                        <c:set value="" var="isout"></c:set>
                    </c:if>


                    <span class="wishlist_icon">
	<a href="${link}" title="wishlist" class="removeWishlistEntry ${PopupWishlist} wishlistbtn    ${isin} "
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
	<a href="${link}" title="wishlist" class="addWishlistEntry ${PopupWishlist} wishlistbtn    ${isout}"
       data-productcode="${product.code}" data-pk="8796093055677"><i class="fas fa-heart"></i></a>
				
				</span>


                </c:if>
                <div class="stockCont">
                    <c:choose>
                        <c:when test="${empty product.unitFactorRangeData}">
                            <c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}">
                                <a href="${fn:escapeXml(productUrl)}" class="stockLevel">
                                                <span class="outstock"><i class="fas fa-times-circle"></i><spring:theme
                                                        code='product.variants.out.of.stock' /></span>
                                </a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${product.unitFactorRangeData[0].inStock}">
                                    <a href="${fn:escapeXml(productUrl)}" class="stockLevel hidden">
                                                    <span class="OutOfStock "><i
                                                            class="fas fa-times-circle"></i><spring:theme
                                                            code="product.variants.out.of.stock" /></span>
                                    </a>
                                </c:when>

                                <c:otherwise>
                                    <a href="${fn:escapeXml(productUrl)}" class="stockLevel">
                                                    <span class="OutOfStock "><i
                                                            class="fas fa-times-circle"></i><spring:theme
                                                            code="product.variants.out.of.stock" /></span>
                                    </a>
                                </c:otherwise>
                            </c:choose>


                        </c:otherwise>
                    </c:choose>
                </div>

            </div>
            <div class="details">
				<div class="section-one-height">
                <ycommerce:testId code="product_productName">
                    <a class="name" href="${fn:escapeXml(productUrl)}">${product.name}</a>
                </ycommerce:testId>
				
				
             <c:if test="${not empty product.brand}">
             
                    <c:if test="${not empty product.brand.name}">
                       <div class="brand-name ">
                        ${product.brand.name}
                               </div>
                    </c:if></c:if>
         
                
                <c:if test="${not empty product.productLabel}">
                    <div class="labelText ">${fn:escapeXml(product.productLabel)}</div>
                </c:if>
                    <%--                <div class="">
                                        <c:if test="${not empty product.countryOfOriginIsocode}">
                                            ${fn:escapeXml(product.countryOfOriginIsocode)}
                                        </c:if>
                                    </div>--%>
                <div class="countryCode ">

                    <c:if test="${not empty product.countryOfOrigin}">

                        <c:set value="${fn:replace(product.countryOfOrigin, ' ', '-')}" var="countryOfOrigin"></c:set>
                        <i class="flagicon ${fn:toLowerCase(countryOfOrigin)}"></i>${product.countryOfOrigin}
                    </c:if>
                </div>
</div>

                <c:if test="${not empty product.potentialPromotions}">
                    <div class="promo">
                        <c:forEach items="${product.potentialPromotions}" var="promotion">
                            ${ycommerce:sanitizeHTML(promotion.description)}
                        </c:forEach>
                    </div>
                </c:if>
                <c:set var="product" value="${product}" scope="request" />
                <c:set var="addToCartText" value="${addToCartText}" scope="request" />
                <c:set var="addToCartUrl" value="${addToCartUrl}" scope="request" />
                <c:set var="isGrid" value="true" scope="request" />
                <div class="addtocart">

   <c:if test="${not empty product.unitFactorRangeData}">
               <div>     <span class="price-per-unit ">${product.price.currencyIso}&nbsp;${product.unitPrice.value} <span class="unit"> / ${product.unit.unit}</span></span></div>
                </c:if>
                  <c:if test="${ empty product.unitFactorRangeData}">
                  <div class="min-height"> </div>
                  </c:if>
                    <div class=" price-cont">
                        <c:if test="${ product.expressDelivery}">
                           <span class="expressdelivery"><img
                                   src="${fn:escapeXml(themeResourcePath)}/images/Express.svg" />
											</span>
                        </c:if>

                        <c:choose>
                            <c:when test="${not empty product.unitFactorRangeData}">


                                <c:choose>
                                    <c:when test="${not empty product.unitFactorRangeData[0].discountUnitPrice}">
                                   
                                     <span class="disCountPrice">  
                                     <span class="scratched">
                                        <span class="Pricescratched">  
                                        <format:fromPrice priceData="${product.unitFactorRangeData[0].discountUnitPrice.price}" /></span>
                                    <span class="line_dis ${display_inline} "></span></span>
                                        </span>

                                        <span class="price discountPricePlp">

                                           <format:fromPrice
                                                   priceData="${product.unitFactorRangeData[0].discountUnitPrice.discountPrice}" />


                                   </span>


                                    </c:when>
                                    <c:otherwise>

								 <span class="disCountPrice">  
								       
								       <span class="scratched hidden">
                                          <span class="Pricescratched"> </span>
                                           <span class="line_dis hidden "></span>
                                       </span>
                                 </span>

                                     <span class="price">
                                    <format:fromPrice priceData="${product.unitFactorRangeData[0].price}" />
                                          

                                    </span>
                                    
                                   

                                    </c:otherwise>

                                </c:choose>


                            </c:when>
                            <c:otherwise>


                                <c:choose>

                                    <c:when test="${not empty product.discount}">
                                        <c:set value="display_in" var="display_inline"></c:set>
                                        <span class="scratched"><format:fromPrice
                                                priceData="${product.discount.price}" /><span
                                                class="line_dis ${display_inline} "></span></span>

                                        <span class="price">




                                                <format:fromPrice priceData="${product.discount.discountPrice}" />

                                    </span>


                                        <%--                                        <span class="discount_style">(${product.discount.percentage})</span> --%>

                                    </c:when>
                                    <c:otherwise>

                                        <span class="scratched"></span>
                                        <product:productPricePanel product="${product}" />

                                    </c:otherwise>

                                </c:choose>


                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="actions-container-for-${fn:escapeXml(component.uid)} <c:if test="${ycommerce:checkIfPickupEnabledForStore() and product.availableForPickup}"> pickup-in-store-available</c:if>">
                            <%-- 				<action:actions element="div" parentComponent="${component}"/> --%>
                        <product:addtocartcarousel showQuantityBox="true" product="${product}" />
                    </div>
                </div>
                <ycommerce:testId code="product_productPrice">

                </ycommerce:testId>
                <c:forEach var="variantOption" items="${product.variantOptions}">
                    <c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
                        <c:if test="${variantOptionQualifier.qualifier eq 'rollupProperty'}">
                            <c:set var="rollupProperty" value="${variantOptionQualifier.value}" />
                        </c:if>
                        <c:if test="${variantOptionQualifier.qualifier eq 'thumbnail'}">
                            <c:set var="imageUrlHtml" value="${fn:escapeXml(variantOptionQualifier.value)}" />
                        </c:if>
                        <c:if test="${variantOptionQualifier.qualifier eq rollupProperty}">
                            <c:set var="variantNameHtml" value="${fn:escapeXml(variantOptionQualifier.value)}" />
                        </c:if>
                    </c:forEach>
                    <img style="width: 32px; height: 32px;" src="${imageUrlHtml}" title="${variantNameHtml}"
                         alt="${variantNameHtml}" />
                </c:forEach>


            </div>


        </ycommerce:testId>
    </div>
</div>