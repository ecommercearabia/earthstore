<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="account-section-header">
	
			<spring:theme code="text.account.profile.updatePersonalDetails" />

</div>

<div class="row">
	<div class="container-lg col-md-8 col-md-offset-2">
		<div class="account-section-content">
			<div class="account-section-form">
				<form:form action="update-profile" method="post"
					modelAttribute="updateProfileForm">
					<div class="row">
					<div class="col-sm-12 col-md-2">
					<formElement:formSelectBoxDefaultEnabled idKey="profile.title" labelKey="profile.title" path="titleCode" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.none" items="${titleData}" selectCSSClass="form-control" />
					</div>
					<div class="col-sm-12 col-md-5">
					<formElement:formInputBox idKey="profile.firstName" placeholder="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" />
					</div>
					<div class="col-sm-12 col-md-5">
					<formElement:formInputBox idKey="profile.lastName" placeholder="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" />
					</div>
					<div class="col-sm-12 col-md-4">
				    <formElement:formCountrySelectBoxDefaultEnabled idKey="mobileCountry" labelKey="profile.mobileCountry" selectCSSClass="form-control f16 "	path="mobileCountry" mandatory="true" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${mobileCountries}" />
					</div>
					<div class="col-sm-12 col-md-8">
					<formElement:formInputBox idKey="profile.mobileNumber" labelKey="profile.mobileNumber" path="mobileNumber" inputCSS="form-control"	mandatory="true" placeholder="register.mobileNumber" />	
					</div>
					
					
					
					<c:if test="${cmsSite.birthOfDateCustomerEnabled}">
					<div class="col-sm-12 col-md-12 dateInput">
						<div
							class="<c:if test="${cmsSite.birthOfDateCustomerHidden}">hidden</c:if>">
							<formElement:formInputBox idKey="profile.dob" labelKey="profile.dob" inputCSS="form-control" mandatory="${cmsSite.birthOfDateCustomerRequired}" path="birthDate" placeholder="profile.dob" />
													
						</div>
					</div>
					</c:if>
					
					
					
					<c:if test="${cmsSite.customerMaritalStatusEnabled}">
					<div class="col-sm-12 col-md-12 checkboxMartal">
						<div class="<c:if test="${cmsSite.customerMaritalStatusHidden}">hidden</c:if>">
							<formElement:formSelectBoxDefaultEnabled idKey="profile.maritalStatus" labelKey="profile.maritalStatus" selectCSSClass="form-control hidden" path="maritalStatusCode" mandatory="${cmsSite.customerMaritalStatusRequired}" skipBlank="false" skipBlankMessageKey="form.select.none" items="${maritalStatuses }" />
						
						<span class="titleStatus"><spring:theme code="Marital.Status"/></span>

		<label class="container firstCheckout"><spring:theme code="Single"/>
  <input type="radio"  name="radioMaritalprofile" value="SINGLE">
  <span class="checkmark"></span>
</label>
<label class="container"><spring:theme code="Married"/>
  <input type="radio" name="radioMaritalprofile" value="MARRIED">
  <span class="checkmark"></span>
</label>
						
						</div>
						</div>
					</c:if>
					
					<c:if test="${cmsSite.nationalityCustomerEnabled}">
					<div class="col-sm-12 col-md-12">
						<div
							class="<c:if test="${cmsSite.nationalityCustomerHidden}">hidden</c:if>">
							
							<formElement:formSelectBoxDefaultEnabled idKey="profile.nationality" labelKey="profile.nationality" selectCSSClass="form-control" path="nationality" mandatory="${cmsSite.nationalityCustomerRequired}" skipBlank="false" skipBlankMessageKey="form.select.none" items="${nationalities}" />
						</div>
						</div>
					</c:if>

					<c:if test="${cmsSite.nationalityIdCustomerEnabled}">
					<div class="col-sm-12 col-md-12">
						<div
							class="<c:if test="${cmsSite.nationalityIdCustomerHidden}">hidden</c:if>">
							<div class="checkbox profilecheckbox">
							
							
							<label class="control-label uncased">
				<span class="hidden"> <form:checkbox id="nationalIdBox" path="hasNationalId" disabled="false"/></span>
		
			
				
				<label class="el-switch">
					<input  type="checkbox" name="switchNationalform" id="switchNationalform">
					<span class="el-switch-style"></span>
					<span class="margin-r"><spring:theme code="profile.emirates.id.label" /></span>
				</label>
			<div class="FormNational">
										<formElement:formInputBox idKey="profile.national" placeholder="profile.national" labelKey="profile.national" path="nationalityId" inputCSS="form-control" mandatory="${cmsSite.nationalityIdCustomerRequired}" />
		     <button class="btn">			 
			<spring:theme code="Verify"/>
			</button>
			</div>
			</label>
							
							
							
							
							
							
								
							</div>
						</div>
						</div>
					</c:if>

















			
<!-- 					<div class="col-sm-12 col-md-12"> -->
<%-- 					<spring:theme code="profile.involvedInLoyaltyProgram" var="involvedInLoyaltyProgram" htmlEscape="false"/> --%>
<%-- 					<template:errorSpanField path="involvedInLoyaltyProgram"> --%>
<!-- 						<div class="checkbox"> -->
<!-- 			<label class="control-label uncased"> -->
<%-- 				<span class="hidden"><form:checkbox id="involvedInLoyaltyProgram" path="involvedInLoyaltyProgram" disabled="false"/> --%>
<!-- 				</span> -->
<!-- 				<label class="el-switch"> -->
<!-- 					<input  type="checkbox" name="involvedInLoyalty" id="involvedInLoyalty"> -->
<!-- 					<span class="el-switch-style"></span> -->
<%-- 					<span class="margin-r">${ycommerce:sanitizeHTML(involvedInLoyaltyProgram)}</span> --%>
<!-- 				</label>	 -->
<!-- 			<div> -->
				
<!-- 			</div> -->
				
				
				
<!-- 			</label> -->
<!-- 		</div> -->
<%-- 					</template:errorSpanField> --%>
<!-- 					</div> -->
						<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 col-sm-offset-2 col-md-offset-6 col-lg-offset-6   accountButtons">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_savePersonalDetails_button">
									<button type="submit" class="btn btn-primary btn-block">
										<spring:theme code="text.account.profile.saveUpdates"
											text="Save Updates" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 accountButtons">
							<div class="accountActions">
								<ycommerce:testId
									code="personalDetails_cancelPersonalDetails_button">
									<button type="button"
										class="btn btn-default btn-block backToHome">
										<spring:theme code="text.account.profile.cancel" text="Cancel" />
									</button>
								</ycommerce:testId>
							</div>
						</div>
					
					
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>