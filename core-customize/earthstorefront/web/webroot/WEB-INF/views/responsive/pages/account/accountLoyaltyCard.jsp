<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="row">
		<div class="container-lg col-md-10 col-lg-8 col-md-offset-1">
		
<c:if test="${ not empty pageTitle}">
<c:set var="titlePage" value="${fn:split(pageTitle, '|')}" />
</c:if>
<c:choose>

<c:when test="${loyaltyEnabled}">
<span class="titleotherPages">
${titlePage[0]}
</span>
<br/>


<span class="loyaltypoint"><b><spring:theme code='text.account.loyaltycard.availablepoints'/> <span class="green_color"> ${loyaltyBalanceData.balancePoints}</span><span class="sepratechar">|</span></b></span>
<span class="loyaltyamount"><b><spring:theme code='text.account.loyaltycard.availableamount'/> <span class="green_color">  ${loyaltyBalanceData.balanceAmount.formattedValue}</span></b></span>
<c:set var="hideclass" value=""/>
<c:if test="${not empty transactionHistory.loyaltyRedeemVouchers and not empty transactionHistory.loyaltyTransactionPointHistories}">
	<c:set var="hideclass" value="hidden"/>	
<ul class="center-text tabsnew">
		<li><a href="javascript:;" class="transactiontable active"><spring:theme code='text.account.loyalty.transaction.point'/></a></li>
		<li class="sep">|</li>
		<li><a href="javascript:;" class="redeemtable"><spring:theme code='text.account.loyalty.redeem.voucher'/></a></li>
	</ul>

</c:if>

<c:if test="${not empty transactionHistory.loyaltyTransactionPointHistories}">
	<div class="account-overview-table loyaltyTable transactiontablecon">
		<h5><b><spring:theme code='text.account.loyalty.transaction.point'/></b></h5>
		<br/>
					<table class="orderhistory-list-table responsive-table">
						<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
								<th id="header1" class="head_account_cridit"><spring:theme code='text.account.loyalty.date'/></th>
		                        <th id="header3" class="head_account_cridit"><spring:theme code='text.account.loyalty.initialAmount'/></th>
						</tr>
						<c:forEach items="${transactionHistory.loyaltyTransactionPointHistories}" var="loyaltyTransactionPointHistory">
							<tr class="responsive-table-item">
								<ycommerce:testId code="orderHistoryItem_orderDetails_link">
									
									<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.date'/></td>
									<td class="responsive-table-cell">
										${loyaltyTransactionPointHistory.createdDate}
									</td>
									<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.initialAmount'/></td>
									<td class="responsive-table-cell">
										${loyaltyTransactionPointHistory.amount}
									</td>
									
								</ycommerce:testId>
							</tr>
						</c:forEach>
					</table>
	            </div>
</c:if>

<c:if test="${not empty transactionHistory.loyaltyRedeemVouchers}">




	<div class="account-overview-table loyaltyTable redeemtablecon ${hideclass}">
		<h5><b><spring:theme code='text.account.loyalty.redeem.voucher'/></b></h5>
<br/>
					<table class="orderhistory-list-table responsive-table">
						<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
								<th id="header1" class="head_account_cridit"><spring:theme code='text.account.loyalty.date'/></th>
		                        <th id="header3" class="head_account_cridit"><spring:theme code='text.account.loyalty.initialAmount'/></th>
						</tr>
						<c:forEach items="${transactionHistory.loyaltyRedeemVouchers}" var="loyaltyRedeemVoucher">
							<tr class="responsive-table-item">
								<ycommerce:testId code="orderHistoryItem_orderDetails_link">
									
									<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.date'/></td>
									<td class="responsive-table-cell">
										${loyaltyRedeemVoucher.purchaseDate}
									</td>
									<td class="hidden-sm hidden-md hidden-lg"><spring:theme code='text.account.loyalty.initialAmount'/></td>
									<td class="responsive-table-cell">
										${loyaltyRedeemVoucher.redeemedPoints}
									</td>
									
								</ycommerce:testId>
							</tr>
						</c:forEach>
					</table>
	            </div>
</c:if>
            
 </c:when>
 
 <c:otherwise>
		
			<c:choose>
				<c:when test="${needUpdateProfile}">
				<c:url var="updateProfileURL" value="/my-account/update-profile" scope="page"/>
				
					<p><spring:theme code="msg.loyalty.needupdate.msg"/></p>
								<a href="${ updateProfileURL}">update profile</a>
				
				</c:when>
			
			<c:otherwise>
				<c:url var="registerUserURL" value="/my-account/loyalty-card/reg" scope="page"/>
 		<form action="${registerUserURL}" id="loyaltyForm" method="GET">
			<p><spring:theme code="msg.loyalty.msg"/></p>
			<button type="submit" class="btn btn-primary btn-block re-order" id="reorderButton">
				<spring:theme code="text.account.loyalty.register"/>
			</button>
				</form>
			</c:otherwise>
			
			</c:choose>
			
	
 
 
 
 
 
 </c:otherwise>
            
  </c:choose>

</div>
</div>


