<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<span class="title"><spring:theme code="download.Our.App"/><span class="far fa-plus hidden-lg hidden-md"></span></span>  
<div class="footer__nav--links"> 
<a class="apple_store" href="https://apps.apple.com/jo/app/earth-supermarket/id1619611608"  target="_blank">
<img src="${fn:escapeXml(themeResourcePath)}/images/app.svg" class=""/></a>


<a class="googleStore" href="https://play.google.com/store/apps/details?id=com.erabia.earth.prod"  target="_blank">
<img src="${fn:escapeXml(themeResourcePath)}/images/play.svg" class=""/>
</a>
</div>      