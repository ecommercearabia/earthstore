<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tabhead">
	<a href="">${fn:escapeXml(title)}</a> <i class="fal fa-chevron-right"></i><i class="fal fa-chevron-down"></i>
</div>
<div class="tabbody">
	<div class="container-lg">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-container">
					<product:productDetailsClassifications product="${product}" />
				</div>
			</div>
		</div>
	</div>
</div>

