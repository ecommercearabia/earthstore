<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="rotatingImagesComponent"
	tagdir="/WEB-INF/tags/responsive/component/rotatingImagesComponent"%>
<c:if test="${component.visible && not empty banners}">
	<c:choose>
		<c:when test="${component.theme eq 'FOR_YOUR_CONVENIENCE'}">
			<rotatingImagesComponent:forYourConvenience
				banners="${banners}" />
		</c:when>
		<c:when test="${component.theme eq 'RE_ORDER'}">
			<rotatingImagesComponent:reOrder
				banners="${banners}" />
		</c:when>
  		<c:when test="${component.theme eq 'TOP_BANNER'}">
			<rotatingImagesComponent:topBanner
				banners="${banners}" />
		</c:when>
		<c:when test="${component.theme eq 'HEALTHY'}">
			<rotatingImagesComponent:healthy
				banners="${banners}" />
		</c:when>
		<c:otherwise>
		<rotatingImagesComponent:brands
				banners="${banners}" />
		</c:otherwise>
	</c:choose>
</c:if>
