<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<input class="defaultCountry" type="hidden" value="${defaultCountry.isocode}">
<input class="defaultArea" type="hidden" value="${defaultArea.code}">
<input class="defaultCity" type="hidden" value="${defaultCity.code}">
<input class="googlemapreference" type="hidden" value="${defaultCity.googleMapReference}">








<div class="deliverypopup hidden">
    <div class="popupBoxdelivery">
        <div class="deliveryContent">
            <div class="headerdeliveryBox">
                <span class="titleheaderdelivery"><spring:theme code="Choose.your.Delivery"/></span>
                <span class="closepopupdelivery"><i class="fal fa-times"></i></span>
            </div>
            <div class="pagesdeliveryBox">
                <div class="default-view">
                    <div class="select-default">
                        <div class="title-select"><span class="title-select-text"><spring:theme
                                code="City.Selected"/></span>:<span
                                class="title-select-value">${defaultCity.name}</span></div>
                        <div class="btnChange"><spring:theme code="Change"/></div>


                    </div>
                    <div class="titleSerachdelivery"><spring:theme code="Select.area"/></div>

                    <div class="inputSerach">

                        <input id="searchInputText" class="form-control "
                               placeholder='<spring:theme code="Select.area"/>'>
                        <i class="fal fa-search"></i>
                    </div>

                    <ul id="itemListdata" class="itemListPlaces">

                    </ul>

                </div>
                <div class="select-city hidden">
                    <div class="titleSerachdelivery"><spring:theme code="Select.city"/></div>

                    <div class="inputSerach">

                        <input id="searchInputTextCity" class="form-control "
                               placeholder='<spring:theme code="Select.city"/>'>
                        <i class="fal fa-search"></i>
                    </div>

                    <ul id="itemListdataCity" class="itemListPlaces">


                </div>


            </div>
        </div>
    </div>
</div>
<div class="deliveryhead">
    <div class="row w1300">
    <c:if test="${not empty cmsSite.contactUsNumber}">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 left_side hidden-xs hidden-sm">
            <a href="tel:${cmsSite.contactUsNumber}">
                <i class="fas fa-phone-alt"></i>
                <span class="phone-number-text">${cmsSite.contactUsNumber}</span>
            </a>

        </div>
        </c:if>
        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 right_side ">
        
            <c:if test="${not empty cartMinAmount}"><div class="minorder hidden-xs hidden-sm"><spring:theme code="minimum.Order.lbl"/> <span class="minval">${cartMinAmount}</span></div></c:if>
            <div><span class="title_part_location"><i class="fas fa-map-marker-alt"></i><spring:theme
                    code="Deliver.To"/> </span> <a href="javaScript:;" class="changeLocation">
                                       
                   ${timeDeliveryLocationData.area.name}
                - ${timeDeliveryLocationData.city.name} </a></div>


            <div ><span class="title_part_drive"><i class="fas fa-truck"></i> <spring:theme code="next.Drive"/></span> <a
                    href="javaScript:;" class="time-format">${timeDeliveryLocationData.formatedTime}</a></div>
            <span class="hidden-xs hidden-sm">
	<header:languageSelector languages="${languages}" currentLanguage="${currentLanguage}"/>


	<header:currencySelector currencies="${currencies}" currentCurrency="${currentCurrency}"/>
	</span>
            <div class="hidden">


                Default Country: ${defaultCountry.isocode}   </br>
                Default Area: ${defaultArea.code}  </br>
                Default City: ${defaultCity.code}   </br>


                Selected City:${selectedCityCode}    </br>
                Selected Area :${selectedAreaCode}   </br>

                isAnonymousUser: ${isAnonymousUser}  </br>

                ${timeDeliveryLocationData.area.code} </br>

                ${timeDeliveryLocationData.startTime} </br>
                ${timeDeliveryLocationData.endTime} </br>
                ${timeDeliveryLocationData.formatedDeliveryLocationMessage} </br>
                ${timeDeliveryLocationData.formatedDeliveryTimeMessage} </br>
                ${timeDeliveryLocationData.formatedTime} </br>
            </div>
        </div>
    </div>

</div>
