<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<spring:htmlEscape defaultHtmlEscape="true" />


<c:choose>
    <c:when test="${not empty categories}">


        <div class="carousel__component categories">
            <div class="titleSection"><c:if test="${not empty title}">
                <div class="carousel__component--headline">${fn:escapeXml(title)}

                    <c:if test="${not empty component.link}">
                        <span class="linkView">	<cms:component component="${component.link}" /></span>
                    </c:if>
                </div>
            </c:if>

            </div>
            <div class="carousel__component--carousel js-owl-carousel owl-carousel js-owl-categories-page owl-theme">
                <c:forEach items="${categories}" var="category">

                    <div class="carousel__item">
                        <div class="banner__component simple-banner box_img text-center">
                            <c:url value="${category.url}" var="categoryUrl" />
                            <c:if test="${ycommerce:validateUrlScheme(category.image.url)}">
                                <c:choose>
                                    <c:when test="${empty categoryUrl || categoryUrl eq '#' || !ycommerce:validateUrlScheme(categoryUrl)}">
                                        <img title="${fn:escapeXml(media.altText)}"
                                             alt="${fn:escapeXml(category.image.altText)}"
                                             src="${fn:escapeXml(category.image.url)}">
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${fn:escapeXml(categoryUrl)}" class="img-cont"><img
                                                title="${fn:escapeXml(category.image.altText)}"
                                                alt="${fn:escapeXml(category.image.altText)}"
                                                src="${fn:escapeXml(category.image.url)}"></a>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>

                            <h2 class="titleBox"><a href="${fn:escapeXml(categoryUrl)}">${fn:escapeXml(category.name)}</a></h2>
                                <%--             ${category.image.imageType}<br />
                                            ${category.image.format}<br />
                                            ${category.image.url}<br />
                                            ${category.image.altText}<br />
                                            ${category.image.galleryIndex}<br />
                                            ${category.image.width}<br />--%>

                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>
    </c:when>
    <c:otherwise>
        <component:emptyComponent />
    </c:otherwise>
</c:choose>

