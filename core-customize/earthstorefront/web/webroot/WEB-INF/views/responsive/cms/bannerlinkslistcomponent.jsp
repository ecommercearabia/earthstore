<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="bannerLinksListComponent"
	tagdir="/WEB-INF/tags/responsive/component/bannerLinksListComponent"%>

<c:if test="${not empty bannerLinksList}">
	<c:choose>
		<c:when test="${bannerLinksList.theme eq 'RECOMMENDED_FOR_YOU'}">
			<bannerLinksListComponent:recommendedForYou
				bannerLinksList="${bannerLinksList}" />
		</c:when>
		<c:when test="${bannerLinksList.theme eq 'CATEGORIES'}">
			<bannerLinksListComponent:categories
				bannerLinksList="${bannerLinksList}" />
		</c:when>
		<c:when test="${bannerLinksList.theme eq 'FRESH_FOOD'}">
			<bannerLinksListComponent:freshFood
				bannerLinksList="${bannerLinksList}" />
		</c:when> 
 		<c:when test="${bannerLinksList.theme eq 'FRESH_FOOD_EXPRESS'}"> 
 			<bannerLinksListComponent:freshFoodExpress
 				bannerLinksList="${bannerLinksList}" />
		</c:when>  
  		<c:when test="${bannerLinksList.theme eq 'FOOD_CUPBOARD'}">
			<bannerLinksListComponent:foodCupboard
				bannerLinksList="${bannerLinksList}" />
		</c:when>
		 <c:when test="${bannerLinksList.theme eq 'FOOD_CUPBOARD_EXPRESS'}">
			<bannerLinksListComponent:foodCupboardExpress
				bannerLinksList="${bannerLinksList}" />
		</c:when>
	</c:choose>
</c:if>
