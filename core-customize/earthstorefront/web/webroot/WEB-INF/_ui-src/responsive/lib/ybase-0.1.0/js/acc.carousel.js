ACC.carousel = {

    _autoload: [
        ["bindCarousel", $(".js-owl-carousel").length > 0],
        "bindJCarousel"
    ],

    carouselConfig: {
        "default-1": {},
        "default-2": {},
        "default-3": {},
        "default-4": {},
        "default-5": {},
        "default-6":{},
        "pdp-img-mobile": {},
        "default": {},
        "mainbanner": {},
        "rotating-image-banner-two": {},
        "rotating-image": {},
        "rotating-image-banner-new": {},
        "lazy-reference": {},
        "categories": {},
        "categories-page": {},
        "freshFood": {},
        "boardExpress":{},
        "board": {},

        "slot-time": {}
    },

    bindCarousel: function () {

        $(".js-owl-carousel").each(function () {
            var $c = $(this);
            var autoRotate = $(this).parent('.carousel__component').find('.autoRotate').val();
            if (autoRotate === 'true') {
                autoRotate = '1000';
            } else {
                autoRotate = 'false';
            }
            var displayNavigation = $(this).parents('.carousel-component').find('.displayNavigation').val();
            if (displayNavigation === 'true') {
                displayNavigation = 'true';
            } else {
                displayNavigation = 'false';
            }
            var displayPagination = $(this).parents('.carousel-component').find('.displayPagination').val();
            if (displayPagination === 'true') {
                displayPagination = 'true';
            } else {
                displayPagination = 'false';
            }

            $.each(ACC.carousel.carouselConfig, function (key, config) {
                if ($c.hasClass("js-owl-" + key)) {
                    var $e = $(document).find(".js-owl-" + key);
                    //$e.owlCarousel(config);
                    if (key == 'rotating-image') {
                        $e.owlCarousel({
                            dots: true,
                            nav: false,

                            navText: ["<span class='far fa-chevron-left'></span>", "<span class='far fa-chevron-right'></span>"],
                            items: 1,
                            autoplay: true,
                            loop: true,
                            rewind: false,
                            autoplayTimeout: 5000,

                            animateOut: 'fadeOut',
                            animateIn: 'fadeIn',
                            autoplayHoverPause: false,
                            smartSpeed: 450,
                            mouseDrag: false,
                            touchDrag: false,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false


                                },
                                480: {
                                    items: 1,
                                    dots: false



                                },
                                481:{
                                    dots: true,
                                }
                            }

                        });

                    }



                    if (key == 'boardExpress') {
                        $e.owlCarousel({
                            dots: false,
                            nav: true,

                            navText: ["<span class='fal fa-chevron-left'></span>", "<span class='fal fa-chevron-right'></span>"],
                            items: 2,

                            margin: 30,

                            responsive: {
                                0: {
                                    items: 1,
                                    margin: 5,
                                    stagePadding: 0,


                                },
                                480: {
                                    items: 1,



                                },
                                1024: {
                                    items: 2,

                                }


                            }

                        });

                    }





                    if (key == 'board') {
                        $e.owlCarousel({
                            dots: false,
                            nav: true,

                            navText: ["<span class='fal fa-chevron-left'></span>", "<span class='fal fa-chevron-right'></span>"],
                            items: 2,

                            margin: 30,

                            responsive: {
                                0: {
                                    items: 1,
                                    margin: 5,
                                    stagePadding: 0,


                                },
                                480: {
                                    items: 1,
                                    rewind: true,
                                    center: true,
                                    margin: 15,
                                    stagePadding: 40,


                                },
                                1024: {
                                    items: 2,

                                }


                            }

                        });

                    }


                    if (key == 'rotating-image-banner-two') {
                        $e.owlCarousel({
                            dots: false,
                            nav: true,

                            navText: ["<span class='fal fa-chevron-left'></span>", "<span class='fal fa-chevron-right'></span>"],
                            items: 2,
                            autoplay: false,
                            loop: true,
                            rewind: false,
                            autoplayTimeout: 5000,
                            margin: 30,

                            responsive: {
                                0: {
                                    items: 1,


                                },
                                480: {
                                    items: 1,

                                },
                                1024: {
                                    items: 2,

                                }

                            }

                        });

                    }

                    if (key == 'freshFood') {
                        $e.owlCarousel({
                            dots: false,
                            nav: true,

                            navText: ["<span class='fal fa-chevron-left'></span>", "<span class='fal fa-chevron-right'></span>"],
                            items: 4,
                            autoplay: false,
                            loop: true,
                            rewind: false,
                            autoplayTimeout: 5000,
                            margin: 30,

                            responsive: {
                                0: {
                                    items: 1,


                                },
                                480: {
                                    items: 2,

                                },
                                768: {
                                    items: 3,

                                },
                                1200: {
                                    items: 4,

                                }

                            }

                        });

                    }


                    if (key == 'categories') {
                        $e.owlCarousel({
                            dots: false,
                            nav: false,
                            margin: 10,
                            navText: ["<span class='far fa-chevron-left'></span>", "<span class='far fa-chevron-right'></span>"],
                            items: 4,
                            autoplay: false,
                            loop: true,
                            rewind: false,
                            responsive: {
                                0: {
                                    items: 2,
                                    dots: false,
                                    nav: false,
                                    stagePadding: 40,
                                    margin:10,
                                },
                                480: {
                                    items: 2,
                                    dots: false,
                                    nav: false,stagePadding: 40,
                                    margin:10,
                                },
                                768: {
                                    items: 3,
                                    stagePadding: 40, margin:10,

                                },
                                1024: {
                                    items: 4, margin:10,stagePadding: 40,
                                },
                                1300: {
                                    items: 5,
                                    margin:5
                                },
                                1400: {
                                    items: 6,
                                    margin:5
                                }
                            }

                        });

                    }
                    if (key == 'categories-page') {
                        $e.owlCarousel({
                            dots: false,
                            nav: false,
                            margin: 10,
                            navText: ["<span class='far fa-chevron-left'></span>", "<span class='far fa-chevron-right'></span>"],
                            items: 4,
                            autoplay: false,
                            loop: true,
                            rewind: false,
                            responsive: {
                                0: {
                                    items: 2,
                                    dots: false,
                                    nav: false,
                                    stagePadding: 40,
                                    margin:10,
                                },
                                480: {
                                    items: 2,
                                    dots: false,
                                    nav: false,stagePadding: 40,
                                    margin:10,
                                },
                                768: {
                                    items: 3,
                                    stagePadding: 40, margin:10,

                                },
                                1024: {
                                    items: 4, margin:10,stagePadding: 40,
                                },
                                1300: {
                                    items: 5,
                                    margin:5,
                                    stagePadding: 60,
                                },
                                1400: {
                                    items: 6,
                                    margin:5,
                                    stagePadding: 60,
                                }
                            }

                        });

                    }
                    if (key == 'default-3') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 30,
                            center: true,
                            autoplay: false,
                            loop: true,
                            autoplayHoverPause: true,
                            rewind: true,
                            slideTransition: 'linear',
                            items: 3,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: true,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false
                                },
                                480: {
                                    items: 2,
                                    dots: false
                                },
                                768: {
                                    items: 3,

                                }
                            }


                        });
                    }
                    if (key == 'rotating-image-banner-new') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 20,
                            stagePadding: 30,


                            autoplayHoverPause: true,
                            rewind: true,
                            slideTransition: 'linear',
                            items: 6,
                            navText: ["<span class='fas fa-chevron-left'></span>", "<span class='fas fa-chevron-right'></span>"],
                            dots: true,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false,
                                    nav: true
                                },
                                480: {
                                    items: 2,
                                    dots: false,
                                    nav: true
                                },
                                768: {
                                    items: 5,
                                    nav: false

                                }
                            }


                        });
                    }
                    if (key == 'default-2') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 30,
                            center: true,
                            autoplay: true,
                            loop: true,
                            autoplayHoverPause: true,
                            rewind: true,
                            slideTransition: 'linear',
                            items: 2,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: true,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false
                                },
                                480: {
                                    items: 2,
                                    dots: false
                                },
                                768: {
                                    items: 2,

                                }
                            }


                        });
                    }
                    if (key == 'default-1') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 30,
                            center: true,
                            autoplay: true,
                            loop: true,
                            autoplayHoverPause: true,
                            animateOut: 'fadeOut',
                            animateIn: 'fadeIn',
                            smartSpeed: 450,
                            mouseDrag: false,
                            touchDrag: false,
                            rewind: true,
                            slideTransition: 'linear',
                            items: 1,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: displayPagination,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false
                                },
                                480: {
                                    items: 1,
                                    dots: false
                                },
                                768: {
                                    items: 1,

                                }
                            }


                        });
                    }
                    if (key == 'default-4') {
                        $e.owlCarousel({
                            nav: false,
                            margin: 10,

                            dots: false,
                            loop: true,

                            slideTransition: 'linear',
                            rewind: false,
                            items: 4,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: displayPagination,
                            responsive: {
                                0: {
                                    items: 1,
                                    margin: 0,
                                    stagePadding: 40,
                                    dots: false
                                },
                                480: {
                                    items: 2,
                                    margin: 0,
                                    stagePadding: 40, dots: false,
                                    dots: false
                                },
                                768: {
                                    items: 2,
                                    margin: 10,
                                    stagePadding: 40,
                                    dots: false
                                }
                            }


                        });
                    }
                    if (key == 'default-5') {
                        $e.owlCarousel({
                            nav: false,
                            margin: 10,
                            center: false,
                            autoplay: true,
                            loop: true,
                            autoplayHoverPause: true,
                            slideTransition: 'linear',
                            rewind: true,
                            dots: false,
                            items: 5,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: false,
                            responsive: {
                                0: {
                                    items: 3,
                                    margin: 5,
                                    stagePadding: 40,
                                    dots: false,
                                    loop: false,
                                },
                                480: {
                                    items: 4,
                                    margin: 5,
                                    stagePadding: 40,
                                    dots: false,
                                    loop: false,
                                },
                                768: {
                                    items: 5,
                                    margin: 5,
                                    stagePadding: 40,
                                    dots: false,
                                    loop: false,

                                },

                                900: {
                                    items: 6,
                                    margin: 5,
                                    stagePadding: 40,
                                    dots: false,
                                    loop: false,
                                }
                                ,
                                1020: {
                                    items: 8
                                }
                            }


                        });
                    }
                    if (key == 'default-6') {
                        $e.owlCarousel({
                            nav: false,
                            margin: 10,
                            center: false,
                            autoplay: false,
                            loop: false,
                            autoplayHoverPause: true,
                            slideTransition: 'linear',
                            rewind: false,
                            dots: false,
                            items: 4,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: false,
                            responsive: {
                                0: {
                                    items: 2,
                                    margin: 5,

                                    dots: false,
                                    loop: false,
                                },
                                480: {
                                    items: 3,
                                    margin: 5,

                                    dots: false,
                                    loop: false,
                                },
                                768: {
                                    items: 4,
                                    margin: 5,

                                    dots: false,
                                    loop: false,

                                },

                                900: {
                                    items: 4,
                                    margin: 5,
                                    dots: false,
                                    loop: false,
                                }
                                ,
                                1020: {
                                    items: 4
                                }
                            }


                        });
                    }
                    if (key == 'pdp-img-mobile') {
                        $e.owlCarousel({
                            nav: false,
                            margin: 0,
                            center: false,
                            autoplay: false,
                            loop: true,
                            autoplayHoverPause: false,
                            slideTransition: 'linear',
                            rewind: false,
                            dots: true,
                            items: 5,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: false,
                            responsive: {
                                0: {
                                    items: 1,


                                    dots: true,

                                },
                                480: {
                                    items: 1,

                                    dots: true,

                                },
                                768: {
                                    items: 1,

                                    dots: true,


                                },

                                900: {
                                    items: 1,

                                    dots: true,

                                }
                                ,
                                1020: {
                                    items: 7
                                }
                            }


                        });
                    }
                    if (key == 'default') {
                        $e.owlCarousel({
                            nav: false,

                            loop: true,

                            slideTransition: 'linear',
                            rewind: false,
                            items: 4,
                            navText: ["<span class='fal fa-chevron-left'></span>", "<span class='fal fa-chevron-right'></span>"],
                            dots: false,
                            responsive: {
                                0: {
                                    items: 1,
                                    margin: 15,
                                    stagePadding: 40,
                                    dots: false,
                                    nav: false,

                                },
                                600: {
                                    items: 2,
                                    margin: 15,
                                    stagePadding: 40,
                                    dots: false,
                                    nav: false,
                                },


                                900: {
                                    items: 3, margin: 15,
                                    stagePadding: 60,
                                }

                                ,
                                1000: {
                                    items: 3, margin: 15,
                                    stagePadding: 60,
                                },
                                1100: {
                                    items: 4, margin: 15,
                                    stagePadding: 60,
                                }
                                ,
                                1300: {
                                    items: 5,
                                    margin: 15,
                                    stagePadding: 60,
                                    nav: false
                                }
                            }


                        });
                    }


                    if (key == 'mainbanner') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 15,

                            center: false,
                            animateOut: 'fadeOut',
                            animateIn: 'fadeIn',
                            loop: true,
                            dots: true,
                            slideTransition: 'linear',
                            rewind: false,
                            autoplay: true,
                            autoplayTimeout: 6500,
                            autoplayHoverPause: true,
                            items: 1,
                            navText: ["<span class='fas fa-chevron-left'></span>", "<span class='fas fa-chevron-right'></span>"],


                        });
                    }
                    if (key == 'lazy-reference') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 30,
                            center: true,
                            autoplayHoverPause: true,
                            autoplay: true,
                            loop: true,
                            rewind: true,
                            items: 3,
                            navText: ["<span class='far fa-chevron-circle-left'></span>", "<span class='far fa-chevron-circle-right'></span>"],
                            dots: displayPagination,
                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false,

                                },
                                480: {
                                    items: 2,
                                    dots: false
                                },
                                768: {
                                    items: 3,

                                }
                            }


                        });
                    }
                    if (key == 'slot-time') {
                        $e.owlCarousel({
                            nav: true,
                            margin: 8,

                            center: false,
                            autoplayHoverPause: true,
                            autoplay: false,
                            loop: false,
                            rewind: false,
                            items: 3,
                            navText: ["<span class='fas fa-chevron-left'></span>", "<span class='fas fa-chevron-right'></span>"],
                            dots: false,

                            responsive: {
                                0: {
                                    items: 1,
                                    dots: false,
                                    center: true,

                                },
                                1024: {
                                    items: 2,
                                    dots: false
                                },

                                1200: {
                                    items: 3,

                                }
                            }


                        });
                    }


                }
            });
        });

    },

    bindJCarousel: function () {


        $(".modal").colorbox({
            onComplete: function () {
                ACC.common.refreshScreenReaderBuffer();
            },
            onClosed: function () {
                ACC.common.refreshScreenReaderBuffer();
            }
        });
        $('.svw').each(function () {
            $(this).waitForImages(function () {
                $(this).slideView({toolTip: true, ttOpacity: 0.6, autoPlay: true, autoPlayTime: 8000});
            });
        });
    }

};

