/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstocknotificationcustomaddon.constants;

/**
 * Global class for all Earthstocknotificationcustomaddon web constants. You can add global constants for your extension into this class.
 */
public final class EarthstocknotificationcustomaddonWebConstants // NOSONAR
{
	private EarthstocknotificationcustomaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
