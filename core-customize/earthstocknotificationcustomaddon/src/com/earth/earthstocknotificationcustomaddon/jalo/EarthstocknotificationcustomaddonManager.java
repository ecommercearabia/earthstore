/*
 *  
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstocknotificationcustomaddon.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.earth.earthstocknotificationcustomaddon.constants.EarthstocknotificationcustomaddonConstants;
import org.apache.log4j.Logger;

public class EarthstocknotificationcustomaddonManager extends GeneratedEarthstocknotificationcustomaddonManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( EarthstocknotificationcustomaddonManager.class.getName() );
	
	public static final EarthstocknotificationcustomaddonManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (EarthstocknotificationcustomaddonManager) em.getExtension(EarthstocknotificationcustomaddonConstants.EXTENSIONNAME);
	}
	
}
