/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthstocknotificationcustomaddon.constants;

/**
 * Global class for all Earthstocknotificationcustomaddon constants. You can add global constants for your extension into this class.
 */
public final class EarthstocknotificationcustomaddonConstants extends GeneratedEarthstocknotificationcustomaddonConstants
{
	public static final String EXTENSIONNAME = "earthstocknotificationcustomaddon";

	private EarthstocknotificationcustomaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
