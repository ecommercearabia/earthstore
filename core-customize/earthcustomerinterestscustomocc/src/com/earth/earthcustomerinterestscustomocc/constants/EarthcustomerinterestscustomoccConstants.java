/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomocc.constants;

/**
 * Global class for all earthcustomerinterestscustomocc constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public final class EarthcustomerinterestscustomoccConstants extends GeneratedEarthcustomerinterestscustomoccConstants
{
	public static final String EXTENSIONNAME = "earthcustomerinterestscustomocc"; //NOSONAR

	private EarthcustomerinterestscustomoccConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
