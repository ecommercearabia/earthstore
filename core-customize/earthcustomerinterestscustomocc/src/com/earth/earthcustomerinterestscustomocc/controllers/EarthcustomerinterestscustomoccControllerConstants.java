/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthcustomerinterestscustomocc.controllers;

/**
 */
public interface EarthcustomerinterestscustomoccControllerConstants
{
	// implement here controller constants used by this extension
}
