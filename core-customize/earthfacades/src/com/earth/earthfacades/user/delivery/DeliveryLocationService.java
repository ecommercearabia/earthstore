/**
 *
 */
package com.earth.earthfacades.user.delivery;

import java.util.Optional;

import com.earth.earthfacades.customer.DeliveryCityAreaData;
import com.earth.earthfacades.customer.TimeDeliveryLocationData;
import com.earth.earthtimeslotfacades.exception.TimeSlotException;


/**
 * @author mohammad-abumuhasien
 *
 */
public interface DeliveryLocationService
{
	public Optional<TimeDeliveryLocationData> saveCustomerDeliveryLocation(final String AreaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationData(final String areaCode, final String cityCode)
			throws TimeSlotException;

	public Optional<TimeDeliveryLocationData> getTimeDeliveryLocationDataForCurrentCustomer() throws TimeSlotException;

	Optional<DeliveryCityAreaData> getSelectedDeliveryCityAndArea();
}
