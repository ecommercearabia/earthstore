/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.user.customer.facade;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

import java.text.ParseException;
import java.util.List;

import com.earth.earthfacades.customer.data.MaritalStatusData;
import com.earth.earthloyaltyprogramprovider.exception.EarthLoyaltyException;


/**
 *
 */
public interface CustomCustomerFacade extends CustomerFacade
{

	CustomerData getCustomerByCustomerId(String id);

	boolean isCustomerExists(String customerId);

	List<MaritalStatusData> getAllMaritailStatuses();

	void updateSignatureIdCurrentCustomer(String signatureId);

	public void registerCustomerInLoyalty(CustomerModel customer) throws EarthLoyaltyException, ParseException;

	public void setLoyalty(CustomerModel customer, boolean isInvolvedInLoyalty) throws EarthLoyaltyException, ParseException;

	public void setLoyaltyByCurrentCustomer(boolean isInvolvedInLoyalty) throws EarthLoyaltyException, ParseException;


	public void registerCustomerInLoyaltyByCurrentCustomer() throws EarthLoyaltyException, ParseException;


}
