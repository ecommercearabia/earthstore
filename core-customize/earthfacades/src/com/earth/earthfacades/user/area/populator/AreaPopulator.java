/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.user.area.populator;

import de.hybris.platform.commercefacades.user.data.AreaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.earth.earthcore.model.AreaModel;


/**
 * The Class AreaPopulator.
 *
 * @author mnasro
 */
public class AreaPopulator implements Populator<AreaModel, AreaData>
{

	/**
	 * Fill the source to target.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final AreaModel source, final AreaData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setGoogleMapReference(source.getGoogleMapReference());
		if (source.getLatitude() != null)
		{
			target.setLatitude(source.getLatitude().toString());
		}

		if (source.getLongitude() != null)
		{

			target.setLongitude(source.getLongitude().toString());
		}
	}

}
