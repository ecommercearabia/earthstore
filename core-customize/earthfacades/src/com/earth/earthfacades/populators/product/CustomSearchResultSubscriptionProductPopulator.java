package com.earth.earthfacades.populators.product;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionfacades.data.BillingPlanData;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionfacades.data.TermOfServiceFrequencyData;
import de.hybris.platform.subscriptionfacades.data.TermOfServiceRenewalData;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceRenewal;
import de.hybris.platform.subscriptionservices.subscription.SubscriptionProductService;

import javax.annotation.Resource;


/**
 * SOLR Populator for subscription-capable {@link ProductModel}.
 *
 * @param <SOURCE>
 *           source class
 * @param <TARGET>
 *           target class
 */
public class CustomSearchResultSubscriptionProductPopulator<SOURCE extends SearchResultValueData, TARGET extends ProductData>
		implements Populator<SOURCE, TARGET>
{
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "subscriptionProductPricePlanPopulator")
	private Populator<ProductModel, ProductData> subscriptionPricePlanPopulator;
	@Resource(name = "termOfServiceRenewalConverter")
	private Converter<TermOfServiceRenewal, TermOfServiceRenewalData> termOfServiceRenewalConverter;
	@Resource(name = "subscriptionProductService")
	private SubscriptionProductService subscriptionProductService;

	@Override
	public void populate(final SOURCE source, final TARGET target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		final String billingTimeAsString = this.getValue(source, "billingTime");
		final BillingTimeData billingTime = new BillingTimeData();
		billingTime.setName(billingTimeAsString);

		if (target.getSubscriptionTerm() == null)
		{
			target.setSubscriptionTerm(new SubscriptionTermData());
			target.getSubscriptionTerm().setBillingPlan(new BillingPlanData());
		}

		if (target.getSubscriptionTerm().getBillingPlan() == null)
		{
			target.getSubscriptionTerm().setBillingPlan(new BillingPlanData());
		}

		target.getSubscriptionTerm().getBillingPlan().setBillingTime(billingTime);

		final String termOfServiceFrequencyAsString = this.getValue(source, "termLimit");
		final TermOfServiceFrequencyData termOfServiceFrequencyData = new TermOfServiceFrequencyData();
		termOfServiceFrequencyData.setName(termOfServiceFrequencyAsString);
		target.getSubscriptionTerm().setTermOfServiceFrequency(termOfServiceFrequencyData);

		ProductModel productModel = null;
		try
		{
			productModel = getProductService().getProductForCode(target.getCode());

		}
		catch (final Exception e)
		{

		}

		if (productModel != null && getSubscriptionProductService().isSubscription(productModel))
		{
			final PriceData oldPrice = target.getPrice();
			getSubscriptionPricePlanPopulator().populate(productModel, target);

			final TermOfServiceRenewal termOfServiceRenewal = productModel.getSubscriptionTerm().getTermOfServiceRenewal();
			target.getSubscriptionTerm().setTermOfServiceRenewal(getTermOfServiceRenewalConverter().convert(termOfServiceRenewal));
			// restore values from old price as they may have been overridden by the SubscriptionPricePlanPopulator
			if (oldPrice != null)
			{
				target.getPrice().setValue(oldPrice.getValue());
				target.getPrice().setCurrencyIso(oldPrice.getCurrencyIso());
				target.getPrice().setFormattedValue(oldPrice.getFormattedValue());
				target.getPrice().setMaxQuantity(oldPrice.getMaxQuantity());
				target.getPrice().setMinQuantity(oldPrice.getMinQuantity());
				target.getPrice().setPriceType(oldPrice.getPriceType());
			}
		}

	}

	protected <T> T getValue(final SOURCE source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	protected ProductService getProductService()
	{
		return productService;
	}



	protected Populator<ProductModel, ProductData> getSubscriptionPricePlanPopulator()
	{
		return subscriptionPricePlanPopulator;
	}



	protected Converter<TermOfServiceRenewal, TermOfServiceRenewalData> getTermOfServiceRenewalConverter()
	{
		return termOfServiceRenewalConverter;
	}



	/**
	 * @return subscription product service.
	 */
	protected SubscriptionProductService getSubscriptionProductService()
	{
		return subscriptionProductService;
	}


}
