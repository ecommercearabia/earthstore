package com.earth.earthfacades.populators.product;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.earth.earthfacades.units.UnitData;


/**
 * @author mnasro
 *
 */
public class UnitProductPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "unitConverter")
	private Converter<UnitModel, UnitData> unitConverter;

	/**
	 * @return the unitConverter
	 */
	protected Converter<UnitModel, UnitData> getUnitConverter()
	{
		return unitConverter;
	}
	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getUnit() != null)
		{
			target.setUnit(getUnitConverter().convert(source.getUnit()));
		}

	}
}
