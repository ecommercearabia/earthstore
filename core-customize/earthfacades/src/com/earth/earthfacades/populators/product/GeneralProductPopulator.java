/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.populators.product;


import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.earth.earthcore.model.BarcodeIdentifierModel;


/**
 * @author amjad.shati@erabia.com
 *
 */
public class GeneralProductPopulator implements Populator<ProductModel, ProductData>
{
	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	private static final String SUGAR_FREE = "EarthClassificationAE/1.0/EarthClassificationsAE.sugarfree";
	private static final String ORGANIC = "EarthClassificationAE/1.0/EarthClassificationsAE.organic";
	private static final String VEGAN = "EarthClassificationAE/1.0/EarthClassificationsAE.vegan";
	private static final String GLUTEN_FREE = "EarthClassificationAE/1.0/EarthClassificationsAE.glutenfree";
	private static final String ECO_FRIENDLY = "EarthClassificationAE/1.0/EarthClassificationsAE.ecofriendly";

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source == null || target == null)
		{
			return;
		}

		populateExpress(source, target);
		populateSugarFree(source, target);
		populateOrganic(source, target);
		populateVegan(source, target);
		populateGlutenFree(source, target);
		populateEcoFriendly(source, target);
		populateBrand(source, target);
		populateBarCode(source, target);

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateBarCode(final ProductModel source, final ProductData target)
	{
		source.getBarcodes();
		source.getBarcodeIdentifiers();

		target.setBarcodeIdentifiers(getsetBarcodeIdentifiers(source.getBarcodeIdentifiers()));

	}

	/**
	 * @param barcodeIdentifiers
	 * @return
	 */
	private List<String> getsetBarcodeIdentifiers(final List<BarcodeIdentifierModel> barcodeIdentifiers)
	{

		if (CollectionUtils.isEmpty(barcodeIdentifiers))
		{
			return java.util.Collections.emptyList();
		}

		final List<String> list = new ArrayList<>();
		for (final BarcodeIdentifierModel barcodeIdentifierModel : barcodeIdentifiers)
		{
			list.add(barcodeIdentifierModel.getCode());
		}

		return list;
	}

	private String isFoundClassification(final ProductModel source, final String featureByCode)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature feature = features.getFeatureByCode(featureByCode);
			return (feature != null && feature.getValue() != null
					&& StringUtils.isNotBlank(feature.getValue().getValue().toString())) ? "true" : "false";
		}
		return "false";
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateEcoFriendly(final ProductModel source, final ProductData target)
	{
		target.setEcoFriendly(isFoundClassification(source, ECO_FRIENDLY));

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateGlutenFree(final ProductModel source, final ProductData target)
	{
		target.setGlutenFree(isFoundClassification(source, GLUTEN_FREE));

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateVegan(final ProductModel source, final ProductData target)
	{
		target.setVegan(isFoundClassification(source, VEGAN));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateOrganic(final ProductModel source, final ProductData target)
	{
		target.setOrganic(isFoundClassification(source, ORGANIC));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateSugarFree(final ProductModel source, final ProductData target)
	{
		target.setSugarFree(isFoundClassification(source, SUGAR_FREE));
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateExpress(final ProductModel source, final ProductData target)
	{
		target.setExpressDelivery(source.isExpressDelivery());

	}

	/**
	 * @return the categoryConverter
	 */
	protected Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateBrand(final ProductModel source, final ProductData target)
	{
		if (CollectionUtils.isEmpty(source.getSupercategories()))
		{
			return;
		}

		final List<CategoryModel> brand = source
				.getSupercategories().stream().filter(c -> !CollectionUtils.isEmpty(c.getSupercategories())
						&& c.getSupercategories().size() == 1 && "brands".equalsIgnoreCase(c.getSupercategories().get(0).getCode()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(brand))
		{
			return;
		}
		target.setBrand(getCategoryConverter().convert(brand.get(0)));



	}


}
