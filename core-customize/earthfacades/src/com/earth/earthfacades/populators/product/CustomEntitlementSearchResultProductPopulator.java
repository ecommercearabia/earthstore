/**
 *
 */
package com.earth.earthfacades.populators.product;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;


/**
 * SOLR Populator for {@link de.hybris.platform.subscriptionservices.model.SubscriptionProductModel}
 */
public class CustomEntitlementSearchResultProductPopulator<SOURCE extends SearchResultValueData, TARGET extends ProductData>
		implements Populator<SOURCE, TARGET>
{
	@Resource(name = "productService")
	private ProductService productService;
	@Resource(name = "productEntitlementCollectionPopulator")
	private Populator<ProductModel, ProductData> productEntitlementCollectionPopulator;

	@Override
	public void populate(final SOURCE source, final TARGET target)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("source", source);
		ServicesUtil.validateParameterNotNullStandardMessage("target", target);
		try
		{
			final ProductModel productModel = getProductService().getProductForCode(target.getCode());
			getProductEntitlementCollectionPopulator().populate(productModel, target);
		}
		catch (final Exception e)
		{

		}
	}

	protected <T> T getValue(final SOURCE source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	protected ProductService getProductService()
	{
		return productService;
	}


	protected Populator<ProductModel, ProductData> getProductEntitlementCollectionPopulator()
	{
		return productEntitlementCollectionPopulator;
	}

}
