/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.populators.product;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;



/**
 * @author mnasro
 *
 */
public class SearchResultGeneralProductPopulator implements Populator<SearchResultValueData, ProductData>
{

	private static final String EXPRESS_DELIVERY_SOLR_KEY = "expressDelivery";

	private static final String SUGAR_FREE = "EarthClassificationAE/1.0/EarthClassificationsAE.sugarfree";
	private static final String ORGANIC = "EarthClassificationAE/1.0/EarthClassificationsAE.organic";
	private static final String VEGAN = "EarthClassificationAE/1.0/EarthClassificationsAE.vegan";
	private static final String GLUTEN_FREE = "EarthClassificationAE/1.0/EarthClassificationsAE.glutenfree";
	private static final String ECO_FRIENDLY = "EarthClassificationAE/1.0/EarthClassificationsAE.ecofriendly";

	private static final Logger LOG = Logger.getLogger(SearchResultGeneralProductPopulator.class);

	@Resource(name = "classificationService")
	private ClassificationService classificationService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "unitProductPopulator")
	private Populator<ProductModel, ProductData> unitProductPopulator;

	@Resource(name = "productUnitPricePopulator")
	private Populator<ProductModel, ProductData> productUnitPricePopulator;

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		populateExpressDelivery(source, target);

		final ProductModel product = getProduct(source);
		if (product == null)
		{
			return;
		}
		populateUnit(product, target);
		populateUnitPrice(product, target);
		populateSugarFree(product, target);
		populateOrganic(product, target);
		populateVegan(product, target);
		populateGlutenFree(product, target);
		populateEcoFriendly(product, target);
		populateBrand(product, target);

	}


	private String isFoundClassification(final ProductModel source, final String featureByCode)
	{
		final FeatureList features = classificationService.getFeatures(source);

		if (features != null)
		{
			final Feature feature = features.getFeatureByCode(featureByCode);
			return (feature != null && feature.getValue() != null
					&& StringUtils.isNotBlank(feature.getValue().getValue().toString())) ? "true" : "false";
		}
		return "false";
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateEcoFriendly(final ProductModel source, final ProductData target)
	{
		target.setEcoFriendly(isFoundClassification(source, ECO_FRIENDLY));

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateGlutenFree(final ProductModel source, final ProductData target)
	{
		target.setGlutenFree(isFoundClassification(source, GLUTEN_FREE));

	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateVegan(final ProductModel source, final ProductData target)
	{
		target.setVegan(isFoundClassification(source, VEGAN));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateOrganic(final ProductModel source, final ProductData target)
	{
		target.setOrganic(isFoundClassification(source, ORGANIC));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateSugarFree(final ProductModel source, final ProductData target)
	{
		target.setSugarFree(isFoundClassification(source, SUGAR_FREE));
	}

	/**
	 * @param product
	 * @param target
	 */
	private void populateUnitPrice(final ProductModel product, final ProductData target)
	{
		getProductUnitPricePopulator().populate(product, target);
	}

	/**
	 * @param product
	 * @param target
	 */
	private void populateUnit(final ProductModel product, final ProductData target)
	{
		getUnitProductPopulator().populate(product, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateExpressDelivery(final SearchResultValueData source, final ProductData target)
	{
		final Boolean value = this.<Boolean> getValue(source, EXPRESS_DELIVERY_SOLR_KEY);
		if (value == null)
		{
			return;
		}
		target.setExpressDelivery(value.booleanValue());
	}

	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}
		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}


	private ProductModel getProduct(final SearchResultValueData source)
	{
		ProductModel productModel = null;
		final String productCode = (String) source.getValues().get(ProductModel.CODE);
		try
		{
			productModel = productService.getProductForCode(productCode);
			if (productModel == null)
			{
				LOG.warn("product code [" + productCode + "] not found");
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			LOG.warn("product code [" + productCode + "] not found");
		}

		return productModel;
	}

	/**
	 * @return the unitProductPopulator
	 */
	protected Populator<ProductModel, ProductData> getUnitProductPopulator()
	{
		return unitProductPopulator;
	}

	/**
	 * @return the productUnitPricePopulator
	 */
	protected Populator<ProductModel, ProductData> getProductUnitPricePopulator()
	{
		return productUnitPricePopulator;
	}


	/**
	 * @return the categoryConverter
	 */
	protected Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateBrand(final ProductModel source, final ProductData target)
	{
		if (CollectionUtils.isEmpty(source.getSupercategories()))
		{
			return;
		}

		final List<CategoryModel> brand = source
				.getSupercategories().stream().filter(c -> !CollectionUtils.isEmpty(c.getSupercategories())
						&& c.getSupercategories().size() == 1 && "brands".equalsIgnoreCase(c.getSupercategories().get(0).getCode()))
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(brand))
		{
			return;
		}
		target.setBrand(getCategoryConverter().convert(brand.get(0)));



	}

}
