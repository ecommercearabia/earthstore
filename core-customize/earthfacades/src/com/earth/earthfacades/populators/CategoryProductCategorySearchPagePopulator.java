/**
 *
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.earth.earthfacades.facade.CategoryFacade;


/**
 * @author mnasro
 *
 */
public class CategoryProductCategorySearchPagePopulator<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY> implements
		Populator<ProductCategorySearchPageData<QUERY, RESULT, SCAT>, ProductCategorySearchPageData<STATE, ITEM, CATEGORY>>
{

	@Resource(name = "categoryFacade")
	private CategoryFacade categoryFacade;

	/**
	 * @return the categoryFacade
	 */
	protected CategoryFacade getCategoryFacade()
	{
		return categoryFacade;
	}

	@Override
	public void populate(final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source,
			final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target) throws ConversionException
	{
		final String categoryCode = source.getCategoryCode();

		if (source != null && !StringUtils.isBlank(source.getCategoryCode()))
		{
			try
			{
				final Optional<CategoryData> categoryForCode = getCategoryFacade().getCategoryForCode(categoryCode);

				if (categoryForCode.isPresent())
				{
					target.setCategory(categoryForCode.get());
				}
			}
			catch (final Exception e)
			{

			}
		}

	}

}
