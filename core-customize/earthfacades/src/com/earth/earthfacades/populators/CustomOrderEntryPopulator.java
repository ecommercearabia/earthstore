/**
 *
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author monzer
 *
 */
public class CustomOrderEntryPopulator extends OrderEntryPopulator
{

	@Override
	protected void addCommon(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addCommon(orderEntry, entry);
		if (orderEntry.getQuantity() != null && orderEntry.getProduct() != null
				&& orderEntry.getProduct().getUnitFactorRange() != null && orderEntry.getProduct().isEnableUnitFactorRange())
		{
			entry.setWieghtedQuantity(String.valueOf(orderEntry.getQuantity() / 1000d));
		}
		populateTaxValues(orderEntry, entry);
	}


	/**
	 * @param orderEntry
	 * @param entry
	 */
	private void populateTaxValues(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		entry.setTaxValues(getTaxAppliedValue(orderEntry));

	}


	@Override
	protected void addTotals(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addTotals(orderEntry, entry);

		final double taxAppliedValue = getTaxAppliedValue(orderEntry);
		if (orderEntry.getBasePrice() != null)
		{
			// createPrice(orderEntry, value);
			entry.setBasePriceWithoutTax(createPrice(orderEntry, orderEntry.getBasePrice()));
			final double basePriceWithTax = orderEntry.getQuantity() != null && orderEntry.getQuantity().doubleValue() > 0
					? formatDouble(orderEntry.getBasePrice() + (taxAppliedValue / orderEntry.getQuantity()))
					: 0;
			entry.setBasePriceWithTax(createPrice(orderEntry, basePriceWithTax));
		}
		if (orderEntry.getTotalPrice() != null)
		{
			entry.setTotalPriceWithoutTax(createPrice(orderEntry, orderEntry.getTotalPrice()));
			entry.setTotalPriceWithTax(createPrice(orderEntry, calcTotalWithTaxOrderEntry(orderEntry)));
		}

		if (!CollectionUtils.isEmpty(orderEntry.getDiscountValues()))
		{
			entry.setTotalDiscounts(DiscountValue.sumAppliedValues(orderEntry.getDiscountValues()));
		}

		if (!CollectionUtils.isEmpty(orderEntry.getTaxValues()))
		{
			entry.setTotalTaxAmount(taxAppliedValue);
		}
		if (orderEntry.getQuantity() != null && orderEntry.getBasePrice() != null && orderEntry.getProduct() != null
				&& orderEntry.getProduct().getUnitFactorRange() != null && orderEntry.getProduct().isEnableUnitFactorRange())
		{
			entry.setBasePrice(createPrice(orderEntry, orderEntry.getBasePrice() * orderEntry.getQuantity()));
		}
		if (orderEntry instanceof OrderEntryModel)

		{

			final OrderEntryModel orderEntryModel = (OrderEntryModel) orderEntry;

			entry.setQuantityCancelled(orderEntryModel.getQuantityCancelled());

		}

	}

	protected Double calcTotalWithTaxOrderEntry(final AbstractOrderEntryModel orderEntry)
	{
		if (orderEntry == null || orderEntry.getOrder() ==null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}
		if (orderEntry.getTotalPrice() == null)
		{
			return 0.0d;
		}

		BigDecimal totalPrice = BigDecimal.valueOf(orderEntry.getTotalPrice().doubleValue());
		double taxAppliedValue = 0.0d;
		if (Boolean.TRUE.equals(orderEntry.getOrder().getNet()))
				{
			 taxAppliedValue = getTaxAppliedValue(orderEntry);

				}
		// Add the taxes to the total price if the cart is net; if the total was null taxes should be null as well
		if (Boolean.TRUE.equals(orderEntry.getOrder().getNet()) && totalPrice.compareTo(BigDecimal.ZERO) != 0
				&& taxAppliedValue > 0)
		{
			totalPrice = totalPrice.add(BigDecimal.valueOf(taxAppliedValue));
		}

		return totalPrice.doubleValue();
	}

	/**
	 * @param orderEntry
	 */
	protected double getTaxAppliedValue(final AbstractOrderEntryModel orderEntry)
	{
		if (CollectionUtils.isEmpty(orderEntry.getTaxValues()))
		{
			return 0;
		}
		return formatDouble(TaxValue.sumAppliedTaxValues(orderEntry.getTaxValues()));

	}

	protected double formatDouble(final double val)
	{
		final DecimalFormat df = new DecimalFormat("#.###");
		final String format = df.format(val);
		return Double.valueOf(format);
	}
}
