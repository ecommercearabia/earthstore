/**
 *
 */
package com.earth.earthfacades.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.consignmenttrackingfacades.ConsignmentTrackingFacade;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import javax.annotation.Resource;


/**
 * @author monzer
 *
 */
public class ConsignmentTrackingUrlPopulator implements Populator<ConsignmentModel, ConsignmentData>
{

	@Resource(name = "consignmentTrackingFacade")
	private ConsignmentTrackingFacade consignmentTrackingFacade;

	@Override
	public void populate(final ConsignmentModel source, final ConsignmentData target)
	{
		generateTrackingUrl(source, target);
	}

	/**
	 * @param source
	 * @param target
	 */
	private void generateTrackingUrl(final ConsignmentModel source, final ConsignmentData target)
	{
		final String trackingUrl = consignmentTrackingFacade.getTrackingUrlForConsignmentCode(source.getOrder().getCode(),
				source.getCode());
		target.setTrackingUrl(trackingUrl);
	}

}
