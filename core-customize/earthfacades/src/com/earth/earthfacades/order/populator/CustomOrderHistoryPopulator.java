/**
 *
 */
package com.earth.earthfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 * @author Tuqa.
 *
 */
public class CustomOrderHistoryPopulator implements Populator<OrderModel, OrderHistoryData>
{

	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;


	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	@Override
	public void populate(final OrderModel source, final OrderHistoryData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setTrackingId(getTrackingId(source));
		target.setTrackingURL(getTrackingURL(source));

	}

	private String getTrackingURL(final OrderModel source)
	{
		if (source == null || CollectionUtils.isEmpty(source.getConsignments()))
		{
			return StringUtils.EMPTY;
		}

		final String trackingId = getTrackingId(source);

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(source.getStore());

		String trackingUrl = "";

		if (fulfillmentProvider.isPresent())
		{
			trackingUrl = fulfillmentProvider.get().getTrackingUrl();
		}

		return trackingUrl + trackingId;
	}

	/**
	 * @param source
	 * @return
	 */
	private String getTrackingId(final OrderModel source)
	{
		final List<String> collect = source.getConsignments().stream().map(ConsignmentModel::getTrackingID).filter(Objects::nonNull)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(collect))
		{
			return null;
		}

		return String.join(", ", collect);
	}

}
