package com.earth.earthfacades.order.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.earth.earthfulfillment.context.FulfillmentProviderContext;
import com.earth.earthfulfillment.model.FulfillmentProviderModel;


/**
 *
 * The Class CustomAbstractOrderPopulator.
 *
 * @author mnasro
 *
 * @param <S>
 *           SOURCE, the generic type extends AbstractOrderModel
 * @param <T>
 *           TARGET, the generic type extends AbstractOrderData
 */
public class CustomAbstractOrderPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	@Resource(name = "fulfillmentProviderContext")
	private FulfillmentProviderContext fulfillmentProviderContext;

	/**
	 * @return the fulfillmentProviderContext
	 */
	protected FulfillmentProviderContext getFulfillmentProviderContext()
	{
		return fulfillmentProviderContext;
	}

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source != null && target != null)
		{
			populateAllEntriesCount(source, target);
			target.setOrderRefId(source.getOrderRefId());
			target.setTrackingId(getTrackingId(source));
			target.setTrackingURL(getTrackingURL(source));
			target.setAddressNotes(source.getAddressNotes());

		}

	}

	/**
	 * @param source
	 * @param target
	 */
	protected void populateAllEntriesCount(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (target instanceof CartData)
		{
			final int allEntriesCount = CollectionUtils.isEmpty(source.getEntries()) ? 0 : source.getEntries().size();
			((CartData) target).setAllEntriesCount(allEntriesCount);
		}
	}

	private String getTrackingURL(final AbstractOrderModel source)
	{
		if (source == null || CollectionUtils.isEmpty(source.getConsignments()))
		{
			return StringUtils.EMPTY;
		}

		final String trackingId = getTrackingId(source);

		final Optional<FulfillmentProviderModel> fulfillmentProvider = getFulfillmentProviderContext()
				.getProvider(source.getStore());

		String trackingUrl = "";

		if (fulfillmentProvider.isPresent())
		{
			trackingUrl = fulfillmentProvider.get().getTrackingUrl();
		}

		return trackingUrl + trackingId;
	}

	/**
	 * @param source
	 * @return
	 */
	private String getTrackingId(final AbstractOrderModel source)
	{
		final List<String> collect = source.getConsignments().stream().map(ConsignmentModel::getTrackingID).filter(Objects::nonNull)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(collect))
		{
			return null;
		}

		return String.join(", ", collect);
	}

}
