/**
 *
 */
package com.earth.earthfacades.facade;

import com.earth.earthfacades.dto.config.OMSActionConfigData;

/**
 * @author monzer
 *
 */
public interface OMSMobileActionConfigFacade
{
	OMSActionConfigData getOMSActionConfigForOrder(String orderCode);
}
