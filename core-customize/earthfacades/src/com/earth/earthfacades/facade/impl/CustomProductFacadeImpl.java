/**
 *
 */
package com.earth.earthfacades.facade.impl;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import com.earth.earthcore.service.CustomProductService;
import com.earth.earthfacades.facade.CustomProductFacade;


/**
 * @author user2
 *
 */
public class CustomProductFacadeImpl extends DefaultProductFacade<ProductModel> implements CustomProductFacade
{

	@Resource(name = "productService")
	private CustomProductService customProductService;


	/**
	 * @return the customProductService
	 */
	protected CustomProductService getCustomProductService()
	{
		return customProductService;
	}


	@Override
	public ProductData getProductForBarcodeIdentifierAndOptions(final String barcodeIdentifier,
			final Collection<ProductOption> options)
	{
		final ProductModel productModel = getCustomProductService().getProductForBarcodeIdentifier(barcodeIdentifier);
		final ProductData productData = getProductConverter().convert(productModel);

		if (options != null)
		{
			getProductConfiguredPopulator().populate(productModel, productData, options);
		}

		return productData;
	}



}
