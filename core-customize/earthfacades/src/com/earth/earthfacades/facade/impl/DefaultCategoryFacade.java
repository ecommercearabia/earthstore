package com.earth.earthfacades.facade.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;

import javax.annotation.Resource;

import com.earth.earthfacades.facade.CategoryFacade;


/**
 * @author mnasro
 *
 */
public class DefaultCategoryFacade implements CategoryFacade
{

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	/**
	 * @return the categoryConverter
	 */
	protected Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	/**
	 * @return the commerceCategoryService
	 */
	protected CommerceCategoryService getCommerceCategoryService()
	{
		return commerceCategoryService;
	}

	@Override
	public Optional<CategoryData> getCategoryForCode(final String code)
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(code);

		if (category == null)
		{
			return Optional.empty();
		}

		final CategoryData categoryData = getCategoryConverter().convert(category);

		return categoryData == null ? Optional.empty() : Optional.ofNullable(categoryData);

	}

}
