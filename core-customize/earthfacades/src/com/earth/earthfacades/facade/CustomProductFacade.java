/**
 *
 */
package com.earth.earthfacades.facade;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;


/**
 * @author user2
 *
 */
public interface CustomProductFacade extends ProductFacade
{
	/**
	 * Gets the product by barcodeIdentifier. The current session data (catalog versions, user) are used, so the valid
	 * product for the current session parameters will be returned. Use
	 * {@link ProductFacade#getProductForOptions(ProductModel, Collection)} if you have the model already.
	 *
	 * @param barcodeIdentifier
	 *           the barcodeIdentifier of the product to be found
	 * @param options
	 *           options set that determines amount of information that will be attached to the returned product. If
	 *           empty or null default BASIC option is assumed
	 * @return the {@link ProductData}
	 */
	ProductData getProductForBarcodeIdentifierAndOptions(String barcodeIdentifier, Collection<ProductOption> options);
}
