/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */

package com.earth.earthfacades.price.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.util.Assert;


/**
 * @author mnasro
 *
 */
public class CustomPriceDataFactory extends de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory
{
	@Override
	public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currency, "Parameter currency cannot be null.");

		final PriceData priceData = createPriceData();
		currency.setDigits(currency.getDisplayDigits());
		priceData.setPriceType(priceType);
		priceData.setValue(value);
		priceData.setCurrencyIso(currency.getIsocode());
		priceData.setFormattedValue(formatPrice(value, currency));

		return priceData;
	}



	@Override
	protected String formatPrice(final BigDecimal value, final CurrencyModel currency)
	{
		final Locale locale = Locale.ENGLISH;
		final NumberFormat currencyFormat = createCurrencyFormat(locale, currency);
		if (currencyFormat instanceof DecimalFormat)
		{
			final DecimalFormat fmt = (DecimalFormat) currencyFormat;
			fmt.setPositivePrefix(fmt.getPositivePrefix() + " ");
			fmt.setNegativePrefix(fmt.getNegativePrefix() + " ");
		}
		return currencyFormat.format(value);
	}
}
