/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.earth.earthfacades.labels.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.DocumentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.warehousingfacades.order.WarehousingConsignmentFacade;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.earth.earthcore.service.BarcodeGenaratorService;


/***
 *
 * @author monzer
 *
 */
public class CustomPickContext extends de.hybris.platform.warehousing.labels.context.PickSlipContext
{

	private static final Logger LOG = Logger.getLogger(CustomPickContext.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "warehousingConsignmentFacade")
	private WarehousingConsignmentFacade warehousingConsignmentFacade;

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "barcodeGenaratorService")
	private BarcodeGenaratorService barcodeGenaratorService;

	private OrderData orderData;

	private ConsignmentData consignmentData;

	private MediaModel barcode;

	/**
	 * @return the orderData
	 */
	public OrderData getOrderData()
	{
		return orderData;
	}


	/**
	 * @param orderData
	 *           the orderData to set
	 */
	public void setOrderData(final OrderData orderData)
	{
		this.orderData = orderData;
	}


	@Override
	public void init(final ConsignmentProcessModel businessProcessModel, final DocumentPageModel documentPageModel)
	{
		super.init(businessProcessModel, documentPageModel);
		orderData = orderFacade.getOrderDetailsForCode(getOrder().getCode());
		this.consignmentData = warehousingConsignmentFacade.getConsignmentForCode(getConsignment().getCode());

		if (getConsignment().getBarcode() == null)
		{
			try
			{
				barcode = barcodeGenaratorService.generateBarcodeAsMedia(businessProcessModel.getConsignment());

			}
			catch (final IOException e)
			{
				LOG.error(e.getMessage());
			}
		}

		barcode = getConsignment().getBarcode();

	}

	public String getCustomProductImageURL(final ProductData product)
	{
		String path = null;

		if (product.getImages() != null && !product.getImages().isEmpty())
		{
			final Optional<ImageData> findFirst = product.getImages().stream()
					.filter(p -> p.getFormat().equals("thumbnail") && p.getImageType().equals(ImageDataType.PRIMARY)).findFirst();

			if (findFirst.isPresent())
			{
				path = findFirst.get().getUrl();
			}
		}
		return path;
	}

	/**
	 * @return the consignmentData
	 */
	public ConsignmentData getConsignmentData()
	{
		return consignmentData;
	}


	/**
	 * @param consignmentData
	 *           the consignmentData to set
	 */
	public void setConsignmentData(final ConsignmentData consignmentData)
	{
		this.consignmentData = consignmentData;
	}

	/**
	 * @return the barcode
	 */
	public MediaModel getBarcode()
	{
		return barcode;
	}

	/**
	 * @param barcode
	 *           the barcode to set
	 */
	public void setBarcode(final MediaModel barcode)
	{
		this.barcode = barcode;
	}


	public String getProductName(final ProductModel productModel)
	{
		return productModel.getName(Locale.ENGLISH);
	}

	public String getCityName(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getShippingAddress().getCity().getName(Locale.ENGLISH);
	}

	public String getAreaName(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getShippingAddress().getArea().getName(Locale.ENGLISH);
	}

	/**
	* 
	*/
	public CustomPickContext()
	{
		// XXX Auto-generated constructor stub
	}

	public String getCountryName(final ConsignmentModel consignmentModel)
	{
		return consignmentModel.getShippingAddress().getCountry().getName(Locale.ENGLISH);
	}



}
